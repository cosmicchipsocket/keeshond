Derive macros for Keeshond Game Engine.

Use this to easily derive `Component` and related traits:

```rust
#[derive(Component)]
struct Doggy
{
   // ...
}
```

You can also use the `component` attribute on the struct to configure how `Component` is derived:

```rust
#[derive(Component)]
#[component(removal = "fast")]
struct Bunny
{
   // ...
}
```

## License

Licensed under either of

 * Apache License, Version 2.0
   ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license
   ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.


## Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.
