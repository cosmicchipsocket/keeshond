#version 140

uniform sampler2D t_texture;
in vec2 v_uv;
in vec4 v_tex_coords;
in vec4 v_rgba;

out vec4 TargetScreen;

vec2 slice_coords()
{
    return vec2((v_uv.x - v_tex_coords[0]) / v_tex_coords[2], (v_uv.y - v_tex_coords[1]) / v_tex_coords[3]);
}

void main()
{
    vec4 color = texture(t_texture, v_uv) * vec4(slice_coords(), 1.0, 1.0);

    if (color.a <= 0.0)
    {
        discard;
    }

    TargetScreen = color;
}
