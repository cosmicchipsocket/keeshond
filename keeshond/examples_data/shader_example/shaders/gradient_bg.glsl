#version 140

in vec2 v_uv;
in vec4 v_rgba;

out vec4 TargetScreen;

void main()
{
    vec4 color = vec4(v_uv.x, v_uv.y, 1.0, 1.0);

    if (color.a <= 0.0)
    {
        discard;
    }

    TargetScreen = color;
}
