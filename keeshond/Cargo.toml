[package]
name = "keeshond"
description = "A fast and fun 2D game engine for Rust"
version = "0.28.0"
authors = ["Danni H <dannihfoss@fastmail.fm>"]
repository = "https://gitlab.com/cosmicchipsocket/keeshond"
readme = "README.md"
categories = ["game-engines"]
license = "MIT/Apache-2.0"
edition = "2021"
exclude = ["/examples_data/*", "/examples_data_src/*"]

[dependencies]
# Error handling (may want to switch these out in the future)
failure = "0.1.5"
failure_derive = "0.1.5"
# ECS data structures
rustc-hash = "1.1.0"
bit-set = "0.5.0"
bitarray-set = "0.4.1"
smallvec = { version = "1.6.1", features = ["serde"] }
bitflags = "1.3.2"
typenum = "1.10.0"
generic-array = "0.14.1"
downcast-rs = "1.2.0"
strum = "0.23.0"
strum_macros = "0.23.1"
# For CLI parsing
getopts = "0.2.18"
# For renderer
cgmath = "0.17.0" # Keep this on 0.17 for now for performance reasons
png = "0.16.8"
serde_json = "1.0"
ouroboros = "0.15.0" # Needed for framebuffer struct self-references
glium = { version = "0.30.2", default-features = false }
# For windowing
sdl2 = "0.35.2"
sdl2-sys = "0.35.2"
# For default logging
backtrace = { version = "0.3", optional = true }
# For imgui
imgui = { version = "0.8.1", optional = true }
imgui-sys = { version = "0.8.1", optional = true }
imgui-glium-renderer = { version = "0.8.1", optional = true }
cstr-macro = {version = "0.1.0", optional = true }

[dev-dependencies]
rand = "0.8.5"

[target.'cfg(windows)'.dependencies.winapi]
version = "0.3.9"
features = [ "winuser", "windef" ]

[dependencies.serde]
version = "1.0"
features = ["derive"]

[dependencies.log]
version = "0.4.6"
features = ["std"]

[dependencies.keeshond_datapack]
path = "../keeshond_datapack"
version = "0.14.0"
features = ["serde"]

[dependencies.keeshond_derive]
path = "../keeshond_derive"
version = "0.28.0"
optional = true

[dependencies.keeshond_audio]
path = "../keeshond_audio"
version = "0.28.0"
optional = true

[features]
default = ["default_logger", "graphical_panic", "derive", "audio"]
derive = ["keeshond_derive"]
default_logger = []
graphical_panic = ["backtrace"]
audio = ["keeshond_audio"]
imgui_feature = ["imgui", "imgui-sys", "cstr-macro", "imgui-glium-renderer"]

[[example]]
name = "frame_lerp"
path = "examples_imgui/frame_lerp/main.rs"
required-features = ["imgui_feature"]

[[example]]
name = "imgui"
path = "examples_imgui/imgui/main.rs"
required-features = ["imgui_feature"]

[[example]]
name = "voice"
path = "examples_imgui/voice/main.rs"
required-features = ["imgui_feature"]
