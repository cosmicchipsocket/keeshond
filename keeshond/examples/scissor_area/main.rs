extern crate keeshond;
extern crate rand;

use keeshond::gameloop::{Gameloop, GameInfo, GameControl};
use keeshond::scene::{DrawerSystem, ThinkerSystem, SceneType, SpawnControl, DynArgList, SceneConfig, ThinkerArgs, DrawerArgs, Entity, ComponentStore};
use keeshond::renderer::{DrawSpriteInfo, SliceId, Sheet, DrawOptions, DrawOrdering, DrawBackgroundColorInfo};
use keeshond::util::{Rect, SimpleTransform};

use keeshond::datapack::{DataHandle, DataId, LoadErrorMode};
use keeshond::datapack::source::{FilesystemSource, TrustLevel};

use keeshond_derive::Component;

use rand::Rng;
use keeshond::darg_named;

const DOGGY_SCALE : f64 = 32.0;
const DOGGY_HSPEED_MIN : f64 = 1.0;
const DOGGY_HSPEED_MAX : f64 = 2.0;
const DOGGY_VSPEED_MAX : f64 = 2.0;
const DOGGY_BOUNCE_MIN : f64 = 1.0;
const DOGGY_BOUNCE_MAX : f64 = 2.0;
const DOGGY_COUNT_MAX : usize = 10_000;
const DOGGY_SPAWN_PROBABILITY : f64 = 0.3;
const TREE_COUNT_PER_LAYER : usize = 20;

const PRIORITY_POS_THINKER : i32 = -16777216;


////////////////////////////////////////


keeshond::spawnables!
{
    SpawnId;
    Doggy,
    Tree
}

struct GameScene
{
    
}

impl SceneType for GameScene
{
    type SpawnableIdType = SpawnId;
    
    fn new() -> Self where Self : Sized { GameScene {} }
    fn config(&mut self, game: &mut GameControl) -> SceneConfig<Self>
    {
        let mut config = SceneConfig::new();

        config.thinker(DoggyThinker {})
            .thinker(PosThinker {})
            .drawer(SpriteDrawer::new(game));

        config
    }
    fn spawn(&mut self, spawn : &mut SpawnControl, spawnable_id : SpawnId,
        game : &mut GameControl, transform : &SimpleTransform, args : &DynArgList)
    {
        spawn.with(Position::new(transform.x, transform.y));

        match args.get_int_named("layer", 1)
        {
            2 => { spawn.with(Layer2 {}) }
            3 => { spawn.with(Layer3 {}) }
            4 => { spawn.with(Layer4 {}) }
            _ => { spawn.with(Layer1 {}) }
        }

        let mut store = game.res().store_mut();
        let sheet_handle = DataHandle::<Sheet>::with_path(args.get_string(0, "shader_example"),
                                                          args.get_string(1, "sprites.png"));
        let sheet = sheet_handle.get(&mut store, LoadErrorMode::Fatal).unwrap();
        let slice_name;
        let depth_bias;

        match spawnable_id
        {
            SpawnId::Doggy =>
            {
                spawn.with(Doggy
                {
                    vel_x : args.get_float_named("vel_x", 0.0),
                    vel_y : args.get_float_named("vel_y", 0.0)
                });
                slice_name = "doggy";
                depth_bias = 16.0;
            }
            SpawnId::Tree =>
            {
                spawn.with(Tree {});
                slice_name = "tree";
                depth_bias = 32.0;
            }
        }

        let slice = sheet.slice_id(slice_name).unwrap_or(0);

        spawn.with(Sprite
        {
            sheet : sheet_handle,
            slice,
            depth_bias,
            alpha : args.get_float_named("alpha", 1.0) as f32
        });
    }
}


////////////////////////////////////////


#[derive(Component)]
struct Doggy
{
    vel_x : f64,
    vel_y : f64
}

#[derive(Component)]
struct Tree {}

#[derive(Component)]
struct Layer1 {}

#[derive(Component)]
struct Layer2 {}

#[derive(Component)]
struct Layer3 {}

#[derive(Component)]
struct Layer4 {}

#[derive(Component)]
struct Position
{
    x : f64,
    y : f64,
    old_x : f64,
    old_y : f64
}

#[derive(Component)]
struct Sprite
{
    sheet : DataHandle<Sheet>,
    slice : SliceId,
    depth_bias : f32,
    alpha : f32
}

impl Position
{
    pub fn new(x : f64, y : f64) -> Position
    {
        Position
        {
            x,
            y,
            old_x : x,
            old_y : y
        }
    }

    #[inline(always)]
    pub fn get_render_position(&self, interpolation : f32) -> (f32, f32)
    {
        (self.x as f32 + (self.x - self.old_x) as f32 * interpolation, self.y as f32 + (self.y - self.old_y) as f32 * interpolation)
    }
}

struct PosThinker {}

impl ThinkerSystem<GameScene> for PosThinker
{
    fn think(&mut self, args : ThinkerArgs<GameScene>)
    {
        let mut pos_store = args.components.store_mut::<Position>();

        for (_, pos) in pos_store.iter_mut()
        {
            pos.old_x = pos.x;
            pos.old_y = pos.y;
        }
    }

    fn priority(&self) -> i32
    {
        PRIORITY_POS_THINKER
    }
}

struct DoggyThinker {}

impl ThinkerSystem<GameScene> for DoggyThinker
{
    fn start(&mut self, args : ThinkerArgs<GameScene>)
    {
        let (base_width, base_height) = args.game.renderer().base_size_f64();
        let mut rng = rand::thread_rng();

        for layer in 1..=4
        {
            for _ in 0..TREE_COUNT_PER_LAYER
            {
                let x = rng.gen_range(0.0..base_width);
                let y = rng.gen_range(0.0..base_height);

                args.scene.spawn_later_with(SpawnId::Tree, args.game, &SimpleTransform::with_xy(x, y), &darg_named!(; "layer" => layer));
            }
        }
    }

    fn think(&mut self, args : ThinkerArgs<GameScene>)
    {
        let mut rng = rand::thread_rng();
        let mut pos_store = args.components.store_mut::<Position>();
        let mut doggy_store = args.components.store_mut::<Doggy>();
        
        let (base_width, base_height) = args.game.renderer().base_size_f64();
        
        let topleft_side = DOGGY_SCALE / 2.0;
        let right_side = base_width - topleft_side;
        let bottom_side = base_height - topleft_side;
        
        for (entity, doggy) in doggy_store.iter_mut()
        {
            if let Some(pos) = pos_store.get_entity_mut(&entity)
            {
                pos.x += doggy.vel_x;
                pos.y += doggy.vel_y;

                if pos.x < topleft_side && doggy.vel_x < 0.0
                {
                    pos.x = topleft_side;
                    doggy.vel_x = rng.gen_range(DOGGY_HSPEED_MIN..DOGGY_HSPEED_MAX);
                }
                else if pos.x > right_side + DOGGY_SCALE
                {
                    args.scene.remove_entity_later(&entity);
                }

                if pos.y < topleft_side
                {
                    pos.y = topleft_side;
                    doggy.vel_y = rng.gen_range(DOGGY_BOUNCE_MIN..DOGGY_BOUNCE_MAX);
                }
                else if pos.y > bottom_side
                {
                    pos.y = bottom_side;
                    doggy.vel_y = rng.gen_range(-DOGGY_BOUNCE_MAX..-DOGGY_BOUNCE_MIN);
                }
            }
        }

        if doggy_store.count() < DOGGY_COUNT_MAX && rng.gen_bool(DOGGY_SPAWN_PROBABILITY)
        {
            let y = rng.gen_range(topleft_side..bottom_side);
            let vel_x = rng.gen_range(DOGGY_HSPEED_MIN..DOGGY_HSPEED_MAX);
            let vel_y = rng.gen_range(-DOGGY_VSPEED_MAX..DOGGY_VSPEED_MAX);
            let layer = rng.gen_range(1..=4);
            let mut alpha = 1.0;

            if rng.gen_bool(0.5)
            {
                alpha = rng.gen_range(0.3..0.7);
            }

            args.scene.spawn_later_with(SpawnId::Doggy, args.game, &SimpleTransform::with_xy(-DOGGY_SCALE, y),
                &darg_named!(; "vel_x" => vel_x, "vel_y" => vel_y, "layer" => layer, "alpha" => alpha));
        }
    }
}

struct SpriteDrawer {}

impl SpriteDrawer
{
    fn new(_control : &mut GameControl) -> SpriteDrawer
    {
        SpriteDrawer {}
    }

    fn draw_sprite(&self, entity : &Entity, pos_store : &ComponentStore<Position>, sprite_store : &ComponentStore<Sprite>,
                   r : f32, g : f32, b : f32, args : &mut DrawerArgs)
    {
        if let Some(sprite) = sprite_store.get_entity(&entity)
        {
            if let Some(pos) = pos_store.get_entity(&entity)
            {
                let (x, y) = pos.get_render_position(args.interpolation);
                let transform = args.transform.clone();

                args.drawing.draw_sprite(&DrawSpriteInfo
                {
                    sheet_id : sprite.sheet.id(),
                    shader_id : DataId::new(0),
                    x,
                    y,
                    angle : 0.0,
                    scale_x : 1.0,
                    scale_y : 1.0,
                    slice : sprite.slice,
                    transform,
                    r,
                    g,
                    b,
                    alpha : sprite.alpha,
                    depth : (y + sprite.depth_bias) / 1000.0
                }, &DrawOptions
                {
                    ordering : DrawOrdering::OrderedAutomatic
                });
            }
        }
    }

    fn new_layer(&self, scissor : Option<Rect<f32>>, r : f32, g : f32, b : f32, args : &mut DrawerArgs)
    {
        args.drawing.flush_depth_sort_queue();
        args.drawing.set_scissor_area(scissor);

        args.drawing.draw_background_color(&DrawBackgroundColorInfo
        {
            r,
            g,
            b,
            alpha : 0.8,
            depth : -1.0,
            shader_id : DataId::new(0)
        }, &DrawOptions
        {
            ordering : DrawOrdering::PainterZWrite
        })
    }
}

impl DrawerSystem for SpriteDrawer
{
    fn draw(&self, mut args : DrawerArgs)
    {
        let pos_store = args.components.store::<Position>();
        let sprite_store = args.components.store::<Sprite>();
        let layer1_store = args.components.store::<Layer1>();
        let layer2_store = args.components.store::<Layer2>();
        let layer3_store = args.components.store::<Layer3>();
        let layer4_store = args.components.store::<Layer4>();
        
        for (entity, _) in layer1_store.iter()
        {
            self.draw_sprite(&entity, &pos_store, &sprite_store, 1.0, 1.0, 1.0, &mut args);
        }

        self.new_layer(Some(Rect { x : 40.0, y : 60.0, w : 400.0, h : 400.0 }), 0.6, 0.0, 0.0, &mut args);

        for (entity, _) in layer2_store.iter()
        {
            self.draw_sprite(&entity, &pos_store, &sprite_store, 1.5, 0.5, 0.5, &mut args);
        }

        self.new_layer(Some(Rect { x : 440.0, y : 160.0, w : 400.0, h : 400.0 }), 0.0, 0.6, 0.0, &mut args);

        for (entity, _) in layer3_store.iter()
        {
            self.draw_sprite(&entity, &pos_store, &sprite_store, 0.5, 1.5, 0.5, &mut args);
        }

        self.new_layer(Some(Rect { x : 840.0, y : 260.0, w : 400.0, h : 400.0 }), 0.0, 0.0, 0.6, &mut args);

        for (entity, _) in layer4_store.iter()
        {
            self.draw_sprite(&entity, &pos_store, &sprite_store, 0.5, 0.5, 1.5, &mut args);
        }
    }
}


////////////////////////////////////////


fn main()
{
    let gameinfo = GameInfo
    {
        package_name : "scissor_area",
        friendly_name : "Keeshond Scissor Area Example",
        base_width : 1280,
        base_height : 720,
        texture_filtering : false,
        cursor_visible : true,
        .. Default::default()
    };
    
    let mut gameloop = Gameloop::new(gameinfo);
    
    let mut rng = rand::thread_rng();
    
    // Make first few numbers more random
    for _ in 0..100
    {
        rng.gen::<f64>();
    }
    
    let source_manager = gameloop.control_mut().source_manager();
    
    let source = FilesystemSource::new("examples_data", TrustLevel::TrustedSource);
    source_manager.borrow_mut().add_source(Box::new(source));
    
    let source = FilesystemSource::new("keeshond/examples_data", TrustLevel::TrustedSource);
    source_manager.borrow_mut().add_source(Box::new(source));
    
    gameloop.control_mut().goto_new_scene::<GameScene>();
    
    gameloop.run();
}
