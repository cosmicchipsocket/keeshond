extern crate keeshond;

use keeshond::audio::Sound;
use keeshond::gameloop::{Gameloop, GameInfo, GameControl};
use keeshond::scene::{ThinkerSystem, DrawerSystem, ThinkerArgs, DrawerArgs, SceneType, NullSpawnable, SceneConfig};
use keeshond::renderer::{DrawBackgroundColorInfo, DrawOptions, DrawOrdering, DrawSpriteRawInfo, Sheet};

use keeshond::datapack::{DataHandle, DataId, LoadErrorMode};
use keeshond::datapack::source::{FilesystemSource, TrustLevel};

use keeshond_derive::Component;

const LOGO_WIDTH : f32 = 512.0;
const LOGO_HEIGHT : f32 = 256.0;
const LOGO_HITBOX_WIDTH : f64 = 214.0;
const LOGO_HITBOX_HEIGHT : f64 = 66.0;

struct GameScene {}

impl SceneType for GameScene
{
    type SpawnableIdType = NullSpawnable;

    fn new() -> Self where Self : Sized { GameScene {} }
    fn config(&mut self, game: &mut GameControl) -> SceneConfig<Self>
    {
        let mut config = SceneConfig::new();

        config.thinker(LogoThinker::new(game))
            .drawer(BgDrawer::new())
            .drawer(LogoDrawer::new(game));

        config
    }
}

struct BgDrawer
{
    r : f32,
    g : f32,
    b : f32
}

impl BgDrawer
{
    fn new() -> BgDrawer
    {
        BgDrawer
        {
            r : 1.0,
            g : 1.0,
            b : 1.0
        }
    }
}

impl DrawerSystem for BgDrawer
{
    fn draw(&self, args : DrawerArgs)
    {
        let info = DrawBackgroundColorInfo
        {
            shader_id : DataId::new(0),
            r : self.r,
            g : self.g,
            b : self.b,
            alpha : 1.0,
            depth : 0.0
        };
        
        args.drawing.draw_background_color(&info, &DrawOptions
        {
            ordering : DrawOrdering::Painter
        });
    }
}


#[derive(Component)]
struct Logo
{
    x : f64,
    y : f64,
    old_x : f64,
    old_y : f64,
    vel_x : f64,
    vel_y : f64
}

impl Logo
{
    pub fn new() -> Logo
    {
        Logo
        {
            x : LOGO_HITBOX_WIDTH,
            y : LOGO_HITBOX_HEIGHT,
            old_x : LOGO_HITBOX_WIDTH,
            old_y : LOGO_HITBOX_HEIGHT,
            vel_x : 4.0,
            vel_y : 4.0
        }
    }
    
    pub fn get_render_position(&self, interpolation : f32) -> (f32, f32)
    {
        (self.x as f32 + (self.x - self.old_x) as f32 * interpolation, self.y as f32 + (self.y - self.old_y) as f32 * interpolation)
    }
}

struct LogoThinker
{
    sound1 : DataHandle<Sound>,
    sound2 : DataHandle<Sound>
}

impl LogoThinker
{
    fn new(control : &mut GameControl) -> LogoThinker
    {
        let mut sound_store = control.res().store_mut::<Sound>();
        let sound1 = DataHandle::with_path("audio_example", "ping.wav");
        let sound2 = DataHandle::with_path("audio_example", "pong.wav");
        sound1.resolve(&mut sound_store, LoadErrorMode::Fatal);
        sound2.resolve(&mut sound_store, LoadErrorMode::Fatal);

        LogoThinker
        {
            sound1,
            sound2
        }
    }
}

impl ThinkerSystem<GameScene> for LogoThinker
{
    fn start(&mut self, args: ThinkerArgs<GameScene>)
    {
        let entity = args.scene.add_entity_later();
        let logo = Logo::new();

        args.scene.add_component_later(&entity, logo);
    }

    fn think(&mut self, args : ThinkerArgs<GameScene>)
    {
        let mut logo_store = args.components.store_mut::<Logo>();
        let iter = logo_store.iter_mut();
        
        for (_, logo) in iter
        {
            logo.old_x = logo.x;
            logo.old_y = logo.y;

            logo.x += logo.vel_x;
            logo.y += logo.vel_y;
            
            if (logo.x < LOGO_HITBOX_WIDTH && logo.vel_x < 0.0)
                || (logo.x > 1280.0 - LOGO_HITBOX_WIDTH && logo.vel_x > 0.0)
            {
                logo.vel_x = 0.0 - logo.vel_x;
                args.game.audio_mut().play_sound(self.sound1.id());
            }
            
            if (logo.y < LOGO_HITBOX_HEIGHT && logo.vel_y < 0.0)
                || (logo.y > 720.0 - LOGO_HITBOX_HEIGHT && logo.vel_y > 0.0)
            {
                logo.vel_y = 0.0 - logo.vel_y;
                args.game.audio_mut().play_sound(self.sound2.id());
            }
        }
    }
}

struct LogoDrawer
{
    sheet : DataId<Sheet>
}

impl LogoDrawer
{
    fn new(control : &mut GameControl) -> LogoDrawer
    {
        LogoDrawer
        {
            sheet : control.res().store_mut::<Sheet>().get_id_mut("common", "objects.png")
                .expect("Failed to load package"),
        }
    }
}

impl DrawerSystem for LogoDrawer
{
    fn draw(&self, args : DrawerArgs)
    {
        let logo_store = args.components.store::<Logo>();
        let iter = logo_store.iter();
        
        for (_, logo) in iter
        {
            let (x, y) = logo.get_render_position(args.interpolation);
            let mut transform = args.transform.clone();
            transform.translate(x as f32, y as f32);
            transform.scale(LOGO_WIDTH, LOGO_HEIGHT);
            
            let info = DrawSpriteRawInfo
            {
                sheet_id : self.sheet,
                shader_id : DataId::new(0),
                transform,
                transform_offset_x : -0.5,
                transform_offset_y : -0.5,
                texture_x : 0.0,
                texture_y : 0.0,
                texture_w : 1.0,
                texture_h : 0.5,
                r : 1.0,
                g : 1.0,
                b : 1.0,
                alpha : 1.0,
                depth : 0.0
            };
            
            args.drawing.draw_sprite_raw(&info, &DrawOptions
            {
                ordering : DrawOrdering::Painter
            });
        }
    }
}

////////////////////////////////////////


fn main()
{
    let gameinfo = GameInfo
    {
        package_name : "audio",
        friendly_name : "Keeshond Audio Example",
        base_width : 1280,
        base_height : 720,
        cursor_visible : false,
        .. Default::default()
    };
    
    let mut gameloop = Gameloop::new(gameinfo);
    
    let source_manager = gameloop.control_mut().source_manager();
    
    let source = FilesystemSource::new("examples_data", TrustLevel::TrustedSource);
    source_manager.borrow_mut().add_source(Box::new(source));
    
    let source = FilesystemSource::new("keeshond/examples_data", TrustLevel::TrustedSource);
    source_manager.borrow_mut().add_source(Box::new(source));
    
    gameloop.control_mut().goto_new_scene::<GameScene>();
    gameloop.run();
}
