extern crate keeshond;

use keeshond::darg;
use keeshond::gameloop::{Gameloop, GameInfo, GameControl};
use keeshond::scene::{DrawerSystem, DrawerArgs, SceneType, SceneConfig, SpawnControl, DynArgList, ThinkerSystem, ThinkerArgs};
use keeshond::renderer::{DrawBackgroundColorInfo, DrawOptions, DrawOrdering, DrawSpriteInfo, Shader, Sheet, SliceId, WindowScaleMode};
use keeshond::util::SimpleTransform;

use keeshond::datapack::{DataHandle, LoadErrorMode};
use keeshond::datapack::source::{FilesystemSource, TrustLevel};

use keeshond_derive::Component;

const SHADER_FILENAMES : [&str; 4] =
[
    "gradient_sprite.glsl",
    "invert.glsl",
    "pixel_outline.glsl",
    "screen_door.glsl"
];

keeshond::spawnables!
{
    SpawnId;
    ShaderSprite
}

struct GameScene
{

}

impl SceneType for GameScene
{
    type SpawnableIdType = SpawnId;

    fn new() -> Self where Self : Sized
    {
        GameScene
        {

        }
    }
    fn config(&mut self, game : &mut GameControl) -> SceneConfig<Self>
    {
        let mut config = SceneConfig::new();

        config.thinker(ShaderThinker::new())
            .drawer(ShaderDrawer::new(game));

        config
    }
    fn spawn(&mut self, spawn : &mut SpawnControl, spawnable_id : Self::SpawnableIdType,
             game : &mut GameControl, transform : &SimpleTransform, args : &DynArgList)
    {
        match spawnable_id
        {
            SpawnId::ShaderSprite =>
            {
                let mut store = game.res().store_mut();
                let sheet_handle = DataHandle::<Sheet>::with_path(args.get_string(0, "shader_example"),
                                                                  args.get_string(1, "sprites.png"));
                let slice_name = args.get_string(2, "default");
                let sheet = sheet_handle.get(&mut store, LoadErrorMode::Fatal).unwrap();
                let slice = sheet.slice_id(slice_name).unwrap_or(0);

                spawn.with(ShaderSprite::new(transform.x, transform.y, sheet_handle.clone_without_path(), slice));
            }
        }
    }
}

#[derive(Component)]
struct ShaderSprite
{
    x : f64,
    y : f64,
    sheet : DataHandle<Sheet>,
    slice : SliceId
}

impl ShaderSprite
{
    fn new(x : f64, y : f64, sheet : DataHandle<Sheet>, slice : SliceId) -> ShaderSprite
    {
        ShaderSprite
        {
            x, y, sheet, slice
        }
    }
}

#[derive(Component)]
struct ShaderParams
{
    shader : DataHandle<Shader>,
    r : f32,
    g : f32,
    b : f32,
    alpha : f32
}

impl Default for ShaderParams
{
    fn default() -> Self
    {
        ShaderParams
        {
            shader : DataHandle::null(),
            r : 1.0,
            g : 1.0,
            b : 1.0,
            alpha : 1.0
        }
    }
}

struct ShaderThinker
{
    selected_shader : usize
}

impl ShaderThinker
{
    fn new() -> ShaderThinker
    {
        ShaderThinker
        {
            selected_shader : 0
        }
    }
}

impl ThinkerSystem<GameScene> for ShaderThinker
{
    fn start(&mut self, args : ThinkerArgs<GameScene>)
    {
        args.scene.spawn_later_with(SpawnId::ShaderSprite, args.game, &SimpleTransform::with_xy(320.0, 200.0), &darg!("shader_example", "sprites.png", "logo"));
        args.scene.spawn_later_with(SpawnId::ShaderSprite, args.game, &SimpleTransform::with_xy(472.0, 192.0), &darg!("shader_example", "sprites.png", "kobold"));
        args.scene.spawn_later_with(SpawnId::ShaderSprite, args.game, &SimpleTransform::with_xy(520.0, 162.0), &darg!("shader_example", "sprites.png", "palette"));
        args.scene.spawn_later_with(SpawnId::ShaderSprite, args.game, &SimpleTransform::with_xy(520.0, 219.0), &darg!("shader_example", "sprites.png", "doggy"));
        args.scene.spawn_later_with(SpawnId::ShaderSprite, args.game, &SimpleTransform::with_xy(168.0, 204.0), &darg!("shader_example", "sprites.png", "tree"));
        args.scene.spawn_later_with(SpawnId::ShaderSprite, args.game, &SimpleTransform::with_xy(260.0, 140.0), &darg!("shader_example", "sprites.png", "froggy"));
        args.scene.spawn_later_with(SpawnId::ShaderSprite, args.game, &SimpleTransform::with_xy(290.0, 140.0), &darg!("shader_example", "sprites.png", "raspberry"));
        args.scene.spawn_later_with(SpawnId::ShaderSprite, args.game, &SimpleTransform::with_xy(320.0, 140.0), &darg!("shader_example", "sprites.png", "banana"));
        args.scene.spawn_later_with(SpawnId::ShaderSprite, args.game, &SimpleTransform::with_xy(350.0, 140.0), &darg!("shader_example", "sprites.png", "pear"));
        args.scene.spawn_later_with(SpawnId::ShaderSprite, args.game, &SimpleTransform::with_xy(380.0, 140.0), &darg!("shader_example", "sprites.png", "blueberry"));
    }

    fn think(&mut self, args : ThinkerArgs<GameScene>)
    {
        let mut shader_param_store = args.components.store_mut::<ShaderParams>();
        let mut shader_params = shader_param_store.singleton_mut();
        let mut shader_changed = false;

        if args.game.input().down_once("next_shader")
        {
            self.selected_shader = (self.selected_shader + 1).min(SHADER_FILENAMES.len() - 1);
            shader_changed = true;
        }
        if args.game.input().down_once("previous_shader")
        {
            self.selected_shader = self.selected_shader.saturating_sub(1);
            shader_changed = true;
        }

        if shader_changed || shader_params.shader.path().is_empty()
        {
            let filename = SHADER_FILENAMES[self.selected_shader];
            shader_params.shader = DataHandle::with_path("shader_example", filename);
            shader_params.shader.resolve(&mut args.game.res().store_mut(), LoadErrorMode::Fatal);

            args.game.renderer_mut().set_window_title(&format!("Keeshond Shader Example | {} | Arrow keys to change", filename));
        }
    }
}

struct ShaderDrawer
{
    shader : DataHandle<Shader>
}

impl ShaderDrawer
{
    fn new(control : &mut GameControl) -> ShaderDrawer
    {
        let shader = DataHandle::with_path("shader_example", "gradient_bg.glsl");
        shader.resolve(&mut control.res().store_mut(), LoadErrorMode::Fatal);

        ShaderDrawer
        {
            shader
        }
    }
}

impl DrawerSystem for ShaderDrawer
{
    fn draw(&self, args : DrawerArgs)
    {
        let shader_param_store = args.components.store::<ShaderParams>();
        let shader_params = shader_param_store.singleton().unwrap();
        let sprite_store = args.components.store::<ShaderSprite>();

        let info = DrawBackgroundColorInfo
        {
            shader_id : self.shader.id(),
            r : 1.0,
            g : 1.0,
            b : 1.0,
            alpha : 1.0,
            depth : 0.0
        };

        let options = DrawOptions
        {
            ordering : DrawOrdering::Painter
        };
        
        args.drawing.draw_background_color(&info, &options);

        for (_, sprite) in sprite_store.iter()
        {
            let info = DrawSpriteInfo
            {
                sheet_id : sprite.sheet.id(),
                shader_id : shader_params.shader.id(),
                x : sprite.x as f32,
                y : sprite.y as f32,
                angle : 0.0,
                scale_x : 1.0,
                scale_y : 1.0,
                slice : sprite.slice,
                transform : args.transform.clone(),
                r : shader_params.r,
                g : shader_params.g,
                b : shader_params.b,
                alpha : shader_params.alpha,
                depth : 0.0
            };

            args.drawing.draw_sprite(&info, &options);
        }
    }
}


////////////////////////////////////////


fn main()
{
    let gameinfo = GameInfo
    {
        package_name : "shader",
        friendly_name : "Keeshond Shader Example",
        base_width : 640,
        base_height : 360,
        cursor_visible : true,
        texture_filtering : false,
        default_window_scale : WindowScaleMode::PixelMultiplierDpiScaled(2),
        .. Default::default()
    };
    
    let mut gameloop = Gameloop::new(gameinfo);

    let input = gameloop.control_mut().input_mut();

    input.add_bind("next_shader", "right");
    input.add_bind("next_shader", "down");
    input.add_bind("previous_shader", "left");
    input.add_bind("previous_shader", "up");

    let source_manager = gameloop.control_mut().source_manager();
    
    let source = FilesystemSource::new("examples_data", TrustLevel::TrustedSource);
    source_manager.borrow_mut().add_source(Box::new(source));
    
    let source = FilesystemSource::new("keeshond/examples_data", TrustLevel::TrustedSource);
    source_manager.borrow_mut().add_source(Box::new(source));
    
    gameloop.control_mut().goto_new_scene::<GameScene>();
    gameloop.run();
}
