extern crate keeshond;

use keeshond::gameloop::{Gameloop, GameInfo, GameControl};
use keeshond::renderer::ViewportMode;
use keeshond::scene::{ImGuiSystem, ThinkerArgs, SceneType, NullSpawnable, SceneConfig};

struct GameScene {}

impl SceneType for GameScene
{
    type SpawnableIdType = NullSpawnable;

    fn new() -> Self where Self : Sized { GameScene {} }
    fn config(&mut self, _game: &mut GameControl) -> SceneConfig<Self>
    {
        let mut config = SceneConfig::new();

        config.imgui(Some(MyGui::new()));

        config
    }
}

pub struct MyGui
{
    demo_open : bool,
}

impl MyGui
{
    pub fn new() -> MyGui { MyGui { demo_open : true } }
}

impl ImGuiSystem<GameScene> for MyGui
{
    fn imgui_think(&mut self, ui : &mut imgui::Ui, _args : ThinkerArgs<GameScene>)
    {
        imgui::Window::new("ImGui Example")
            .size([320.0, 200.0], imgui::Condition::FirstUseEver)
            .build(ui, ||
        {
            ui.text("Hello Keeshond!");
            ui.text_wrapped("Keeshond is a 2D game engine with a focus on quickly bringing ideas onscreen.");
            
            ui.spacing();
            ui.separator();
            ui.spacing();
            
            ui.checkbox("Show imgui demo window", &mut self.demo_open);
        });
        
        if self.demo_open
        {
            ui.show_demo_window(&mut self.demo_open);
        }
    }
}


////////////////////////////////////////


fn main()
{
    let gameinfo = GameInfo
    {
        package_name : "imgui",
        friendly_name : "Keeshond ImGui Example",
        base_width : 1280,
        base_height : 720,
        viewport_mode : ViewportMode::OneToOneDpiScaled,
        .. Default::default()
    };
    
    let mut gameloop = Gameloop::new(gameinfo);
    
    gameloop.control_mut().goto_new_scene::<GameScene>();
    gameloop.run();
}
