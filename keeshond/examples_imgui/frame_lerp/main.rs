extern crate keeshond;

use std::time::Duration;

use keeshond::gameloop::{Gameloop, GameInfo, GameControl, FrameInterpolationMode, FrameInterpolationCap, FrameInterpolationPacing};
use keeshond::scene::{DrawerSystem, ThinkerSystem, SceneType, SpawnControl, DynArgList, SceneConfig, ThinkerArgs, DrawerArgs, ImGuiSystem};
use keeshond::renderer::{DrawOptions, DrawOrdering, DrawSpriteInfo, Sheet, VsyncMode};
use keeshond::util::SimpleTransform;

use keeshond::datapack::{DataHandle, DataId, LoadErrorMode};
use keeshond::datapack::source::{FilesystemSource, TrustLevel};

use keeshond_derive::Component;

const MOVER_SCALE : f32 = 0.25;
const MOVER_NUM_ROW_1 : usize = 16;
const MOVER_NUM_ROW_2 : usize = 32;
const MOVER_SPEED : f64 = 16.0;


////////////////////////////////////////


keeshond::spawnables!
{
    ExampleSpawnId;
    Mover
}

struct LerpExampleScene
{

}

impl SceneType for LerpExampleScene
{
    type SpawnableIdType = ExampleSpawnId;

    fn new() -> Self where Self : Sized { LerpExampleScene {} }
    fn config(&mut self, game: &mut GameControl) -> SceneConfig<Self>
    {
        let mut config = SceneConfig::new();

        config.thinker(MoverThinker {})
            .drawer(LerpExampleDrawer::new(game))
            .imgui(Some(MyGui::new()));

        config
    }
    fn spawn(&mut self, spawn : &mut SpawnControl, spawnable_id : ExampleSpawnId,
             _game : &mut GameControl, transform : &SimpleTransform, _args : &DynArgList)
    {
        match spawnable_id
        {
            ExampleSpawnId::Mover =>
            {
                spawn.with(Mover::new(transform.x, transform.y));
            }
        }
    }
}


////////////////////////////////////////

#[derive(Component)]
struct LerpExampleControl
{
    speed : f64,
    sleep_think : u8,
    sleep_draw : u8,
    lerp_cap : u32
}

impl Default for LerpExampleControl
{
    fn default() -> LerpExampleControl
    {
        LerpExampleControl
        {
            speed : MOVER_SPEED,
            sleep_think : 0,
            sleep_draw : 0,
            lerp_cap : 120
        }
    }
}

#[derive(Component)]
struct Mover
{
    x : f64,
    y : f64,
    old_x : f64,
    old_y : f64,
    scale_x : f32,
    scale_y : f32
}

impl Mover
{
    pub fn new(x : f64, y : f64) -> Mover
    {
        Mover
        {
            x,
            y,
            old_x : x,
            old_y : y,
            scale_x : MOVER_SCALE,
            scale_y : MOVER_SCALE
        }
    }

    pub fn get_render_position(&self, interpolation : f32) -> (f32, f32)
    {
        (self.x as f32 + (self.x - self.old_x) as f32 * interpolation, self.y as f32 + (self.y - self.old_y) as f32 * interpolation)
    }
}

struct MoverThinker {}

impl ThinkerSystem<LerpExampleScene> for MoverThinker
{
    fn start(&mut self, args : ThinkerArgs<LerpExampleScene>)
    {
        let (screen_width, screen_height) = args.game.renderer().base_size();

        for i in 0..MOVER_NUM_ROW_1
        {
            args.scene.spawn_later(ExampleSpawnId::Mover, args.game,
                                   &SimpleTransform::with_xy(screen_width as f64 * (i as f64 / MOVER_NUM_ROW_1 as f64),
                                   screen_height as f64 / 2.0));
        }

        for i in 0..MOVER_NUM_ROW_2
        {
            args.scene.spawn_later(ExampleSpawnId::Mover, args.game,
                                   &SimpleTransform::with_xy(screen_width as f64 * (i as f64 / MOVER_NUM_ROW_2 as f64),
                                   screen_height as f64 / 2.0 + 64.0));
        }
    }

    fn think(&mut self, args : ThinkerArgs<LerpExampleScene>)
    {
        let (screen_width, _) = args.game.renderer().base_size();
        let mut mover_store = args.components.store_mut::<Mover>();
        let mut control_store = args.components.store_mut::<LerpExampleControl>();
        let control = control_store.singleton_mut();

        std::thread::sleep(Duration::from_millis(control.sleep_think as u64));

        for (_, mover) in mover_store.iter_mut()
        {
            mover.old_x = mover.x;
            mover.old_y = mover.y;

            mover.x += control.speed;

            if mover.x > screen_width as f64
            {
                mover.x -= screen_width as f64;
                mover.old_x -= screen_width as f64;
            }
        }
    }
}

struct LerpExampleDrawer
{
    example_sheet : DataHandle<Sheet>
}

impl LerpExampleDrawer
{
    fn new(control : &mut GameControl) -> LerpExampleDrawer
    {
        let example_sheet = DataHandle::with_path("frame_lerp", "objects.png");
        example_sheet.resolve(&mut control.res().store_mut(), LoadErrorMode::Fatal);

        LerpExampleDrawer
        {
            example_sheet
        }
    }
}

impl DrawerSystem for LerpExampleDrawer
{
    fn draw(&self, args : DrawerArgs)
    {
        let mover_store = args.components.store::<Mover>();

        if let Some(control) = args.components.store::<LerpExampleControl>().singleton()
        {
            std::thread::sleep(Duration::from_millis(control.sleep_draw as u64));
        }

        for (_, mover) in mover_store.iter()
        {
            let (x, y) = mover.get_render_position(args.interpolation);
            let transform = args.transform.clone();

            args.drawing.draw_sprite(&DrawSpriteInfo
            {
                sheet_id : self.example_sheet.id(),
                shader_id : DataId::new(0),
                x,
                y,
                angle : 0.0,
                scale_x : mover.scale_x,
                scale_y : mover.scale_y,
                slice : 0,
                transform,
                r : 1.0,
                g : 1.0,
                b : 1.0,
                alpha : 1.0,
                depth : 0.0
            }, &DrawOptions
            {
                ordering : DrawOrdering::Painter
            });
        }
    }
}

pub struct MyGui {}

impl MyGui
{
    pub fn new() -> MyGui { MyGui {} }
}

impl ImGuiSystem<LerpExampleScene> for MyGui
{
    fn imgui_think(&mut self, ui : &mut imgui::Ui, args : ThinkerArgs<LerpExampleScene>)
    {
        let mut control_store = args.components.store_mut::<LerpExampleControl>();
        let control = control_store.singleton_mut();

        if let Some(window) = imgui::Window::new("Frame Interpolation Example")
            .collapsible(false)
            .position([32.0, 32.0], imgui::Condition::Always)
            .size([1000.0, 0.0], imgui::Condition::Always)
            .save_settings(false)
            .begin(ui)
        {
            ui.columns(5, "example_columns", false);

            ui.text("Frame Interpolation Mode");
            ui.spacing();

            ui.indent();

            let mut lerp_mode = args.game.frame_interpolation();
            let mut update_lerp_mode = false;

            update_lerp_mode |= ui.radio_button("Off##LerpOff", &mut lerp_mode, FrameInterpolationMode::Off);
            update_lerp_mode |= ui.radio_button("Automatic", &mut lerp_mode, FrameInterpolationMode::Automatic);
            update_lerp_mode |= ui.radio_button("Force On", &mut lerp_mode, FrameInterpolationMode::Force);

            if update_lerp_mode
            {
                args.game.set_frame_interpolation(lerp_mode);
            }

            if args.game.frame_interpolation_active()
            {
                ui.spacing();

                let mut lerp_cap = args.game.frame_interpolation_cap();
                let mut lerp_cap_active = lerp_cap != FrameInterpolationCap::None;
                let mut lerp_cap_auto = lerp_cap == FrameInterpolationCap::Automatic
                    || lerp_cap == FrameInterpolationCap::None;
                let mut update_lerp_cap = false;

                update_lerp_cap |= ui.checkbox("Framerate Cap", &mut lerp_cap_active);

                if lerp_cap_active
                {
                    ui.indent();
                    update_lerp_cap |= ui.checkbox("Auto Set Cap", &mut lerp_cap_auto);

                    if !lerp_cap_auto
                    {
                        update_lerp_cap |= imgui::Slider::new("##framerate_cap_value", 5, 1000)
                            .flags(imgui::SliderFlags::ALWAYS_CLAMP)
                            .display_format("%d FPS")
                            .build(ui, &mut control.lerp_cap);
                    }
                    ui.unindent();
                }

                if update_lerp_cap
                {
                    lerp_cap = match lerp_cap_active
                    {
                        true =>
                        {
                            match lerp_cap_auto
                            {
                                true => { FrameInterpolationCap::Automatic }
                                false => { FrameInterpolationCap::Manual(control.lerp_cap as f64) }
                            }
                        }
                        false => { FrameInterpolationCap::None }
                    };

                    args.game.set_frame_interpolation_cap(lerp_cap);
                }
            }

            ui.unindent();

            ui.set_current_column_width(175.0);
            ui.next_column();

            ui.text("Lerp Frame Pacing");
            ui.spacing();

            ui.indent();

            let mut lerp_pacing = args.game.frame_interpolation_pacing();
            let mut update_lerp_pacing = false;

            update_lerp_pacing |= ui.radio_button("High Framerate", &mut lerp_pacing, FrameInterpolationPacing::PreferHighFramerate);
            update_lerp_pacing |= ui.radio_button("Smooth Motion", &mut lerp_pacing, FrameInterpolationPacing::PreferSmoothMotion);

            if update_lerp_pacing
            {
                args.game.set_frame_interpolation_pacing(lerp_pacing);
            }

            ui.unindent();

            ui.spacing();
            ui.spacing();

            ui.text(&format!("Display refresh rate: {}hz", args.game.renderer().current_refresh_rate()));

            ui.spacing();

            let lerp_active = match args.game.frame_interpolation_active()
            {
                true => { "ON" }
                false => { "OFF" }
            };

            ui.text(&format!("Frame lerp is {}", lerp_active));

            ui.set_current_column_width(175.0);
            ui.next_column();

            ui.text("Vertical Synchronization");
            ui.spacing();

            ui.indent();

            let mut vsync_mode = args.game.renderer().vsync();
            let mut update_vsync = false;

            update_vsync |= ui.radio_button("Off##OffVsync", &mut vsync_mode, VsyncMode::Off);
            update_vsync |= ui.radio_button("On", &mut vsync_mode, VsyncMode::On);
            update_vsync |= ui.radio_button("Adaptive", &mut vsync_mode, VsyncMode::Adaptive);

            if update_vsync
            {
                args.game.renderer_mut().set_vsync(vsync_mode);
            }

            ui.unindent();

            ui.set_current_column_width(175.0);
            ui.next_column();

            ui.text("Movement");
            ui.spacing();

            let (screen_width, _) = args.game.renderer().base_size();

            let item_width = ui.push_item_width(150.0);

            ui.indent();
            imgui::Slider::new("Speed", 0.0, screen_width as f64 / MOVER_NUM_ROW_2 as f64)
                .flags(imgui::SliderFlags::ALWAYS_CLAMP)
                .display_format("%.3f px/frame")
                .build(ui, &mut control.speed);
            ui.unindent();

            ui.spacing();
            ui.text("Interpolation Debug");
            ui.spacing();

            ui.indent();
            imgui::Slider::new("Wait draw", 0, 50).flags(imgui::SliderFlags::ALWAYS_CLAMP)
                .display_format("%d ms").build(ui, &mut control.sleep_draw);
            imgui::Slider::new("Wait think", 0, 15).flags(imgui::SliderFlags::ALWAYS_CLAMP)
                .display_format("%d ms").build(ui, &mut control.sleep_think);
            ui.unindent();

            item_width.pop(ui);

            ui.set_current_column_width(275.0);
            ui.next_column();

            let stats = args.game.stats();

            ui.text(&format!("Draw FPS: {}", stats.draw_fps));
            ui.text(&format!("Think FPS: {}", stats.think_fps));

            ui.spacing();

            ui.text(&format!("Think ms: {:.2} avg, {:.2} cur", stats.average_ms_think, stats.frame_ms_think));
            ui.text(&format!("Draw ms: {:.2} avg, {:.2} cur", stats.average_ms_draw, stats.frame_ms_draw));
            ui.text(&format!("Present ms: {:.2} avg, {:.2} cur", stats.average_ms_present, stats.frame_ms_present));
            ui.text(&format!("Wait ms: {:.2} avg, {:.2} cur", stats.average_ms_wait, stats.frame_ms_wait));

            ui.set_current_column_width(200.0);
            ui.columns(1, "", false);

            window.end();
        }
    }
}


////////////////////////////////////////


fn main()
{
    let gameinfo = GameInfo
    {
        package_name : "frame_lerp",
        friendly_name : "Keeshond Frame Interpolation Example",
        base_width : 1280,
        base_height : 720,
        fullscreen : true,
        texture_filtering : true,
        cursor_visible : true,
        frame_interpolation : FrameInterpolationMode::Automatic,
        .. Default::default()
    };

    let mut gameloop = Gameloop::new(gameinfo);

    let source_manager = gameloop.control_mut().source_manager();

    let source = FilesystemSource::new("examples_data", TrustLevel::TrustedSource);
    source_manager.borrow_mut().add_source(Box::new(source));

    let source = FilesystemSource::new("keeshond/examples_data", TrustLevel::TrustedSource);
    source_manager.borrow_mut().add_source(Box::new(source));

    gameloop.control_mut().goto_new_scene::<LerpExampleScene>();

    gameloop.run();
}
