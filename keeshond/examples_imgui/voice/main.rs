extern crate keeshond;

use keeshond::audio::{Voice, VoiceInfo, Sound};
use keeshond::gameloop::{Gameloop, GameInfo, GameControl};
use keeshond::renderer::ViewportMode;
use keeshond::scene::{ThinkerSystem, ImGuiSystem, ThinkerArgs, SceneConfig, SceneType, NullSpawnable};

use keeshond::datapack::{DataHandle, LoadErrorMode};
use keeshond::datapack::source::{FilesystemSource, TrustLevel};

use keeshond_derive::Component;

use imgui;

struct GameScene {}

impl SceneType for GameScene
{
    type SpawnableIdType = NullSpawnable;

    fn new() -> Self where Self : Sized { GameScene {} }
    fn config(&mut self, _game: &mut GameControl) -> SceneConfig<Self>
    {
        let mut config = SceneConfig::new();

        config.thinker(VoiceSystem::new())
            .imgui(Some(MyGui::new()));

        config
    }
}

#[derive(Component)]
pub struct VoiceControl
{
    new_volume : f32,
    new_pitch : f32,
    new_pos_x : f32,
    new_pos_y : f32,
    new_distance : f32,
    new_rolloff_start : f32,
    new_local : bool,
    new_looping : bool,
    play_sound : bool,
    update_sound : Option<usize>,
    stop_sound : Option<usize>,
    unit_scale : f32,
    listener_position : (f32, f32),
    voice_generations : Vec<u64>,
    voice_info : Vec<Option<VoiceInfo>>,
    sound : DataHandle<Sound>
}

impl VoiceControl
{
    pub fn new(control : &mut GameControl) -> VoiceControl
    {
        let mut voice_generations = Vec::new();
        let mut voice_info = Vec::new();
        
        for _ in 0..control.audio().max_voices()
        {
            voice_generations.push(0);
            voice_info.push(None);
        }

        let sound = DataHandle::with_path("audio_example", "ping_mono.wav");
        sound.resolve(&mut control.res().store_mut(), LoadErrorMode::Fatal);
        
        VoiceControl
        {
            new_volume : 1.0,
            new_pitch : 1.0,
            new_pos_x : 0.0,
            new_pos_y : 0.0,
            new_distance : 1.0,
            new_rolloff_start : 0.0,
            new_local : false,
            new_looping : false,
            play_sound : false,
            update_sound : None,
            stop_sound : None,
            unit_scale : 1.0,
            listener_position : (0.0, 0.0),
            voice_info,
            voice_generations,
            sound
        }
    }
}

pub struct VoiceSystem {}

impl VoiceSystem
{
    pub fn new() -> VoiceSystem
    {
        VoiceSystem {}
    }
}

impl ThinkerSystem<GameScene> for VoiceSystem
{
    fn start(&mut self, args: ThinkerArgs<GameScene>)
    {
        let entity = args.scene.add_entity_later();
        let voice_control = VoiceControl::new(args.game);

        args.scene.add_component_later(&entity, voice_control);
    }

    fn think(&mut self, args : ThinkerArgs<GameScene>)
    {
        let mut voice_control_store = args.components.store_mut::<VoiceControl>();
        let (_, voice_control) = voice_control_store.first_mut().unwrap();
        
        args.game.audio_mut().set_unit_scale(voice_control.unit_scale);
        
        let (listener_x, listener_y) = voice_control.listener_position;
        args.game.audio_mut().set_listener_position(listener_x, listener_y);
        
        let voice_info = VoiceInfo
        {
            volume : voice_control.new_volume,
            pitch : voice_control.new_pitch,
            pos_x : voice_control.new_pos_x,
            pos_y : voice_control.new_pos_y,
            distance : voice_control.new_distance,
            rolloff_start : voice_control.new_rolloff_start,
            local : voice_control.new_local,
            looping : voice_control.new_looping,
            .. Default::default()
        };
        
        if voice_control.play_sound
        {
            if let Some(voice) = args.game.audio_mut().play_sound_with(voice_control.sound.id(), &voice_info)
            {
                voice_control.voice_generations[voice.slot] = voice.generation;
            }
            
            voice_control.play_sound = false;
        }
        
        if let Some(slot) = voice_control.update_sound
        {
            let voice = Voice
            {
                slot,
                generation : voice_control.voice_generations[slot]
            };
            
            args.game.audio_mut().set_voice(&voice, &voice_info);
            voice_control.update_sound = None;
        }
        
        if let Some(slot) = voice_control.stop_sound
        {
            let voice = Voice
            {
                slot,
                generation : voice_control.voice_generations[slot]
            };
            
            args.game.audio_mut().stop_voice(&voice);
            voice_control.stop_sound = None;
        }
        
        for i in 0..args.game.audio().max_voices()
        {
            let voice = Voice
            {
                slot : i,
                generation : voice_control.voice_generations[i]
            };
            
            voice_control.voice_info[i] = args.game.audio().voice(&voice);
        }
    }
}

pub struct MyGui {}

impl MyGui
{
    pub fn new() -> MyGui { MyGui {} }
}

fn clamp<T>(value : &mut T, min_value : T, max_value : T)
    where T : PartialOrd
{
    if *value < min_value
    {
        *value = min_value;
    }
    else if *value > max_value
    {
        *value = max_value;
    }
}

impl ImGuiSystem<GameScene> for MyGui
{
    fn imgui_think(&mut self, ui : &mut imgui::Ui, args : ThinkerArgs<GameScene>)
    {
        let mut voice_control_store = args.components.store_mut::<VoiceControl>();
        let (_, voice_control) = voice_control_store.first_mut().unwrap();
        
        imgui::Window::new("Audio Voice Example")
            .collapsible(false)
            .always_auto_resize(true)
            .build(ui, ||
        {
            ui.text("Voice options");
            ui.spacing();
            
            let push = ui.push_item_width(150.0);
            ui.input_float("Volume", &mut voice_control.new_volume).step(0.05).build();
            ui.input_float("Pitch", &mut voice_control.new_pitch).step(0.05).build();

            let mut pos_x = voice_control.new_pos_x;
            let mut pos_y = voice_control.new_pos_y;
            
            ui.input_float("X", &mut pos_x).step(0.1).build();
            ui.same_line_with_pos(190.0);
            ui.input_float("Y", &mut pos_y).step(0.1).build();
            
            voice_control.new_pos_x = pos_x;
            voice_control.new_pos_y = pos_y;
            
            ui.input_float("Distance", &mut voice_control.new_distance).step(0.05).build();
            ui.input_float("Rolloff start", &mut voice_control.new_rolloff_start).step(0.05).build();
            push.pop(ui);
            
            ui.checkbox("Local Position", &mut voice_control.new_local);
            
            ui.checkbox("Looping", &mut voice_control.new_looping);
            
            clamp(&mut voice_control.new_volume, 0.0, 1.0);
            clamp(&mut voice_control.new_pitch, 0.05, 10.0);
            
            if ui.button_with_size("Play new voice", [120.0, 30.0])
            {
                voice_control.play_sound = true;
            }
            
            ui.spacing();
            ui.separator();
            ui.spacing();
            
            ui.text("Listener options (applied immediately)");
            ui.spacing();
            
            let (mut listener_x, mut listener_y) = voice_control.listener_position;
            
            let push = ui.push_item_width(150.0);
            ui.input_float("Position unit scale", &mut voice_control.unit_scale).step(1.0).build();
            ui.input_float("Listener X", &mut listener_x).step(0.1).build();
            ui.input_float("Listener Y", &mut listener_y).step(0.1).build();
            push.pop(ui);
            
            voice_control.listener_position = (listener_x, listener_y);
            
            clamp(&mut voice_control.unit_scale, 1.0, 999999.0);
        });
        
        imgui::Window::new("Playing Voices")
            .position([440.0, 60.0], imgui::Condition::FirstUseEver)
            .collapsible(false)
            .always_auto_resize(true)
            .build(ui, ||
        {
            let mut any_voices = false;
            
            for (slot, voice_option) in voice_control.voice_info.iter().enumerate()
            {
                if let Some(voice) = voice_option
                {
                    let mut extras = String::new();
                    
                    if voice.local
                    {
                        extras.push_str(", local");
                    }
                    if voice.looping
                    {
                        extras.push_str(", loop");
                    }
                    
                    ui.text(format!("Voice {} - vol = {:.3}, pitch = {:.3}, pos = {:.3}, {:.3}{}",
                        slot, voice.volume, voice.pitch, voice.pos_x, voice.pos_y, extras));
                    
                    let [info_x, _] = ui.item_rect_size();
                    
                    ui.same_line_with_pos(info_x + 50.0);
                    if ui.small_button(format!("Update voice {}", slot))
                    {
                        voice_control.update_sound = Some(slot);
                    }
                    
                    let [update_x, _] = ui.item_rect_size();
                    
                    ui.same_line_with_pos(info_x + update_x + 60.0);
                    if ui.small_button(format!("Stop {}", slot))
                    {
                        voice_control.stop_sound = Some(slot);
                    }
                    
                    any_voices = true;
                }
            }
            
            if !any_voices
            {
                ui.text("(Nothing currently playing)");
            }
        });
    }
}


////////////////////////////////////////


fn main()
{
    let gameinfo = GameInfo
    {
        package_name : "voice",
        friendly_name : "Keeshond Audio Voice Example",
        base_width : 1280,
        base_height : 720,
        viewport_mode : ViewportMode::OneToOneDpiScaled,
        .. Default::default()
    };
    
    let mut gameloop = Gameloop::new(gameinfo);
    
    let source_manager = gameloop.control_mut().source_manager();
    
    let source = FilesystemSource::new("examples_data", TrustLevel::TrustedSource);
    source_manager.borrow_mut().add_source(Box::new(source));
    
    let source = FilesystemSource::new("keeshond/examples_data", TrustLevel::TrustedSource);
    source_manager.borrow_mut().add_source(Box::new(source));
    
    gameloop.control_mut().goto_new_scene::<GameScene>();
    gameloop.run();
}
