use crate::scene::{Component, ComponentListEntry, ComponentRef, ComponentRefMut, ComponentStore, Entity, FreezeFlags};

pub struct ComponentAllIter<'a, C : Component + ComponentRef<'a> + 'static>
{
    data_iter : std::slice::Iter<'a, ComponentListEntry<C>>,
}

impl<'a, C : Component + ComponentRef<'a> + 'static> ComponentAllIter<'a, C>
{
    pub(crate) fn new(store : &'a ComponentStore<C>) -> ComponentAllIter<C>
    {
        ComponentAllIter::<C> { data_iter : store.component_list.iter() }
    }
}

impl<'a, C : Component + ComponentRef<'a> + 'static> Iterator for ComponentAllIter<'a, C>
{
    type Item = (Entity, C::StoreRef);

    fn next(&mut self) -> Option<(Entity, C::StoreRef)>
    {
        if let Some(entry) = self.data_iter.next()
        {
            return Some((entry.entity.clone(), entry.component.store_get()));
        }

        None
    }
}


pub struct ComponentAllIterMut<'a, C : Component + ComponentRefMut<'a> + 'static>
{
    data_iter : std::slice::IterMut<'a, ComponentListEntry<C>>,
}

impl<'a, C : Component + ComponentRefMut<'a> + 'static> ComponentAllIterMut<'a, C>
{
    pub(crate) fn new(store : &'a mut ComponentStore<C>) -> ComponentAllIterMut<C>
    {
        ComponentAllIterMut::<C> { data_iter : store.component_list.iter_mut() }
    }
}

impl<'a, C : Component + ComponentRefMut<'a> + 'static> Iterator for ComponentAllIterMut<'a, C>
{
    type Item = (Entity, C::StoreRefMut);

    fn next(&mut self) -> Option<(Entity, C::StoreRefMut)>
    {
        if let Some(entry) = self.data_iter.next()
        {
            return Some((entry.entity.clone(), entry.component.store_get_mut()));
        }

        None
    }
}


pub struct ComponentIter<'a, C : Component + ComponentRef<'a> + 'static>
{
    data_iter : std::slice::Iter<'a, ComponentListEntry<C>>
}

impl<'a, C : Component + ComponentRef<'a> + 'static> ComponentIter<'a, C>
{
    pub(crate) fn new(store : &'a ComponentStore<C>) -> ComponentIter<C>
    {
        ComponentIter::<C>
        {
            data_iter : store.component_list.iter()
        }
    }
}

impl<'a, C : Component + ComponentRef<'a> + 'static> Iterator for ComponentIter<'a, C>
{
    type Item = (Entity, C::StoreRef);

    fn next(&mut self) -> Option<(Entity, C::StoreRef)>
    {
        while let Some(entry) = self.data_iter.next()
        {
            if entry.freeze_flags.is_empty()
            {
                return Some((entry.entity.clone(), entry.component.store_get()));
            }
        }

        None
    }
}


pub struct ComponentIterMut<'a, C : Component + ComponentRefMut<'a> + 'static>
{
    data_iter : std::slice::IterMut<'a, ComponentListEntry<C>>
}

impl<'a, C : Component + ComponentRefMut<'a> + 'static> ComponentIterMut<'a, C>
{
    pub(crate) fn new(store : &'a mut ComponentStore<C>) -> ComponentIterMut<C>
    {
        ComponentIterMut::<C>
        {
            data_iter : store.component_list.iter_mut()
        }
    }
}

impl<'a, C : Component + ComponentRefMut<'a> + 'static> Iterator for ComponentIterMut<'a, C>
{
    type Item = (Entity, C::StoreRefMut);

    fn next(&mut self) -> Option<(Entity, C::StoreRefMut)>
    {
        while let Some(entry) = self.data_iter.next()
        {
            if entry.freeze_flags.is_empty()
            {
                return Some((entry.entity.clone(), entry.component.store_get_mut()));
            }
        }

        None
    }
}


pub struct ComponentFilterIter<'a, C : Component + ComponentRef<'a> + 'static>
{
    data_iter : std::slice::Iter<'a, ComponentListEntry<C>>,
    freeze_flags : FreezeFlags
}

impl<'a, C : Component + ComponentRef<'a> + 'static> ComponentFilterIter<'a, C>
{
    pub(crate) fn new(store : &'a ComponentStore<C>, freeze_flags : FreezeFlags) -> ComponentFilterIter<C>
    {
        ComponentFilterIter::<C>
        {
            data_iter : store.component_list.iter(),
            freeze_flags
        }
    }
}

impl<'a, C : Component + ComponentRef<'a> + 'static> Iterator for ComponentFilterIter<'a, C>
{
    type Item = (Entity, C::StoreRef, FreezeFlags);

    fn next(&mut self) -> Option<(Entity, C::StoreRef, FreezeFlags)>
    {
        while let Some(entry) = self.data_iter.next()
        {
            if (entry.freeze_flags & self.freeze_flags).is_empty()
            {
                return Some((entry.entity.clone(), entry.component.store_get(), entry.freeze_flags));
            }
        }

        None
    }
}


pub struct ComponentFilterIterMut<'a, C : Component + ComponentRefMut<'a> + 'static>
{
    data_iter : std::slice::IterMut<'a, ComponentListEntry<C>>,
    freeze_flags : FreezeFlags
}

impl<'a, C : Component + ComponentRefMut<'a> + 'static> ComponentFilterIterMut<'a, C>
{
    pub(crate) fn new(store : &'a mut ComponentStore<C>, freeze_flags : FreezeFlags) -> ComponentFilterIterMut<C>
    {
        ComponentFilterIterMut::<C>
        {
            data_iter : store.component_list.iter_mut(),
            freeze_flags
        }
    }
}

impl<'a, C : Component + ComponentRefMut<'a> + 'static> Iterator for ComponentFilterIterMut<'a, C>
{
    type Item = (Entity, C::StoreRefMut, FreezeFlags);

    fn next(&mut self) -> Option<(Entity, C::StoreRefMut, FreezeFlags)>
    {
        while let Some(entry) = self.data_iter.next()
        {
            if (entry.freeze_flags & self.freeze_flags).is_empty()
            {
                return Some((entry.entity.clone(), entry.component.store_get_mut(), entry.freeze_flags));
            }
        }

        None
    }
}
