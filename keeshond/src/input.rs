//! Input module, for handling control bindings
//!
//! # Key Names
//!
//! The following strings can be used when binding actions to keys:
//!
//!
//! ## Keyboard
//!
//! ### Letter/Number Keys
//!
//! `a` `b` `c` `d` `e` `f` `g` `h` `i` `j` `k` `l` `m` `n` `o` `p` `q` `r` `s` `t` `u` `v` `w` `x` `y` `z`
//!
//! `1` `2` `3` `4` `5` `6` `7` `8` `9` `0`
//!
//! ### Symbol Keys
//!
//! `` ` `` `-` `=` `[` `]` `\\` `;` `'` `,` `.` `/`
//!
//! ### Function Keys
//!
//! `f1` `f2` `f3` `f4` `f5` `f6` `f7` `f8` `f9` `f10` `f11` `f12`
//!
//! ### Modifier/Typing Keys
//!
//! `escape` `tab` `space` `backspace` `return` (main Enter key)
//!
//! `left shift` `right shift` `left ctrl` `right ctrl` `left alt` `right alt`
//!
//! ### Arrow/Editing Keys
//!
//! `left` `right` `up` `down` `insert` `delete` `home` `end` `pageup` `pagedown`
//!
//! ### Keypad Keys
//!
//! `keypad 1` `keypad 2` `keypad 3` `keypad 4` `keypad 5` `keypad 6` `keypad 7` `keypad 8` `keypad 9` `keypad 0`
//!
//! `keypad .` `keypad +` `keypad -` `keypad *` `keypad /` `keypad enter`
//!
//! ### Other
//!
//! A full list of key names can be found [here, in the leftmost column titled "Key Name"](https://wiki.libsdl.org/SDL_Scancode).
//!
//! ## Gamepad
//!
//! The following key names bind to any controller that works with the SDL controller API:
//!
//! ### Buttons
//!
//! - `gamepad left` (D-pad left)
//! - `gamepad right` (D-pad right)
//! - `gamepad up` (D-pad up)
//! - `gamepad down` (D-pad down)
//! - `gamepad a` (A face button, south)
//! - `gamepad b` (B face button, east)
//! - `gamepad x` (X face button, west)
//! - `gamepad y` (Y face button, north)
//! - `gamepad l` (L shoulder button, i.e. L1 or "left bumper")
//! - `gamepad r` (R shoulder button, i.e. R1 or "right bumper")
//! - `gamepad lstick` (click left stick inward, i.e. L3)
//! - `gamepad rstick` (click right stick inward, i.e. R3)
//! - `gamepad back`
//! - `gamepad start`
//! - `gamepad guide`
//!
//! ### Axes
//!
//! - `gamepad lstick left` (left stick negative X)
//! - `gamepad lstick right` (left stick positive X)
//! - `gamepad lstick up` (left stick negative Y)
//! - `gamepad lstick down` (left stick positive Y)
//! - `gamepad rstick left` (right stick negative X)
//! - `gamepad rstick right` (right stick positive X)
//! - `gamepad rstick up` (right stick negative Y)
//! - `gamepad rstick down` (right stick positive Y)
//! - `gamepad ltrigger` (L trigger, i.e. L2, positive only)
//! - `gamepad rtrigger` (R trigger, i.e. R2, positive only)
//!
//! # Composite Axis Names
//!
//! The following names can be used to refer to combined axes for configuring deadzones:
//!
//! - `gamepad lstick`
//! - `gamepad rstick`
//! - `gamepad ltrigger`
//! - `gamepad rtrigger`

use std::collections::{HashMap, HashSet};

use sdl2::controller::{Button, Axis};
use sdl2::mouse::MouseButton;

use crate::renderer::Renderer;
use crate::util::{InputDirection4, InputDirection8};

pub const DIAGONAL_DEAD_ZONE : f64 = std::f64::consts::FRAC_PI_8 / 2.0;
const DEFAULT_STICK_DEADZONE : f64 = 0.2;
const DEFAULT_TRIGGER_DEADZONE : f64 = 0.15;
const AXIS_DEADZONE_MAX : f64 = 0.999;
const AXIS_TO_KEY_DEADZONE : f64 = 0.5;

struct InputAction
{
    time_in_state : u64,
    held : bool,
    old_held : bool,
    axis : f64
}

impl InputAction
{
    pub fn new() -> InputAction
    {
        InputAction { time_in_state : 0, held : false, old_held : false, axis : 0.0 }
    }
    
    pub fn update_state(&mut self, is_held : bool, axis : f64)
    {
        self.old_held = self.held;
        
        if self.held == is_held
        {
            self.time_in_state += 1;
        }
        else
        {
            self.time_in_state = 1;
            self.held = is_held
        }

        self.axis = axis;
    }
    
    pub fn time_in_state(&self) -> u64
    {
        self.time_in_state
    }
    
    pub fn is_held(&self) -> bool
    {
        self.held
    }
    
    pub fn down_once(&self) -> bool
    {
        self.held && !self.old_held
    }
    
    pub fn up_once(&self) -> bool
    {
        !self.held && self.old_held
    }

    pub fn axis(&self) -> f64
    {
        self.axis
    }
}

#[derive(Debug, Clone)]
struct AxisData
{
    raw : f64,
    value : f64
}

#[derive(Debug, Clone)]
enum CompositeAxisData
{
    AxisTrigger,
    AxisStick
    {
        neg_x : String,
        pos_x : String,
        neg_y : String,
        pos_y : String
    }
}

fn apply_deadzone(deadzone_opt : Option<&f64>, value : f64) -> f64
{
    if let Some(deadzone) = deadzone_opt
    {
        let deadzone = deadzone.clamp(0.0, AXIS_DEADZONE_MAX);

        return ((value - deadzone) * (1.0 / (1.0 - deadzone))).clamp(0.0, 1.0);
    }

    value
}

/// Manages player input bindings and allows reading key/button presses and axes
pub struct Input
{
    keys_held : HashSet<String>,
    keys_frame_pressed : HashSet<String>,
    keys_same_frame_released : HashSet<String>,
    axis_values : HashMap<String, AxisData>,
    actions_to_keys : HashMap<String, Vec<String>>,
    action_map : HashMap<String, InputAction>,
    mouse_to_key : HashMap<MouseButton, String>,
    gamepad_to_key : HashMap<Button, String>,
    gamepad_to_axis_pos : HashMap<Axis, String>,
    gamepad_to_axis_neg : HashMap<Axis, String>,
    composite_axes : HashMap<String, CompositeAxisData>,
    gamepad_deadzones : HashMap<String, f64>,
    cursor_pos : (f64, f64),
    cursor_pos_raw : (f64, f64),
    cursor_motion : (f64, f64),
    mouse_offset : (f64, f64),
    mouse_scale : (f64, f64),
    mouse_util : Option<sdl2::mouse::MouseUtil>
}

impl Input
{
    pub(crate) fn new(mouse_util : Option<sdl2::mouse::MouseUtil>) -> Input
    {
        let mut mouse_to_key = HashMap::new();

        mouse_to_key.insert(MouseButton::Left, String::from("mouse button left"));
        mouse_to_key.insert(MouseButton::Middle, String::from("mouse button middle"));
        mouse_to_key.insert(MouseButton::Right, String::from("mouse button right"));
        mouse_to_key.insert(MouseButton::X1, String::from("mouse button back"));
        mouse_to_key.insert(MouseButton::X2, String::from("mouse button forward"));

        let mut gamepad_to_key = HashMap::new();

        gamepad_to_key.insert(Button::A, String::from("gamepad a"));
        gamepad_to_key.insert(Button::B, String::from("gamepad b"));
        gamepad_to_key.insert(Button::X, String::from("gamepad x"));
        gamepad_to_key.insert(Button::Y, String::from("gamepad y"));
        gamepad_to_key.insert(Button::Back, String::from("gamepad back"));
        gamepad_to_key.insert(Button::Start, String::from("gamepad start"));
        gamepad_to_key.insert(Button::LeftStick, String::from("gamepad lstick"));
        gamepad_to_key.insert(Button::RightStick, String::from("gamepad rstick"));
        gamepad_to_key.insert(Button::LeftShoulder, String::from("gamepad l"));
        gamepad_to_key.insert(Button::RightShoulder, String::from("gamepad r"));
        gamepad_to_key.insert(Button::DPadLeft, String::from("gamepad left"));
        gamepad_to_key.insert(Button::DPadRight, String::from("gamepad right"));
        gamepad_to_key.insert(Button::DPadUp, String::from("gamepad up"));
        gamepad_to_key.insert(Button::DPadDown, String::from("gamepad down"));
        gamepad_to_key.insert(Button::Guide, String::from("gamepad guide"));

        let mut gamepad_to_axis_pos = HashMap::new();
        let mut gamepad_to_axis_neg = HashMap::new();

        gamepad_to_axis_neg.insert(Axis::LeftX, String::from("gamepad lstick left"));
        gamepad_to_axis_pos.insert(Axis::LeftX, String::from("gamepad lstick right"));
        gamepad_to_axis_neg.insert(Axis::LeftY, String::from("gamepad lstick up"));
        gamepad_to_axis_pos.insert(Axis::LeftY, String::from("gamepad lstick down"));
        gamepad_to_axis_neg.insert(Axis::RightX, String::from("gamepad rstick left"));
        gamepad_to_axis_pos.insert(Axis::RightX, String::from("gamepad rstick right"));
        gamepad_to_axis_neg.insert(Axis::RightY, String::from("gamepad rstick up"));
        gamepad_to_axis_pos.insert(Axis::RightY, String::from("gamepad rstick down"));

        // L and R triggers only go from 0.0 to 1.0, so they have no negative axis.
        gamepad_to_axis_pos.insert(Axis::TriggerLeft, String::from("gamepad ltrigger"));
        gamepad_to_axis_pos.insert(Axis::TriggerRight, String::from("gamepad rtrigger"));

        let mut composite_axes = HashMap::new();

        composite_axes.insert(String::from("gamepad lstick"), CompositeAxisData::AxisStick
        {
            neg_x : String::from("gamepad lstick left"),
            pos_x : String::from("gamepad lstick right"),
            neg_y : String::from("gamepad lstick up"),
            pos_y : String::from("gamepad lstick down")
        });
        composite_axes.insert(String::from("gamepad rstick"), CompositeAxisData::AxisStick
        {
            neg_x : String::from("gamepad rstick left"),
            pos_x : String::from("gamepad rstick right"),
            neg_y : String::from("gamepad rstick up"),
            pos_y : String::from("gamepad rstick down")
        });
        composite_axes.insert(String::from("gamepad ltrigger"), CompositeAxisData::AxisTrigger);
        composite_axes.insert(String::from("gamepad rtrigger"), CompositeAxisData::AxisTrigger);

        let mut gamepad_deadzones = HashMap::new();

        gamepad_deadzones.insert(String::from("gamepad lstick"), DEFAULT_STICK_DEADZONE);
        gamepad_deadzones.insert(String::from("gamepad rstick"), DEFAULT_STICK_DEADZONE);
        gamepad_deadzones.insert(String::from("gamepad ltrigger"), DEFAULT_TRIGGER_DEADZONE);
        gamepad_deadzones.insert(String::from("gamepad rtrigger"), DEFAULT_TRIGGER_DEADZONE);

        Input
        {
            keys_held : HashSet::new(),
            keys_frame_pressed : HashSet::new(),
            keys_same_frame_released : HashSet::new(),
            axis_values: HashMap::new(),
            actions_to_keys : HashMap::new(),
            action_map : HashMap::new(),
            mouse_to_key,
            gamepad_to_key,
            gamepad_to_axis_pos,
            gamepad_to_axis_neg,
            composite_axes,
            gamepad_deadzones,
            cursor_pos : (0.0, 0.0),
            cursor_pos_raw : (0.0, 0.0),
            cursor_motion : (0.0, 0.0),
            mouse_offset : (0.0, 0.0),
            mouse_scale : (0.0, 0.0),
            mouse_util
        }
    }

    /// Creates a new input action with the given name.
    pub fn add_action(&mut self, name : &str)
    {
        self.action_map.insert(name.to_string(), InputAction::new());
        self.actions_to_keys.insert(name.to_string(), Vec::new());
    }

    /// Adds an input binding to the given action. If the action doesn't exist, it will be created.
    ///  See the [module-level documentation](crate::input) for a list of key names.
    pub fn add_bind(&mut self, name : &str, key : &str)
    {
        if !self.action_map.contains_key(name)
        {
            self.add_action(name);
        }

        let lower_key = key.to_lowercase();

        if let Some(key_list) = self.actions_to_keys.get_mut(name)
        {
            key_list.push(lower_key);
        }
    }

    /// Removes all the bindings from the given action.
    pub fn clear_binds(&mut self, name : &str)
    {
        if let Some(key_list) = self.actions_to_keys.get_mut(name)
        {
            key_list.clear();
        }

        if let Some(action) = self.action_map.get_mut(name)
        {
            action.axis = 0.0;
        }
    }

    /// Gets the deadzone for the given composite axis.
    ///  See the [module-level documentation](crate::input) for a list of composite axis names.
    pub fn deadzone(&self, composite_axis : &str) -> Option<f64>
    {
        self.gamepad_deadzones.get(composite_axis).cloned()
    }

    /// Sets the deadzone for the given composite axis.
    ///  See the [module-level documentation](crate::input) for a list of composite axis names.
    pub fn set_deadzone(&mut self, composite_axis : &str, deadzone : f64) -> bool
    {
        let clamped = deadzone.clamp(0.0, AXIS_DEADZONE_MAX);

        if let Some(deadzone) = self.gamepad_deadzones.get_mut(composite_axis)
        {
            *deadzone = clamped;
            return true;
        }

        false
    }

    /// Returns true if the given action was pressed this frame
    pub fn down_once(&self, name : &str) -> bool
    {
        if let Some(action) = self.action_map.get(name)
        {
            return action.down_once();
        }

        false
    }

    /// Returns true if the given action was released this frame
    pub fn up_once(&self, name : &str) -> bool
    {
        if let Some(action) = self.action_map.get(name)
        {
            return action.up_once();
        }

        false
    }

    /// Returns true if the given action is currently being held
    pub fn held(&self, name : &str) -> bool
    {
        if let Some(action) = self.action_map.get(name)
        {
            return action.is_held();
        }

        false
    }

    /// Returns the number of frames that the given action was held for, or 0 if it is not held.
    pub fn time_held(&self, name : &str) -> u64
    {
        if let Some(action) = self.action_map.get(name)
        {
            if action.is_held()
            {
                return action.time_in_state();
            }
        }
        
        0
    }

    /// Returns the number of frames that the given action was released for, or 0 if it is not released.
    pub fn time_released(&self, name : &str) -> u64
    {
        if let Some(action) = self.action_map.get(name)
        {
            if !action.is_held()
            {
                return action.time_in_state();
            }
        }
        
        0
    }

    /// Returns the current value between 0.0 and 1.0 for the axis associated with the given action.
    ///  This will only return the value of an individual positive/negative component.
    ///  If you want a combined positive/negative range, see [Input::paired_axis()].
    pub fn axis(&self, name : &str) -> f64
    {
        if let Some(action) = self.action_map.get(name)
        {
            return action.axis().clamp(0.0, 1.0);
        }

        0.0
    }

    /// Returns the current value between -1.0 and 1.0 for the negative and positive part associated
    ///  with the negative and positive actions, respectively.
    pub fn paired_axis(&self, name_neg : &str, name_pos : &str) -> f64
    {
        let neg_value = self.axis(name_neg);
        let pos_value = self.axis(name_pos);

        pos_value - neg_value
    }

    /// Returns the current values between -1.0 and 1.0 for the 2D axis given four actions
    ///  representing each cardinal direction. The distance is capped at a Euclidean distance value
    ///  of 1.0 to avoid overly large diagonal values for sticks with square gates.
    pub fn paired_axis_2d(&self, name_left : &str, name_right : &str, name_up : &str, name_down : &str) -> (f64, f64)
    {
        let mut x = self.paired_axis(name_left, name_right);
        let mut y = self.paired_axis(name_up, name_down);
        let distance = (x.powi(2) + y.powi(2)).sqrt();

        if distance > 1.0
        {
            x *= 1.0 / distance;
            y *= 1.0 / distance;
        }

        (x, y)
    }

    /// Returns the current angle and distance for the 2D axis given four actions representing
    ///  each cardinal direction. The distance is capped at a Euclidean distance value of 1.0 to
    ///  avoid overly large diagonal values for sticks with square gates.
    pub fn angle_and_distance(&self, name_left : &str, name_right : &str, name_up : &str, name_down : &str) -> (f64, f64)
    {
        let x = self.paired_axis(name_left, name_right);
        let y = self.paired_axis(name_up, name_down);

        let angle = y.atan2(x);
        let distance = (x.powi(2) + y.powi(2)).sqrt().min(1.0);

        (angle, distance)
    }

    /// Returns the currently pressed four-way direction given four actions representing
    ///  each cardinal direction. If no direction is held, or the 2D axis is too close to a
    ///  diagonal, returns None.
    pub fn four_way_direction(&self, name_left : &str, name_right : &str, name_up : &str, name_down : &str) -> Option<InputDirection4>
    {
        let (angle, distance) = self.angle_and_distance(name_left, name_right, name_up, name_down);

        if distance < AXIS_TO_KEY_DEADZONE
        {
            return None;
        }

        crate::util::angle_to_four_way_direction_deadzone(angle, DIAGONAL_DEAD_ZONE)
    }

    /// Returns the currently pressed eight-way direction given four actions representing
    ///  each cardinal direction. If no direction is held, returns None.
    pub fn eight_way_direction(&self, name_left : &str, name_right : &str, name_up : &str, name_down : &str) -> Option<InputDirection8>
    {
        let (angle, distance) = self.angle_and_distance(name_left, name_right, name_up, name_down);

        if distance < AXIS_TO_KEY_DEADZONE
        {
            return None;
        }

        crate::util::angle_to_eight_way_direction(angle)
    }

    /// Returns the current mouse cursor position in game screen coordinates. These are not raw
    ///  window coordinates. For example, if you have a game at 320x224 and the window is scaled to
    ///  640x448, then placing your cursor in the bottom right of the window's client area will
    ///  return a position close to (320, 224).
    pub fn cursor_position(&self) -> (f64, f64)
    {
        self.cursor_pos
    }

    /// Returns the amount that the mouse moved this logic frame. This is *not* guaranteed to match
    ///  1-to-1 with the values returned from [Input::cursor_position()]. With the cursor unlocked,
    ///  the motion may not match with the reported position if the window is a different size from
    ///  the game's base size. With the cursor locked, the motion is taken from raw mouse input if
    ///  available, so it most likely will not match with the reported position even if the values
    ///  are scaled up or down.
    pub fn cursor_motion(&self) -> (f64, f64)
    {
        self.cursor_motion
    }

    /// Returns true if the mouse cursor is displayed when it is over the game window.
    pub fn cursor_visible(&self) -> bool
    {
        if let Some(mouse_util) = &self.mouse_util
        {
            return mouse_util.is_cursor_showing();
        }

        true
    }

    /// Sets whether the mouse cursor is displayed when it is over the game window.
    pub fn set_cursor_visible(&mut self, visible : bool)
    {
        if let Some(mouse_util) = &self.mouse_util
        {
            mouse_util.show_cursor(visible);
        }
    }

    /// Returns true if the mouse cursor is currently locked.
    pub fn cursor_locked(&self) -> bool
    {
        if let Some(mouse_util) = &self.mouse_util
        {
            return mouse_util.relative_mouse_mode();
        }

        false
    }

    /// Sets whether the mouse cursor is locked. When locked, the cursor will be trapped inside the
    ///  window, and [Input::cursor_motion()] will report raw mouse motion if available.
    pub fn set_cursor_locked(&mut self, locked : bool)
    {
        if let Some(mouse_util) = &self.mouse_util
        {
            mouse_util.set_relative_mouse_mode(locked);
        }
    }
    
    pub(crate) fn update_key(&mut self, key : &str, pressed : bool, update_axis : bool)
    {
        let lower_key = key.to_lowercase();

        if pressed
        {
            self.keys_held.insert(lower_key.clone());
            self.keys_frame_pressed.insert(lower_key.clone());

            if update_axis
            {
                self.axis_values.insert(lower_key, AxisData { value: 1.0, raw : 1.0 });
            }
        }
        else
        {
            // If pressed and released on same frame, treat as pressed this frame and then
            //  released next frame.
            if self.keys_frame_pressed.contains(&lower_key)
            {
                self.keys_same_frame_released.insert(lower_key.clone());
            }
            else
            {
                self.keys_held.remove(&lower_key);
            }

            if update_axis
            {
                self.axis_values.insert(lower_key, AxisData { value: 0.0, raw : 0.0 });
            }
        }
    }

    pub(crate) fn update_same_frame_key(&mut self, key : &str)
    {
        self.update_key(key, true, true);
        self.update_key(key, false, true);
    }

    pub(crate) fn update_axis(&mut self, axis : &str, value : f64)
    {
        let lower_axis = axis.to_lowercase();

        self.axis_values.insert(lower_axis, AxisData { value, raw : value } );
    }

    pub(crate) fn update_mouse_movement(&mut self, x : i32, y : i32, x_rel : i32, y_rel : i32)
    {
        self.cursor_pos_raw = (x as f64, y as f64);
        self.cursor_motion = (x_rel as f64, y_rel as f64);

        self.scale_mouse_position();
    }

    fn scale_mouse_position(&mut self)
    {
        let (x, y) = self.cursor_pos_raw;
        let (offset_x, offset_y) = self.mouse_offset;
        let (scale_x, scale_y) = self.mouse_scale;

        self.cursor_pos = ((x + offset_x) * scale_x, (y + offset_y) * scale_y);
    }

    pub(crate) fn update_mouse_metrics(&mut self, renderer : &Renderer)
    {
        let (base_w, base_h) = renderer.base_size();
        let rect = renderer.window_scissor();

        self.mouse_offset = (-(rect.x as f64), -(rect.y as f64));
        self.mouse_scale = (base_w as f64 / rect.w as f64, base_h as f64 / rect.h as f64);

        self.scale_mouse_position();
    }

    pub(crate) fn update_mouse_button(&mut self, button : MouseButton, pressed : bool)
    {
        let mouse_key_string = self.mouse_to_key[&button].clone();

        self.update_key(&mouse_key_string, pressed, true)
    }

    pub(crate) fn update_mouse_wheel(&mut self, x : i32, y : i32)
    {
        if x < 0 { self.update_same_frame_key("mouse wheel left"); }
        else if x > 0 { self.update_same_frame_key("mouse wheel right"); }

        if y < 0 { self.update_same_frame_key("mouse wheel down"); }
        else if y > 0 { self.update_same_frame_key("mouse wheel up"); }
    }

    pub(crate) fn update_gamepad_key(&mut self, button : Button, pressed : bool)
    {
        let gamepad_key_string = self.gamepad_to_key[&button].clone();

        self.update_key(&gamepad_key_string, pressed, true)
    }

    pub(crate) fn update_gamepad_axis(&mut self, axis : Axis, value : i16)
    {
        if let Some(gamepad_axis_string_neg) = self.gamepad_to_axis_neg.get(&axis).cloned()
        {
            let split_value = (value as f64 / -32768.0).clamp(0.0, 1.0);

            self.update_axis(&gamepad_axis_string_neg, split_value);
            self.update_key(&gamepad_axis_string_neg, split_value >= AXIS_TO_KEY_DEADZONE, false);
        }
        if let Some(gamepad_axis_string_pos) = self.gamepad_to_axis_pos.get(&axis).cloned()
        {
            let split_value = (value as f64 / 32767.0).clamp(0.0, 1.0);

            self.update_axis(&gamepad_axis_string_pos, split_value);
            self.update_key(&gamepad_axis_string_pos, split_value >= AXIS_TO_KEY_DEADZONE, false);
        }
    }

    fn adjust_axes_for_deadzone(&mut self)
    {
        fn try_get_axis_raw(axis_map : &HashMap<String, AxisData>, axis_name : &str) -> f64
        {
            if let Some(axis) = axis_map.get(axis_name)
            {
                return axis.raw;
            }

            0.0
        }

        for (composite_axis, axes) in self.composite_axes.iter()
        {
            match axes
            {
                CompositeAxisData::AxisTrigger =>
                {
                    if let Some(axis) = self.axis_values.get_mut(composite_axis)
                    {
                        axis.value = apply_deadzone(self.gamepad_deadzones.get(composite_axis), axis.raw);
                    }
                }
                CompositeAxisData::AxisStick { neg_x, pos_x, neg_y, pos_y } =>
                {
                    let x = try_get_axis_raw(&self.axis_values, pos_x) - try_get_axis_raw(&self.axis_values, neg_x);
                    let y = try_get_axis_raw(&self.axis_values, pos_y) - try_get_axis_raw(&self.axis_values, neg_y);

                    let distance = (x.powi(2) + y.powi(2)).sqrt().min(1.0);
                    let adjusted = apply_deadzone(self.gamepad_deadzones.get(composite_axis), distance);

                    for axis_name in &[neg_x, pos_x, neg_y, pos_y]
                    {
                        if let Some(axis) = self.axis_values.get_mut(*axis_name)
                        {
                            axis.value = axis.raw * adjusted;
                        }
                    }
                }
            }
        }
    }

    pub(crate) fn pre_update(&mut self)
    {
        self.cursor_motion = (0.0, 0.0);
    }

    pub(crate) fn update_actions(&mut self)
    {
        self.adjust_axes_for_deadzone();

        for (name, action) in self.action_map.iter_mut()
        {
            let mut is_held = false;
            let mut axis_cumulative = 0.0;

            for key in &self.actions_to_keys[name]
            {
                if self.keys_held.contains(key)
                {
                    is_held = true;
                }

                if let Some(data) = self.axis_values.get_mut(key)
                {
                    axis_cumulative += data.value;
                }
            }

            axis_cumulative = axis_cumulative.clamp(-1.0, 1.0);

            action.update_state(is_held, axis_cumulative);
        }

        // If pressed and released on same frame, treat as pressed this frame and then
        //  released next frame.
        for key in &self.keys_same_frame_released
        {
            self.keys_held.remove(key);
        }

        self.keys_frame_pressed.clear();
        self.keys_same_frame_released.clear();
    }
}
