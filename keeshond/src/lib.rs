//! # 🐶 KEESHOND Game Engine 🐶
//! 
//! Keeshond is a 2D game engine with a focus on quickly bringing ideas onscreen.
//! 
//! 
//! # Goals
//! 
//! - Runs reasonably well on hardware from 2010
//! - Deterministic game logic across systems, important for speedrunners
//! - Implements features games need, without adding unneeded complexity
//! - First-class Linux support via SDL2
//! 
//! # How to use this documentation
//!
//! The document you see here serves as an API reference. It is not intended as a
//! starting point for new users. If you're looking to get started, it is suggested
//! that you look at other material instead, such as the included examples.
//!
//! # Points of interest
//!
//! You might be interested in API documentation for the following:
//!
//! - [gameloop::GameControl] - For accessing various functionality about the game while it's running
//! - [input::Input] - For player input bindings and detecting key/button presses and axes.
//! - [scene::Component], [scene::ThinkerSystem], and [scene::DrawerSystem] - For defining components and systems
//! - [scene::SceneType] - For defining information about a scene such as spawnable objects
//! - [scene::SceneControl] - For adding and removing [Entities](scene::Entity)
//!     and [Components](scene::Component) while the scene is running
//! - [scene::ComponentControl] - For accessing [Components](scene::Component) while the scene is running
//! - [renderer::Renderer] - For functionality regarding the window and global rendering state
//! - [renderer::DrawControl] - For basic rendering functionality including sprite drawing (this might not be relevant if you're using keeshond_treats)
//! - [audio::Audio] - For playing audio
//!
//! And for Keeshond Datapack:
//!
//! - [datapack::DataHandle] - For keeping track of data resources in use
//! - [datapack::DataMultistore] and [datapack::DataStore] - For loading and retrieving data resources
//! - [datapack::source::Source] and [datapack::source::SourceManager] - For defining where resources are loaded from
//!
//! See the documentation for keeshond_treats for the higher level APIs included in that package.

#[cfg(test)] extern crate rand;

#[cfg(feature = "graphical_panic")] extern crate backtrace;

#[cfg(feature = "imgui_feature")] extern crate imgui;
#[cfg(feature = "imgui_feature")] extern crate imgui_sys;
#[cfg(feature = "imgui_feature")] extern crate imgui_glium_renderer;
#[cfg(feature = "imgui_feature")] #[macro_use] extern crate cstr_macro;

extern crate failure;
#[macro_use] extern crate failure_derive;
#[macro_use] extern crate log;
#[macro_use] extern crate downcast_rs;
extern crate getopts;
extern crate sdl2;
extern crate sdl2_sys;
extern crate serde;
extern crate serde_json;
extern crate rustc_hash;
extern crate smallvec;
extern crate bit_set;
extern crate bitarray_set;
extern crate typenum;
extern crate generic_array;
extern crate bitflags;
extern crate cgmath;
extern crate ouroboros;
#[macro_use] extern crate glium;
#[cfg(windows)] extern crate winapi;

mod thirdparty;
#[cfg(test)] mod tests;

pub extern crate keeshond_datapack as datapack;
pub mod crate_reexport;
pub mod doglog;
pub mod util;
#[cfg(feature = "imgui_feature")] mod imgui_handler;
#[cfg(feature = "audio")] pub extern crate keeshond_audio as audio;
pub mod input;
pub mod scene;
pub mod renderer;
pub mod gameloop;
