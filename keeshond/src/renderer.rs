//! Rendering functionality

mod opengl;

use cgmath::{Matrix3, SquareMatrix};

use std::io::Read;
use std::rc::Rc;
use std::cell::RefCell;

use serde::{Deserialize, Serialize};

#[cfg(feature = "imgui_feature")] use imgui;

use crate::datapack::{DataError, DataId, DataMultistore, DataObject, DataStore, PreparedStoreError, TrustPolicy};
use crate::datapack::source::Source;
use crate::renderer::opengl::GlDrawControl;
use crate::scene::BaseScene;
use crate::util::{NamedItemStore, Rect};

#[cfg(not(windows))] use std::io::Cursor;
#[cfg(not(windows))] use sdl2::surface::Surface;
#[cfg(not(windows))] use sdl2::pixels::PixelFormatEnum;

#[cfg(not(target_os = "macos"))] const BASE_DPI : f32 = 96.0;
#[cfg(target_os = "macos")] const BASE_DPI : f32 = 72.0;

const MAX_MULTIPLIER_ADJUST : f32 = 0.9;

#[cfg(target_os = "emscripten")] const MAX_RENDERER_SCALE_WIDTH : f32 = 1920.0;
#[cfg(target_os = "emscripten")] const MAX_RENDERER_SCALE_HEIGHT : f32 = 1080.0;

pub(crate) fn png_to_raw<R : Read>(png_reader : R) -> Result<(Vec<u8>, u32, u32), RendererError>
{
    let decoder = png::Decoder::new(png_reader);
    let (info, mut reader) = decoder.read_info().map_err(
        |error| RendererError::LoadResourceFailed(format!("Failed to read PNG: {}", error)))?;
    let mut image_raw = vec![0; info.buffer_size()];
    reader.next_frame(&mut image_raw).map_err(
        |error| RendererError::LoadResourceFailed(format!("Failed to decode PNG: {}", error)))?;

    Ok((image_raw, info.width, info.height))
}

/// Determines how the game is scaled to fit the window or screen
#[derive(Copy, Clone, Debug, PartialEq)]
pub enum ViewportMode
{
    /// The view is scaled up or down to fit the window in a resolution-independent way
    Independent,
    /// The view is rendered at a fixed size and then scaled up or down with minimal filtering.
    ///  Use for pixel art style games.
    Pixel,
    /// The viewport base size is the same as the window size, taking display DPI into account.
    ///  Enlarging the window means you see more of the scene. Use for editors and similar utilities.
    OneToOneDpiScaled,
    /// The viewport base size is the same as the window size. Enlarging the window means you
    ///  see more of the scene. You probably want to use OneToOneDpiScaled instead, unless you are
    ///  doing your own DPI scaling using current_display_scale_factor().
    OneToOneUnscaled
}

/// The strategy used to intelligently find the window size based on the game's base resolution
#[derive(Copy, Clone, Debug, PartialEq)]
pub enum WindowScaleMode
{
    /// The window size is a multiple of the base size, taking the current display DPI into account.
    ///  If the resulting size would be too big to fit on-screen, uses a smaller size that still fills
    ///  most of the area.
    MultiplierDpiScaled(f32),
    /// The window size is a percentage of the available screen space. Values are between 0.1 and 0.9.
    ///  Not available on web.
    #[cfg(not(target_os = "emscripten"))]
    ScreenPercentage(f32),
    /// The window size is an integer multiple of the base size, taking the current display DPI into account
    ///  like MultiplierDpiScaled, but rounding the final size to the closest integer. If the resulting
    ///  size would be too big to fit on-screen, uses the largest integer scale that fits.
    PixelMultiplierDpiScaled(u32),
    /// The window size is the largest integer multiple of the base size that fits on-screen.
    ///  Not available on web.
    #[cfg(not(target_os = "emscripten"))]
    PixelHighestMultiplier,
    /// The window size is a multiple of the base size, ignoring current display DPI. Not recommended
    ///  as this can make the window too small for very high DPIs.
    MultiplierDpiIgnored(f32),
    /// The window size is an integer multiple of the base size, ignoring current display DPI.
    ///  This can be useful for getting screenshots or footage at a specific size, but otherwise is
    ///  not recommended.
    PixelMultiplierDpiIgnored(u32),
}

/// Determines the vertical synchronization mode to use
#[derive(Copy, Clone, Debug, PartialEq)]
pub enum VsyncMode
{
    /// Rendering is not synced to vertical blank, causing tearing. Not recommended.
    Off,
    /// Rendering is synced to vertical blank
    On,
    /// Rendering is synced to vertical blank unless the framerate drops below the display's refresh rate
    Adaptive
}

/// A transformation matrix used for drawing operations
#[derive(Clone, Debug)]
pub struct DrawTransform
{
    mat : Matrix3<f32>
}

impl DrawTransform
{
    pub fn identity() -> DrawTransform
    {
        DrawTransform
        {
            mat : Matrix3::identity()
        }
    }
    
    pub fn from_array(array : &[f32; 9]) -> DrawTransform
    {
        DrawTransform
        {
            mat : Matrix3::new(array[0], array[1], array[2],
                               array[3], array[4], array[5],
                               array[6], array[7], array[8])
        }
    }
    
    #[inline(always)]
    pub fn translate(&mut self, x : f32, y : f32)
    {
        self.mat = self.mat * Matrix3::new(1.0, 0.0, 0.0, 0.0, 1.0, 0.0, x, y, 1.0);
    }
    
    #[inline(always)]
    pub fn rotate(&mut self, angle : f32)
    {
        let (angle_sin, angle_cos) = angle.sin_cos();
        
        self.mat = self.mat * Matrix3::new(angle_cos, angle_sin, 0.0, -angle_sin, angle_cos, 0.0, 0.0, 0.0, 1.0);
    }
    
    #[inline(always)]
    pub fn scale(&mut self, scale_x : f32, scale_y : f32)
    {
        self.mat = self.mat * Matrix3::new(scale_x, 0.0, 0.0, 0.0, scale_y, 0.0, 0.0, 0.0, 1.0);
    }
    
    pub fn shear(&mut self, x : f32, y : f32)
    {
        self.mat = self.mat * Matrix3::new(1.0, y, 0.0, x, 1.0, 0.0, 0.0, 0.0, 1.0);
    }
}

pub struct WindowViewportInfo
{
    pub scale_offset_x : f32,
    pub scale_offset_y : f32,
    pub scale_width : f32,
    pub scale_height : f32,
    pub base_multiplier_width : f32,
    pub base_multiplier_height : f32,
}

pub(crate) struct ViewportInfo
{
    pub scale_offset_x : f32,
    pub scale_offset_y : f32,
    pub scale_width : f32,
    pub scale_height : f32,
    pub pixel_scale : u32,
    pub scissor : Option<Rect<u32>>
}

impl ViewportInfo
{
    fn new() -> ViewportInfo
    {
        ViewportInfo
        {
            scale_offset_x : 0.0,
            scale_offset_y : 0.0,
            scale_width: 1.0,
            scale_height: 1.0,
            pixel_scale : 1,
            scissor : None
        }
    }
}

fn calculate_display_scale_factor(video_subsystem : &sdl2::VideoSubsystem, display_index : i32) -> f32
{
    if let Ok((ddpi, _, _)) = video_subsystem.display_dpi(display_index)
    {
        return ddpi / BASE_DPI;
    }

    1.0
}

pub(crate) fn calculate_window_size(base_width : f32, base_height : f32, mode : WindowScaleMode, video_subsystem : &sdl2::VideoSubsystem) -> (u32, u32)
{
    let display_scale_factor = calculate_display_scale_factor(video_subsystem, 0);
    let screen_width;
    let screen_height;

    #[cfg(not(target_os = "emscripten"))]
    {
        if let Ok(screen_rect) = video_subsystem.display_usable_bounds(0)
        {
            screen_width = screen_rect.width() as f32;
            screen_height = screen_rect.height() as f32;
        }
        else
        {
            return (base_width as u32, base_height as u32);
        }
    }
    #[cfg(target_os = "emscripten")]
    {
        screen_width = MAX_RENDERER_SCALE_WIDTH;
        screen_height = MAX_RENDERER_SCALE_HEIGHT;
    }

    let base_width_dpi = base_width * display_scale_factor;
    let base_height_dpi = base_height * display_scale_factor;
    let max_multiplier = (screen_width / base_width).min(screen_height / base_height);
    let max_multiplier_dpi = (screen_width / base_width_dpi).min(screen_height / base_height_dpi);

    match mode
    {
        WindowScaleMode::MultiplierDpiScaled(multiplier) =>
        {
            let clamped_multiplier = multiplier.clamp(0.1, max_multiplier_dpi * MAX_MULTIPLIER_ADJUST);

            ((base_width_dpi * clamped_multiplier) as u32,
                (base_height_dpi * clamped_multiplier) as u32)
        }
        #[cfg(not(target_os = "emscripten"))]
        WindowScaleMode::ScreenPercentage(percentage) =>
        {
            ((base_width_dpi * (max_multiplier_dpi * percentage.clamp(0.1, MAX_MULTIPLIER_ADJUST))) as u32,
                (base_height_dpi * (max_multiplier_dpi * percentage.clamp(0.1, MAX_MULTIPLIER_ADJUST))) as u32)
        }
        WindowScaleMode::PixelMultiplierDpiScaled(multiplier) =>
        {
            let clamped_multiplier = (multiplier as f32 * display_scale_factor).round()
                .min((max_multiplier * MAX_MULTIPLIER_ADJUST).floor()).max(1.0);

            ((base_width * (clamped_multiplier)) as u32,
                (base_height * (clamped_multiplier)) as u32)
        }
        #[cfg(not(target_os = "emscripten"))]
        WindowScaleMode::PixelHighestMultiplier =>
        {
            let pixel_highest = (max_multiplier * MAX_MULTIPLIER_ADJUST).floor().max(1.0);

            ((base_width * pixel_highest) as u32,
                (base_height * pixel_highest) as u32)
        }
        WindowScaleMode::MultiplierDpiIgnored(multiplier) =>
        {
            let clamped_multiplier = multiplier.clamp(0.1, max_multiplier * MAX_MULTIPLIER_ADJUST);

            ((base_width * clamped_multiplier) as u32,
                (base_height * clamped_multiplier) as u32)
        }
        WindowScaleMode::PixelMultiplierDpiIgnored(multiplier) =>
        {
            let clamped_multiplier = multiplier.min((max_multiplier * MAX_MULTIPLIER_ADJUST).floor() as u32).max(1);

            ((base_width * (clamped_multiplier as f32)) as u32,
                (base_height * (clamped_multiplier as f32)) as u32)
        }
    }
}

pub(crate) fn recalculate_viewport_common(base_width : f32, base_height : f32, width : u32, height : u32, mode : ViewportMode) -> ViewportInfo
{
    let mut info = ViewportInfo::new();

    let base_ratio = base_width / base_height;
    let ratio = width as f32 / height as f32;

    match mode
    {
        ViewportMode::Independent | ViewportMode::OneToOneDpiScaled | ViewportMode::OneToOneUnscaled =>
        {
            info.scale_width = base_width;
            info.scale_height = base_height;

            let mut scissor = Rect::<u32> { x : 0, y : 0, w : width, h : height };
            scissor.w = width;
            scissor.h = height;

            if ratio > base_ratio
            {
                // Wider than base resolution
                info.scale_width = ratio / base_ratio * base_width;
                scissor.w = (height as f32 * base_ratio) as u32;
                scissor.x = ((width) - scissor.w) / 2;
            }
            else if ratio < base_ratio
            {
                // Taller than base resolution
                info.scale_height = base_ratio / ratio * base_height;
                scissor.h = (width as f32 / base_ratio) as u32;
                scissor.y = ((height) - scissor.h) / 2;
            }

            info.scale_offset_x = (info.scale_width - base_width) / 2.0;
            info.scale_offset_y = (info.scale_height - base_height) / 2.0;

            info.scissor = Some(scissor);
        },
        ViewportMode::Pixel =>
        {
            info.scale_width = 1.0;
            info.scale_height = 1.0;

            if ratio > base_ratio
            {
                info.scale_width = ratio / base_ratio;
            }
            else if ratio < base_ratio
            {
                info.scale_height = base_ratio / ratio;
            }

            info.scale_offset_x = (info.scale_width - 1.0) / 2.0;
            info.scale_offset_y = (info.scale_height - 1.0) / 2.0;
            info.pixel_scale = ((width as f32 / base_width / info.scale_width).round() as u32).max(1);
        }
    }

    info
}

#[derive(Debug, Fail)]
pub enum RendererError
{
    #[fail(display = "Renderer init failed: {}", _0)]
    RendererInitFailed(String),
    #[fail(display = "Resource load failed: {}", _0)]
    LoadResourceFailed(String),
    #[cfg(feature = "imgui_feature")]
    #[fail(display = "Imgui renderer failed: {}", _0)]
    ImguiRendererFailed(String)
}

/// Information on how the renderer should handle draw order
#[derive(Eq, PartialEq, Copy, Clone, Debug)]
pub enum DrawOrdering
{
    /// Use Painter's Algorithm (draw in order received, first items are drawn in back,
    ///  last items are drawn in front)
    Painter,
    /// Use either Z-buffering or depth sorting based on the source texture and the alpha used in
    ///  the draw command.
    OrderedAutomatic,
    /// Use Z-buffering to draw opaque items independent of order received. This will not work
    ///  correctly for semitransparency.
    ZBuffer,
    /// Use depth sorting to draw semi-transparent items independent of order received. Unlike
    ///  Z-buffering, this will work correctly for semi-transparency, but performance will be slower.
    ///  Since depth-sorted items are stored for later drawing, keep in mind that additional memory
    ///  is required for this option.
    DepthSort,
    /// Use Painter's Algorithm, but write to the Z-buffer. Unlike [ZBuffer], the item itself is
    ///  unaffected by the Z-buffer's existing contents.
    PainterZWrite,
}

impl Default for DrawOrdering
{
    fn default() -> Self
    {
        DrawOrdering::Painter
    }
}

#[derive(PartialEq, Clone, Debug)]
pub struct DrawOptions
{
    pub ordering : DrawOrdering
}

/// Information on how to draw a background color
#[derive(PartialEq, Clone, Debug)]
pub struct DrawBackgroundColorInfo
{
    pub r : f32,
    pub g : f32,
    pub b : f32,
    pub alpha : f32,
    pub depth : f32,
    pub shader_id : DataId<Shader>
}

impl Default for DrawBackgroundColorInfo
{
    fn default() -> DrawBackgroundColorInfo
    {
        DrawBackgroundColorInfo
        {
            r : 0.0,
            g : 0.0,
            b : 0.0,
            alpha : 1.0,
            depth : 0.0,
            shader_id : DataId::new(0)
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct SpriteSlice
{
    #[serde(rename = "tx")]
    #[serde(alias = "texture_x")]
    pub texture_x : f32,
    #[serde(rename = "ty")]
    #[serde(alias = "texture_y")]
    pub texture_y : f32,
    #[serde(rename = "tw")]
    #[serde(alias = "texture_w")]
    pub texture_w : f32,
    #[serde(rename = "th")]
    #[serde(alias = "texture_h")]
    pub texture_h : f32,
    #[serde(rename = "ox")]
    #[serde(alias = "origin_x")]
    pub origin_x : f32,
    #[serde(rename = "oy")]
    #[serde(alias = "origin_y")]
    pub origin_y : f32,
}

pub type SliceId = usize;

/// Information on how to draw a sprite
pub struct DrawSpriteInfo
{
    pub sheet_id : DataId<Sheet>,
    pub shader_id : DataId<Shader>,
    pub x : f32,
    pub y : f32,
    pub angle : f32,
    pub scale_x : f32,
    pub scale_y : f32,
    pub slice : SliceId,
    pub transform : DrawTransform,
    pub r : f32,
    pub g : f32,
    pub b : f32,
    pub alpha : f32,
    pub depth : f32
}

/// Information on how to draw a sprite (raw form with just transform and texture coordinates)
pub struct DrawSpriteRawInfo
{
    pub sheet_id : DataId<Sheet>,
    pub shader_id : DataId<Shader>,
    pub transform : DrawTransform,
    pub transform_offset_x : f32,
    pub transform_offset_y : f32,
    pub texture_x : f32,
    pub texture_y : f32,
    pub texture_w : f32,
    pub texture_h : f32,
    pub r : f32,
    pub g : f32,
    pub b : f32,
    pub alpha : f32,
    pub depth : f32
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub struct Color
{
    pub r : u8,
    pub g : u8,
    pub b : u8,
    pub a : u8
}

impl Color
{
    fn palette_from_png_info(info : &png::Info) -> Option<Vec<Color>>
    {
        if let Some(palette) = &info.palette
        {
            let num_colors = palette.len() / 3;
            let mut new_palette = Vec::with_capacity(num_colors);

            for i in 0..num_colors
            {
                let mut a = 255;

                if let Some(trans) = &info.trns
                {
                    if let Some(alpha) = trans.get(i)
                    {
                        a = *alpha;
                    }
                }

                new_palette.push(Color
                {
                    r : palette[i * 3],
                    g : palette[i * 3 + 1],
                    b : palette[i * 3 + 2],
                    a
                });
            }

            return Some(new_palette);
        }

        None
    }

    fn png_plte_from_palette(palette : &Vec<Color>) -> Vec<u8>
    {
        let mut plte = Vec::with_capacity(palette.len() * 3);

        for color in palette
        {
            plte.push(color.r);
            plte.push(color.g);
            plte.push(color.b);
        }

        plte
    }

    fn png_trns_from_palette(palette : &Vec<Color>) -> Vec<u8>
    {
        let mut trns = Vec::with_capacity(palette.len() * 3);

        for color in palette
        {
            trns.push(color.a);
        }

        trns
    }
}

/// A resource representing a texture sheet for drawing sprites from
pub struct Sheet
{
    width : u32,
    height : u32,
    palette : Option<Vec<Color>>,
    image_raw : Vec<u8>,
    #[allow(dead_code)]
    metadata : SheetMetadata,
    texture_dirty : bool,
    image_file_dirty : bool,
    generation : u64
}

impl Sheet
{
    fn next_generation(&mut self)
    {
        self.generation += 1;

        if self.generation == 0
        {
            warn!("Generation overflow on Sheet!");
            self.generation = 1;
        }
    }

    pub fn new(width : u32, height : u32, palette : Option<Vec<Color>>) -> Sheet
    {
        let length = match palette
        {
            None => { (width * height * 4) as usize }
            Some(_) => { (width * height) as usize }
        };

        Sheet
        {
            width,
            height,
            palette,
            image_raw : vec![0; length],
            metadata : SheetMetadata::new(),
            texture_dirty : true,
            image_file_dirty : false,
            generation : 1
        }
    }

    pub fn width(&self) -> u32 { self.width }
    pub fn height(&self) -> u32 { self.height }

    pub fn generation(&self) -> u64 { self.generation }

    pub fn get_pixel(&self, x : u32, y : u32) -> Option<Color>
    {
        if x < self.width && y < self.height
        {
            let mut i = (x + y * self.width) as usize;

            if let Some(palette) = &self.palette
            {
                return palette.get(self.image_raw[i] as usize).cloned();
            }
            else
            {
                i *= 4;

                if i + 3 < self.image_raw.len()
                {
                    return Some(Color
                    {
                        r: self.image_raw[i],
                        g: self.image_raw[i + 1],
                        b: self.image_raw[i + 2],
                        a: self.image_raw[i + 3]
                    });
                }
            }
        }

        None
    }

    pub fn texture_filtering(&self) -> Option<bool>
    {
        self.metadata.texture_filtering
    }

    pub fn set_texture_filtering(&mut self, enabled : Option<bool>)
    {
        self.metadata.texture_filtering = enabled;

        self.next_generation();
    }

    pub fn slice(&self, id : usize) -> Option<&SpriteSlice>
    {
        self.metadata.slices.get(id)
    }

    pub fn set_slice(&mut self, id : usize, slice : SpriteSlice) -> bool
    {
        let res = self.metadata.slices.set(id, slice);

        if res
        {
            self.next_generation();
        }

        res
    }

    pub fn slice_name(&self, id : usize) -> Option<String>
    {
        self.metadata.slices.get_name(id)
    }

    pub fn slice_id(&self, name : &str) -> Option<usize>
    {
        self.metadata.slices.get_id(name)
    }

    pub fn add_slice(&mut self, slice : SpriteSlice)
    {
        self.insert_slice(self.metadata.slices.len(), slice);
    }

    pub fn insert_slice(&mut self, pos : usize, slice : SpriteSlice) -> bool
    {
        let res = self.metadata.slices.insert_item(pos, slice);

        if res
        {
            self.next_generation();
        }

        res
    }

    pub fn remove_slice(&mut self, id : usize) -> Option<SpriteSlice>
    {
        let res = self.metadata.slices.remove_item(id);

        if res.is_some()
        {
            self.next_generation();
        }

        res
    }

    pub fn rename_slice(&mut self, id : usize, new_name : &str) -> bool
    {
        let res = self.metadata.slices.rename_item(id, new_name);

        if res
        {
            self.next_generation();
        }

        res
    }

    pub fn clear_slices(&mut self)
    {
        self.metadata.slices.clear();
        self.next_generation();
    }

    pub fn slice_count(&self) -> usize
    {
        self.metadata.slices.len()
    }

    pub fn has_palette(&self) -> bool
    {
        self.palette.is_some()
    }

    pub fn palette(&self) -> Option<Vec<Color>>
    {
        self.palette.clone()
    }

    pub fn set_palette(&mut self, palette : Option<Vec<Color>>)
    {
        self.palette = palette;

        self.next_generation();
    }

    pub fn data(&self) -> Vec<u8>
    {
        self.image_raw.clone()
    }

    pub fn set_data(&mut self, data : Vec<u8>) -> bool
    {
        if data.len() == self.image_raw.len()
        {
            self.image_raw = data;
            self.texture_dirty = true;
            self.image_file_dirty = true;

            self.next_generation();

            return true;
        }

        false
    }

    pub fn set_rgba_data_with_size(&mut self, data : Vec<u8>, width : u32, height : u32) -> bool
    {
        let new_length = (width * height * 4) as usize;

        if data.len() == new_length
        {
            self.image_raw = data;
            self.width = width;
            self.height = height;
            self.palette = None;
            self.texture_dirty = true;
            self.image_file_dirty = true;

            self.next_generation();

            return true;
        }

        false
    }

    pub fn set_indexed_data_with_size(&mut self, data : Vec<u8>, width : u32, height : u32, palette : Vec<Color>) -> bool
    {
        let new_length = (width * height) as usize;

        if data.len() == new_length
        {
            self.image_raw = data;
            self.width = width;
            self.height = height;
            self.palette = Some(palette);
            self.texture_dirty = true;
            self.image_file_dirty = true;

            self.next_generation();

            return true;
        }

        false
    }
}

impl Clone for Sheet
{
    fn clone(&self) -> Self
    {
        let mut sheet = Sheet
        {
            width : self.width,
            height : self.height,
            palette : self.palette.clone(),
            image_raw : self.image_raw.clone(),
            metadata : self.metadata.clone(),
            texture_dirty : true,
            image_file_dirty : self.image_file_dirty,
            generation : self.generation
        };

        sheet.next_generation();

        sheet
    }
}

impl DataObject for Sheet
{
    fn folder_name() -> &'static str where Self : Sized
    {
        "sheets"
    }
    fn trust_policy() -> TrustPolicy { TrustPolicy::UntrustedOk }
    fn want_file(pathname : &str) -> bool where Self : Sized
    {
        pathname.ends_with(".png")
    }
    fn from_package_source(source : &mut Box<dyn Source>, package_name : &str, pathname : &str) -> Result<Self, DataError> where Self : Sized
    {
        let mut metadata = SheetMetadata::new();

        let mut new_path = String::from(pathname);
        new_path.push_str(".json");

        if let Ok(mut json_read) = source.read_file(package_name, &new_path)
        {
            let mut json_text = String::new();
            let metadata_result = json_read.read_to_string(&mut json_text);

            match metadata_result
            {
                Err(error) =>
                {
                    return Err(DataError::IoError(error));
                },
                _ => {}
            }

            match serde_json::from_str(&json_text)
            {
                Ok(loaded_metadata) =>
                {
                    metadata = loaded_metadata;
                },
                Err(error) =>
                {
                    return Err(DataError::BadData(format!("{}/{}: {}", package_name, pathname, error)));
                }
            }
        }

        let reader = source.read_file(package_name, pathname)?;
        let mut decoder = png::Decoder::new(reader);

        decoder.set_transformations(png::Transformations::IDENTITY);
        
        match decoder.read_info()
        {
            Ok((info, mut info_reader)) =>
            {
                let mut palette = None;

                match info.color_type
                {
                    png::ColorType::Indexed =>
                    {
                        palette = Color::palette_from_png_info(info_reader.info());
                    },
                    png::ColorType::RGBA =>
                    {

                    },
                    _ =>
                    {
                        return Err(DataError::BadData(String::from("Unsupported PNG color type.")));
                    }
                }

                let mut image_raw = vec![0; info.buffer_size()];
                match info_reader.next_frame(&mut image_raw)
                {
                    Err(error) =>
                    {
                        return Err(DataError::BadData(format!("{}/{}: {}", package_name, pathname, error)));
                    },
                    _ => {}
                }

                return Ok(Sheet
                {
                    width : info.width as u32,
                    height : info.height as u32,
                    palette,
                    image_raw,
                    metadata,
                    texture_dirty : true,
                    image_file_dirty : false,
                    generation : 1,
                });
            },
            Err(png::DecodingError::IoError(io_error)) =>
            {
                return Err(DataError::IoError(io_error));
            },
            Err(error) =>
            {
                return Err(DataError::BadData(format!("{}/{}: {}", package_name, pathname, error)));
            }
        };
    }

    fn write(&mut self, package_name : &str, pathname : &str, source : &mut Box<dyn Source>) -> Result<(), DataError>
    {
        if self.image_file_dirty
        {
            let writer = source.write_file(package_name, pathname).map_err(
                |error| DataError::PackageSourceError(error))?;

            let mut encoder = png::Encoder::new(writer, self.width, self.height);
            encoder.set_depth(png::BitDepth::Eight);
            encoder.set_compression(png::Compression::Best);

            if let Some(palette) = &self.palette
            {
                encoder.set_color(png::ColorType::Indexed);
                encoder.set_palette(Color::png_plte_from_palette(palette));
                encoder.set_trns(Color::png_trns_from_palette(palette));
                encoder.set_filter(png::FilterType::NoFilter);
            }
            else
            {
                encoder.set_color(png::ColorType::RGBA);
                encoder.set_filter(png::FilterType::Paeth);
            }

            match encoder.write_header()
            {
                Ok(mut writer) =>
                {
                    match writer.write_image_data(&self.image_raw)
                    {
                        Ok(_) =>
                        {
                            info!("Wrote sheet PNG to {}/{}", package_name, pathname);
                        },
                        Err(png::EncodingError::IoError(io_error)) =>
                        {
                            return Err(DataError::IoError(io_error));
                        },
                        Err(error) =>
                        {
                            return Err(DataError::BadData(format!("{}/{}: {}", package_name, pathname, error)));
                        }
                    }
                },
                Err(png::EncodingError::IoError(io_error)) =>
                {
                    return Err(DataError::IoError(io_error));
                },
                Err(error) =>
                {
                    return Err(DataError::BadData(format!("{}/{}: {}", package_name, pathname, error)));
                }
            }
        }
        else
        {
            info!("Sheet image unchanged, not writing PNG");
        }

        let mut new_path = String::from(pathname);
        new_path.push_str(".json");

        let mut writer = source.write_file(package_name, &new_path).map_err(
                |error| DataError::PackageSourceError(error))?;

        let json_string = serde_json::to_string(&self.metadata).map_err(
            |error| DataError::BadData(format!("{}/{}: {}", package_name, pathname, error)))?;

        match writer.write_all(json_string.as_bytes())
        {
            Err(error) =>
            {
                return Err(DataError::IoError(error));
            },
            _ => {}
        }

        info!("Wrote sheet metadata to {}", new_path);

        Ok(())
    }

    fn generation(&self) -> u64
    {
        self.generation
    }

    fn set_generation(&mut self, generation : u64)
    {
        self.generation = generation;
    }
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct SheetMetadata
{
    #[serde(rename = "txfl")]
    #[serde(alias = "texture_filtering")]
    texture_filtering : Option<bool>,
    #[serde(rename = "sli")]
    #[serde(alias = "slices")]
    slices : NamedItemStore<SpriteSlice>
}

impl SheetMetadata
{
    fn new() -> SheetMetadata
    {
        SheetMetadata
        {
            texture_filtering: None,
            slices: NamedItemStore::<SpriteSlice>::new()
        }
    }
}

/// A resource representing a fragment shader that can be used when drawing
pub struct Shader
{
    #[allow(dead_code)]
    program_source : String
}

impl DataObject for Shader
{
    fn folder_name() -> &'static str where Self : Sized
    {
        "shaders"
    }
    fn trust_policy() -> TrustPolicy { TrustPolicy::TrustRequired }
    fn want_file(pathname : &str) -> bool where Self : Sized
    {
        pathname.ends_with(".glsl")
    }
    fn from_package_source(source : &mut Box<dyn Source>, package_name : &str, pathname : &str) -> Result<Self, DataError> where Self : Sized
    {
        let mut reader = source.read_file(package_name, pathname)?;
        let mut program_source = String::new();
        match reader.read_to_string(&mut program_source)
        {
            Ok(_) =>
            {
                return Ok(Shader
                {
                    program_source
                });
            },
            Err(io_error) =>
            {
                return Err(DataError::IoError(io_error));
            }
        }
    }
}

pub type DrawControlResult = Result<(Box<dyn DrawControl>, Rc<RefCell<sdl2::video::Window>>), RendererError>;

pub(crate) mod draw_control_private
{
    use crate::datapack::{DataMultistore, DataStore, PreparedStoreError};
    use crate::renderer::{DrawControlResult, Shader, Sheet, ViewportMode, WindowScaleMode};
    #[cfg(feature = "imgui_feature")] use crate::renderer::RendererError;

    #[doc(hidden)]
    pub struct RendererNewInfo<'a>
    {
        pub width : u32,
        pub height : u32,
        pub default_window_scale : WindowScaleMode,
        pub texture_filtering : bool,
        pub fullscreen : bool,
        pub kiosk_mode : bool,
        pub viewport_mode : ViewportMode,
        pub video_subsystem : sdl2::VideoSubsystem,
        pub resources : &'a mut DataMultistore
    }

    #[doc(hidden)]
    pub trait DrawControlBackend
    {
        fn new(renderer_new_info : &mut RendererNewInfo) -> DrawControlResult where Self : Sized;
        fn sync_sheet_store(&mut self, sheet_store : &mut DataStore<Sheet>) -> Result<(), PreparedStoreError>;
        fn sync_shader_store(&mut self, shader_store : &mut DataStore<Shader>) -> Result<(), PreparedStoreError>;
        #[cfg(feature = "imgui_feature")]
        fn draw_imgui(&mut self, ui : imgui::Ui);
        fn recalculate_viewport(&mut self, base_width : f32, base_height : f32);
        fn start_drawing(&mut self);
        fn end_drawing(&mut self);
        fn present(&mut self);
        #[cfg(feature = "imgui_feature")]
        fn init_imgui_renderer(&mut self, imgui : &mut imgui::Context) -> Result<(), RendererError>;
        #[cfg(feature = "imgui_feature")]
        fn update_imgui_renderer(&mut self, imgui : &mut imgui::Context);
    }
}

use draw_control_private::{DrawControlBackend, RendererNewInfo};

/// Public interface to controlling rendering in your game
pub trait DrawControl : DrawControlBackend
{
    /// Gets the base screen transform for use when drawing.
    fn screen_transform(&self) -> &DrawTransform;
    /// Draws a sprite given a position, rotation, scale, and other parameters, plus a base
    ///  transformation.
    fn draw_sprite(&mut self, draw_info : &DrawSpriteInfo, options : &DrawOptions);
    /// Draws a sprite given a transformation matrix, and other parameters.
    fn draw_sprite_raw(&mut self, draw_info : &DrawSpriteRawInfo, options : &DrawOptions);
    /// Draws a simple background color. This can also be used for simple screen fades or shaders.
    fn draw_background_color(&mut self, draw_info : &DrawBackgroundColorInfo, options : &DrawOptions);
    /// Gets the user-sepcified scissor area for cropping drawing operations.
    fn scissor_area(&self) -> &Option<Rect<f32>>;
    /// Sets the user-sepcified scissor area for cropping drawing operations. This function should
    ///  not be called frequently as it may result in poor drawing performance. If you are looking
    ///  to have multiple views on-screen (i.e. for splitscreen), you should iterate your draws
    ///  based on camera to minimize calls to this function. There is no guarantee to the
    ///  correctness of depth sorting for two overlapping scissor regions.
    fn set_scissor_area(&mut self, scissor_area : Option<Rect<f32>>);
    /// Draws any pending depth-sorted sprites. It is a good idea to do this whenever you switch
    ///  between drawing each camera.
    fn flush_depth_sort_queue(&mut self);
    fn depth_sort_queue_max(&self) -> usize;
    fn set_depth_sort_queue_max(&mut self, capacity : usize);
}

struct NullDrawControl
{
    transform : DrawTransform,
    scissor_area : Option<Rect<f32>>,
    queue_max : usize
}

impl NullDrawControl
{
    pub fn new_headless() -> NullDrawControl
    {
        NullDrawControl
        {
            transform : DrawTransform::identity(),
            scissor_area : None,
            queue_max : 0
        }
    }
}

impl DrawControlBackend for NullDrawControl
{
    fn new(renderer_new_info : &mut RendererNewInfo) -> DrawControlResult where Self : Sized
    {
        let video_subsystem = &renderer_new_info.video_subsystem;
        let mut window_builder = video_subsystem.window("", renderer_new_info.width, renderer_new_info.height);
        
        window_builder.hidden().position_centered().resizable().allow_highdpi();
        
        let window_raw = window_builder.build().map_err(
            |error| RendererError::RendererInitFailed(format!("Could not create game window: {}", error)))?;
        let window = Rc::new(RefCell::new(window_raw));
        
        Ok((Box::new(NullDrawControl
        {
            transform : DrawTransform::identity(),
            scissor_area : None,
            queue_max : 0
        }), window))
    }
    
    #[allow(unused_variables)]
    fn sync_sheet_store(&mut self, sheet_store : &mut DataStore<Sheet>) -> Result<(), PreparedStoreError>
    {
        Ok(())
    }
    
    #[allow(unused_variables)]
    fn sync_shader_store(&mut self, shader_store : &mut DataStore<Shader>) -> Result<(), PreparedStoreError>
    {
        Ok(())
    }

    #[cfg(feature = "imgui_feature")]
    #[allow(unused_variables)]
    fn draw_imgui(&mut self, ui : imgui::Ui) {}
    #[allow(unused_variables)]
    fn recalculate_viewport(&mut self, base_width : f32, base_height : f32) {}
    #[allow(unused_variables)]
    fn start_drawing(&mut self) {}
    #[allow(unused_variables)]
    fn end_drawing(&mut self) {}
    #[allow(unused_variables)]
    fn present(&mut self) {}
    #[cfg(feature = "imgui_feature")]
    #[allow(unused_variables)]
    fn init_imgui_renderer(&mut self, imgui : &mut imgui::Context) -> Result<(), RendererError>
    {
        Ok(())
    }
    #[cfg(feature = "imgui_feature")]
    #[allow(unused_variables)]
    fn update_imgui_renderer(&mut self, imgui : &mut imgui::Context) {}
}

impl DrawControl for NullDrawControl
{
    fn screen_transform(&self) -> &DrawTransform
    {
        &self.transform
    }
    fn draw_sprite(&mut self, _draw_info : &DrawSpriteInfo, _options : &DrawOptions) {}
    fn draw_sprite_raw(&mut self, _draw_info : &DrawSpriteRawInfo, _options : &DrawOptions) {}
    fn draw_background_color(&mut self, _draw_info : &DrawBackgroundColorInfo, _options : &DrawOptions) {}
    fn scissor_area(&self) -> &Option<Rect<f32>>
    {
        &self.scissor_area
    }
    fn set_scissor_area(&mut self, scissor_area : Option<Rect<f32>>) { self.scissor_area = scissor_area; }
    fn flush_depth_sort_queue(&mut self) {}
    fn depth_sort_queue_max(&self) -> usize { self.queue_max }
    fn set_depth_sort_queue_max(&mut self, capacity : usize) { self.queue_max = capacity }
}

#[derive(Eq, PartialEq)]
enum OneToOneMode
{
    Off,
    DpiScaled,
    Unscaled
}

/// Represents the renderer's game window
pub struct Renderer
{
    control : Box<dyn DrawControl>,
    window : Option<Rc<RefCell<sdl2::video::Window>>>,
    video_subsystem : Option<sdl2::VideoSubsystem>,
    base_width : f32,
    base_height : f32,
    one_to_one_viewport : OneToOneMode,
    kiosk_mode : bool
}

impl Renderer
{
    /// (Advanced) Creates a new instance. This is already created by the [crate::gameloop::GameControl]
    ///  so you don't need to create this yourself.
    pub(crate) fn new(renderer_new_info : Option<RendererNewInfo>) -> Result<Renderer, RendererError>
    {
        let mut control : Box<dyn DrawControl> = Box::new(NullDrawControl::new_headless());
        let mut window = None;
        let mut video_subsystem = None;
        let mut fullscreen = false;
        let mut kiosk_mode = false;
        let mut one_to_one_viewport = OneToOneMode::Off;
        let mut base_width = 100.0;
        let mut base_height = 100.0;

        if let Some(mut new_info) = renderer_new_info
        {
            #[cfg(windows)]
            unsafe
            {
                winapi::um::winuser::SetProcessDpiAwarenessContext(winapi::shared::windef::DPI_AWARENESS_CONTEXT_PER_MONITOR_AWARE_V2);
            }

            let (out_control, out_window) = GlDrawControl::new(&mut new_info)?;

            #[cfg(not(windows))]
            {
                let (mut icon_raw, icon_width, icon_height) = png_to_raw(Cursor::new(include_bytes!("../res/icon.png").to_vec()))?;
                let surface = Surface::from_data(&mut icon_raw, icon_width, icon_height, icon_width * 4, PixelFormatEnum::ABGR8888).map_err(
                    |error| RendererError::LoadResourceFailed(format!("Failed to create raw surface: {}", error)))?;

                out_window.borrow_mut().set_icon(surface);
            }

            control = out_control;
            window = Some(out_window);
            video_subsystem = Some(new_info.video_subsystem);
            fullscreen = new_info.fullscreen;
            kiosk_mode = new_info.kiosk_mode;
            one_to_one_viewport = match new_info.viewport_mode
            {
                ViewportMode::OneToOneDpiScaled => { OneToOneMode::DpiScaled }
                ViewportMode::OneToOneUnscaled => { OneToOneMode::Unscaled }
                _ => { OneToOneMode::Off }
            };
            base_width = new_info.width as f32;
            base_height = new_info.height as f32;
        }
        
        let mut renderer = Renderer
        {
            control,
            window,
            video_subsystem,
            base_width,
            base_height,
            one_to_one_viewport,
            kiosk_mode
        };

        renderer.recalculate_viewport();
        renderer.set_fullscreen(fullscreen);
        renderer.set_visible(true);

        renderer.control.start_drawing();
        renderer.control.end_drawing();
        renderer.control.present();
        
        Ok(renderer)
    }
    
    pub(crate) fn control(&mut self) -> &mut Box<dyn DrawControl>
    {
        &mut self.control
    }

    pub fn base_size(&self) -> (f32, f32)
    {
        (self.base_width, self.base_height)
    }

    pub fn base_size_f64(&self) -> (f64, f64)
    {
        (self.base_width as f64, self.base_height as f64)
    }

    pub fn set_base_size(&mut self, width : f32, height : f32)
    {
        self.base_width = width;
        self.base_height = height;

        self.recalculate_viewport();
    }
    
    pub fn window_size(&self) -> (u32, u32)
    {
        if let Some(window) = &self.window
        {
            return window.borrow().size();
        }
        
        (0, 0)
    }

    pub fn window_scissor(&self) -> Rect<u32>
    {
        let (window_w, window_h) = self.window_size();

        // Forcing Independent viewport mode because we are getting window viewport, not drawing viewport
        let info = crate::renderer::recalculate_viewport_common(self.base_width, self.base_height,
                                                                window_w, window_h, ViewportMode::Independent);

        info.scissor.unwrap_or_default()
    }
    
    pub(crate) fn draw(&mut self, scene : &mut Box<dyn BaseScene>, resources : &mut DataMultistore, interpolation : f32)
    {
        self.control().sync_sheet_store(&mut resources.store_mut())
        .expect("GPU sheet store sync failed");
        self.control().sync_shader_store(&mut resources.store_mut())
            .expect("GPU shader store sync failed");

        self.control.start_drawing();
        self.control.set_scissor_area(None);
        let first_transform = self.control.screen_transform().clone();
        
        scene.draw(resources, &mut self.control, &first_transform, interpolation);
    }

    #[cfg(feature = "imgui_feature")]
    pub(crate) fn update_imgui(&mut self, imgui : &mut imgui::Context)
    {
        self.control().update_imgui_renderer(imgui);
    }

    #[cfg(feature = "imgui_feature")]
    pub(crate) fn draw_imgui(&mut self, ui : imgui::Ui, resources : &mut DataMultistore)
    {
        self.control().sync_sheet_store(&mut resources.store_mut())
            .expect("GPU sheet store sync failed");
        self.control().sync_shader_store(&mut resources.store_mut())
            .expect("GPU shader store sync failed");

        self.control().draw_imgui(ui);
    }
    
    pub(crate) fn present(&mut self)
    {
        self.control.present();
    }
    
    pub(crate) fn recalculate_viewport(&mut self)
    {
        match self.one_to_one_viewport
        {
            OneToOneMode::Off => {}
            OneToOneMode::DpiScaled | OneToOneMode::Unscaled =>
            {
                let (width, height) = self.window_size();
                let mut scale = 1.0;

                if self.one_to_one_viewport == OneToOneMode::DpiScaled
                {
                    scale = self.current_display_scale_factor();
                }

                self.base_width = width as f32 / scale;
                self.base_height = height as f32 / scale;
            }
        }

        self.control.recalculate_viewport(self.base_width, self.base_height);
    }
    
    pub fn window_title(&self) -> String
    {
        if let Some(window) = &self.window
        {
            let window_borrow = window.borrow();
            
            return window_borrow.title().to_string();
        }
        
        String::new()
    }
    
    pub fn set_window_title(&mut self, window_title : &str)
    {
        if let Some(window) = &mut self.window
        {
            let mut window_borrow = window.borrow_mut();
            
            if let Err(error) = window_borrow.set_title(window_title)
            {
                warn!("Window title error: {}", error);
            }
        }
    }

    pub fn set_visible(&mut self, visible : bool)
    {
        if let Some(window) = &mut self.window
        {
            if visible
            {
                window.borrow_mut().show();
            }
            else
            {
                window.borrow_mut().hide();
            }
        }
    }
    
    pub fn fullscreen(&self) -> bool
    {
        if let Some(window) = &self.window
        {
            return window.borrow().fullscreen_state() == sdl2::video::FullscreenType::Off
        }
        
        false
    }
    
    pub fn set_fullscreen(&mut self, enabled : bool)
    {
        if let Some(window) = &mut self.window
        {
            let mut window_borrow = window.borrow_mut();
            let new_mode;
            
            if enabled || self.kiosk_mode
            {
                new_mode = sdl2::video::FullscreenType::Desktop;
            }
            else
            {
                new_mode = sdl2::video::FullscreenType::Off;
            }

            if let Err(error) = window_borrow.set_fullscreen(new_mode)
            {
                warn!("Could not set fullscreen mode: {}", error);
            }
        }
    }
    
    pub fn toggle_fullscreen(&mut self)
    {
        if let Some(window) = &mut self.window
        {
            let mut window_borrow = window.borrow_mut();
            let new_mode;
            
            if window_borrow.fullscreen_state() == sdl2::video::FullscreenType::Off || self.kiosk_mode
            {
                new_mode = sdl2::video::FullscreenType::Desktop;
            }
            else
            {
                new_mode = sdl2::video::FullscreenType::Off;
            }

            if let Err(error) = window_borrow.set_fullscreen(new_mode)
            {
                warn!("Could not set fullscreen mode: {}", error);
            }
        }
    }

    pub fn vsync(&self) -> VsyncMode
    {
        if let Some(video_subsystem) = &self.video_subsystem
        {
            match video_subsystem.gl_get_swap_interval()
            {
                sdl2::video::SwapInterval::Immediate => { return VsyncMode::Off }
                sdl2::video::SwapInterval::VSync => { return VsyncMode::On }
                sdl2::video::SwapInterval::LateSwapTearing => { return VsyncMode::Adaptive }
            }
        }

        VsyncMode::On
    }

    pub fn set_vsync(&mut self, mode : VsyncMode) -> bool
    {
        if let Some(video_subsystem) = &self.video_subsystem
        {
            let interval = match mode
            {
                VsyncMode::Off => { sdl2::video::SwapInterval::Immediate }
                VsyncMode::On => { sdl2::video::SwapInterval::VSync }
                VsyncMode::Adaptive => { sdl2::video::SwapInterval::LateSwapTearing }
            };

            match video_subsystem.gl_set_swap_interval(interval)
            {
                Ok(_) => { return true; }
                Err(error) =>
                {
                    error!("Could not set Vsync mode: {}", error);
                    return false;
                }
            }
        }

        false
    }
    
    pub fn set_power_management_enabled(&mut self, enabled : bool)
    {
        if let Some(video_subsystem) = &mut self.video_subsystem
        {
            if enabled
            {
                video_subsystem.enable_screen_saver();
            }
            else
            {
                video_subsystem.disable_screen_saver();
            }
        }
    }

    pub fn get_clipboard(&self) -> Option<String>
    {
        if let Some(video_subsystem) = &self.video_subsystem
        {
            return video_subsystem.clipboard().clipboard_text().ok();
        }

        None
    }

    pub fn set_clipboard(&self, text : &str) -> bool
    {
        if let Some(video_subsystem) = &self.video_subsystem
        {
            return video_subsystem.clipboard().set_clipboard_text(text).is_ok();
        }

        false
    }

    pub fn current_refresh_rate(&self) -> i32
    {
        if let Some(window) = &self.window
        {
            let window_borrow = window.borrow();

            if let Ok(index) = window_borrow.display_index()
            {
                if let Some(video_subsystem) = &self.video_subsystem
                {
                    if let Ok(mode) = video_subsystem.current_display_mode(index)
                    {
                        return mode.refresh_rate;
                    }
                }
            }
        }

        0
    }

    pub fn current_display_scale_factor(&self) -> f32
    {
        if let Some(window) = &self.window
        {
            let window_borrow = window.borrow();

            if let Ok(index) = window_borrow.display_index()
            {
                if let Some(video_subsystem) = &self.video_subsystem
                {
                    return calculate_display_scale_factor(video_subsystem, index);
                }
            }
        }

        1.0
    }
}
