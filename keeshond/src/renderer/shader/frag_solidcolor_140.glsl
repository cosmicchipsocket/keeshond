#version 140

in vec2 v_uv;
in vec4 v_rgba;

out vec4 TargetScreen;

void main()
{
    vec4 color = vec4(v_rgba.r, v_rgba.g, v_rgba.b, v_rgba.a);

    TargetScreen = color;
}
