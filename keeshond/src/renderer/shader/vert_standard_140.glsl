#version 140

in vec2 a_position;
in vec2 a_transform_col1;
in vec2 a_transform_col2;
in vec2 a_transform_col3;
in vec2 a_transform_offset;
in vec4 a_tex_coords;
in vec4 a_rgba;
in float a_depth;

out vec2 v_uv;
out vec4 v_tex_coords;
out vec4 v_rgba;
out float v_depth;

void main()
{
    mat3 instance_transform = mat3(vec3(a_transform_col1, 0.0), vec3(a_transform_col2, 0.0), vec3(a_transform_col3, 1.0));
    vec2 position = (instance_transform * vec3(a_position + a_transform_offset, 1.0)).xy;
    
    v_uv = vec2(a_position.xy * a_tex_coords.zw + a_tex_coords.xy);
    v_tex_coords = a_tex_coords;
    v_rgba = a_rgba;
    
    gl_Position = vec4(position, a_depth, 1.0);
}
