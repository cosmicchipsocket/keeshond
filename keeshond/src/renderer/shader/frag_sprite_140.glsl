#version 140

uniform sampler2D t_texture;
in vec2 v_uv;
in vec4 v_rgba;

out vec4 TargetScreen;

void main()
{
    vec4 color = texture(t_texture, v_uv);
    
    color = color * v_rgba;

    if (color.a <= 0.0)
    {
        discard;
    }

    TargetScreen = color;
}
