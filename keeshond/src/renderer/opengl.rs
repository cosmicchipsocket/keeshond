//! OpenGL 3 rendering backend

use std::io::Cursor;
use std::rc::Rc;
use std::cell::RefCell;

use glium::{Frame, Surface, Program, ProgramCreationError, IndexBuffer, VertexBuffer, DepthTest};
use glium::texture::{DepthFormat, DepthTexture2d, RawImage2d, Texture2d, UncompressedFloatFormat};
use glium::uniforms::{Sampler, MagnifySamplerFilter, MinifySamplerFilter, SamplerWrapFunction};
use glium::program::ProgramCreationInput;
use glium::backend::Facade;
use glium::draw_parameters::DepthClamp;
use glium::framebuffer::SimpleFrameBuffer;
use ouroboros::self_referencing;

use crate::thirdparty::glium_sdl2::{DisplayBuild, SDL2Facade};

use crate::datapack::{DataId, DataStore, PreparedStore, PreparedStoreError, DataPreparer};

use crate::renderer::{DrawControl, DrawControlResult, DrawTransform, DrawSpriteRawInfo, DrawBackgroundColorInfo, RendererError, Sheet, Shader, ViewportMode, DrawSpriteInfo, Color, DrawOptions, DrawOrdering};
use crate::renderer::draw_control_private::{DrawControlBackend, RendererNewInfo};
use crate::util::{BakedRect, Rect};

#[cfg(feature = "imgui_feature")] use imgui_glium_renderer;
#[cfg(feature = "imgui_feature")] use glium::uniforms::SamplerBehavior;

const INSTANCES_MAX : usize = 2048; // How many instances to store in each instance buffer.
const INSTANCE_BUFFER_MAX: usize = 4; // How many instance buffers to have.
const INSTANCE_RUN_COARSENESS : usize = 128; // The "resolution" of start index for new batches within a given instance buffer.
                                             // Use to tune performance of batch breaks vs VAO memory bloat

const UNBORKED_IDENTITY_TRANSFORM : [f32; 9] = [2.0, 0.0, 0.0, 0.0, -2.0, 0.0, -1.0, 1.0, 1.0];
const UNBORKED_UPSCALE_TRANSFORM : [f32; 9] = [2.0, 0.0, 0.0, 0.0, 2.0, 0.0, -1.0, -1.0, 1.0];
const DEFAULT_DEPTH_SORT_QUEUE_MAX : usize = 2097152;

#[derive(Copy, Clone, Eq, PartialEq)]
enum RendererOp
{
    #[allow(dead_code)]
    BackgroundColor,
    #[allow(dead_code)]
    Sprite
}

impl Default for RendererOp
{
    fn default() -> Self
    {
        RendererOp::BackgroundColor
    }
}

#[derive(Copy, Clone)]
struct Vertex
{
    a_position : [f32; 2]
}

implement_vertex!(Vertex, a_position);

#[derive(Copy, Clone, Default)]
struct Instance
{
    a_transform_col1 : [f32; 2],
    a_transform_col2 : [f32; 2],
    a_transform_col3 : [f32; 2],
    a_transform_offset : [f32; 2],
    a_tex_coords : [f32; 4],
    a_rgba : [f32; 4],
    a_depth : f32
}

implement_vertex!(Instance, a_transform_col1, a_transform_col2, a_transform_col3,
    a_transform_offset, a_tex_coords, a_rgba, a_depth);

const VERTEX_QUAD : [Vertex; 4] =
[
    Vertex { a_position: [ 0.0, 0.0 ] },
    Vertex { a_position: [ 1.0, 0.0 ] },
    Vertex { a_position: [ 1.0, 1.0 ] },
    Vertex { a_position: [ 0.0, 1.0 ] }
];
const INDEX_QUAD : [u16; 6] = [ 0, 1, 2, 2, 3, 0 ];

struct GpuSpriteSlice
{
    pub texture_x : f32,
    pub texture_y : f32,
    pub texture_w : f32,
    pub texture_h : f32,
    pub scale_x : f32,
    pub scale_y : f32,
    pub offset_x : f32,
    pub offset_y : f32,
}

struct GpuSheetHandle
{
    texture : Rc<Texture2d>,
    has_transparent : bool,
    has_semitransparent : bool,
    mag_filter : MagnifySamplerFilter,
    min_filter : MinifySamplerFilter,
    slices : Vec<GpuSpriteSlice>,
    auto_ordering : DrawOrdering
}

struct GpuSheetPreparer
{
    display : Rc<RefCell<SDL2Facade>>,
    #[allow(dead_code)]
    default_texture : Rc<Texture2d>,
    texture_filtering : bool,
    #[cfg(feature = "imgui_feature")]
    imgui_renderer : Rc<RefCell<Option<imgui_glium_renderer::Renderer>>>
}

impl GpuSheetPreparer
{
    fn texture_raw_from_data(data : &Vec<u8>, palette : &Option<Vec<Color>>) -> Vec<u8>
    {
        if let Some(palette) = palette
        {
            let mut expanded = Vec::with_capacity(data.len() * 4);

            for i in 0..data.len()
            {
                let color = data[i] as usize;

                if color < palette.len()
                {
                    expanded.push(palette[color].r);
                    expanded.push(palette[color].g);
                    expanded.push(palette[color].b);
                    expanded.push(palette[color].a);
                }
                else
                {
                    for _ in 0..4
                    {
                        expanded.push(0);
                    }
                }
            }

            return expanded;
        }

        data.clone()
    }

    fn make_sheet_handle(&mut self, data : &mut Sheet) -> GpuSheetHandle
    {
        let mut handle =
        {
            let display_ref: &SDL2Facade = &self.display.borrow();
            let image_raw = GpuSheetPreparer::texture_raw_from_data(&data.image_raw, &data.palette);
            let mut has_transparent = false;
            let mut has_semitransparent = false;

            for a in image_raw.iter().skip(3).step_by(4)
            {
                if *a == 0
                {
                    has_transparent = true;
                } else if *a < 255
                {
                    has_semitransparent = true;
                    break;
                }
            }

            let image = RawImage2d::from_raw_rgba(image_raw, (data.width, data.height));
            let texture = Rc::new(Texture2d::new(display_ref, image).expect("Texture upload failed"));

            GpuSheetHandle
            {
                texture,
                has_transparent,
                has_semitransparent,
                mag_filter: MagnifySamplerFilter::Linear,
                min_filter: MinifySamplerFilter::Linear,
                slices: Vec::new(),
                auto_ordering: DrawOrdering::ZBuffer
            }
        };

        self.update_sheet_handle(data, &mut handle);

        handle
    }

    fn update_sheet_handle(&mut self, data : &Sheet, handle : &mut GpuSheetHandle)
    {
        let (mag_filter, min_filter) = self.sample_filters(data);

        handle.min_filter = min_filter;
        handle.mag_filter = mag_filter;
        handle.slices = Vec::new();

        for slice in &data.metadata.slices
        {
            let texture_w = slice.texture_w / data.width as f32;
            let texture_h = slice.texture_h / data.height as f32;

            handle.slices.push(GpuSpriteSlice
            {
                texture_x: slice.texture_x / data.width as f32,
                texture_y: slice.texture_y / data.height as f32,
                texture_w,
                texture_h,
                scale_x: slice.texture_w,
                scale_y: slice.texture_h,
                offset_x: -slice.origin_x / slice.texture_w,
                offset_y: -slice.origin_y / slice.texture_h,
            })
        }

        if handle.has_semitransparent
        {
            handle.auto_ordering = DrawOrdering::DepthSort;
        }
        else if handle.has_transparent
        {
            if (handle.min_filter == MinifySamplerFilter::Nearest || handle.min_filter == MinifySamplerFilter::NearestMipmapNearest)
                && handle.mag_filter == MagnifySamplerFilter::Nearest
            {
                handle.auto_ordering = DrawOrdering::ZBuffer;
            }
            else
            {
                handle.auto_ordering = DrawOrdering::DepthSort;
            }
        }
        else
        {
            handle.auto_ordering = DrawOrdering::ZBuffer;
        }
    }

    #[cfg(feature = "imgui_feature")]
    fn update_imgui_texture(&mut self, id : DataId<Sheet>, handle : &mut GpuSheetHandle)
    {
        let mut imgui_borrow = self.imgui_renderer.borrow_mut();

        if let Some(imgui_renderer) = imgui_borrow.as_mut()
        {
            imgui_renderer.textures().replace(imgui::TextureId::from(id.index()), imgui_glium_renderer::Texture
            {
                texture : handle.texture.clone(),
                sampler : SamplerBehavior
                {
                    wrap_function: (SamplerWrapFunction::Repeat, SamplerWrapFunction::Repeat, SamplerWrapFunction::Repeat),
                    minify_filter: handle.min_filter,
                    magnify_filter: handle.mag_filter,
                    .. Default::default()
                }
            });
        }
    }

    fn sample_filters(&mut self, data : &Sheet) -> (MagnifySamplerFilter, MinifySamplerFilter)
    {
        let filtered;

        if let Some(filter) = data.metadata.texture_filtering
        {
            filtered = filter;
        }
        else
        {
            filtered = self.texture_filtering;
        }

        let mag_filter = match filtered
        {
            true => MagnifySamplerFilter::Linear,
            false => MagnifySamplerFilter::Nearest
        };
        let min_filter = match filtered
        {
            true => MinifySamplerFilter::Linear,
            false => MinifySamplerFilter::Nearest
        };

        (mag_filter, min_filter)
    }
}

impl DataPreparer<Sheet, GpuSheetHandle> for GpuSheetPreparer
{
    #[allow(unused_variables)]
    fn prepare(&mut self, data : &mut Sheet, id : DataId<Sheet>) -> GpuSheetHandle
    {
        #[allow(unused_mut)]
        let mut handle = self.make_sheet_handle(data);

        data.texture_dirty = false;

        #[cfg(feature = "imgui_feature")]
        self.update_imgui_texture(id, &mut handle);

        handle
    }

    #[allow(unused_variables)]
    fn unprepare(&mut self, _prepared : &mut GpuSheetHandle, id : DataId<Sheet>)
    {
        #[cfg(feature = "imgui_feature")]
        {
            let mut imgui_borrow = self.imgui_renderer.borrow_mut();

            if let Some(imgui_renderer) = imgui_borrow.as_mut()
            {
                imgui_renderer.textures().replace(imgui::TextureId::from(id.index()), imgui_glium_renderer::Texture
                {
                    texture : self.default_texture.clone(),
                    sampler : SamplerBehavior
                    {
                        wrap_function: (SamplerWrapFunction::Repeat, SamplerWrapFunction::Repeat, SamplerWrapFunction::Repeat),
                        minify_filter: MinifySamplerFilter::Linear,
                        magnify_filter: MagnifySamplerFilter::Linear,
                        .. Default::default()
                    }
                });
            }
        }
    }

    #[allow(unused_variables)]
    fn reprepare(&mut self, data : &mut Sheet, prepared : &mut GpuSheetHandle, id : DataId<Sheet>)
    {
        if data.texture_dirty
        {
            *prepared = self.make_sheet_handle(data);
        }
        else
        {
            self.update_sheet_handle(data, prepared);
        }

        #[cfg(feature = "imgui_feature")]
        self.update_imgui_texture(id, prepared);

        data.texture_dirty = false;
    }
}

#[allow(unused_mut)]
fn modify_shader_header(mut shader_text : &str) -> String
{
    #[cfg(not(target_os = "emscripten"))]
    {
        shader_text.to_string()
    }
    #[cfg(target_os = "emscripten")]
    {
        shader_text.replace("#version 140", "#version 300 es\nprecision mediump float;")
    }
}

struct GpuShaderHandle
{
    program : Program
}

struct GpuShaderPreparer
{
    display : Rc<RefCell<SDL2Facade>>
}

impl DataPreparer<Shader, GpuShaderHandle> for GpuShaderPreparer
{
    fn prepare(&mut self, data : &mut Shader, _id : DataId<Shader>) -> GpuShaderHandle
    {
        let program = make_program(&self.display.borrow(),
            &modify_shader_header(include_str!("shader/vert_standard_140.glsl")),
            &modify_shader_header(&data.program_source)).expect("Shader compilation failed");
        
        GpuShaderHandle
        {
            program
        }
    }
    fn unprepare(&mut self, _prepared : &mut GpuShaderHandle, _id : DataId<Shader>)
    {
        
    }
}

fn make_program(display : &SDL2Facade, vertex_shader : &str, fragment_shader : &str) -> Result<Program, ProgramCreationError>
{
    let source = ProgramCreationInput::SourceCode
    {
        vertex_shader,
        fragment_shader,
        geometry_shader : None,
        tessellation_control_shader : None,
        tessellation_evaluation_shader : None,
        transform_feedback_varyings : None,
        uses_point_size : false,
        outputs_srgb : true
    };
    
    glium::Program::new(display, source)
}

fn upscale_instance_from_transform(transform : &DrawTransform) -> Instance
{
    Instance
    {
        a_transform_col1 : [ transform.mat.x.x, transform.mat.x.y ],
        a_transform_col2 : [ transform.mat.y.x, transform.mat.y.y ],
        a_transform_col3 : [ transform.mat.z.x, transform.mat.z.y ],
        a_transform_offset : [ 0.0, 0.0 ],
        a_tex_coords : [0.0, 0.0, 1.0, 1.0],
        a_rgba : [1.0, 1.0, 1.0, 1.0],
        a_depth : 0.0
    }
}

#[self_referencing]
struct PixelRenderTarget
{
    pub color : Texture2d,
    pub depth : DepthTexture2d,
    #[borrows(color, depth)]
    #[covariant]
    pub framebuffer : SimpleFrameBuffer<'this>
}

#[derive(Copy, Clone, Eq, PartialEq, Default)]
struct InstanceBatchParam
{
    op : RendererOp,
    sheet : DataId<Sheet>,
    shader : DataId<Shader>,
    ordering : DrawOrdering
}

#[derive(Copy, Clone, Default)]
struct BatchRun
{
    start : usize,
    len : usize,
    param : InstanceBatchParam
}

struct DepthSortQueueItem
{
    instance : Instance,
    instance_param : InstanceBatchParam,
    generation : u8,
    param_index : usize
}

struct DepthSortQueueParam
{
    scissor_area : Option<Rect<f32>>
}

struct DepthSortQueue
{
    items : Vec<DepthSortQueueItem>,
    indexes : Vec<usize>,
    count : usize,
    sort_count : usize,
    draw_count : usize,
    generation : u8,
    params : Vec<DepthSortQueueParam>,
    params_dirty : bool,
    queue_max : usize
}

impl DepthSortQueue
{
    fn new() -> DepthSortQueue
    {
        DepthSortQueue
        {
            items : Vec::new(),
            indexes : Vec::new(),
            count : 0,
            sort_count : 0,
            draw_count : 0,
            generation : 0,
            params : Vec::new(),
            params_dirty : true,
            queue_max : DEFAULT_DEPTH_SORT_QUEUE_MAX
        }
    }

    fn push(&mut self, instance : Instance, instance_param : InstanceBatchParam)
    {
        debug_assert!(!self.params.is_empty());

        if self.count >= self.items.len()
        {
            self.items.push(DepthSortQueueItem
            {
                instance,
                instance_param,
                generation : self.generation,
                param_index : self.params.len() - 1
            });
            self.indexes.push(self.count);
        }
        else
        {
            self.items[self.count] = DepthSortQueueItem
            {
                instance,
                instance_param,
                generation : self.generation,
                param_index : self.params.len() - 1
            };
        }

        self.count += 1;
    }

    fn sort(&mut self)
    {
        self.indexes.retain(|index| self.items[*index].generation == self.generation);
        self.items.truncate(self.count);

        self.indexes[self.sort_count..].sort_unstable_by(|a, b|
        {
            match self.items[*a].instance.a_depth.partial_cmp(&self.items[*b].instance.a_depth)
            {
                None | Some(std::cmp::Ordering::Equal) => { a.cmp(&b) }
                Some(result) => { result }
            }
        });

        self.sort_count = self.count;
    }

    fn next_frame(&mut self)
    {
        self.generation = self.generation.wrapping_add(1);
        self.count = 0;
        self.sort_count = 0;
        self.draw_count = 0;
        self.params_dirty = true;

        self.items.shrink_to_fit();
    }
}

pub(crate) struct GlDrawControl
{
    display : Rc<RefCell<SDL2Facade>>,
    window : Rc<RefCell<sdl2::video::Window>>,
    base_width : f32,
    base_height : f32,
    scale_width : f32,
    scale_height : f32,
    need_recalc_viewport : bool,
    viewport_offset : (f32, f32),
    viewport_mode : ViewportMode,
    quad_buffer : VertexBuffer<Vertex>,
    index_buffer : IndexBuffer<u16>,
    instance_buffer_src : Box<[Instance ; INSTANCES_MAX]>,
    instance_buffer : Vec<VertexBuffer<Instance>>,
    instance_buffer_flip : usize,
    default_texture : Rc<Texture2d>,
    shader_list : Vec<Program>,
    base_scissor : Option<glium::Rect>,
    scissor_area : Option<Rect<f32>>,
    full_scissor : Option<Option<glium::Rect>>,
    frame : Option<Frame>,
    pixel_scale : u32,
    last_pixel_scale : u32,
    pixel_target : Option<PixelRenderTarget>,
    pixel_upscale_texture : Option<Texture2d>,
    current_index : usize,
    index_runs : Vec<BatchRun>,
    current_run : BatchRun,
    depth_sort_queue : DepthSortQueue,
    last_view_transform : DrawTransform,
    gpu_sheet_store : PreparedStore<Sheet, GpuSheetHandle>,
    gpu_shader_store : PreparedStore<Shader, GpuShaderHandle>,
    #[cfg(feature = "imgui_feature")]
    imgui_renderer : Rc<RefCell<Option<imgui_glium_renderer::Renderer>>>
}

impl GlDrawControl
{
    fn get_or_compute_scissor(&mut self) -> Option<glium::Rect>
    {
        if let Some(full_scissor) = &self.full_scissor
        {
            return full_scissor.clone();
        }

        let computed = self.compute_scissor();

        self.full_scissor = Some(computed.clone());

        return computed;
    }

    fn compute_scissor(&self) -> Option<glium::Rect>
    {
        if let Some(scissor_area) = &self.scissor_area
        {
            match self.viewport_mode
            {
                ViewportMode::Pixel =>
                {
                    let limits = BakedRect
                    {
                        x1 : 0.0,
                        y1 : 0.0,
                        x2 : self.base_width,
                        y2 : self.base_height
                    };

                    let mut baked = scissor_area.bake();
                    baked.intersection(&limits);

                    let width = baked.w() as u32;
                    let height = baked.h() as u32;

                    return Some(glium::Rect
                    {
                        left : baked.x1.ceil() as u32,
                        bottom : (self.base_height as u32).saturating_sub(baked.y1.ceil() as u32 + height),
                        width,
                        height
                    });
                }
                _ =>
                {
                    if let Some(base_scissor) = &self.base_scissor
                    {
                        let (_, window_height) = self.window.borrow().size();
                        let scissor_width = base_scissor.width as f32;
                        let scissor_height = base_scissor.height as f32;

                        let limits = BakedRect
                        {
                            x1 : base_scissor.left as f32,
                            y1 : window_height as f32 - base_scissor.bottom as f32 - scissor_height,
                            x2 : base_scissor.left as f32 + scissor_width,
                            y2 : window_height as f32 - base_scissor.bottom as f32
                        };

                        let mut baked = BakedRect
                        {
                            x1 : limits.x1 + scissor_width * (scissor_area.x / self.base_width),
                            y1 : limits.y1 + scissor_height * (scissor_area.y / self.base_height),
                            x2 : limits.x1 + scissor_width * ((scissor_area.x + scissor_area.w.max(0.0)) / self.base_width),
                            y2 : limits.y1 + scissor_height * ((scissor_area.y + scissor_area.h.max(0.0)) / self.base_height),
                        };

                        baked.intersection(&limits);

                        let width = (baked.x2.round() - baked.x1.round()) as u32;
                        let height = (baked.y2.round() - baked.y1.round()) as u32;

                        return Some(glium::Rect
                        {
                            left : baked.x1.round() as u32,
                            bottom : window_height.saturating_sub(baked.y1.round() as u32 + height),
                            width,
                            height,
                        });
                    }
                }
            }
        }

        self.base_scissor.clone()
    }

    fn queue_draw_auto_ordering(&mut self, instance : &Instance, instance_param : &mut InstanceBatchParam)
    {
        if instance.a_rgba[3] > 0.0 && instance.a_rgba[3] < 1.0
        {
            instance_param.ordering = DrawOrdering::DepthSort;
        }
        else
        {
            if let Some(sheet) = self.gpu_sheet_store.get(instance_param.sheet)
            {
                instance_param.ordering = sheet.auto_ordering;
            }
            else
            {
                instance_param.ordering = DrawOrdering::ZBuffer;
            }
        }
    }

    fn queue_draw_depth_sorted(&mut self, instance : Instance, instance_param : InstanceBatchParam)
    {
        if self.depth_sort_queue.count >= self.depth_sort_queue.queue_max
        {
            self.perform_depth_sort_draw();
            self.depth_sort_queue.next_frame();
        }

        if self.depth_sort_queue.params_dirty
        {
            self.depth_sort_queue.params.push(DepthSortQueueParam
            {
                scissor_area : self.scissor_area
            });
            self.depth_sort_queue.params_dirty = false;
        }

        self.depth_sort_queue.push(instance, instance_param);
    }

    fn queue_draw_item(&mut self, instance : Instance, mut instance_param : InstanceBatchParam)
    {
        if instance_param.ordering == DrawOrdering::OrderedAutomatic
        {
            self.queue_draw_auto_ordering(&instance, &mut instance_param);
        }

        if instance_param.ordering == DrawOrdering::DepthSort
        {
            self.queue_draw_depth_sorted(instance, instance_param);
        }
        else
        {
            self.draw_item(instance, instance_param);
        }
    }

    fn draw_item(&mut self, instance : Instance, instance_param : InstanceBatchParam)
    {
        self.check_batch_break(instance_param);

        self.instance_buffer_src[self.current_index] = instance;
        self.current_index += 1;
        self.current_run.len += 1;
    }

    fn perform_depth_sort_draw(&mut self)
    {
        let mut depth_sort_queue = DepthSortQueue::new();

        std::mem::swap(&mut depth_sort_queue, &mut self.depth_sort_queue);

        depth_sort_queue.sort();

        let mut last_param_index = usize::MAX;

        for index in &depth_sort_queue.indexes[depth_sort_queue.draw_count..]
        {
            let item = &depth_sort_queue.items[*index];

            if last_param_index != item.param_index
            {
                last_param_index = item.param_index;
                self.set_scissor_area(depth_sort_queue.params[item.param_index].scissor_area);
            }

            self.draw_item(item.instance.clone(), item.instance_param.clone());
        }

        depth_sort_queue.draw_count = depth_sort_queue.count;

        self.perform_draw_batch();

        std::mem::swap(&mut depth_sort_queue, &mut self.depth_sort_queue);
    }

    fn perform_draw_batch(&mut self)
    {
        if self.current_index <= 0
        {
            return;
        }

        self.index_runs.push(self.current_run);

        self.instance_buffer[self.instance_buffer_flip].invalidate();
        self.instance_buffer[self.instance_buffer_flip].write(&self.instance_buffer_src[..]);

        let scissor = self.get_or_compute_scissor();
        
        let mut do_draw = | first : usize, last : usize, param : &InstanceBatchParam |
        {
            if first >= last
            {
                return;
            }
            
            if let Some(frame) = &mut self.frame
            {
                let mut texture_ref = self.default_texture.as_ref();
                let mut shader_ref = &self.shader_list[param.op as usize];
                let mut mag_filter = MagnifySamplerFilter::Linear;
                let mut min_filter = MinifySamplerFilter::Linear;
                let depth = match &param.ordering
                {
                    DrawOrdering::Painter => glium::Depth
                    {
                        test : DepthTest::Overwrite,
                        write : false,
                        range : (1.0, 0.0),
                        clamp : DepthClamp::NoClamp
                    },
                    DrawOrdering::ZBuffer => glium::Depth
                    {
                        test : DepthTest::IfLessOrEqual,
                        write : true,
                        range : (1.0, 0.0),
                        clamp : DepthClamp::NoClamp
                    },
                    DrawOrdering::DepthSort => glium::Depth
                    {
                        test : DepthTest::IfLessOrEqual,
                        write : false,
                        range : (1.0, 0.0),
                        clamp : DepthClamp::NoClamp
                    },
                    DrawOrdering::PainterZWrite => glium::Depth
                    {
                        test : DepthTest::Overwrite,
                        write : true,
                        range : (1.0, 0.0),
                        clamp : DepthClamp::NoClamp
                    },
                    DrawOrdering::OrderedAutomatic =>
                    {
                        unreachable!() // Should have been replaced during earlier processing
                    }
                };
                
                if let Some(gpu_sheet_info) = self.gpu_sheet_store.get(param.sheet)
                {
                    texture_ref = gpu_sheet_info.texture.as_ref();
                    mag_filter = gpu_sheet_info.mag_filter;
                    min_filter = gpu_sheet_info.min_filter;
                }
                if let Some(gpu_shader_info) = self.gpu_shader_store.get(param.shader)
                {
                    shader_ref = &gpu_shader_info.program;
                }
                
                let uniform = uniform!
                {
                    t_texture : Sampler::new(texture_ref)
                        .magnify_filter(mag_filter).minify_filter(min_filter)
                        .wrap_function(SamplerWrapFunction::Repeat),
                    u_tex_size : [ texture_ref.width() as f32, texture_ref.height() as f32 ]
                };
                
                match self.viewport_mode
                {
                    ViewportMode::Independent | ViewportMode::OneToOneDpiScaled | ViewportMode::OneToOneUnscaled =>
                    {
                        let params = glium::DrawParameters
                        {
                            depth,
                            blend : glium::draw_parameters::Blend::alpha_blending(),
                            scissor,
                            .. Default::default()
                        };
                        
                        frame.draw((&self.quad_buffer,
                            self.instance_buffer[self.instance_buffer_flip].slice(first..last)
                                .expect("Failed to prepare instance buffer").per_instance()
                                .expect("Failed to prepare instance buffer")),
                            &self.index_buffer, shader_ref,
                            &uniform, &params).expect("Draw operation failed");
                    },
                    ViewportMode::Pixel =>
                    {
                        let params = glium::DrawParameters
                        {
                            depth,
                            blend : glium::draw_parameters::Blend::alpha_blending(),
                            scissor,
                            multisampling : false,
                            dithering : false,
                            .. Default::default()
                        };
                        
                        self.pixel_target.as_mut().unwrap().with_framebuffer_mut(|framebuffer|
                        {
                            framebuffer.draw((&self.quad_buffer,
                                   self.instance_buffer[self.instance_buffer_flip].slice(first..last)
                                       .expect("Failed to prepare instance buffer").per_instance()
                                       .expect("Failed to prepare instance buffer")),
                                  &self.index_buffer, shader_ref,
                                  &uniform, &params).expect("Draw operation failed");
                        });
                    }
                }
            }
        };

        for run in &self.index_runs
        {
            do_draw(run.start, run.start + run.len, &run.param);
        }

        self.flip_instance_buffers();
    }

    fn check_batch_break(&mut self, param : InstanceBatchParam)
    {
        let skip_index = (self.current_index + INSTANCE_RUN_COARSENESS) / INSTANCE_RUN_COARSENESS * INSTANCE_RUN_COARSENESS;

        // TODO if we don't need to skip, don't end batch this early
        if skip_index >= INSTANCES_MAX
        {
            self.perform_draw_batch();
        }

        if self.current_index == 0
        {
            self.current_run.param = param;
        }
        else if self.current_run.param != param
        {
            self.index_runs.push(self.current_run);
            self.current_index = skip_index;
            self.current_run = BatchRun { start : self.current_index, len : 0, param };
        }
    }

    fn flip_instance_buffers(&mut self)
    {
        self.instance_buffer_flip = (self.instance_buffer_flip + 1) % self.instance_buffer.len();
        self.current_index = 0;
        self.current_run = BatchRun { start : 0, len : 0, param : InstanceBatchParam::default() };
        self.index_runs.clear();

        if self.instance_buffer_flip % (self.instance_buffer.len() / 2) == 0
        {
            self.display.borrow().get_context().flush();
        }
    }
    
    fn update_pixel_upscale(&mut self)
    {
        if self.pixel_scale == self.last_pixel_scale
        {
            return;
        }
        
        self.last_pixel_scale = self.pixel_scale;
        let display_ref : &SDL2Facade = &self.display.borrow();
        let pixel_surface = self.pixel_target.as_mut().unwrap().borrow_framebuffer();
        let (width, height) = pixel_surface.get_dimensions();
        
        self.pixel_upscale_texture = Some(Texture2d::empty_with_format(
            display_ref, UncompressedFloatFormat::U8U8U8U8, glium::texture::MipmapsOption::NoMipmap,
            width * self.pixel_scale, height * self.pixel_scale).expect("Could not create upscale framebuffer"));
    }
}

impl DrawControl for GlDrawControl
{
    fn screen_transform(&self) -> &DrawTransform
    {
        &self.last_view_transform
    }

    fn draw_sprite(&mut self, draw_info : &DrawSpriteInfo, options : &DrawOptions)
    {
        let mut transform = draw_info.transform.clone();
        let mut transform_offset_x = -0.5;
        let mut transform_offset_y = -0.5;
        let mut texture_x = 0.0;
        let mut texture_y = 0.0;
        let mut texture_w = 1.0;
        let mut texture_h = 1.0;
        let mut sheet_id = DataId::new(0);

        transform.translate(draw_info.x, draw_info.y);
        if draw_info.angle != 0.0
        {
            transform.rotate(draw_info.angle);
        }

        let mut got_slice = false;

        if let Some(gpu_sheet_info) = self.gpu_sheet_store.get(draw_info.sheet_id)
        {
            if draw_info.slice < gpu_sheet_info.slices.len()
            {
                let slice = &gpu_sheet_info.slices[draw_info.slice];

                transform.scale(draw_info.scale_x * slice.scale_x, draw_info.scale_y * slice.scale_y);

                sheet_id = draw_info.sheet_id;
                transform_offset_x = slice.offset_x;
                transform_offset_y = slice.offset_y;
                texture_x = slice.texture_x;
                texture_y = slice.texture_y;
                texture_w = slice.texture_w;
                texture_h = slice.texture_h;

                got_slice = true;
            }
        }

        if !got_slice
        {
            transform.scale(draw_info.scale_x * 32.0, draw_info.scale_y * 32.0);
        }

        self.draw_sprite_raw(&DrawSpriteRawInfo
        {
            sheet_id,
            shader_id : draw_info.shader_id,
            transform,
            transform_offset_x,
            transform_offset_y,
            texture_x,
            texture_y,
            texture_w,
            texture_h,
            r : draw_info.r,
            g : draw_info.g,
            b : draw_info.b,
            alpha : draw_info.alpha,
            depth : draw_info.depth
        }, options);
    }

    fn draw_sprite_raw(&mut self, draw_info : &DrawSpriteRawInfo, options : &DrawOptions)
    {
        let transform = &draw_info.transform;
        let instance = Instance
        {
            a_transform_col1 : [ transform.mat.x.x, transform.mat.x.y ],
            a_transform_col2 : [ transform.mat.y.x, transform.mat.y.y ],
            a_transform_col3 : [ transform.mat.z.x, transform.mat.z.y ],
            a_transform_offset : [ draw_info.transform_offset_x, draw_info.transform_offset_y ],
            a_tex_coords : [ draw_info.texture_x, draw_info.texture_y, draw_info.texture_w, draw_info.texture_h ],
            a_rgba : [ draw_info.r, draw_info.g, draw_info.b, draw_info.alpha ],
            a_depth : draw_info.depth
        };

        let instance_param = InstanceBatchParam
        {
            op : RendererOp::Sprite,
            sheet : draw_info.sheet_id,
            shader : draw_info.shader_id,
            ordering : options.ordering
        };

        self.queue_draw_item(instance, instance_param);
    }

    fn draw_background_color(&mut self, draw_info : &DrawBackgroundColorInfo, options : &DrawOptions)
    {
        let instance = Instance
        {
            a_transform_col1 : [ 2.0, 0.0 ],
            a_transform_col2 : [ 0.0, -2.0 ],
            a_transform_col3 : [ -1.0, 1.0 ],
            a_transform_offset : [ 0.0, 0.0 ],
            a_tex_coords : [ 0.0, 0.0, 1.0, 1.0 ],
            a_rgba : [ draw_info.r, draw_info.g, draw_info.b, draw_info.alpha ],
            a_depth : draw_info.depth
        };

        let instance_param = InstanceBatchParam
        {
            op : RendererOp::BackgroundColor,
            sheet : DataId::new(0),
            shader : draw_info.shader_id,
            ordering : options.ordering
        };

        self.queue_draw_item(instance, instance_param);
    }

    fn scissor_area(&self) -> &Option<Rect<f32>>
    {
        &self.scissor_area
    }

    fn set_scissor_area(&mut self, scissor_area : Option<Rect<f32>>)
    {
        self.perform_draw_batch();

        self.scissor_area = scissor_area;
        self.full_scissor = None;
        self.depth_sort_queue.params_dirty = true;
        self.depth_sort_queue.sort();
    }

    fn flush_depth_sort_queue(&mut self)
    {
        self.perform_depth_sort_draw();
    }

    fn depth_sort_queue_max(&self) -> usize
    {
        self.depth_sort_queue.queue_max
    }

    fn set_depth_sort_queue_max(&mut self, capacity : usize)
    {
        self.depth_sort_queue.queue_max = capacity;
    }
}

impl DrawControlBackend for GlDrawControl
{
    fn new(renderer_new_info : &mut RendererNewInfo) -> DrawControlResult where Self : Sized
    {
        let video_subsystem = &renderer_new_info.video_subsystem;

        let (width, height) = crate::renderer::calculate_window_size(renderer_new_info.width as f32,
                                                                     renderer_new_info.height as f32,
                                                                     renderer_new_info.default_window_scale,
                                                                     video_subsystem);

        let mut window_builder = video_subsystem.window("", width, height);
        
        window_builder.hidden().position_centered().resizable().allow_highdpi();

        #[cfg(not(target_os = "emscripten"))]
        {
            video_subsystem.gl_attr().set_context_profile(sdl2::video::GLProfile::Core);
            video_subsystem.gl_attr().set_context_version(3, 2);
        }
        #[cfg(target_os = "emscripten")]
        {
            video_subsystem.gl_attr().set_context_profile(sdl2::video::GLProfile::GLES);
            video_subsystem.gl_attr().set_context_version(3, 0);
        }
        
        let display_raw = window_builder.build_glium().map_err(
            |error| RendererError::RendererInitFailed(format!("Could not create game window: {}", error)))?;
        
        let mut pixel_target = None;
        
        match renderer_new_info.viewport_mode
        {
            ViewportMode::Independent | ViewportMode::OneToOneDpiScaled | ViewportMode::OneToOneUnscaled => (),
            ViewportMode::Pixel =>
            {
                pixel_target = Some(PixelRenderTargetTryBuilder
                {
                    color : Texture2d::empty_with_format(
                        &display_raw, UncompressedFloatFormat::U8U8U8U8, glium::texture::MipmapsOption::NoMipmap, renderer_new_info.width, renderer_new_info.height).map_err(
                        |error| RendererError::RendererInitFailed(format!("Could not create framebuffer: {}", error)))?,
                    depth : DepthTexture2d::empty_with_format(
                        &display_raw, DepthFormat::I24, glium::texture::MipmapsOption::NoMipmap, renderer_new_info.width, renderer_new_info.height).map_err(
                        |error| RendererError::RendererInitFailed(format!("Could not create framebuffer: {}", error)))?,
                    framebuffer_builder : |c, d| SimpleFrameBuffer::with_depth_buffer(&display_raw, c, d).map_err(
                        |error| RendererError::RendererInitFailed(format!("Could not create framebuffer: {}", error))),
                }.try_build()?);
            }
        };
        
        let quad_buffer = glium::VertexBuffer::new(&display_raw, &VERTEX_QUAD).map_err(
            |error| RendererError::RendererInitFailed(format!("Could not create vertex buffer: {}", error)))?;
        let index_buffer = glium::IndexBuffer::new(&display_raw,
            glium::index::PrimitiveType::TrianglesList, &INDEX_QUAD).map_err(
            |error| RendererError::RendererInitFailed(format!("Could not create index buffer: {}", error)))?;

        let mut instance_buffer = Vec::new();

        for _ in 0..INSTANCE_BUFFER_MAX
        {
            instance_buffer.push(glium::VertexBuffer::<Instance>::empty_dynamic(&display_raw, INSTANCES_MAX).map_err(
                |error| RendererError::RendererInitFailed(format!("Could not create instance buffer: {}", error)))?);
        }
        
        let context = display_raw.get_context();
        info!("Renderer: {} {}", context.get_opengl_vendor_string(), context.get_opengl_renderer_string());
        info!("OpenGL version: {}", context.get_opengl_version_string());
        if let Some(free_vram) = context.get_free_video_memory()
        {
            info!("VRAM: {} MiB free", free_vram / 1024 / 1024);
        }
        
        let shader_background = make_program(&display_raw,
            &modify_shader_header(include_str!("shader/vert_standard_140.glsl")),
            &modify_shader_header(include_str!("shader/frag_solidcolor_140.glsl"))).map_err(
            |error| RendererError::RendererInitFailed(format!("Could not load shader: {}", error)))?;
        let shader_sprite = make_program(&display_raw,
            &modify_shader_header(include_str!("shader/vert_standard_140.glsl")),
            &modify_shader_header(include_str!("shader/frag_sprite_140.glsl"))).map_err(
            |error| RendererError::RendererInitFailed(format!("Could not load shader: {}", error)))?;
        
        let shader_list = vec!(shader_background, shader_sprite);

        let (default_texture, default_width, default_height) = crate::renderer::png_to_raw(
            Cursor::new(include_bytes!("data/default.png").to_vec()))?;
        
        let image = RawImage2d::from_raw_rgba(default_texture, (default_width, default_height));
        let default_texture = Rc::new(Texture2d::new(&display_raw, image).map_err(
            |error| RendererError::RendererInitFailed(format!("Could not upload texture: {}", error)))?);
        
        let window = display_raw.window_clone();
        
        let display = Rc::new(RefCell::new(display_raw));
        #[cfg(feature = "imgui_feature")]
        let imgui_renderer = Rc::new(RefCell::new(None));
        let sheet_data_preparer = Box::new(GpuSheetPreparer
        {
            display : display.clone(),
            default_texture: default_texture.clone(),
            texture_filtering : renderer_new_info.texture_filtering,
            #[cfg(feature = "imgui_feature")]
            imgui_renderer: imgui_renderer.clone()
        });
        let shader_data_preparer = Box::new(GpuShaderPreparer
        {
            display : display.clone()
        });
        
        let gpu_sheet_store = PreparedStore::new(&mut renderer_new_info.resources.store_mut(),
            sheet_data_preparer).map_err(
            |error| RendererError::LoadResourceFailed(format!("Failed to create GPU sheet handle store: {}", error)))?;
        
        let gpu_shader_store = PreparedStore::new(&mut renderer_new_info.resources.store_mut(),
            shader_data_preparer).map_err(
            |error| RendererError::LoadResourceFailed(format!("Failed to create GPU shader handle store: {}", error)))?;
        
        Ok((Box::new(GlDrawControl
        {
            display,
            window : window.clone(),
            viewport_offset : (0.0, 0.0),
            viewport_mode : renderer_new_info.viewport_mode,
            base_width : renderer_new_info.width as f32,
            base_height : renderer_new_info.height as f32,
            scale_width : 0.0,
            scale_height : 0.0,
            need_recalc_viewport : false,
            quad_buffer,
            index_buffer,
            instance_buffer_src : Box::new([Default::default() ; INSTANCES_MAX]),
            instance_buffer,
            instance_buffer_flip : 0,
            default_texture,
            shader_list,
            base_scissor : None,
            scissor_area : None,
            full_scissor : None,
            frame : None,
            pixel_scale : 1,
            last_pixel_scale : 0,
            pixel_target,
            pixel_upscale_texture : None,
            current_index : 0,
            index_runs : Vec::new(),
            current_run : BatchRun { start : 0, len : 0, param : InstanceBatchParam::default() },
            depth_sort_queue : DepthSortQueue::new(),
            last_view_transform : DrawTransform::identity(),
            gpu_sheet_store,
            gpu_shader_store,
            #[cfg(feature = "imgui_feature")]
            imgui_renderer,
        }), window))
    }
    
    fn sync_sheet_store(&mut self, sheet_store : &mut DataStore<Sheet>) -> Result<(), PreparedStoreError>
    {
        self.gpu_sheet_store.sync(sheet_store)
    }
    
    fn sync_shader_store(&mut self, shader_store : &mut DataStore<Shader>) -> Result<(), PreparedStoreError>
    {
        self.gpu_shader_store.sync(shader_store)
    }
    
    #[cfg(feature = "imgui_feature")]
    fn draw_imgui(&mut self, ui : imgui::Ui)
    {
        self.perform_draw_batch();
        self.perform_depth_sort_draw();
        
        if let Some(frame) = &mut self.frame
        {
            let mut imgui_borrow = self.imgui_renderer.borrow_mut();

            if let Some(imgui_renderer) = imgui_borrow.as_mut()
            {
                let draw_data = ui.render();
                imgui_renderer.render(frame, &draw_data).expect("imgui render failed");
            }
        }
    }
    
    fn recalculate_viewport(&mut self, base_width : f32, base_height : f32)
    {
        self.base_width = base_width;
        self.base_height = base_height;

        if self.frame.is_some()
        {
            self.need_recalc_viewport = true;
            return;
        }

        let (width, height) = self.window.borrow().size();
        let info = crate::renderer::recalculate_viewport_common(base_width, base_height, width, height, self.viewport_mode);

        self.scale_width = info.scale_width;
        self.scale_height = info.scale_height;
        self.viewport_offset = (info.scale_offset_x, info.scale_offset_y);
        self.pixel_scale = info.pixel_scale;
        self.base_scissor = match info.scissor
        {
            Some(scissor) =>
            {
                Some(glium::Rect
                {
                    left : scissor.x,
                    bottom : scissor.y,
                    width : scissor.w,
                    height : scissor.h
                })
            },
            None => None
        };
        self.full_scissor = None;
    }
    
    fn start_drawing(&mut self)
    {
        if self.frame.is_some()
        {
            panic!("Cannot call draw() twice without a present()");
        }

        if self.need_recalc_viewport
        {
            self.recalculate_viewport(self.base_width, self.base_height);

            self.need_recalc_viewport = false;
        }
        
        self.frame = Some(self.display.borrow().draw());
        
        self.frame.as_mut().unwrap().clear_color_and_depth((0.0, 0.0, 0.0, 1.0), 1.0);
        
        let mut view_transform = DrawTransform::from_array(&UNBORKED_IDENTITY_TRANSFORM);
        
        match self.viewport_mode
        {
            ViewportMode::Independent | ViewportMode::OneToOneDpiScaled | ViewportMode::OneToOneUnscaled =>
            {
                let (x, y) = self.viewport_offset;
                
                view_transform.scale(1.0 / self.scale_width, 1.0 / self.scale_height);
                view_transform.translate(x, y);
            },
            ViewportMode::Pixel =>
            {
                self.pixel_target.as_mut().unwrap().with_framebuffer_mut(|framebuffer|
                {
                    let (width, height) = framebuffer.get_dimensions();

                    framebuffer.clear_color_and_depth((0.0, 0.0, 0.0, 1.0), 1.0);

                    view_transform.scale(1.0 / width as f32, 1.0 / height as f32);
                });
            }
        }
        
        self.last_view_transform = view_transform.clone();
        self.flip_instance_buffers();
    }

    fn end_drawing(&mut self)
    {
        self.perform_draw_batch();
        self.perform_depth_sort_draw();
        self.depth_sort_queue.next_frame();

        self.display.borrow().get_context().flush();
    }
    
    fn present(&mut self)
    {
        if self.frame.is_some()
        {
            match self.viewport_mode
            {
                ViewportMode::Independent | ViewportMode::OneToOneDpiScaled | ViewportMode::OneToOneUnscaled => (),
                ViewportMode::Pixel =>
                {
                    // Low-res pixel to high-res pixel
                    
                    let mut transform = DrawTransform::from_array(&UNBORKED_UPSCALE_TRANSFORM);
                    
                    let params = glium::DrawParameters::default();

                    self.instance_buffer_src[0] = upscale_instance_from_transform(&transform);
                    self.instance_buffer[self.instance_buffer_flip].invalidate();
                    self.instance_buffer[self.instance_buffer_flip].write(&self.instance_buffer_src[..]);
                    
                    self.update_pixel_upscale();
                    
                    let uniform = uniform!
                    {
                        t_texture : Sampler::new(self.pixel_target.as_ref().unwrap().borrow_color())
                            .magnify_filter(MagnifySamplerFilter::Nearest)
                            .minify_filter(MinifySamplerFilter::Nearest)
                            .wrap_function(SamplerWrapFunction::Clamp)
                    };
                    
                    self.pixel_upscale_texture.as_mut().unwrap().as_surface().draw((&self.quad_buffer,
                        self.instance_buffer[self.instance_buffer_flip].slice(0..1)
                            .expect("Failed to prepare instance buffer").per_instance()
                            .expect("Failed to prepare instance buffer")),
                        &self.index_buffer, &self.shader_list[RendererOp::Sprite as usize],
                        &uniform, &params).expect("Draw operation failed");
                    
                    // High-res pixel to screen
                    
                    let uniform = uniform!
                    {
                        t_texture : Sampler::new(self.pixel_upscale_texture.as_ref().unwrap())
                            .magnify_filter(MagnifySamplerFilter::Linear)
                            .minify_filter(MinifySamplerFilter::Linear)
                            .wrap_function(SamplerWrapFunction::Clamp)
                    };
                    
                    let (x, y) = self.viewport_offset;
                    transform.scale(1.0 / self.scale_width, 1.0 / self.scale_height);
                    transform.translate(x, y);

                    self.instance_buffer_src[0] = upscale_instance_from_transform(&transform);
                    self.instance_buffer[self.instance_buffer_flip].invalidate();
                    self.instance_buffer[self.instance_buffer_flip].write(&self.instance_buffer_src[..]);
                    
                    self.frame.as_mut().unwrap().draw((&self.quad_buffer,
                        self.instance_buffer[self.instance_buffer_flip].slice(0..1)
                            .expect("Failed to prepare instance buffer").per_instance()
                            .expect("Failed to prepare instance buffer")),
                        &self.index_buffer, &self.shader_list[RendererOp::Sprite as usize],
                        &uniform, &params).expect("Draw operation failed");
                }
            }
            
            let mut frame = None;        
            std::mem::swap(&mut frame, &mut self.frame);
            
            frame.unwrap().finish().expect("Draw finish failed");
        }
    }
    
    #[cfg(feature = "imgui_feature")]
    fn init_imgui_renderer(&mut self, imgui : &mut imgui::Context) -> Result<(), RendererError>
    {
        let mut imgui_borrow = self.imgui_renderer.borrow_mut();

        if imgui_borrow.is_some()
        {
            return Err(RendererError::ImguiRendererFailed("Already initialized".to_string()));
        }
        
        let mut imgui_renderer = imgui_glium_renderer::Renderer::init(
            imgui, &*self.display.borrow()).map_err(
            |error| RendererError::ImguiRendererFailed(format!("Could not initialize imgui renderer: {}", error)))?;

        imgui_renderer.textures().replace(imgui::TextureId::from(0), imgui_glium_renderer::Texture
        {
            texture : self.default_texture.clone(),
            sampler : SamplerBehavior
            {
                wrap_function: (SamplerWrapFunction::Repeat, SamplerWrapFunction::Repeat, SamplerWrapFunction::Repeat),
                minify_filter: MinifySamplerFilter::Linear,
                magnify_filter: MagnifySamplerFilter::Linear,
                .. Default::default()
            }
        });
        
        *imgui_borrow = Some(imgui_renderer);
        
        Ok(())
    }

    #[cfg(feature = "imgui_feature")]
    fn update_imgui_renderer(&mut self, imgui : &mut imgui::Context)
    {
        let mut imgui_borrow = self.imgui_renderer.borrow_mut();

        if let Some(imgui_renderer) = imgui_borrow.as_mut()
        {
            if !imgui.fonts().is_built()
            {
                imgui_renderer.reload_font_texture(imgui).expect("Font texture update failed");
            }
        }
    }
}
