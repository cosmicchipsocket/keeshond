//! Gameloop and [GameControl] context, which is used to control the game globally

use crate::datapack::DataMultistore;
use crate::datapack::source::SourceManager;
use crate::input::Input;
use crate::scene::{BaseScene, NullSpawnable, Scene, SceneConfig, SceneType};
#[cfg(feature = "audio")] use crate::audio::{AlAudio, Audio, AudioError, NullAudio};
use crate::renderer::{Renderer, RendererError, ViewportMode, WindowScaleMode};
#[cfg(feature = "imgui_feature")] use crate::imgui_handler::ImGuiHandler;
#[cfg(feature = "default_logger")] use crate::doglog;

use sdl2::event::Event;
use sdl2::event::WindowEvent;
use sdl2::keyboard::Mod as KeyboardMod;
use sdl2::messagebox;

#[cfg(feature = "graphical_panic")] use std::panic::PanicInfo;
#[cfg(feature = "graphical_panic")] use backtrace::Backtrace;

#[cfg(feature = "imgui_feature")] use imgui;

use std::time::{Duration, Instant};
use std::rc::Rc;
use std::cell::RefCell;
use std::collections::HashMap;
use std::any::TypeId;
use downcast_rs::Downcast;
use std::sync::{Arc, Mutex, MutexGuard};
use crate::doglog::ConsoleHistory;
use crate::renderer::draw_control_private::RendererNewInfo;

const MILLISECONDS_PER_NANOSECOND : f64 = 0.000001;
const BUSY_WAIT_MARGIN : u64 = 2; // For smooth results on Windows. Linux can probably get away with 1.
const AUTOMATIC_LERP_CAP_MULTIPLIER : f64 = 1.05;
const MAX_HITCH_MS : u64 = 100;
const MAX_POST_SLOWDOWN_CATCHUP : u32 = 6;
const MAX_FRAME_JIFFY_SUBTRACT : u32 = 1000;
const MAX_FPS_STAT_CATCHUP : u64 = 3;
const LOW_ENERGY_TIMER_MIN : u32 = 60;
const LOW_ENERGY_DRAW_RATE : u32 = 2;

#[derive(Debug)]
pub enum Sdl2Subsystem
{
    Main,
    Event,
    Video,
    GameController,
}

#[derive(Debug, Fail)]
pub enum InitError
{
    #[fail(display = "{}", _0)]
    KeeshondInitError(String),
    #[fail(display = "SDL {:?} subsystem: {}", _0, _1)]
    Sdl2InitError(Sdl2Subsystem, String),
    #[fail(display = "{}", _0)]
    RendererInitError(RendererError),
    #[cfg(feature = "audio")]
    #[fail(display = "{}", _0)]
    AudioInitError(AudioError),
    #[fail(display = "{}", _0)]
    ImguiInitError(String)
}

struct PlaceholderScene
{
    
}

impl SceneType for PlaceholderScene
{
    type SpawnableIdType = NullSpawnable;
    
    fn new() -> Self where Self : Sized { PlaceholderScene {} }
    fn config(&mut self, _game: &mut GameControl) -> SceneConfig<Self>
    {
        warn!("No scene loaded in gameloop!");

        SceneConfig::new()
    }
}

#[cfg(feature = "graphical_panic")]
fn panic_hook(info : &PanicInfo)
{
    // Get error message if that's what the payload happens to be
    let message : &str = match info.payload().downcast_ref::<String>()
    {
        Some(s) => s,
        None => match info.payload().downcast_ref::<&str>()
        {
            Some(s) => s,
            None => "No reason was given.",
        }
    };
    // Format with file/line info if available
    let mut cli_message = match info.location()
    {
        Some(location) => format!("A panic error occurred on {}:{}: \n{}",
                                location.file(),
                                location.line(),
                                message),
        None => format!("A panic error occurred: \n{}", message),
    };
    
    let bt = Backtrace::new();
    
    cli_message = format!("{}\n\n{:?}", cli_message, bt);
    println!("{}", &cli_message);
    
    let gui_message = format!("A panic error occurred.\n\n{}", message);
    
    match messagebox::show_simple_message_box(messagebox::MessageBoxFlag::ERROR, "Panic Error", &gui_message[..], None)
    {
        Ok(_) => (),
        Err(_) => println!("(Messagebox could not be displayed)"),
    };
    
    std::process::exit(1);
}

fn print_log_section(title : &str)
{
    info!("");
    info!("{:-^40}", format!(" {} ", title));
    info!("");
}

/// Displays a message popup in the GUI and prints the message to the console
#[allow(unused_variables)]
pub fn gui_cli_message(message : &str, title : &str)
{
    // Print to console first in case something weird happens
    println!("{}", message);
    
    // Show dialog box
    match messagebox::show_simple_message_box(messagebox::MessageBoxFlag::INFORMATION, title, &message[..], None)
    {
        Ok(_) => (),
        Err(_) => println!("(Messagebox could not be displayed)"),
    };
}

fn read_options() -> getopts::Matches
{
    let args : Vec<String> = std::env::args().collect();
    let mut options = getopts::Options::new();
    
    options.optflag("", "help", "Show this help");
    options.optflag("f", "fullscreen", "Run in fullscreen mode");
    options.optflag("w", "window", "Run in windowed mode");
    options.optflag("", "interpolate", "When an alternate refresh rate is detected, unlock display framerate from logic and enable frame interpolation.");
    options.optflag("", "no-interpolate", "Force disable frame interpolation and lock display rate to logic rate. Use when monitor's refresh rate equals game's logic rate.");
    options.optflag("", "force-interpolate", "Force enable frame interpolation. Not recommended if monitor's refresh rate equals game's logic rate!");
    options.optopt("", "interp-cap", "Upper framerate limit for frame interpolation, determined by FPS_LIMIT (\"auto\" for default, \"off\" to disable).", "FPS_LIMIT");
    options.optflag("", "kiosk", "Enable kiosk mode, which forces fullscreen and disables closing the window. Note that the user may still be able to close the program depending on your operating environment's global shortcuts.");
    
    match options.parse(&args[1..])
    {
        Ok(matches) =>
        {
            if matches.opt_present("help")
            {
                let message = options.usage("Commandline Parameters Help");
                gui_cli_message(&message, "Commandline Help");
                std::process::exit(1);
            }
            
            matches
        }
        Err(fail) =>
        {
            let message = format!("{}\n{}",
                                  fail,
                                  options.usage("Command parsing failed. Run without options to use defaults."));
            gui_cli_message(&message, "Commandline Help");
            std::process::exit(1);
        }
    }
}

fn apply_config(gameinfo : &mut GameInfo)
{
    // Get commandline options
    let matches = read_options();
    
    if matches.opt_present("window")
    {
        gameinfo.fullscreen = false;
    }
    else if matches.opt_present("fullscreen")
    {
        gameinfo.fullscreen = true;
    }
    
    if matches.opt_present("no-interpolate")
    {
        gameinfo.frame_interpolation = FrameInterpolationMode::Off;
    }
    else if matches.opt_present("interpolate")
    {
        gameinfo.frame_interpolation = FrameInterpolationMode::Automatic;
    }
    else if matches.opt_present("force-interpolate")
    {
        gameinfo.frame_interpolation = FrameInterpolationMode::Force;
    }

    match matches.opt_str("interp-cap")
    {
        Some(cap) =>
        {
            match cap.as_str()
            {
                "auto" => { gameinfo.frame_interpolation_cap = FrameInterpolationCap::Automatic; }
                "off" => { gameinfo.frame_interpolation_cap = FrameInterpolationCap::None; }
                _ =>
                {
                    match cap.parse::<f64>()
                    {
                        Ok(value) => { gameinfo.frame_interpolation_cap = FrameInterpolationCap::Manual(value); }
                        Err(_) => { gameinfo.frame_interpolation_cap = FrameInterpolationCap::Automatic; }
                    }
                }
            }
        },
        _ => ()
    };
    
    if matches.opt_present("kiosk")
    {
        gameinfo.kiosk_mode = true;
    }
}

/// Whether to use frame interpolation, which results in cleaner motion on high refresh rate displays for games that support it
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum FrameInterpolationMode
{
    /// Never do frame interpolation (recommended for games that don't support frame interpolation)
    Off,
    /// Do frame interpolation if the display refresh rate doesn't match the game's target framerate (recommended for games that support frame interpolation)
    Automatic,
    /// Always do frame interpolation (not recommended, but since automatic detection doesn't work for web builds, you might want to include this as an in-game option)
    Force
}

/// The framerate cap to use for when frame interpolation is enabled
#[derive(Copy, Clone, Debug, PartialEq)]
pub enum FrameInterpolationCap
{
    /// Sets the framerate limit to slightly above the monitor's current refresh rate. This improves the smoothness of
    ///  lerping when there is significant logic processing.
    Automatic,
    /// Sets the framerate limit to the specified amount (between 5-1000 FPS)
    Manual(f64),
    /// Uncaps the framerate entirely (not recommended)
    None
}

/// Whether to space out display frames for smoother motion when the framerate is higher than the target rate and there is significant logic processing.
#[derive(Copy, Clone, Debug, PartialEq)]
pub enum FrameInterpolationPacing
{
    /// Regulate the timing of display frames so that they are more evenly spaced apart, albeit
    ///  lowering the framerate (except when it falls below the target framerate).
    PreferSmoothMotion,
    /// Don't regulate the timing of display frames, increasing framerate.
    PreferHighFramerate
}

/// Properties and metadata for this game
#[derive(Clone)]
pub struct GameInfo
{
    /// The name of the Rust package, used for some internal operations
    pub package_name : &'static str,
    /// Friendly, user-facing name, displayed in the window title
    pub friendly_name : &'static str,
    /// Base resolution width of the game graphics
    pub base_width : u32,
    /// Base resolution height of the game graphics
    pub base_height : u32,
    /// The window scale strategy to use when creating the window
    pub default_window_scale : WindowScaleMode,
    /// Whether to use texture filtering. Set to false for pixel art style games.
    pub texture_filtering : bool,
    /// The viewport mode to use, which determines how the game scales to different
    ///  resolutions.
    pub viewport_mode : ViewportMode,
    /// Whether to default to fullscreen as opposed to windowed mode
    pub fullscreen : bool,
    /// The framerate that game logic should run at.
    pub target_framerate : f64,
    /// Unlock display framerate from logic and enable frame interpolation for
    ///  alternate refresh rates or Variable Refresh Rate displays.
    pub frame_interpolation : FrameInterpolationMode,
    /// Optional framerate cap for frame interpolation mode, useful for keeping the
    ///  display framerate below the maximum supported VRR rate.
    pub frame_interpolation_cap : FrameInterpolationCap,
    /// Whether to space out display frames for smoother motion when the framerate is higher than
    ///  the target rate and there is significant logic processing.
    pub frame_interpolation_pacing : FrameInterpolationPacing,
    /// Whether to display the mouse cursor when it is over the window.
    pub cursor_visible : bool,
    /// Whether unused resources should be automatically unloaded between scenes.
    pub auto_unload : bool,
    /// Whether the computer should automatically go to sleep while the application is
    ///  running. Useful for editors and other utilities.
    pub allow_system_sleep : bool,
    /// Whether to reduce the framerate when the user is not interacting with the application.
    ///  Also pauses the application when it is not focused unless there is interaction.
    ///  Useful for editors and other utilities.
    pub low_energy_app : bool,
    /// Whether to print basic stats (number of entities and components, framerate, basic timing information)
    ///  to the log. Does nothing on release builds.
    pub log_stats : bool,
    /// A list of additional packages to include in the filter used by the default logger (DogLog).
    pub log_packages : Vec<&'static str>,
    /// Enable kiosk mode, which forces fullscreen and disables closing the window.
    ///  Note that the user may still be able to close the program depending on your
    ///  operating environment's global shortcuts.
    pub kiosk_mode : bool
}

impl Default for GameInfo
{
    fn default() -> GameInfo
    {
        GameInfo
        {
            package_name : "",
            friendly_name : "Untitled",
            base_width : 1280,
            base_height : 720,
            default_window_scale : WindowScaleMode::MultiplierDpiScaled(1.0),
            texture_filtering : true,
            viewport_mode : ViewportMode::Independent,
            fullscreen : false,
            kiosk_mode : false,
            cursor_visible : true,
            auto_unload : true,
            allow_system_sleep : false,
            low_energy_app : false,
            target_framerate : 60.0,
            frame_interpolation : FrameInterpolationMode::Off,
            frame_interpolation_cap : FrameInterpolationCap::Automatic,
            frame_interpolation_pacing : FrameInterpolationPacing::PreferSmoothMotion,
            log_stats : true,
            log_packages : Vec::new()
        }
    }
}

/// (Advanced) Provides basic diagnostic timekeeping facilities
pub struct GameloopStopwatch
{
    jiffies : Instant,
    jiffies_this_frame : Duration,
    jiffies_this_second : Duration,
    jiffies_last_frame : Duration,
    tics_this_frame : u32,
    tics_this_second : u32,
    frames_this_second : u32,
    in_frame : bool
}

impl GameloopStopwatch
{
    pub fn new() -> GameloopStopwatch
    {
        GameloopStopwatch
        {
            jiffies : Instant::now(),
            jiffies_this_frame : Duration::new(0, 0),
            jiffies_this_second : Duration::new(0, 0),
            jiffies_last_frame : Duration::new(0, 0),
            tics_this_frame : 0,
            tics_this_second : 0,
            frames_this_second : 0,
            in_frame : false
        }
    }
    
    pub fn begin_frame_timing(&mut self)
    {
        if self.in_frame
        {
            return;
        }
        
        self.in_frame = true;
        
        self.jiffies = Instant::now();
        self.jiffies_last_frame = self.jiffies_this_frame;
        self.jiffies_this_frame = Duration::new(0, 0);
        self.tics_this_frame = 0;
    }
    
    pub fn end_frame_timing(&mut self)
    {
        if !self.in_frame
        {
            return;
        }
        
        self.in_frame = false;
        
        self.jiffies_this_frame = Instant::now() - self.jiffies;
        self.tics_this_second += self.tics_this_frame;
        self.jiffies_this_second += self.jiffies_this_frame;
        self.frames_this_second += 1;
    }
    
    pub fn tic(&mut self)
    {
        self.tics_this_frame += 1;
    }
    
    pub fn new_second(&mut self)
    {
        self.jiffies_this_second = Duration::new(0, 0);
        self.tics_this_second = 0;
        self.frames_this_second = 0;
    }
    
    pub fn jiffies_this_frame(&self) -> Duration
    {
        if self.in_frame
        {
            return Instant::now() - self.jiffies;
        }
        
        self.jiffies_this_frame
    }
    
    pub fn jiffies_this_second(&self) -> Duration
    {
        self.jiffies_this_second
    }
    
    pub fn tics_this_frame(&self) -> u32
    {
        self.tics_this_frame
    }
    
    pub fn tics_this_second(&self) -> u32
    {
        self.tics_this_second
    }
    
    pub fn frames_this_second(&self) -> u32
    {
        self.frames_this_second
    }

    pub fn current_ms(&self) -> f64
    {
        self.jiffies_this_frame.as_nanos() as f64 * MILLISECONDS_PER_NANOSECOND
    }
    
    pub fn average_ms(&self) -> f64
    {
        self.jiffies_this_second.as_nanos() as f64 * MILLISECONDS_PER_NANOSECOND / self.frames_this_second as f64
    }
}

/// Trait for a struct that stores data global to the game - i.e. presists between scene switches,
///  and is accessible using [GameControl::singleton_mut()].
pub trait GameSingleton : Downcast
{

}

impl_downcast!(GameSingleton);

pub(crate) struct GameControlOptions<'a>
{
    console_history: Arc<Mutex<ConsoleHistory>>,
    sdl_context : Option<&'a sdl2::Sdl>
}

impl<'a> GameControlOptions<'a>
{
    /// Returns a configuration for creating the [GameControl] in headless mode (i.e. no window, renderer, or audio)
    pub fn headless() -> GameControlOptions<'a>
    {
        GameControlOptions
        {
            console_history : Arc::new(Mutex::new(ConsoleHistory::new())),
            sdl_context : None
        }
    }
}

/// Main context, for controlling the game while it is running. You don't normally need to create
///  this yourself as [Gameloop::new()] does this for you, but it can be useful to create this
///  directly if you need to run automated tests.
pub struct GameControl
{
    target_framerate : f64,
    frame_interpolation : FrameInterpolationMode,
    interpolation_cap : FrameInterpolationCap,
    interpolation_pacing : FrameInterpolationPacing,
    low_energy_app : bool,
    low_energy_timer : u32,
    auto_unload : bool,
    input : Input,
    renderer : Renderer,
    #[cfg(feature = "audio")]
    audio : Box<dyn Audio>,
    #[allow(dead_code)]
    controller_subsystem : Option<sdl2::GameControllerSubsystem>,
    controllers : HashMap<u32, sdl2::controller::GameController>,
    next_scene : Box<dyn BaseScene>,
    next_scene_waiting : bool,
    source_manager : Rc<RefCell<SourceManager>>,
    resources : DataMultistore,
    singletons : HashMap<TypeId, Box<dyn GameSingleton>>,
    console_history : Arc<Mutex<ConsoleHistory>>,
    stats : GameStats,
    focused : bool,
    done : bool,
    #[cfg(feature = "imgui_feature")]
    imgui_handler : Option<ImGuiHandler>
}

impl GameControl
{
    pub(crate) fn new(gameinfo : &GameInfo, options : GameControlOptions) -> Result<GameControl, InitError>
    {   
        let source_manager = Rc::new(RefCell::new(SourceManager::new()));
        
        let mut resources = DataMultistore::new(source_manager.clone());
        
        let mut renderer_new_info = None;
        #[cfg(feature = "audio")]
        let audio : Box<dyn Audio>;
        let controller_subsystem;
        let mut input;

        if let Some(sdl) = options.sdl_context
        {
            let video_subsystem = sdl.video().map_err(
                |error| InitError::Sdl2InitError(Sdl2Subsystem::Video, error))?;

            controller_subsystem = Some(sdl.game_controller().map_err(
                |error| InitError::Sdl2InitError(Sdl2Subsystem::GameController, error))?);

            #[cfg(feature = "audio")]
            {
                audio = Box::new(AlAudio::new(&mut resources).map_err(
                    |error| InitError::AudioInitError(error))?);
            }

            input = Input::new(Some(sdl.mouse()));
            input.set_cursor_visible(gameinfo.cursor_visible);

            if gameinfo.allow_system_sleep
            {
                video_subsystem.enable_screen_saver();
            }
            else
            {
                video_subsystem.disable_screen_saver();
            }

            renderer_new_info = Some(RendererNewInfo
            {
                width : gameinfo.base_width,
                height : gameinfo.base_height,
                default_window_scale : gameinfo.default_window_scale,
                texture_filtering : gameinfo.texture_filtering,
                kiosk_mode : gameinfo.kiosk_mode,
                fullscreen : gameinfo.fullscreen,
                viewport_mode : gameinfo.viewport_mode,
                video_subsystem,
                resources : &mut resources
            });
        }
        else
        {
            #[cfg(feature = "audio")]
            {
                audio = Box::new(NullAudio::new());
            }
            input = Input::new(None);
            controller_subsystem = None;
        }

        #[cfg(feature = "imgui_feature")]
        let mut imgui_handler = ImGuiHandler::new().map_err(
            |error| InitError::ImguiInitError(error))?;
        
        #[allow(unused_mut)] // mut used only by imgui feature
        let mut renderer = Renderer::new(renderer_new_info).map_err(
            |error| InitError::RendererInitError(error))?;
        
        #[cfg(feature = "imgui_feature")]
        renderer.control().init_imgui_renderer(imgui_handler.imgui_mut()).map_err(
            |error| InitError::RendererInitError(error))?;

        input.update_mouse_metrics(&renderer);
        
        Ok(GameControl
        {
            target_framerate : gameinfo.target_framerate,
            frame_interpolation : gameinfo.frame_interpolation,
            interpolation_cap : gameinfo.frame_interpolation_cap,
            interpolation_pacing : gameinfo.frame_interpolation_pacing,
            low_energy_app : gameinfo.low_energy_app,
            low_energy_timer : 0,
            auto_unload : gameinfo.auto_unload,
            input,
            renderer,
            #[cfg(feature = "audio")]
            audio,
            controller_subsystem,
            controllers : HashMap::new(),
            next_scene : Box::new(Scene::<PlaceholderScene>::new()),
            next_scene_waiting : false,
            source_manager,
            resources,
            console_history : options.console_history,
            stats : GameStats::new(),
            focused : true,
            done : false,
            singletons : HashMap::new(),
            #[cfg(feature = "imgui_feature")]
            imgui_handler : Some(imgui_handler),
        })
    }

    /// Creates a [GameControl] in a headless configuration, allowing you to run automated tests,
    ///  validation, or other processes without needing a gameloop, window, renderer, or audio.
    pub fn new_headless(gameinfo : &GameInfo) -> Result<GameControl, InitError>
    {
        GameControl::new(gameinfo, GameControlOptions::headless())
    }

    /// Retrieves the [Input] object for reading player input such as keyboard, mouse, and gamepad.
    pub fn input(&self) -> &Input
    {
        &self.input
    }

    /// Retrieves the [Input] object mutably for changing input bindings and deadzones
    pub fn input_mut(&mut self) -> &mut Input
    {
        &mut self.input
    }

    /// Retrieves the [Renderer] object to read various parameters for the renderer window
    pub fn renderer(&self) -> &Renderer
    {
        &self.renderer
    }

    /// Retrieves the [Renderer] object mutably to change various parameters for the renderer window
    pub fn renderer_mut(&mut self) -> &mut Renderer
    {
        &mut self.renderer
    }

    /// Retrieves the [Audio] object for reading information about audio playback
    #[cfg(feature = "audio")]
    pub fn audio(&self) -> &Box<dyn Audio>
    {
        &self.audio
    }

    /// Retrieves the [Audio] object mutably to control audio playback
    #[cfg(feature = "audio")]
    pub fn audio_mut(&mut self) -> &mut Box<dyn Audio>
    {
        &mut self.audio
    }

    #[cfg(feature = "audio")]
    fn sync_audio_store(&mut self)
    {
        self.audio.sync_sound_store(&mut self.resources.store_mut())
            .expect("Audio backend store sync failed");
    }

    /// Retrieves the [SourceManager] for registering and querying data [Sources](crate::datapack::source::Source)
    pub fn source_manager(&self) -> Rc<RefCell<SourceManager>>
    {
        self.source_manager.clone()
    }

    /// Retrieves the [DataMultistore] for accessing data resources
    pub fn res(&self) -> &DataMultistore
    {
        &self.resources
    }

    #[allow(dead_code)]
    pub(crate) fn res_mut(&mut self) -> &mut DataMultistore
    {
        &mut self.resources
    }

    /// Retrieves the [GameSingleton] of the current type mutably. If it has not been created yet,
    ///  it will be [Default]-constructed.
    pub fn singleton_mut<T : 'static + GameSingleton + Default>(&mut self) -> &mut T
    {
        let type_id = TypeId::of::<T>();

        if !self.singletons.contains_key(&type_id)
        {
            self.singletons.insert(type_id.clone(), Box::new(T::default()));
        }

        self.singletons.get_mut(&type_id).unwrap().downcast_mut::<T>().expect("Downcast failed!")
    }

    /// Retrieves the imgui context
    #[cfg(feature = "imgui_feature")]
    pub fn imgui(&self) -> Option<&imgui::Context>
    {
        if let Some(imgui_handler) = &self.imgui_handler
        {
            return Some(imgui_handler.imgui());
        }

        None
    }

    /// Retrieves the imgui context mutably
    #[cfg(feature = "imgui_feature")]
    pub fn imgui_mut(&mut self) -> Option<&mut imgui::Context>
    {
        if let Some(imgui_handler) = &mut self.imgui_handler
        {
            return Some(imgui_handler.imgui_mut());
        }

        None
    }

    /// Returns the framerate that game logic is synchronized to, in frames per second
    pub fn target_framerate(&self) -> f64
    {
        self.target_framerate
    }

    /// Sets the framerate that game logic is synchronized to, in frames per second
    pub fn set_target_framerate(&mut self, new_framerate : f64)
    {
        self.target_framerate = new_framerate.clamp(5.0, 1000.0);
    }

    /// Returns the mode to use for interpolating frames in the renderer.
    pub fn frame_interpolation(&self) -> FrameInterpolationMode
    {
        self.frame_interpolation
    }

    /// Sets the mode to use for interpolating frames in the renderer.
    pub fn set_frame_interpolation(&mut self, mode : FrameInterpolationMode)
    {
        self.frame_interpolation = mode;
    }

    /// Returns the framerate cap mode for when frame interpolation is enabled.
    pub fn frame_interpolation_cap(&self) -> FrameInterpolationCap
    {
        self.interpolation_cap
    }

    /// Sets the framerate cap mode for when frame interpolation is enabled.
    pub fn set_frame_interpolation_cap(&mut self, mut cap : FrameInterpolationCap)
    {
        if let FrameInterpolationCap::Manual(value) = cap
        {
            cap = FrameInterpolationCap::Manual(value.clamp(5.0, 1000.0));
        }

        self.interpolation_cap = cap;
    }

    /// Returns the framerate pacing mode for when frame interpolation is enabled.
    pub fn frame_interpolation_pacing(&self) -> FrameInterpolationPacing
    {
        self.interpolation_pacing
    }

    /// Sets the framerate pacing mode for when frame interpolation is enabled.
    pub fn set_frame_interpolation_pacing(&mut self, pacing : FrameInterpolationPacing)
    {
        self.interpolation_pacing = pacing;
    }

    /// Returns true if frame interpolation is currently active. For the automatic mode, returns
    ///  true if the refresh rate of the window's current monitor does not match with the target
    ///  logic framerate.
    pub fn frame_interpolation_active(&self) -> bool
    {
        match self.frame_interpolation
        {
            FrameInterpolationMode::Off => { return false; }
            FrameInterpolationMode::Automatic =>
            {
                let current_refresh_rate = self.renderer.current_refresh_rate();

                if current_refresh_rate == 0
                {
                    return false;
                }

                ((self.target_framerate.round() as i32) - current_refresh_rate).abs() > 1
            }
            FrameInterpolationMode::Force => { return true; }
        }
    }

    /// Retrieves the [ConsoleHistory] object, which is useful for implementing an in-game console window
    pub fn console_history(&self) -> MutexGuard<'_, ConsoleHistory>
    {
        self.console_history.lock().unwrap()
    }

    /// Retrieves the [GameStats] object, which contains timing statistics
    pub fn stats(&self) -> &GameStats
    {
        &self.stats
    }

    /// Switches the game to a new scene of the given [SceneType] after the current frame is done.
    ///  Doing this will perform automatic unloading of unused data resources (ones without an active
    ///  [crate::datapack::DataHandle] or [crate::datapack::PackageUseToken]) if it is configured to
    ///  do so via the [GameInfo] passed in during creation of the [Gameloop] or [GameControl].
    pub fn goto_new_scene<T : SceneType + 'static>(&mut self)
    {
        let next_scene = Box::new(Scene::<T>::new());

        self.next_scene = next_scene;
        self.next_scene_waiting = true;
    }

    /// Leaves the main gameloop. This usually means closing the game.
    pub fn quit(&mut self)
    {
        self.done = true;
    }
}

struct GameloopTimeState
{
    jiffies : Instant,
    last_jiffies : Instant,
    last_fps_jiffies : Instant,
    jiffy_queue : Duration,
    target_frametime : Duration,
    cap_frametime : Duration,
    frame_start_jiffies : Duration,
    first_draw_jiffies : Duration,
    last_display : Instant,
    #[cfg(feature = "imgui_feature")]
    last_imgui : Instant,
    lag_this_frame : u32,
    lag_this_second : u32,
    thought_yet : bool,
    drawn_thought_yet : bool,
    lag_recovery : bool,
    refresh_rate : i32,
    think_timer : GameloopStopwatch,
    draw_timer : GameloopStopwatch,
    wait_timer : GameloopStopwatch,
    present_timer : GameloopStopwatch
}

impl GameloopTimeState
{
    fn new() -> GameloopTimeState
    {
        let jiffies = Instant::now();
        let last_jiffies = jiffies;
        
        GameloopTimeState
        {
            jiffies,
            last_jiffies,
            last_fps_jiffies : Instant::now(),
            jiffy_queue : Duration::new(0, 0),
            target_frametime : Duration::new(0, 0),
            cap_frametime : Duration::new(0, 0),
            frame_start_jiffies : Duration::new(0, 0),
            first_draw_jiffies : Duration::new(0, 0),
            last_display : Instant::now(),
            #[cfg(feature = "imgui_feature")]
            last_imgui : Instant::now(),
            lag_this_frame : 0,
            lag_this_second : 0,
            thought_yet : false,
            drawn_thought_yet : false,
            lag_recovery : false,
            refresh_rate : 0,
            think_timer : GameloopStopwatch::new(),
            draw_timer : GameloopStopwatch::new(),
            wait_timer : GameloopStopwatch::new(),
            present_timer : GameloopStopwatch::new()
        }
    }

    fn flush(&mut self)
    {
        if self.jiffy_queue > self.target_frametime * MAX_FRAME_JIFFY_SUBTRACT
        {
            self.jiffy_queue = Duration::new(0, 0);
        }
        else
        {
            while self.jiffy_queue > self.target_frametime
            {
                self.jiffy_queue -= self.target_frametime;
            }
        }

        self.new_second();

        self.jiffies = Instant::now();
        self.last_jiffies = self.jiffies;
        self.last_fps_jiffies = Instant::now();
        self.last_display = self.jiffies;
        self.thought_yet = false;
    }
    
    fn new_second(&mut self)
    {
        self.think_timer.new_second();
        self.draw_timer.new_second();
        self.wait_timer.new_second();
        self.present_timer.new_second();

        if self.jiffies > self.last_fps_jiffies + Duration::from_secs(MAX_FPS_STAT_CATCHUP)
        {
            self.last_fps_jiffies = self.jiffies;
        }
        else
        {
            self.last_fps_jiffies += Duration::from_secs(1);
        }

        self.lag_this_second = 0;
    }
    
    fn update_jiffies(&mut self)
    {
        self.jiffies = Instant::now();
        self.jiffy_queue += self.jiffies - self.last_jiffies;
        self.last_jiffies = self.jiffies;
    }
    
    fn sleep(&mut self, framerate_wait_target : Duration, low_power : bool)
    {
        while self.jiffy_queue <= framerate_wait_target
        {
            self.update_jiffies();

            let remaining = framerate_wait_target.saturating_sub(self.jiffy_queue);

            // Sleep as long as we reliably can, then busy wait
            if remaining >= Duration::from_millis(BUSY_WAIT_MARGIN) || low_power
            {
                std::thread::sleep(Duration::from_millis(1));
            }
        }
    }
    
    fn interpolate_sleep(&mut self, framerate_wait_target : Duration, low_power : bool)
    {
        let mut current_display = Instant::now();

        while current_display - self.last_display <= framerate_wait_target
        {
            let remaining = framerate_wait_target.saturating_sub(current_display - self.last_display);

            // Sleep as long as we reliably can, then busy wait
            if remaining >= Duration::from_millis(BUSY_WAIT_MARGIN) || low_power
            {
                std::thread::sleep(Duration::from_millis(1));
            }

            current_display = Instant::now();
        }
        
        self.last_display = Instant::now();
    }
}

/// Contains timing statistics such as FPS and milliseconds taken for each part of the gameloop.
#[derive(Clone)]
pub struct GameStats
{
    pub draw_fps : u32,
    pub think_fps : u32,
    pub frame_ms_think : f64,
    pub frame_ms_draw : f64,
    pub frame_ms_present : f64,
    pub frame_ms_wait : f64,
    pub average_ms_think : f64,
    pub average_ms_draw : f64,
    pub average_ms_present : f64,
    pub average_ms_wait : f64
}

impl GameStats
{
    pub fn new() -> GameStats
    {
        GameStats
        {
            draw_fps : 0,
            think_fps : 0,
            frame_ms_think : 0.0,
            frame_ms_draw : 0.0,
            frame_ms_present : 0.0,
            frame_ms_wait : 0.0,
            average_ms_think : 0.0,
            average_ms_draw : 0.0,
            average_ms_present : 0.0,
            average_ms_wait : 0.0
        }
    }

    fn update_frame_timings(&mut self, time : &GameloopTimeState)
    {
        self.frame_ms_think = time.think_timer.current_ms();
        self.frame_ms_draw = time.draw_timer.current_ms();
        self.frame_ms_present = time.present_timer.current_ms();
        self.frame_ms_wait = time.wait_timer.current_ms();
    }

    fn update_second_timings(&mut self, time : &GameloopTimeState)
    {
        self.draw_fps = time.draw_timer.frames_this_second();
        self.think_fps = time.think_timer.tics_this_second();
        self.average_ms_think = time.think_timer.average_ms();
        self.average_ms_draw = time.draw_timer.average_ms();
        self.average_ms_present = time.present_timer.average_ms();
        self.average_ms_wait = time.wait_timer.average_ms();
    }
}

/// Holds the main game context and performs event handling and timekeeping
///  automatically. One of the first structs you will instantiate in your program.
pub struct Gameloop
{
    #[allow(dead_code)] // Only used by the imgui integration feature
    sdl_context : sdl2::Sdl,
    event_pump : sdl2::EventPump,
    event_queue : Vec<sdl2::event::Event>,
    control : Box<GameControl>,
    current_scene : Box<dyn BaseScene>,
    time : Box<GameloopTimeState>,
    log_stats : bool,
    kiosk_mode : bool
}

impl Gameloop
{
    /// Creates the main game context. Will panic if startup fails for any reason.
    pub fn new(gameinfo : GameInfo) -> Gameloop
    {
        Gameloop::try_new(gameinfo).unwrap_or_else(|error| panic!("Startup failed: {}", error))
    }

    /// Creates the main game context. Will return an error instead of panicking if startup fails for any reason.
    pub fn try_new(gameinfo : GameInfo) -> Result<Gameloop, InitError>
    {
        let gameloop_init_start = Instant::now();

        // Messagebox panic handler
        #[cfg(feature = "graphical_panic")]
        std::panic::set_hook(Box::new(panic_hook));

        let mut gameinfo = gameinfo.clone();
        
        if gameinfo.package_name.is_empty()
        {
            return Err(InitError::KeeshondInitError(String::from("Package name cannot be empty.")));
        }

        let console_history = Arc::new(Mutex::new(ConsoleHistory::new()));
        
        #[cfg(feature = "default_logger")]
        {
            let mut logger = doglog::DogLog::new(console_history.clone());
            logger.add_package_filter(String::from(gameinfo.package_name));

            for package in &gameinfo.log_packages
            {
                logger.add_package_filter(package.to_string());
            }

            #[cfg(debug_assertions)]
            let filter = log::LevelFilter::Debug;
            #[cfg(not(debug_assertions))]
            let filter = log::LevelFilter::Info;
            
            if let Err(_) = logger.install(filter)
            {
                eprintln!("Could not initialize default logger.");
            }
        }
        
        apply_config(&mut gameinfo);
        
        print_log_section("KEESHOND Game Engine");
        
        let version = sdl2::version::version();
        info!("Starting SDL version {}", version);
        
        // Subsystems init
        let sdl_context = sdl2::init().map_err(
            |error| InitError::Sdl2InitError(Sdl2Subsystem::Main, error))?;
        let event_pump = sdl_context.event_pump().map_err(
            |error| InitError::Sdl2InitError(Sdl2Subsystem::Event, error))?;
        
        #[allow(unused_mut)] // mut used only by imgui feature
        let mut control = Box::new(GameControl::new(&gameinfo, GameControlOptions
        {
            console_history,
            sdl_context : Some(&sdl_context)
        }).map_err(|error| error)?);
        
        control.renderer_mut().set_base_size(gameinfo.base_width as f32, gameinfo.base_height as f32);
        control.renderer_mut().set_window_title(&gameinfo.friendly_name);

        let gameloop_init_time = (Instant::now() - gameloop_init_start).as_millis();

        info!("Initialized in {} ms", gameloop_init_time);
        
        Ok(Gameloop
        {
            sdl_context,
            event_pump,
            event_queue : Vec::new(),
            control,
            current_scene : Box::new(Scene::<PlaceholderScene>::new()),
            time : Box::new(GameloopTimeState::new()),
            log_stats : gameinfo.log_stats,
            kiosk_mode : gameinfo.kiosk_mode
        })
    }
    
    pub fn control(&self) -> &GameControl
    {
        &self.control
    }
    
    pub fn control_mut(&mut self) -> &mut GameControl
    {
        &mut self.control
    }

    fn think(&mut self)
    {
        let target_ms = 1000.0 / self.control.target_framerate();
        let cap_ms = match self.control.frame_interpolation_cap()
        {
            FrameInterpolationCap::Automatic =>
            {
                1000.0 / (self.control.renderer.current_refresh_rate() as f64
                    * AUTOMATIC_LERP_CAP_MULTIPLIER)
            }
            FrameInterpolationCap::Manual(value) => { 1000.0 / value }
            FrameInterpolationCap::None => { 0.0 }
        };
        self.time.target_frametime = Duration::from_nanos((target_ms * 1_000_000.0) as u64);
        self.time.cap_frametime = Duration::from_nanos((cap_ms * 1_000_000.0) as u64);

        if self.time.jiffy_queue <= self.time.target_frametime
        {
            return;
        }

        self.time.think_timer.begin_frame_timing();

        #[cfg(feature = "audio")]
        self.control.sync_audio_store();

        self.time.lag_this_frame = 0;
        
        while self.time.jiffy_queue > self.time.target_frametime
        {
            // Don't jump ahead too far after lag ends
            if self.time.lag_recovery
            {
                if self.time.jiffy_queue > self.time.target_frametime * MAX_POST_SLOWDOWN_CATCHUP
                {
                    self.time.jiffy_queue = self.time.target_frametime * MAX_POST_SLOWDOWN_CATCHUP;
                }

                self.time.lag_recovery = false;
            }

            self.control.input.update_actions();

            self.current_scene.think(&mut self.control);
            
            self.time.think_timer.tic();
            self.time.thought_yet = true;
            self.time.drawn_thought_yet = false;
            self.control.low_energy_timer += 1;

            let max_think_jiffies = self.time.target_frametime;

            // Prevent overflow if CPU cannot process all the frames
            //  Scenario 1: Hitch for a single frame. Could be OS taking resource time away from
            //              the program for a brief moment, or a long initialization process. For
            //              this we should allow for a single shot of several frames worth of lag.
            //  Scenario 2: Continuous slow processing. Game is simply doing too much work over
            //              multiple frames. If previous think took longer than a frame and this
            //              think is also taking longer than a frame, stop processing for smoother
            //              slowdown should it occur.
            if self.time.think_timer.jiffies_this_frame() > max_think_jiffies + Duration::from_millis(MAX_HITCH_MS)
                || (self.time.think_timer.jiffies_this_frame() > max_think_jiffies
                && self.time.think_timer.jiffies_last_frame > max_think_jiffies)
            {
                while self.time.jiffy_queue > self.time.target_frametime
                {
                    self.time.jiffy_queue -= self.time.target_frametime;
                    self.time.lag_this_frame += 1;
                    self.time.lag_this_second += 1;
                    self.time.lag_recovery = true;

                    // Don't spend a really long time if the jiffy queue happens to be really large
                    if self.time.lag_this_frame > MAX_FRAME_JIFFY_SUBTRACT
                    {
                        break;
                    }
                }
                break;
            }

            self.time.jiffy_queue -= self.time.target_frametime;
        }
        
        self.time.think_timer.end_frame_timing();

        self.time.frame_start_jiffies = self.time.jiffy_queue;
    }
    
    fn draw(&mut self)
    {
        self.time.draw_timer.begin_frame_timing();

        #[cfg(feature = "imgui_feature")]
        {
            if !self.imgui_draw()
            {
                self.scene_draw();
            }
        }

        #[cfg(not(feature = "imgui_feature"))]
        self.scene_draw();

        if self.time.thought_yet && self.should_draw()
        {
            self.control.renderer.control().end_drawing();
        }
        
        self.time.draw_timer.end_frame_timing();

        if !self.time.drawn_thought_yet
        {
            self.time.first_draw_jiffies = self.time.draw_timer.jiffies_this_frame;
            self.time.drawn_thought_yet = true;
        }
    }

    fn scene_draw(&mut self)
    {
        if self.time.thought_yet && self.should_draw()
        {
            let mut interpolation_amount = 0.0;

            if self.control.frame_interpolation_active() && self.time.lag_this_frame == 0
                && !self.low_energy_timer_expired()
            {
                let jiffy_offset = self.time.jiffy_queue.as_nanos() as f64;
                let jiffy_target = self.time.target_frametime.as_nanos() as f64;

                interpolation_amount = ((jiffy_offset / jiffy_target) - 1.0)
                    .clamp(-1.0, 0.0);
            }

            self.control.renderer.draw(&mut self.current_scene, &mut self.control.resources, interpolation_amount as f32);
        }
    }

    #[cfg(feature = "imgui_feature")]
    fn imgui_draw(&mut self) -> bool
    {
        let mut imgui_handler : Option<ImGuiHandler> = None;

        let frametime = self.time.target_frametime * self.time.think_timer.tics_this_frame();
        let (width, height) = self.control.renderer.window_size();
        let scale = self.control.renderer.current_display_scale_factor();

        let frame_delta = match self.control.frame_interpolation_active()
        {
            false => { frametime.as_nanos() as f64 * MILLISECONDS_PER_NANOSECOND / 1000.0 }
            true =>
            {
                let now = Instant::now();
                let delta = now - self.time.last_imgui;
                self.time.last_imgui = now;

                delta.as_nanos() as f64 * MILLISECONDS_PER_NANOSECOND / 1000.0
            }
        };

        std::mem::swap(&mut imgui_handler, &mut self.control.imgui_handler);

        if let Some(imgui_some) = &mut imgui_handler
        {
            imgui_some.update_size_and_delta(frame_delta as f32, width as f32, height as f32, scale);
            self.control.renderer.update_imgui(imgui_some.imgui_mut());

            let mut ui = imgui_some.imgui_mut().frame();

            self.current_scene.imgui_think(&mut ui, &mut self.control);

            self.scene_draw();

            let imgui_cursor = ui.mouse_cursor();

            self.control.renderer.draw_imgui(ui, &mut self.control.resources);

            imgui_some.update_cursor(&mut self.sdl_context.mouse(), imgui_cursor);
        }

        std::mem::swap(&mut imgui_handler, &mut self.control.imgui_handler);

        true
    }
    
    fn wait(&mut self, do_sleep : bool)
    {
        if !do_sleep
        {
            self.time.update_jiffies();
            return;
        }

        self.time.wait_timer.begin_frame_timing();

        let low_energy = self.low_energy_timer_expired();
        
        if !self.control.frame_interpolation_active() || low_energy
        {
            self.time.sleep(self.time.target_frametime, low_energy);
        }
        else
        {
            let mut cap_frametime = self.time.cap_frametime;

            if self.control.interpolation_pacing != FrameInterpolationPacing::PreferHighFramerate
            {
                let pacing_target = self.time.think_timer.jiffies_this_frame
                    + self.time.first_draw_jiffies;

                cap_frametime = self.time.cap_frametime.max(pacing_target)
                    .min(self.time.target_frametime);
            }

            if !cap_frametime.is_zero()
            {
                self.time.interpolate_sleep(cap_frametime, low_energy);
            }
            else
            {
                self.time.last_display = Instant::now();
            }

            self.time.update_jiffies();
        }
        
        self.time.wait_timer.end_frame_timing();
    }
    
    fn present(&mut self)
    {
        self.time.present_timer.begin_frame_timing();

        if self.should_draw()
        {
            self.control.renderer.present();
        }
        
        self.time.present_timer.end_frame_timing();
    }

    fn low_energy_timer_expired(&self) -> bool
    {
        self.control.low_energy_app && self.control.low_energy_timer > LOW_ENERGY_TIMER_MIN
    }

    fn should_draw(&self) -> bool
    {
        !self.low_energy_timer_expired() || self.control.low_energy_timer % LOW_ENERGY_DRAW_RATE == 0
    }

    fn process_event(&mut self, event : Event) -> bool
    {
        self.control.low_energy_timer = 0;

        #[cfg(feature = "imgui_feature")]
        {
            if let Some(imgui_handler) = &mut self.control.imgui_handler
            {
                if imgui_handler.handle_event(&event)
                {
                    return true;
                }
            }
        }

        match event
        {
            Event::Quit { .. } =>
            {
                if !self.kiosk_mode
                {
                    return false;
                }
            },
            Event::Window { win_event, .. } => match win_event
            {
                WindowEvent::FocusGained { .. } =>
                {
                    self.control.focused = true;
                },
                WindowEvent::FocusLost { .. } =>
                {
                    self.control.focused = false;
                },
                WindowEvent::Moved { .. } | WindowEvent::SizeChanged { .. } =>
                {
                    self.control.renderer.recalculate_viewport();
                    self.control.input.update_mouse_metrics(&self.control.renderer);
                }
                _ => ()
            },
            Event::KeyDown { keycode, keymod, repeat, .. } =>
            {
                if !repeat && (keymod.contains(KeyboardMod::LALTMOD)
                    || keymod.contains(KeyboardMod::RALTMOD))
                    && keycode == Some(sdl2::keyboard::Keycode::Return)
                    && !self.kiosk_mode
                {
                    self.control.renderer.toggle_fullscreen();
                }
                else if !repeat
                {
                    if let Some(some_keycode) = keycode
                    {
                        self.control.input.update_key(&some_keycode.name(), true, true);
                    }
                }
            },
            Event::KeyUp { keycode, .. } =>
            {
                if let Some(some_keycode) = keycode
                {
                    self.control.input.update_key(&some_keycode.name(), false, true);
                }
            },
            Event::MouseMotion { x, y, xrel, yrel, .. } =>
            {
                self.control.input.update_mouse_movement(x, y, xrel, yrel);
            },
            Event::MouseButtonDown { mouse_btn, .. } =>
            {
                self.control.input.update_mouse_button(mouse_btn, true);
            },
            Event::MouseButtonUp { mouse_btn, .. } =>
            {
                self.control.input.update_mouse_button(mouse_btn, false);
            },
            Event::MouseWheel { x, y, .. } =>
            {
                self.control.input.update_mouse_wheel(x, y);
            },
            Event::ControllerDeviceAdded { which, .. } =>
            {
                if let Some(controller_subsystem) = &self.control.controller_subsystem
                {
                    match controller_subsystem.open(which)
                    {
                        Ok(controller) =>
                        {
                            info!("Attached controller {}: {}", which, controller.name());
                            self.control.controllers.insert(which, controller);
                        },
                        Err(error) =>
                        {
                            error!("Could not open controller: {}", error.to_string());
                        }
                    }
                }
            }
            Event::ControllerDeviceRemoved { which, .. } =>
            {
                if let Some(controller) = self.control.controllers.remove(&which)
                {
                    info!("Detached controller {}: {}", which, controller.name());
                }
            }
            Event::ControllerButtonDown { button, .. } =>
            {
                self.control.input.update_gamepad_key(button, true);
            },
            Event::ControllerButtonUp { button, .. } =>
            {
                self.control.input.update_gamepad_key(button, false);
            },
            Event::ControllerAxisMotion { axis, value, .. } =>
            {
                self.control.input.update_gamepad_axis(axis, value);
            },
            _ => ()
        }

        return true;
    }
    
    fn update_fps(&mut self)
    {
        self.control.stats.update_frame_timings(&self.time);

        let current_refresh_rate = self.control.renderer.current_refresh_rate();

        if self.time.refresh_rate != current_refresh_rate
        {
            self.time.refresh_rate = current_refresh_rate;
            info!("Refresh rate: {}hz", current_refresh_rate);
        }

        if self.time.jiffies < self.time.last_fps_jiffies + Duration::from_secs(1)
        {
            return;
        }

        self.control.stats.update_second_timings(&self.time);

        let lerp_active = match self.control.frame_interpolation_active()
        {
            true => { "ON" }
            false => { "OFF" }
        };

        if self.log_stats
        {
            debug!("{} ent {} com  |  {} / {} FPS  |  avg ms  think {:.2}  draw {:.2}  present {:.2}  wait {:.2}  |  lerp {}",
                   self.current_scene.entity_count(),
                   self.current_scene.component_count(),
                   self.time.draw_timer.frames_this_second(),
                   self.time.think_timer.tics_this_second(),
                   self.time.think_timer.average_ms(),
                   self.time.draw_timer.average_ms(),
                   self.time.present_timer.average_ms(),
                   self.time.wait_timer.average_ms(),
                   lerp_active);
        }

        #[cfg(debug_assertions)]
        if self.time.lag_this_second > 0
        {
            warn!("Too much logic processing, game is slowing down!");
        }
        
        self.time.new_second();
    }

    pub fn single_frame(&mut self, do_sleep : bool) -> bool
    {
        if self.low_energy_timer_expired() && !self.control.focused && do_sleep
        {
            self.event_queue.push(self.event_pump.wait_event());

            self.time.flush();
        }

        self.control.input.pre_update();

        // Consume events
        for event in self.event_pump.poll_iter()
        {
            self.event_queue.push(event);
        }

        let mut event_queue = Vec::new();

        std::mem::swap(&mut self.event_queue, &mut event_queue);

        for event in event_queue.drain(..)
        {
            if !self.process_event(event)
            {
                return false;
            }
        }

        std::mem::swap(&mut self.event_queue, &mut event_queue);

        self.think();
        self.draw();
        self.wait(do_sleep);
        self.present();

        self.update_fps();

        // Go to next scene if one is requested
        if self.control.next_scene_waiting
        {
            self.current_scene.end(&mut self.control);

            if self.control.auto_unload
            {
                self.control.resources.unload_unused();
            }

            print_log_section("NEW SCENE");

            std::mem::swap(&mut self.current_scene, &mut self.control.next_scene);
            self.control.next_scene = Box::new(Scene::<PlaceholderScene>::new());

            let scene_init_start = Instant::now();

            self.current_scene.start(&mut self.control);

            let scene_init_time = (Instant::now() - scene_init_start).as_millis();
            info!("Scene started in {} ms", scene_init_time);

            // Reset jiffy queue
            self.time.flush();
            self.control.next_scene_waiting = false;
        }

        return !self.control.done;
    }

    #[allow(unused_mut)]
    pub fn run(mut self)
    {
        #[cfg(not(target_os = "emscripten"))]
        self.run_desktop();
        #[cfg(target_os = "emscripten")]
        self.run_web();
    }

    #[cfg(not(target_os = "emscripten"))]
    pub fn run_desktop(&mut self)
    {
        print_log_section("START GAMELOOP");
        
        self.time = Box::new(GameloopTimeState::new());

        loop
        {
            if !self.single_frame(true)
            {
                break;
            }
        }
        
        print_log_section("END GAMELOOP");
        
        self.current_scene.end(&mut self.control);
    }

    #[cfg(target_os = "emscripten")]
    pub fn run_web(mut self)
    {
        print_log_section("START GAMELOOP");

        self.time = Box::new(GameloopTimeState::new());

        let loop_closure = move ||
        {
            match self.single_frame(false)
            {
                true =>
                {
                    crate::thirdparty::emscripten::MainLoopEvent::Continue
                },
                false =>
                {
                    self.current_scene.end(&mut self.control);
                    return crate::thirdparty::emscripten::MainLoopEvent::Terminate;
                }
            }
        };

        crate::thirdparty::emscripten::set_main_loop_callback(loop_closure);
    }
}
