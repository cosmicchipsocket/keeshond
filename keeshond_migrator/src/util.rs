use std::marker::PhantomData;
use keeshond::datapack::{DataError, DataObject, TrustPolicy};
use keeshond::datapack::source::Source;

pub struct DataPlaceholder {}

impl DataObject for DataPlaceholder
{
    fn folder_name() -> &'static str where Self : Sized
    {
        unreachable!()
    }

    fn trust_policy() -> TrustPolicy
    {
        unreachable!()
    }

    fn want_file(_pathname : &str) -> bool where Self : Sized
    {
        unreachable!()
    }

    fn from_package_source(_source : &mut Box<dyn Source>, _package_name : &str, _pathname : &str) -> Result<Self, DataError> where Self : Sized
    {
        unreachable!()
    }
}

pub struct ExactStructDefaultIter<T : Default + 'static>
{
    count : usize,
    _phantom : PhantomData<T>
}

impl<T : Default + 'static> ExactStructDefaultIter<T>
{
    pub fn new(count : usize) -> ExactStructDefaultIter<T>
    {
        ExactStructDefaultIter
        {
            count,
            _phantom : PhantomData
        }
    }
}

impl<T : Default + 'static> Iterator for ExactStructDefaultIter<T>
{
    type Item = T;

    fn next(&mut self) -> Option<Self::Item>
    {
        if self.count > 0
        {
            self.count -= 1;
            return Some(T::default());
        }

        None
    }
}

impl<T : Default + 'static> ExactSizeIterator for ExactStructDefaultIter<T>
{
    fn len(&self) -> usize
    {
        self.count
    }
}

impl<T : Default + 'static> DoubleEndedIterator for ExactStructDefaultIter<T>
{
    fn next_back(&mut self) -> Option<Self::Item>
    {
        if self.count > 0
        {
            self.count -= 1;
            return Some(T::default());
        }

        None
    }
}
