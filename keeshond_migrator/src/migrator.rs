use std::collections::HashMap;

pub enum MigrationError
{
    AlreadyLatest,
    MigrateFailed(String)
}

struct MigratorFactoryEntry
{
    func : Box<dyn Fn(&str) -> Result<Migrator, String>>,
    description : &'static str
}

pub struct MigratorFactory
{
    entries : HashMap<String, MigratorFactoryEntry>
}

impl MigratorFactory
{
    pub fn new() -> MigratorFactory
    {
        MigratorFactory
        {
            entries : HashMap::new()
        }
    }

    pub fn register<T : Migratable + 'static>(&mut self)
    {
        self.entries.insert(T::id().to_string(), MigratorFactoryEntry
        {
            func : Box::new(|path|
            {
                Migrator::new::<T>(path)
            }),
            description : T::describe()
        });
    }

    pub fn find_migrator(&self, path : &str, ids_to_try : Vec<&'static str>) -> Result<Migrator, String>
    {
        if ids_to_try.is_empty()
        {
            return Err(String::from("No migratables provided"));
        }

        let mut errors = String::from(&format!("Could not find a migrator for {}: ", path));

        for id in &ids_to_try
        {
            if let Some(entry) = self.entries.get(*id)
            {
                match (entry.func)(path)
                {
                    Ok(migrator) => { return Ok(migrator) }
                    Err(error) =>
                    {
                        errors.push_str(&format!("\n{}: {}", entry.description, error));
                    }
                }
            }
            else
            {
                errors.push_str(&format!("\nID {} not registered", id));
            }
        }

        Err(errors)
    }
}

pub struct Migrator
{
    path : String,
    loaded_data : Box<dyn Migratable>
}

impl Migrator
{
    pub fn new<T : Migratable + 'static>(path : &str) -> Result<Migrator, String>
    {
        let loaded_data : Box<dyn Migratable> = match T::read_file(path)
        {
            Ok(data) =>
            {
                Box::new(data)
            }
            Err(error) =>
            {
                return Err(format!("Failed to read \"{}\": {}", path, error).to_string());
            }
        };

        Ok(Migrator
        {
            path : path.to_string(),
            loaded_data
        })
    }

    pub fn run(&mut self)
    {
        println!("Input: \"{}\" : {}", self.path, self.loaded_data.describe_instance());

        'migrate : loop
        {
            match self.loaded_data.migrate()
            {
                Ok(data) =>
                {
                    self.loaded_data = data;
                }
                Err(error) =>
                {
                    match error
                    {
                        MigrationError::AlreadyLatest =>
                        {
                            println!("Migrated to {}", self.loaded_data.describe_instance());
                            break 'migrate;
                        }
                        MigrationError::MigrateFailed(error_message) =>
                        {
                            eprintln!("Failed to migrate \"{}\": {}", self.path, error_message);
                            return;
                        }
                    }
                }
            }
        }

        if let Err(error) = self.loaded_data.write_file(&self.path)
        {
            eprintln!("Failed to write migrated file to \"{}\": {}", self.path, error);
            return;
        }

        println!("Finished migrating \"{}\"", self.path);
    }
}

pub trait Migratable
{
    fn id() -> &'static str where Self : Sized;
    fn describe() -> &'static str where Self : Sized;
    fn describe_instance(&self) -> &'static str;
    fn read_file(path : &str) -> Result<Self, String> where Self : Sized;
    fn migrate(&self) -> Result<Box<dyn Migratable>, MigrationError>;
    fn write_file(&self, path : &str) -> Result<(), String>;
}
