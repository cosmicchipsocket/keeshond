use std::collections::HashMap;
use std::fs::File;
use std::io::{BufReader, BufWriter};
use bincode::Options;
use serde::{Deserialize, Serialize};
use keeshond::datapack::DataHandle;
use keeshond::scene::DynArgList;
use keeshond::util::{BakedRect, NamedItemStore, SimpleTransform};
use crate::Migratable;
use crate::migrator::MigrationError;
use crate::util::DataPlaceholder;

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct BackgroundInfoV1
{
    pub r : f32,
    pub g : f32,
    pub b : f32
}

#[derive(Clone, Default, Serialize, Deserialize, Debug)]
struct SpawnableNameIdMapRawV1
{
    #[serde(rename = "i")]
    #[serde(alias = "ids")]
    ids : HashMap<u32, String>
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct TilemapGridRawV1
{
    #[serde(rename = "w")]
    #[serde(alias = "width")]
    width : usize,
    #[serde(rename = "h")]
    #[serde(alias = "height")]
    height : usize,
    #[serde(rename = "tl")]
    #[serde(alias = "tiles")]
    tiles : Vec<u32>
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct TilemapV1
{
    #[serde(rename = "ts")]
    #[serde(alias = "tileset")]
    tileset : DataHandle<DataPlaceholder>,
    grid : TilemapGridRawV1
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub enum LevelRepresentationV1
{
    None,
    Actor
    {
        custom_costume : Option<DataHandle<DataPlaceholder>>
    },
    Tilemap
    {
        tiles : TilemapV1
    },
    Prop
    {
        custom_sheet : Option<DataHandle<DataPlaceholder>>,
        custom_slice : Option<String>
    }
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct LevelSpawnableV1
{
    #[serde(rename = "ty")]
    #[serde(alias = "spawn_name_id")]
    pub spawn_name_id : u32,
    pub x : f64,
    pub y : f64,
    pub args : Option<DynArgList>,
    #[serde(rename = "re")]
    #[serde(alias = "representation")]
    pub representation : LevelRepresentationV1
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct TilemapV2
{
    #[serde(rename = "ts")]
    #[serde(alias = "tileset")]
    tileset : DataHandle<DataPlaceholder>,
    grid : TilemapGridRawV1,
    #[serde(default)]
    #[serde(rename = "offx")]
    #[serde(alias = "offset_x")]
    offset_x : f64,
    #[serde(default)]
    #[serde(rename = "offy")]
    #[serde(alias = "offset_y")]
    offset_y : f64
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub enum LevelRepresentationV2
{
    None,
    Actor
    {
        custom_costume : Option<DataHandle<DataPlaceholder>>
    },
    Tilemap
    {
        tiles : TilemapV2
    },
    Prop
    {
        custom_sheet : Option<DataHandle<DataPlaceholder>>,
        custom_slice : Option<String>
    }
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct LevelSpawnableV2
{
    #[serde(rename = "ty")]
    #[serde(alias = "spawn_name_id")]
    pub spawn_name_id : u32,
    pub transform : SimpleTransform,
    pub args : Option<DynArgList>,
    #[serde(rename = "re")]
    #[serde(alias = "representation")]
    pub representation : LevelRepresentationV2
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct LevelLayerV1
{
    #[serde(default)]
    spawnables : Vec<LevelSpawnableV1>,
    #[serde(default)]
    depth : Option<f32>,
    #[serde(default)]
    scroll_rate : Option<(f32, f32)>,
    #[serde(default)]
    visible : bool,
    #[serde(default)]
    locked : bool
}

impl Default for LevelLayerV1
{
    fn default() -> LevelLayerV1
    {
        LevelLayerV1
        {
            spawnables : Vec::new(),
            depth : None,
            scroll_rate : None,
            visible : true,
            locked : false
        }
    }
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct LevelLayerV2
{
    #[serde(default)]
    spawnables : Vec<LevelSpawnableV2>,
    #[serde(default)]
    z_offset : f64,
    #[serde(default)]
    scroll_rate : Option<(f32, f32)>,
    #[serde(default)]
    visible : bool,
    #[serde(default)]
    locked : bool
}

impl Default for LevelLayerV2
{
    fn default() -> LevelLayerV2
    {
        LevelLayerV2
        {
            spawnables : Vec::new(),
            z_offset : 0.0,
            scroll_rate : None,
            visible : true,
            locked : false
        }
    }
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct LevelV1
{
    header : String,
    #[serde(rename = "bb")]
    bounds : BakedRect<f64>,
    bg : Option<BackgroundInfoV1>,
    #[serde(default)]
    spawnable_set : DataHandle<DataPlaceholder>,
    spawnable_names : SpawnableNameIdMapRawV1,
    #[serde(default)]
    layers : NamedItemStore<LevelLayerV1>,
}

impl Migratable for LevelV1
{
    fn id() -> &'static str where Self : Sized
    {
        "level_v1"
    }

    fn describe() -> &'static str where Self : Sized
    {
        "Level v1"
    }

    fn describe_instance(&self) -> &'static str
    {
        "Level v1"
    }

    fn read_file(path : &str) -> Result<Self, String> where Self : Sized
    {
        let reader = match File::open(path)
        {
            Ok(file) => { BufReader::new(file) }
            Err(error) => { return Err(error.to_string()); }
        };

        let options = bincode::DefaultOptions::new().with_varint_encoding();
        let loaded_data : Self = options.deserialize_from(reader).map_err(|error| error.to_string())?;

        if loaded_data.header != "Keeshond Level Format r1"
        {
            return Err(String::from("Header mismatch"));
        }

        Ok(loaded_data)
    }

    fn migrate(&self) -> Result<Box<dyn Migratable>, MigrationError>
    {
        let mut level2 = LevelV2
        {
            header : String::from("Keeshond Level Format r2"),
            bounds : self.bounds.clone(),
            bg : self.bg.clone(),
            spawnable_set : self.spawnable_set.clone(),
            spawnable_names : self.spawnable_names.clone(),
            layers : NamedItemStore::new(),
        };

        for layer in &self.layers
        {
            let mut layer2 = LevelLayerV2
            {
                spawnables : Vec::new(),
                z_offset : layer.depth.unwrap_or(0.0) as f64,
                scroll_rate : layer.scroll_rate,
                visible : layer.visible,
                locked : layer.locked
            };

            for spawnable in &layer.spawnables
            {
                let representation = match &spawnable.representation
                {
                    LevelRepresentationV1::None => { LevelRepresentationV2::None }
                    LevelRepresentationV1::Actor { custom_costume } =>
                    {
                        LevelRepresentationV2::Actor { custom_costume : custom_costume.clone() }
                    }
                    LevelRepresentationV1::Tilemap { tiles } =>
                    {
                        LevelRepresentationV2::Tilemap { tiles : TilemapV2
                        {
                            tileset : tiles.tileset.clone(),
                            grid : tiles.grid.clone(),
                            offset_x : 0.0,
                            offset_y : 0.0
                        }}
                    }
                    LevelRepresentationV1::Prop { custom_sheet, custom_slice } =>
                    {
                        LevelRepresentationV2::Prop { custom_sheet : custom_sheet.clone(), custom_slice : custom_slice.clone() }
                    }
                };

                layer2.spawnables.push(LevelSpawnableV2
                {
                    spawn_name_id : spawnable.spawn_name_id,
                    transform : SimpleTransform
                    {
                        x : spawnable.x,
                        y : spawnable.y,
                        z : 0.0,
                        angle : 0.0,
                        scale_x : 1.0,
                        scale_y : 1.0
                    },
                    args : spawnable.args.clone(),
                    representation
                });
            }

            let layer_id = level2.layers.len();

            level2.layers.insert_item(layer_id, layer2);
            if let Some(name) = self.layers.get_name(layer_id)
            {
                level2.layers.rename_item(layer_id, &name);
            }
        }

        Ok(Box::new(level2))
    }

    fn write_file(&self, path: &str) -> Result<(), String>
    {
        let writer = match File::create(path)
        {
            Ok(file) => { BufWriter::new(file) }
            Err(error) => { return Err(error.to_string()); }
        };

        let options = bincode::DefaultOptions::new().with_varint_encoding();

        if let Err(error) = options.serialize_into(writer, &self)
        {
            return Err(error.to_string());
        }

        Ok(())
    }
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct LevelV2
{
    header : String,
    #[serde(rename = "bb")]
    bounds : BakedRect<f64>,
    bg : Option<BackgroundInfoV1>,
    #[serde(default)]
    spawnable_set : DataHandle<DataPlaceholder>,
    spawnable_names : SpawnableNameIdMapRawV1,
    #[serde(default)]
    layers : NamedItemStore<LevelLayerV2>,
}

impl Migratable for LevelV2
{
    fn id() -> &'static str where Self : Sized
    {
        "level_v2"
    }

    fn describe() -> &'static str where Self : Sized
    {
        "Level v2"
    }

    fn describe_instance(&self) -> &'static str
    {
        "Level v2"
    }

    fn read_file(path : &str) -> Result<Self, String> where Self : Sized
    {
        let reader = match File::open(path)
        {
            Ok(file) => { BufReader::new(file) }
            Err(error) => { return Err(error.to_string()); }
        };

        let options = bincode::DefaultOptions::new().with_varint_encoding();
        let loaded_data : Self = options.deserialize_from(reader).map_err(|error| error.to_string())?;

        if loaded_data.header != "Keeshond Level Format r2"
        {
            return Err(String::from("Header mismatch"));
        }

        Ok(loaded_data)
    }

    fn migrate(&self) -> Result<Box<dyn Migratable>, MigrationError>
    {
        Err(MigrationError::AlreadyLatest)
    }

    fn write_file(&self, path: &str) -> Result<(), String>
    {
        let writer = match File::create(path)
        {
            Ok(file) => { BufWriter::new(file) }
            Err(error) => { return Err(error.to_string()); }
        };

        let options = bincode::DefaultOptions::new().with_varint_encoding();

        if let Err(error) = options.serialize_into(writer, &self)
        {
            return Err(error.to_string());
        }

        Ok(())
    }
}
