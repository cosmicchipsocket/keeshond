extern crate keeshond;
extern crate serde;
extern crate serde_json;
extern crate bincode;
extern crate toodee;

use crate::migrator::{Migratable, MigratorFactory};
use crate::types::costume::{CostumeV1, CostumeV2};
use crate::types::level::{LevelV1, LevelV2};

pub mod util;
pub mod migrator;
pub mod types;

fn main()
{
    let args : Vec<String> = std::env::args().collect();

    if args.len() < 3
    {
        eprintln!("Usage: keeshond_migrator DATA_TYPE PATH");
        std::process::exit(1);
    }

    let data_type = args[1].to_lowercase();
    let path = args[2].clone();
    let mut factory = MigratorFactory::new();
    let migratable_ids;

    factory.register::<CostumeV1>();
    factory.register::<CostumeV2>();
    factory.register::<LevelV1>();
    factory.register::<LevelV2>();

    match &data_type[..]
    {
        "costume" =>
        {
            migratable_ids = vec![CostumeV2::id(), CostumeV1::id()];
        },
        "level" =>
        {
            migratable_ids = vec![LevelV2::id(), LevelV1::id()];
        },
        _ =>
        {
            eprintln!("Unknown datatype specified.");
            std::process::exit(1);
        }
    }

    match factory.find_migrator(&path, migratable_ids)
    {
        Ok(mut migrator) =>
        {
            migrator.run();
        }
        Err(error) =>
        {
            eprintln!("{}", error);
            std::process::exit(1);
        }
    }
}
