use crate::collision::{Collidable, CollisionShape};
use crate::level::LevelRepresentation;
use crate::world::Transform;
use crate::visual::{Actor, Costume, Drawable, Prop};

use keeshond::crate_reexport::strum::EnumCount;
use keeshond::crate_reexport::strum_macros::Display;
use keeshond::gameloop::GameControl;
use keeshond::renderer::Sheet;
use keeshond::scene::{DynArgList, SceneType, SpawnableEnum, SpawnControl};
use keeshond::util::{BakedRect, NamedItemStore, SimpleTransform};

use keeshond::datapack::{DataError, DataHandle, DataObject, LoadErrorMode, TrustPolicy};
use keeshond::datapack::source::Source;

use keeshond_derive::Component;

use std::str::FromStr;
use std::marker::PhantomData;

use serde::{Deserialize, Serialize};

#[derive(Clone, Serialize, Deserialize, Debug, Display)]
pub enum Representation
{
    None,
    Actor
    {
        #[serde(rename = "cstm")]
        #[serde(alias = "costume")]
        costume : DataHandle<Costume>
    },
    Tilemap,
    Prop
    {
        #[serde(rename = "she")]
        #[serde(alias = "sheet")]
        sheet : DataHandle<Sheet>,
        #[serde(rename = "sli")]
        #[serde(alias = "slice")]
        slice : String
    }
}

impl Representation
{
    pub fn index(&self) -> usize
    {
        match self
        {
            Representation::None => { 0 }
            Representation::Actor { .. } => { 1 }
            Representation::Tilemap => { 2 }
            Representation::Prop { .. } => { 3 }
        }
    }

    pub fn from_index(index : usize) -> Representation
    {
        match index
        {
            0 => Representation::None,
            1 => Representation::Actor
            {
                costume : DataHandle::null()
            },
            2 => Representation::Tilemap,
            3 => Representation::Prop
            {
                sheet : DataHandle::null(),
                slice : String::new()
            },
            _ => panic!("Unknown representation index")
        }
    }

    pub fn matches_with_level(&self, level_repr : &LevelRepresentation) -> bool
    {
        self.index() == level_repr.index()
    }
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct CollisionSettings
{
    #[serde(rename = "shap")]
    #[serde(alias = "shape")]
    #[serde(default)]
    pub shape : CollisionShape,
    #[serde(rename = "indx")]
    #[serde(alias = "indexed")]
    #[serde(default)]
    pub indexed : bool
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct Spawnable
{
    #[serde(rename = "coll")]
    #[serde(alias = "collision")]
    collision : Option<CollisionSettings>,
    #[serde(rename = "repr")]
    #[serde(alias = "representation")]
    representation : Representation,
    #[serde(rename = "layr")]
    #[serde(alias = "auto_layer_name")]
    #[serde(default)]
    auto_layer_name : String
}

impl Default for Spawnable
{
    fn default() -> Self
    {
        Spawnable::new()
    }
}

impl Spawnable
{
    pub fn new() -> Spawnable
    {
        Spawnable
        {
            collision : None,
            representation : Representation::None,
            auto_layer_name : String::new()
        }
    }

    pub fn representation(&self) -> &Representation
    {
        &self.representation
    }

    pub fn representation_mut(&mut self) -> &mut Representation
    {
        &mut self.representation
    }

    pub fn set_representation(&mut self, representation : Representation)
    {
        self.representation = representation;

        match &self.representation
        {
            Representation::Tilemap =>
            {
                if let Some(collision) = &mut self.collision
                {
                    collision.shape = CollisionShape::None;
                }
            }
            _ =>
            {
                if let Some(collision) = &mut self.collision
                {
                    if collision.shape == CollisionShape::None
                    {
                        collision.shape = CollisionShape::Box(BakedRect::default())
                    }
                }
            }
        }
    }

    pub fn auto_layer_name(&self) -> &String
    {
        &self.auto_layer_name
    }

    pub fn set_auto_layer_name(&mut self, auto_layer_name : String)
    {
        self.auto_layer_name = auto_layer_name;
    }

    pub fn collision(&self) -> Option<&CollisionSettings>
    {
        self.collision.as_ref()
    }

    pub fn set_collision(&mut self, collision : Option<CollisionSettings>)
    {
        self.collision = collision;
    }
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct SpawnableSet
{
    #[serde(rename = "spwn")]
    #[serde(alias = "spawnables")]
    spawnables : NamedItemStore<Spawnable>
}

impl Default for SpawnableSet
{
    fn default() -> Self
    {
        SpawnableSet::new()
    }
}

impl SpawnableSet
{
    pub fn new() -> SpawnableSet
    {
        SpawnableSet
        {
            spawnables : NamedItemStore::new()
        }
    }

    pub fn spawnable(&self, id : usize) -> Option<&Spawnable>
    {
        self.spawnables.get(id)
    }

    pub fn spawnable_by_name(&self, name : &str) -> Option<&Spawnable>
    {
        if let Some(id) = self.spawnable_id(name)
        {
            return self.spawnables.get(id);
        }

        None
    }

    pub fn set_spawnable(&mut self, id : usize, spawnable : Spawnable) -> bool
    {
        self.spawnables.set(id, spawnable)
    }

    pub fn spawnable_name(&self, id : usize) -> Option<String>
    {
        self.spawnables.get_name(id)
    }

    pub fn spawnable_id(&self, name : &str) -> Option<usize>
    {
        self.spawnables.get_id(name)
    }

    pub fn add_spawnable(&mut self, spawnable : Spawnable)
    {
        self.insert_spawnable(self.spawnables.len(), spawnable);
    }

    pub fn insert_spawnable(&mut self, pos : usize, spawnable : Spawnable) -> bool
    {
        self.spawnables.insert_item(pos, spawnable)
    }

    pub fn remove_spawnable(&mut self, id : usize) -> Option<Spawnable>
    {
        self.spawnables.remove_item(id)
    }

    pub fn rename_spawnable(&mut self, id : usize, new_name : &str) -> bool
    {
        self.spawnables.rename_item(id, new_name)
    }

    pub fn spawnable_count(&self) -> usize
    {
        self.spawnables.len()
    }
}

impl DataObject for SpawnableSet
{
    fn folder_name() -> &'static str where Self: Sized { "spawnables" }
    fn trust_policy() -> TrustPolicy { TrustPolicy::UntrustedOk }
    fn want_file(pathname: &str) -> bool where Self : Sized { pathname.ends_with(".json") }

    fn from_package_source(source : &mut Box<dyn Source>, package_name : &str, pathname : &str) -> Result<Self, DataError> where Self : Sized
    {
        let mut reader = source.read_file(package_name, pathname)?;
        let mut json_text = String::new();
        let data_result = reader.read_to_string(&mut json_text);

        match data_result
        {
            Err(error) =>
            {
                return Err(DataError::IoError(error));
            },
            _ => {}
        }

        match serde_json::from_str(&json_text)
        {
            Ok(loaded_data) =>
            {
                return Ok(loaded_data);
            },
            Err(error) =>
            {
                return Err(DataError::BadData(format!("{}/{}: {}", package_name, pathname, error)));
            }
        }
    }

    fn write(&mut self, package_name : &str, pathname : &str, source : &mut Box<dyn Source>) -> Result<(), DataError>
    {
        let mut writer = source.write_file(package_name, pathname).map_err(
                |error| DataError::PackageSourceError(error))?;

        match serde_json::to_string(&self)
        {
            Err(error) =>
            {
                return Err(DataError::BadData(format!("{}/{}: {}", package_name, pathname, error)));
            },
            Ok(json_text) =>
            {
                match writer.write_all(json_text.as_bytes())
                {
                    Err(error) =>
                    {
                        return Err(DataError::IoError(error));
                    },
                    _ => {}
                }
            }
        }

        info!("Wrote spawnable list to {}/{}", package_name, pathname);

        Ok(())
    }
}

#[derive(Clone)]
pub struct SpawnDefaultData
{
    collision : Option<CollisionSettings>,
    representation : RepresentationData
}

impl SpawnDefaultData
{
    pub fn from_spawnable(spawnable : &Spawnable) -> SpawnDefaultData
    {
        let representation = match &spawnable.representation
        {
            Representation::None => { RepresentationData::None }
            Representation::Actor { costume } =>
            {
                RepresentationData::Actor
                {
                    costume : costume.clone()
                }
            }
            Representation::Tilemap => { RepresentationData::Tilemap }
            Representation::Prop { sheet, slice } =>
            {
                RepresentationData::Prop
                {
                    sheet : sheet.clone(),
                    slice : slice.clone()
                }
            }
        };

        SpawnDefaultData
        {
            collision : spawnable.collision.clone(),
            representation
        }
    }
}

impl Default for SpawnDefaultData
{
    fn default() -> Self
    {
        SpawnDefaultData
        {
            collision : None,
            representation : RepresentationData::None
        }
    }
}

#[derive(Clone)]
enum RepresentationData
{
    None,
    Actor
    {
        costume : DataHandle<Costume>
    },
    Tilemap,
    Prop
    {
        sheet : DataHandle<Sheet>,
        slice : String
    }
}

pub(crate) fn drawable_from_args(args : &DynArgList) -> Drawable
{
    let mut drawable = Drawable::new();

    drawable.scroll_rate_x = args.get_float_named("scroll_rate_x", 1.0) as f32;
    drawable.scroll_rate_y = args.get_float_named("scroll_rate_y", 1.0) as f32;

    drawable
}

#[derive(Component)]
#[component(removal = "fast")]
pub struct SpawnableTag<T : SceneType + 'static>
{
    pub spawnable_id : T::SpawnableIdType
}

pub struct SpawnDefaultHelper<T : SceneType + 'static> where T::SpawnableIdType : EnumCount + Into<usize>
{
    list : Vec<SpawnDefaultData>,
    _phantom : PhantomData<T>
}

impl<T : SceneType + 'static> SpawnDefaultHelper<T> where T::SpawnableIdType : EnumCount + Into<usize>
{
    pub fn empty() -> SpawnDefaultHelper<T>
    {
        SpawnDefaultHelper
        {
            list : vec![SpawnDefaultData::default(); T::SpawnableIdType::COUNT],
            _phantom : PhantomData
        }
    }

    pub fn from_spawnables_lookup(package : &str, path : &str, game : &mut GameControl) -> SpawnDefaultHelper<T>
    {
        let source_id = game.res().package_source_id(package);

        if source_id == 0
        {
            panic!("Failed to load \"{}/{}/{}\": {}",
                   package, SpawnableSet::folder_name(), path, "The package was not found");
        }

        let spawnables_result = game.res().store_mut::<SpawnableSet>().load_direct(package, path, source_id);

        match spawnables_result
        {
            Ok(spawnables) =>
            {
                SpawnDefaultHelper::from_spawnables(&spawnables)
            }
            Err(error) =>
            {
                panic!("Failed to load \"{}/{}/{}\": {}",
                       package, SpawnableSet::folder_name(), path, error.to_string());
            }
        }
    }

    pub fn from_spawnables(spawnable_set: &SpawnableSet) -> SpawnDefaultHelper<T>
    {
        let mut list = vec![SpawnDefaultData::default(); T::SpawnableIdType::COUNT];

        for i in 0..spawnable_set.spawnable_count()
        {
            if let Some(name) = spawnable_set.spawnable_name(i)
            {
                if let Ok(enum_key) = T::SpawnableIdType::from_str(&name)
                {
                    list[enum_key.into()] = SpawnDefaultData::from_spawnable(spawnable_set.spawnable(i).unwrap());
                }
            }
        }

        SpawnDefaultHelper
        {
            list,
            _phantom : PhantomData
        }
    }

    pub fn spawn_defaults(&mut self, spawn : &mut SpawnControl, spawnable_id : T::SpawnableIdType,
                          game : &mut GameControl, transform : &SimpleTransform, args : &DynArgList, load_error : LoadErrorMode)
    {
        let data = &mut self.list[spawnable_id.into()];

        if !spawn.has::<Transform>()
        {
            spawn.with(Transform::from_data(transform.clone()));
        }

        if let Some(collision) = &data.collision
        {
            if !spawn.has::<Collidable>()
            {
                spawn.with(Collidable::new(transform.x, transform.y, collision.shape.clone(), collision.indexed));
            }
        }

        match &mut data.representation
        {
            RepresentationData::None => {}
            RepresentationData::Actor { costume } =>
            {
                if !spawn.has::<Drawable>()
                {
                    spawn.with(drawable_from_args(args));
                }

                if !spawn.has::<Actor>()
                {
                    let mut actor = Actor::new();
                    let mut costume_store = game.res().store_mut();

                    costume.resolve(&mut costume_store, load_error);

                    actor.set_costume(costume.clone_without_path());

                    spawn.with(actor);
                }
            }
            RepresentationData::Tilemap =>
            {
                if !spawn.has::<Drawable>()
                {
                    spawn.with(drawable_from_args(args));
                }

                // Spawnable data doesn't define tilemap data, so we won't create a Tilemap
                //  component here.
            }
            RepresentationData::Prop { sheet, slice } =>
            {
                if !spawn.has::<Drawable>()
                {
                    spawn.with(drawable_from_args(args));
                }

                if !spawn.has::<Prop>()
                {
                    let mut prop = Prop::new();
                    let mut sheet_store = game.res().store_mut();

                    sheet.resolve(&mut sheet_store, load_error);

                    prop.set_sheet(sheet.clone_without_path());
                    prop.set_slice_name(slice, &mut sheet_store);

                    spawn.with(prop);
                }
            }
        }

        if !spawn.has::<SpawnableTag<T>>()
        {
            spawn.with(SpawnableTag::<T>
            {
                spawnable_id
            });
        }

        spawnable_id.spawn(spawn, game, transform, args);
    }
}
