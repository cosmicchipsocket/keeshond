use crate::collision::Collidable;

use keeshond::scene::{SceneType, ThinkerArgs, ThinkerSystem};
use keeshond::util::{BakedRect, Rect, SimpleTransform};

use keeshond_derive::Component;
use crate::level::LevelInfo;

#[derive(Component)]
pub struct Transform
{
    pub(crate) data : SimpleTransform,
    pub(crate) lerp_suspended : bool,
    pub(crate) dirty : bool,
}

impl Transform
{
    pub fn new(x : f64, y : f64, z : f64, angle : f64, scale_x : f64, scale_y : f64) -> Transform
    {
        Transform
        {
            data : SimpleTransform
            {
                x,
                y,
                z,
                angle,
                scale_x,
                scale_y
            },
            lerp_suspended : true,
            dirty : false
        }
    }

    pub fn from_data(data : SimpleTransform) -> Transform
    {
        Transform
        {
            data,
            lerp_suspended : true,
            dirty : false
        }
    }

    pub fn data(&self) -> &SimpleTransform
    {
        &self.data
    }

    pub fn x(&self) -> f64 { self.data.x }
    pub fn y(&self) -> f64 { self.data.y }
    pub fn z(&self) -> f64 { self.data.z }

    pub fn xy(&self) -> (f64, f64) { (self.data.x, self.data.y) }
    pub fn set_xy(&mut self, x : f64, y : f64)
    {
        self.data.x = x;
        self.data.y = y;
        self.dirty = true;
    }
    pub fn set_z(&mut self, z : f64)
    {
        self.data.z = z;
        self.dirty = true;
    }

    pub fn pos(&self) -> (f64, f64, f64) { (self.data.x, self.data.y, self.data.z) }
    pub fn set_pos(&mut self, x : f64, y : f64, z : f64)
    {
        self.data.x = x;
        self.data.y = y;
        self.data.z = z;
        self.dirty = true;
    }

    pub fn angle(&self) -> f64 { self.data.angle }
    pub fn set_angle(&mut self, angle : f64)
    {
        self.data.angle = angle;
        self.dirty = true;
    }

    pub fn scale_x(&self) -> f64 { self.data.scale_x }
    pub fn scale_y(&self) -> f64 { self.data.scale_y }
    pub fn scale(&self) -> (f64, f64) { (self.data.scale_x, self.data.scale_y) }
    pub fn set_scale(&mut self, scale_x : f64, scale_y : f64)
    {
        self.data.scale_x = scale_x;
        self.data.scale_y = scale_y;
        self.dirty = true;
    }

    pub fn lerp_suspended(&self) -> bool { self.lerp_suspended }
    pub fn suspend_lerp(&mut self, suspended : bool)
    {
        self.lerp_suspended = suspended;
        self.dirty = true;
    }

    pub fn local_to_world_pos(&self, x : f64, y : f64) -> (f64, f64)
    {
        self.data.local_to_world_pos(x, y)
    }

    pub fn world_to_local_pos(&self, x : f64, y : f64) -> (f64, f64)
    {
        self.data.world_to_local_pos(x, y)
    }
}

pub struct TransformCollidableSyncThinker<T : SceneType + 'static>
{
    phantom : std::marker::PhantomData::<T>
}

impl<T : SceneType + 'static> TransformCollidableSyncThinker<T>
{
    pub fn new() -> TransformCollidableSyncThinker::<T>
    {
        TransformCollidableSyncThinker::<T>
        {
            phantom : std::marker::PhantomData
        }
    }
}

impl<T : SceneType + 'static> ThinkerSystem<T> for TransformCollidableSyncThinker<T>
{
    fn think(&mut self, args : ThinkerArgs<T>)
    {
        let mut transform_store = args.components.store_mut::<Transform>();
        let mut collidable_store = args.components.store_mut::<Collidable>();

        for (entity, transform) in transform_store.iter_all_mut()
        {
            if transform.dirty
            {
                if let Some(collidable) = collidable_store.get_entity_mut(&entity)
                {
                    collidable.reference.x = transform.x();
                    collidable.reference.y = transform.y();
                    collidable.reference.dirty = false;
                    collidable.reference.teleported = transform.lerp_suspended;
                }

                transform.dirty = false;
            }
        }
    }

    fn priority(&self) -> i32 { crate::SYSTEM_PRIORITY_PRE_PHYSICS }
}

#[derive(Component)]
pub struct Camera
{
    pub x : f64,
    pub y : f64,
    pub(crate) old_x : f64,
    pub(crate) old_y : f64,
    pub(crate) older_x : f64,
    pub(crate) older_y : f64,
    pub screen_area : Rect<f32>,
    pub scroll_stop : Option<BakedRect<f64>>
}

impl Camera
{
    pub fn new(x : f64, y : f64, screen_area : Rect<f32>) -> Camera
    {
        Camera
        {
            x,
            y,
            old_x : 0.0,
            old_y : 0.0,
            older_x : 0.0,
            older_y : 0.0,
            screen_area,
            scroll_stop : None
        }
    }

    pub fn bounds(&self, scroll_rate_x : f64, scroll_rate_y : f64) -> BakedRect<f64>
    {
        BakedRect
        {
            x1 : self.x * scroll_rate_x,
            y1 : self.y * scroll_rate_y,
            x2 : self.x * scroll_rate_x + self.screen_area.w as f64,
            y2 : self.y * scroll_rate_y + self.screen_area.h as f64
        }
    }

    pub fn bounds_lerped(&self, scroll_rate_x : f64, scroll_rate_y : f64) -> BakedRect<f64>
    {
        let mut current = BakedRect
        {
            x1 : self.x * scroll_rate_x,
            y1 : self.y * scroll_rate_y,
            x2 : self.x * scroll_rate_x + self.screen_area.w as f64,
            y2 : self.y * scroll_rate_y + self.screen_area.h as f64
        };

        let old = BakedRect
        {
            x1 : self.older_x * scroll_rate_x,
            y1 : self.older_y * scroll_rate_y,
            x2 : self.older_x * scroll_rate_x + self.screen_area.w as f64,
            y2 : self.older_y * scroll_rate_y + self.screen_area.h as f64
        };

        current.combine(&old);

        current
    }
}

pub struct CameraThinker<T : SceneType + 'static>
{
    first_frame : bool,
    auto_create_camera : bool,
    phantom : std::marker::PhantomData::<T>
}

impl<T : SceneType + 'static> CameraThinker<T>
{
    pub fn new(auto_create_camera : bool) -> CameraThinker::<T>
    {
        CameraThinker::<T>
        {
            first_frame : true,
            auto_create_camera,
            phantom : std::marker::PhantomData
        }
    }
}

impl<T : SceneType + 'static> ThinkerSystem<T> for CameraThinker<T>
{
    fn start(&mut self, args : ThinkerArgs<T>)
    {
        let camera_store = args.components.store_mut::<Camera>();

        if self.auto_create_camera && camera_store.count() == 0
        {
            let (base_width, base_height) = args.game.renderer().base_size();
            let entity = args.scene.add_entity_later();

            args.scene.add_component_later(&entity, Camera::new(0.0, 0.0, Rect
            {
                x : 0.0,
                y : 0.0,
                w : base_width,
                h : base_height
            }));
        }
    }

    fn think(&mut self, args : ThinkerArgs<T>)
    {
        let mut camera_store = args.components.store_mut::<Camera>();
        let (screen_width, screen_height) = args.game.renderer().base_size_f64();
        let level_info_store = args.components.store::<LevelInfo>();

        for (_, camera) in camera_store.iter_mut()
        {
            if let Some(level_info) = level_info_store.singleton()
            {
                camera.x = camera.x.clamp(level_info.bounds.x1, level_info.bounds.x2 - screen_width);
                camera.y = camera.y.clamp(level_info.bounds.y1, level_info.bounds.y2 - screen_height);
            }

            if let Some(scroll_stop) = camera.scroll_stop
            {
                camera.x = camera.x.clamp(scroll_stop.x1, scroll_stop.x2 - screen_width);
                camera.y = camera.y.clamp(scroll_stop.y1, scroll_stop.y2 - screen_height);
            }

            let max_lerp_distance = (screen_width + screen_height) / 2.0;
            let lerp_distance = ((camera.x - camera.old_x).powi(2) + (camera.y - camera.old_y).powi(2)).sqrt();

            if self.first_frame || lerp_distance > max_lerp_distance
            {
                camera.old_x = camera.x;
                camera.old_y = camera.y;
            }

            camera.older_x = camera.old_x;
            camera.older_y = camera.old_y;
            camera.old_x = camera.x;
            camera.old_y = camera.y;
        }

        self.first_frame = false;
    }

    fn priority(&self) -> i32 { crate::SYSTEM_PRIORITY_POST_FRAME }
}
