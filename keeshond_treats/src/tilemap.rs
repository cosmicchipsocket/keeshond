use crate::visual::{Drawable, DrawableItem, LerpedTransform, MultiTileSprite, PositionedTile, SliceIndexHandle};
use crate::world::{Camera, Transform};
use crate::LoadErrorMode;

use keeshond::renderer::Sheet;
use keeshond::scene::{FreezeFlags, SceneType, ThinkerArgs, ThinkerSystem};
use keeshond::util::{BakedRect, SimpleTransform};

use keeshond_derive::Component;

use keeshond::datapack::{DataError, DataHandle, DataObject, DataStore, TrustPolicy};
use keeshond::datapack::source::Source;

use block_grid::{BlockGrid, U32};
use serde::{Deserialize, Deserializer, Serialize, Serializer};
use serde::de::{Error, Unexpected};


#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct TileBoundingBoxData
{
    pub x1 : f64,
    pub y1 : f64,
    pub x2 : f64,
    pub y2 : f64,
    #[serde(rename = "st")]
    pub solid_top : bool,
    #[serde(rename = "sb")]
    pub solid_bottom : bool,
    #[serde(rename = "sl")]
    pub solid_left : bool,
    #[serde(rename = "sr")]
    pub solid_right : bool
}

impl TileBoundingBoxData
{
    pub fn from_width_height(width : f64, height : f64) -> TileBoundingBoxData
    {
        TileBoundingBoxData
        {
            x1: 0.0,
            y1: 0.0,
            x2: width,
            y2: height,
            solid_top: true,
            solid_bottom: true,
            solid_left: true,
            solid_right: true
        }
    }
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct TileHeightmapData
{
    #[serde(rename = "t")]
    pub top : Vec<f64>,
    #[serde(rename = "b")]
    pub bottom : Vec<f64>,
    #[serde(rename = "l")]
    pub left : Vec<f64>,
    #[serde(rename = "r")]
    pub right : Vec<f64>,
    #[serde(rename = "st")]
    pub solid_top : bool,
    #[serde(rename = "sb")]
    pub solid_bottom : bool,
    #[serde(rename = "sl")]
    pub solid_left : bool,
    #[serde(rename = "sr")]
    pub solid_right : bool
}

impl TileHeightmapData
{
    pub fn from_width_height(width : f64, height : f64) -> TileHeightmapData
    {
        TileHeightmapData
        {
            top : vec![0.0],
            bottom : vec![height],
            left : vec![0.0],
            right : vec![width],
            solid_top : true,
            solid_bottom : true,
            solid_left : true,
            solid_right : true
        }
    }

    pub fn valid(&self) -> bool
    {
        !self.top.is_empty() && !self.bottom.is_empty() && !self.left.is_empty() && !self.right.is_empty()
    }

    pub fn top_x_range(&self) -> (f64, f64)
    {
        if self.left.is_empty() || self.right.is_empty()
        {
            panic!("Heightmap has zero-length sides")
        }

        (self.left[0], self.right[0])
    }

    pub fn bottom_x_range(&self) -> (f64, f64)
    {
        if self.left.is_empty() || self.right.is_empty()
        {
            panic!("Heightmap has zero-length sides")
        }

        (self.left[self.left.len() - 1], self.right[self.right.len() - 1])
    }

    pub fn left_y_range(&self) -> (f64, f64)
    {
        if self.top.is_empty() || self.bottom.is_empty()
        {
            panic!("Heightmap has zero-length sides")
        }

        (self.top[0], self.bottom[0])
    }

    pub fn right_y_range(&self) -> (f64, f64)
    {
        if self.top.is_empty() || self.bottom.is_empty()
        {
            panic!("Heightmap has zero-length sides")
        }

        (self.top[self.top.len() - 1], self.bottom[self.bottom.len() - 1])
    }
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub enum TileCollision
{
    #[serde(rename = "no")]
    None,
    #[serde(rename = "bb")]
    BoundingBox(TileBoundingBoxData),
    #[serde(rename = "hi")]
    Heightmap(TileHeightmapData)
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct TilesetTile
{
    #[serde(flatten)]
    slice: Option<SliceIndexHandle>,
    #[serde(rename = "co")]
    collision : TileCollision,
    #[serde(rename = "fl")]
    #[serde(default)]
    flags : u32
}

impl TilesetTile
{
    pub fn new(slice_handle : Option<SliceIndexHandle>) -> TilesetTile
    {
        TilesetTile
        {
            slice: slice_handle,
            collision : TileCollision::None,
            flags : 0
        }
    }

    pub fn slice_handle(&self) -> Option<&SliceIndexHandle>
    {
        self.slice.as_ref()
    }

    pub fn set_slice_handle(&mut self, slice_handle : Option<SliceIndexHandle>)
    {
        self.slice = slice_handle;
    }

    pub fn updated_slice_id(&mut self, sheet : &Sheet) -> Option<usize>
    {
        if let Some(handle) = &mut self.slice
        {
            return Some(handle.updated_slice_id(sheet));
        }

        None
    }

    pub fn collision(&self) -> &TileCollision
    {
        &self.collision
    }

    pub fn set_collision(&mut self, collision : TileCollision)
    {
        self.collision = collision;
    }

    pub fn flag(&self, flag : usize) -> bool
    {
        if flag < 32
        {
            return self.flags & (1 << flag) != 0; // TODO make faster
        }

        false
    }

    pub fn set_flag(&mut self, flag : usize, set : bool) -> bool
    {
        if flag < 32
        {
            if set
            {
                self.flags |= 1 << flag; // TODO make faster
            }
            else
            {
                self.flags &= !(1 << flag); // TODO make faster
            }

            return true;
        }

        false
    }

    pub fn flags(&self) -> u32
    {
        self.flags
    }

    pub fn set_flags(&mut self, values : u32)
    {
        self.flags = values;
    }
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct Tileset
{
    #[serde(rename = "she")]
    #[serde(alias = "sheet")]
    sheet : DataHandle<Sheet>,
    #[serde(rename = "ti")]
    #[serde(alias = "tiles")]
    tiles : Vec<TilesetTile>,
    #[serde(rename = "tw")]
    #[serde(alias = "tile_width")]
    tile_width : f64,
    #[serde(rename = "th")]
    #[serde(alias = "tile_height")]
    tile_height : f64,
    #[serde(rename = "fs")]
    #[serde(default)]
    flag_names : [String; 32],
    #[serde(skip)]
    generation : u64
}

impl Tileset
{
    pub fn new(sheet : DataHandle<Sheet>) -> Tileset
    {
        Tileset
        {
            sheet,
            tiles : Vec::new(),
            generation : 0,
            tile_width : 32.0,
            tile_height : 32.0,
            flag_names : Default::default()
        }
    }

    fn next_generation(&mut self)
    {
        self.generation += 1;

        if self.generation == 0
        {
            warn!("Generation overflow!");
            self.generation = 1;
        }
    }

    pub fn sheet(&self) -> &DataHandle<Sheet>
    {
        &self.sheet
    }

    pub fn set_sheet(&mut self, sheet : DataHandle<Sheet>)
    {
        self.sheet = sheet;
        self.next_generation();

        for tile in &mut self.tiles
        {
            if let Some(source) = &mut tile.slice
            {
                source.invalidate();
            }
        }
    }

    pub fn tile_width(&self) -> f64 { self.tile_width }
    pub fn tile_height(&self) -> f64 { self.tile_height }
    pub fn tile_size(&self) -> (f64, f64)
    {
        (self.tile_width, self.tile_height)
    }

    pub fn set_tile_size(&mut self, width : f64, height : f64)
    {
        self.tile_width = width;
        self.tile_height = height;

        self.next_generation();
    }

    pub fn tile(&self, id : usize) -> Option<&TilesetTile>
    {
        self.tiles.get(id)
    }

    pub fn set_tile(&mut self, id : usize, tile : TilesetTile) -> bool
    {
        if let Some(old_tile) = self.tiles.get_mut(id)
        {
            *old_tile = tile;
            self.next_generation();

            return true;
        }

        false
    }

    pub fn insert_tile(&mut self, pos : usize, tile : TilesetTile) -> bool
    {
        if pos > self.tiles.len()
        {
            return false;
        }

        self.tiles.insert(pos, tile);
        self.next_generation();

        true
    }

    pub fn remove_tile(&mut self, id : usize) -> Option<TilesetTile>
    {
        if id >= self.tiles.len()
        {
            return None;
        }

        let tile = self.tiles.remove(id);
        self.next_generation();

        Some(tile)
    }

    pub fn updated_slice_id(&mut self, id : usize, sheet : &Sheet) -> Option<usize>
    {
        if let Some(tile) = self.tiles.get_mut(id)
        {
            return tile.updated_slice_id(sheet);
        }

        None
    }

    pub fn slice_name(&self, id : usize) -> Option<&String>
    {
        if let Some(tile) = self.tiles.get(id)
        {
            if let Some(source) = tile.slice_handle()
            {
                return source.name();
            }
        }

        None
    }

    pub fn tile_count(&self) -> usize
    {
        self.tiles.len()
    }

    pub fn flag_name(&self, flag : usize) -> Option<&String>
    {
        if flag < self.flag_names.len()
        {
            return Some(&self.flag_names[flag]);
        }

        None
    }

    pub fn set_flag_name(&mut self, flag : usize, name : &str) -> bool
    {
        if flag < self.flag_names.len()
        {
            self.flag_names[flag] = String::from(name);

            return true
        }

        false
    }
}

impl DataObject for Tileset
{
    fn folder_name() -> &'static str where Self : Sized { "tilesets" }
    fn trust_policy() -> TrustPolicy { TrustPolicy::UntrustedOk }
    fn want_file(pathname : &str) -> bool where Self : Sized { pathname.ends_with(".json") }

    fn from_package_source(source : &mut Box<dyn Source>, package_name : &str, pathname : &str) -> Result<Self, DataError> where Self : Sized
    {
        let mut reader = source.read_file(package_name, pathname)?;
        let mut json_text = String::new();
        let data_result = reader.read_to_string(&mut json_text);

        match data_result
        {
            Err(error) =>
            {
                return Err(DataError::IoError(error));
            },
            _ => {}
        }

        match serde_json::from_str(&json_text)
        {
            Ok(loaded_data) =>
            {
                return Ok(loaded_data);
            },
            Err(error) =>
            {
                return Err(DataError::BadData(format!("{}/{}: {}", package_name, pathname, error)));
            }
        }
    }

    fn write(&mut self, package_name : &str, pathname : &str, source : &mut Box<dyn Source>) -> Result<(), DataError>
    {
        let mut writer = source.write_file(package_name, pathname).map_err(
            |error| DataError::PackageSourceError(error))?;

        match serde_json::to_string(&self)
        {
            Err(error) =>
            {
                return Err(DataError::BadData(format!("{}/{}: {}", package_name, pathname, error)));
            },
            Ok(json_text) =>
            {
                match writer.write_all(json_text.as_bytes())
                {
                    Err(error) =>
                    {
                        return Err(DataError::IoError(error));
                    },
                    _ => {}
                }
            }
        }

        info!("Wrote tileset to {}/{}", package_name, pathname);

        Ok(())
    }

    fn generation(&self) -> u64
    {
        self.generation
    }

    fn set_generation(&mut self, generation : u64)
    {
        self.generation = generation;
    }
}

#[derive(Copy, Clone, Debug)]
pub struct TilemapTile
{
    pub index : u32,
    pub flags : u32
}

impl Default for TilemapTile
{
    fn default() -> TilemapTile
    {
        TilemapTile
        {
            index : 0,
            flags : 0
        }
    }
}

#[derive(Clone, Debug)]
pub struct TilemapGrid
{
    width : usize,
    height : usize,
    tiles : BlockGrid<TilemapTile, U32>
}

impl TilemapGrid
{
    pub fn new(width : usize, height : usize) -> TilemapGrid
    {
        if width <= 0 || height <= 0
        {
            panic!("Invalid grid dimensions: {}x{}", width, height);
        }

        let (grid_width, grid_height) = TilemapGrid::get_internal_size(width, height);

        TilemapGrid
        {
            width,
            height,
            tiles : BlockGrid::filled(grid_width, grid_height, TilemapTile::default()).expect("Invalid grid dimensions")
        }
    }

    pub fn from_vec_raw(width : usize, height : usize, data : Vec<u32>) -> TilemapGrid
    {
        if data.len() != width * height * 2
        {
            panic!("Data size {} does not match dimensions {}x{}", data.len(), width, height);
        }

        let mut grid = TilemapGrid::new(width, height);

        let mut i = 0;

        for y in 0..grid.height
        {
            for x in 0..grid.width
            {
                let tile = TilemapTile
                {
                    index : data[i],
                    flags : data[i + 1]
                };

                i += 2;

                grid.set_tile(x, y, tile);
            }
        }

        grid
    }

    pub fn size(&self) -> (usize, usize)
    {
        (self.width, self.height)
    }

    fn get_internal_size(mut width : usize, mut height : usize) -> (usize, usize)
    {
        width = width.max(1);
        height = height.max(1);

        if width % 32 != 0 { width += 32 - width % 32; }
        if height % 32 != 0 { height += 32 - height % 32; }

        (width, height)
    }

    pub fn to_vec_raw(&self) -> Vec<u32>
    {
        let mut new_vec = Vec::with_capacity(self.width * self.height);

        for y in 0..self.height
        {
            for x in 0..self.width
            {
                let tile = self.tiles.get((x, y)).cloned().unwrap_or_default();
                new_vec.push(tile.index);
                new_vec.push(tile.flags);
            }
        }

        new_vec
    }

    pub fn tile(&self, x : usize, y : usize) -> Option<&TilemapTile>
    {
        if x >= self.width || y >= self.height
        {
            return None;
        }

        self.tiles.get((x, y))
    }

    pub fn tile_mut(&mut self, x : usize, y : usize) -> Option<&mut TilemapTile>
    {
        if x >= self.width || y >= self.height
        {
            return None;
        }

        self.tiles.get_mut((x, y))
    }

    pub fn set_tile(&mut self, x : usize, y : usize, value : TilemapTile) -> bool
    {
        if let Some(tile) = self.tile_mut(x, y)
        {
            *tile = value;
            return true;
        }

        false
    }

    pub fn to_resized(&self, width : usize, height : usize, nudge_x : usize, nudge_y : usize) -> TilemapGrid
    {
        let mut new_grid = TilemapGrid::new(width, height);

        for y in 0..height
        {
            let source_y = y.wrapping_sub(nudge_y);

            for x in 0..width
            {
                let source_x = x.wrapping_sub(nudge_x);

                new_grid.set_tile(x, y, self.tile(source_x, source_y).cloned().unwrap_or(TilemapTile::default()));
            }
        }

        new_grid
    }

    pub fn world_to_tile_pos(&self, x : f64, y : f64, transform : &SimpleTransform,
                             offset_x : f64, offset_y : f64, tileset : &Tileset) -> (f64, f64)
    {
        let (mut transformed_x, mut transformed_y) = transform.world_to_local_pos(x, y);

        transformed_x /= tileset.tile_width.max(0.0000001);
        transformed_y /= tileset.tile_height.max(0.0000001);

        (transformed_x - offset_x, transformed_y - offset_y)
    }

    pub fn world_to_tile_pos_int(&self, x : f64, y : f64, transform : &SimpleTransform,
                                 offset_x : f64, offset_y : f64, tileset : &Tileset) -> Option<(usize, usize)>
    {
        let (transformed_x, transformed_y) = self.world_to_tile_pos(x, y, transform, offset_x, offset_y, tileset);

        if transformed_x >= 0.0 && transformed_x < self.width as f64
            && transformed_y >= 0.0 && transformed_y < self.height as f64
        {
            return Some((transformed_x as usize, transformed_y as usize));
        }

        None
    }

    pub fn world_to_tile_pos_int_clamped(&self, x : f64, y : f64, transform : &SimpleTransform,
                                         offset_x : f64, offset_y : f64, tileset : &Tileset) -> (usize, usize)
    {
        let (transformed_x, transformed_y) = self.world_to_tile_pos(x, y, transform, offset_x, offset_y, tileset);

        let clamped_x = (transformed_x as usize).clamp(0, self.width.saturating_sub(1));
        let clamped_y = (transformed_y as usize).clamp(0, self.height.saturating_sub(1));

        (clamped_x, clamped_y)
    }

    pub fn tile_range_for_screen_bounds(&self, transform : &LerpedTransform<f32>, offset_x : f64, offset_y : f64,
                                        mut bounds : BakedRect<f64>, tileset : &Tileset) -> BakedRect<usize>
    {
        // TODO: Take lerping into account!
        bounds.translate(-transform.x.value as f64, -transform.y.value as f64);
        bounds.grow_for_angle(-transform.angle.value as f64);
        bounds.scale(1.0 / transform.scale_x.value.max(0.0000001) as f64, 1.0 / transform.scale_y.value.max(0.0000001) as f64);
        bounds.translate(-offset_x * tileset.tile_width, -offset_y * tileset.tile_height);
        bounds.extrude(2.0);

        let x1 = (bounds.x1 / tileset.tile_width) as usize;
        let y1 = (bounds.y1 / tileset.tile_height) as usize;
        let x2 = (bounds.x2 / tileset.tile_width) as usize + 1;
        let y2 = (bounds.y2 / tileset.tile_height) as usize + 1;

        return BakedRect
        {
            x1,
            y1,
            x2,
            y2
        };
    }

    pub fn update_sprite_layout(&self, tileset : &mut Tileset, sheet : &Sheet, item : &mut MultiTileSprite, range : &BakedRect<usize>)
    {
        item.tile_w = tileset.tile_width as f32;
        item.tile_h = tileset.tile_height as f32;

        item.tiles.clear();

        for y in range.y1..range.y2
        {
            for x in range.x1..range.x2
            {
                if let Some(tile) = self.tile(x, y)
                {
                    if let Some(drawable_tile) = TilemapGrid::drawable_positioned_tile(tileset, sheet, x as f32, y as f32, tile.index)
                    {
                        item.tiles.push(drawable_tile);
                    }
                }
            }
        }
    }

    pub fn drawable_positioned_tile(tileset : &mut Tileset, sheet : &Sheet, x : f32, y : f32, index : u32) -> Option<PositionedTile>
    {
        let sheet_w = sheet.width() as f32;
        let sheet_h = sheet.height() as f32;

        if let Some(tileset_tile) = tileset.tiles.get_mut(index as usize)
        {
            if let Some(slice_source) = tileset_tile.slice.as_mut()
            {
                let slice_id = slice_source.updated_slice_id(sheet);

                if let Some(slice) = sheet.slice(slice_id)
                {
                    let sprite_x = x as f32;
                    let sprite_y = y as f32;

                    return Some(PositionedTile
                    {
                        x : sprite_x,
                        y : sprite_y,
                        texture_x : slice.texture_x / sheet_w,
                        texture_y : slice.texture_y / sheet_h,
                        texture_w : slice.texture_w / sheet_w,
                        texture_h : slice.texture_h / sheet_h
                    });
                }
            }
        }

        None
    }
}

impl Serialize for TilemapGrid
{
    fn serialize<S : Serializer>(&self, serializer : S) -> Result<S::Ok, S::Error>
    {
        let tiles = self.to_vec_raw();

        let raw = TilemapGridRaw
        {
            width : self.width,
            height : self.height,
            tiles
        };

        raw.serialize(serializer)
    }
}

impl<'de> Deserialize<'de> for TilemapGrid
{
    fn deserialize<D : Deserializer<'de>>(deserializer : D) -> Result<TilemapGrid, D::Error>
    {
        let raw = TilemapGridRaw::deserialize(deserializer)?;

        if raw.width <= 0
        {
            return Err(D::Error::invalid_value(Unexpected::Unsigned(raw.width as u64), &"tilemap width > 0"));
        }

        if raw.height <= 0
        {
            return Err(D::Error::invalid_value(Unexpected::Unsigned(raw.height as u64), &"tilemap height > 0"));
        }

        if raw.tiles.len() != raw.width * raw.height * 2
        {
            return Err(D::Error::invalid_length(raw.tiles.len(), &"tile length = width * height * 2"));
        }

        Ok(TilemapGrid::from_vec_raw(raw.width, raw.height, raw.tiles))
    }
}

#[derive(Serialize, Deserialize)]
pub struct TilemapGridRaw
{
    #[serde(rename = "w")]
    #[serde(alias = "width")]
    width : usize,
    #[serde(rename = "h")]
    #[serde(alias = "height")]
    height : usize,
    #[serde(rename = "tl")]
    #[serde(alias = "tiles")]
    tiles : Vec<u32>
}

#[derive(Component, Clone, Serialize, Deserialize, Debug)]
pub struct Tilemap
{
    #[serde(rename = "ts")]
    #[serde(alias = "tileset")]
    tileset : DataHandle<Tileset>,
    grid : TilemapGrid,
    #[serde(default)]
    #[serde(rename = "offx")]
    #[serde(alias = "offset_x")]
    offset_x : f64,
    #[serde(default)]
    #[serde(rename = "offy")]
    #[serde(alias = "offset_y")]
    offset_y : f64
}

impl Tilemap
{
    pub fn new(width : usize, height : usize, tileset : DataHandle<Tileset>) -> Tilemap
    {
        Tilemap
        {
            grid : TilemapGrid::new(width, height),
            tileset,
            offset_x : 0.0,
            offset_y : 0.0
        }
    }

    pub fn with_grid(grid : TilemapGrid, tileset : DataHandle<Tileset>) -> Tilemap
    {
        Tilemap
        {
            grid,
            tileset,
            offset_x : 0.0,
            offset_y : 0.0
        }
    }

    pub fn grid(&self) -> &TilemapGrid
    {
        &self.grid
    }

    pub fn grid_mut(&mut self) -> &mut TilemapGrid
    {
        &mut self.grid
    }

    pub fn tileset(&self) -> &DataHandle<Tileset>
    {
        &self.tileset
    }

    pub fn set_tileset(&mut self, tileset : DataHandle<Tileset>)
    {
        self.tileset = tileset;
    }

    pub fn offset_x(&self) -> f64 { self.offset_x }
    pub fn offset_y(&self) -> f64 { self.offset_y }
    pub fn offset(&self) -> (f64, f64)
    {
        (self.offset_x, self.offset_y)
    }

    pub fn set_offset(&mut self, offset_x : f64, offset_y : f64)
    {
        self.offset_x = offset_x;
        self.offset_y = offset_y;
    }

    pub fn world_to_tile_pos(&self, x : f64, y : f64, transform : &SimpleTransform,
                             tileset_res : &mut DataStore<Tileset>, load_error : LoadErrorMode) -> Option<(f64, f64)>
    {
        // TODO needing to load tileset to get tile size isn't great
        if let Some(tileset) = self.tileset.get_mut(tileset_res, load_error)
        {
            return Some(self.grid.world_to_tile_pos(x, y, &transform, self.offset_x, self.offset_y, tileset));
        }

        None
    }

    pub fn world_to_tile_pos_int(&self, x : f64, y : f64, transform : &SimpleTransform,
                             tileset_res : &mut DataStore<Tileset>, load_error : LoadErrorMode) -> Option<(usize, usize)>
    {
        // TODO needing to load tileset to get tile size isn't great
        if let Some(tileset) = self.tileset.get_mut(tileset_res, load_error)
        {
            return self.grid.world_to_tile_pos_int(x, y, &transform, self.offset_x, self.offset_y, tileset);
        }

        None
    }

    pub fn world_to_tile_pos_int_clamped(&self, x : f64, y : f64, transform : &SimpleTransform,
                                 tileset_res : &mut DataStore<Tileset>, load_error : LoadErrorMode) -> Option<(usize, usize)>
    {
        if let Some(tileset) = self.tileset.get_mut(tileset_res, load_error)
        {
            return Some(self.grid.world_to_tile_pos_int_clamped(x, y, &transform, self.offset_x, self.offset_y, tileset));
        }

        None
    }

    pub fn tile_range_for_camera(&self, drawable : &Drawable,
                                 camera : &Camera, tileset_res : &mut DataStore<Tileset>, load_error : LoadErrorMode) -> Option<BakedRect<usize>>
    {
        // TODO needing to load tileset to get tile size isn't great
        if let Some(tileset) = self.tileset.get_mut(tileset_res, load_error)
        {
            let bounds = camera.bounds_lerped(drawable.scroll_rate_x as f64, drawable.scroll_rate_y as f64);

            return Some(self.grid.tile_range_for_screen_bounds(&drawable.transform, self.offset_x, self.offset_y, bounds, tileset));
        }

        None
    }

    pub fn update_sprite_layout(&self, tileset_res : &mut DataStore<Tileset>, sheet_res : &mut DataStore<Sheet>,
                                item : &mut MultiTileSprite, range : &BakedRect<usize>, load_error : LoadErrorMode)
    {
        if let Some(tileset) = self.tileset.get_mut(tileset_res, load_error)
        {
            if let Some(sheet) = tileset.sheet.get(sheet_res, load_error)
            {
                self.grid.update_sprite_layout(tileset, sheet, item, range);

                item.sheet_id = tileset.sheet.id();
                item.offset_x = self.offset_x as f32;
                item.offset_y = self.offset_y as f32;
            }
        }
    }
}

pub struct TilemapThinker<T : SceneType + 'static>
{
    load_error : LoadErrorMode,
    phantom : std::marker::PhantomData::<T>
}

impl<T : SceneType + 'static> TilemapThinker<T>
{
    pub fn new(load_error : LoadErrorMode) -> TilemapThinker::<T>
    {
        TilemapThinker::<T>
        {
            load_error,
            phantom : std::marker::PhantomData
        }
    }
}

impl<T : SceneType + 'static> ThinkerSystem<T> for TilemapThinker<T>
{
    fn think(&mut self, args : ThinkerArgs<T>)
    {
        let mut tilemap_store = args.components.store_mut::<Tilemap>();
        let mut drawable_store = args.components.store_mut::<Drawable>();
        let mut transform_store = args.components.store_mut::<Transform>();
        let camera_store = args.components.store_mut::<Camera>();
        let mut sheet_res = args.game.res().store_mut::<Sheet>();
        let mut tileset_res = args.game.res().store_mut::<Tileset>();

        // TODO redo drawing infrastructure to support multiple cameras for tilemaps
        if let Some((_, camera)) = camera_store.first()
        {
            for (entity, tilemap, freeze_flags) in tilemap_store.iter_filtered_mut(FreezeFlags::REASON_HIDDEN | FreezeFlags::REASON_CULLED)
            {
                if let Some(drawable) = drawable_store.get_entity_mut(&entity)
                {
                    if let Some(transform) = transform_store.get_entity_mut(&entity)
                    {
                        drawable.update_from_transform(transform);
                    }

                    if freeze_flags.is_empty()
                    {
                        if let Some(tile_range) = tilemap.tile_range_for_camera(drawable, camera, &mut tileset_res, self.load_error)
                        {
                            match &mut drawable.item
                            {
                                DrawableItem::MultiTileSprite(_) => (),
                                _ => drawable.item = DrawableItem::MultiTileSprite(MultiTileSprite::new())
                            }

                            match &mut drawable.item
                            {
                                DrawableItem::MultiTileSprite(item) =>
                                {
                                    tilemap.update_sprite_layout(&mut tileset_res, &mut sheet_res, item, &tile_range, self.load_error);
                                }
                                _ => ()
                            }
                        }
                    }
                }
            }
        }
    }

    fn priority(&self) -> i32 { crate::SYSTEM_PRIORITY_POST_FRAME }
}
