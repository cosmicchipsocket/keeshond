
use crate::collision::optvec::OptVec;

use keeshond::scene::Entity;
use keeshond::util::BakedRect;

use std::cell::{RefCell, RefMut};

use smallvec::SmallVec;
use bit_set::BitSet;
use rustc_hash::FxHashMap;
use crate::collision::broadstore::CelId::Overflow;

const MIN_CEL_GRID_SIZE : u16 = 4;
const MAX_SMALL_CEL_RANGE : i32 = 4;
const MAX_LARGE_CEL_RANGE : i32 = 96;
const LARGE_CEL_FACTOR : i32 = 8;
const CEL_MAINTAIN_INTERVAL : i32 = 8;

enum CelId
{
    Small(SmallVec<[DirectoryCelInfo; 4]>),
    Large(SmallVec<[DirectoryCelInfo; 4]>),
    Overflow(usize)
}

enum CelLocation
{
    Small(BakedRect<i32>),
    Large(BakedRect<i32>),
    Overflow
}

#[derive(Debug, Clone, PartialEq)]
pub struct CollisionBroadItem
{
    pub entity : Entity,
    pub positioned_bound : BakedRect<f64>
}

struct DirectoryCelInfo
{
    cel : (i32, i32),
    id : usize
}

struct CollisionDirectoryItem
{
    item : CollisionBroadItem,
    small_bound : BakedRect<i32>,
    dirty : bool,
    cel_id : CelId
}

impl CollisionDirectoryItem
{
    fn new(item : CollisionBroadItem) -> CollisionDirectoryItem
    {
        CollisionDirectoryItem
        {
            item,
            small_bound : BakedRect::default(),
            dirty : false,
            cel_id : Overflow(std::usize::MAX)
        }
    }
}

fn find_cel_coords(bound : &BakedRect<f64>, cel_size : f64) -> BakedRect<i32>
{
    let mut x1 = (bound.x1 / cel_size).floor() as i32;
    let mut y1 = (bound.y1 / cel_size).floor() as i32;
    let mut x2 = (bound.x2 / cel_size).floor() as i32;
    let mut y2 = (bound.y2 / cel_size).floor() as i32;

    if x1 > x2
    {
        std::mem::swap(&mut x1, &mut x2);
    }

    if y1 > y2
    {
        std::mem::swap(&mut y1, &mut y2);
    }

    BakedRect
    {
        x1,
        y1,
        x2,
        y2
    }
}

#[inline(always)]
fn small_coord_to_large(coord : i32) -> i32
{
    if coord < 0
    {
        return (coord - LARGE_CEL_FACTOR + 1) / LARGE_CEL_FACTOR;
    }

    coord / LARGE_CEL_FACTOR
}

#[inline(always)]
fn small_cel_coords_to_large(cel_coords : &BakedRect<i32>) -> BakedRect<i32>
{
    BakedRect
    {
        x1 : small_coord_to_large(cel_coords.x1),
        y1 : small_coord_to_large(cel_coords.y1),
        x2 : small_coord_to_large(cel_coords.x2),
        y2 : small_coord_to_large(cel_coords.y2)
    }
}

#[inline(always)]
fn cel_coords_to_location(cel_coords : &BakedRect<i32>) -> CelLocation
{
    let size = (cel_coords.x2 - cel_coords.x1) + (cel_coords.y2 - cel_coords.y1);

    if size <= MAX_SMALL_CEL_RANGE
    {
        return CelLocation::Small(cel_coords.clone());
    }
    else if size <= MAX_LARGE_CEL_RANGE
    {
        return CelLocation::Large(small_cel_coords_to_large(cel_coords));
    }

    CelLocation::Overflow
}

struct CelStore
{
    small : FxHashMap<(i32, i32), OptVec<usize>>,
    large : FxHashMap<(i32, i32), OptVec<usize>>,
    overflow : OptVec<usize>,
}

pub struct CollisionBroadStore
{
    directory : OptVec<CollisionDirectoryItem>,
    cels : CelStore,
    already_overlapping : RefCell<BitSet>,
    cel_size : f64,
    dirty_ids : Vec<usize>,
    maintain_timer : i32
}

fn add_internal(cels : &mut CelStore, directory_id : usize,
                cel_bounds : BakedRect<i32>, entry : &mut CollisionDirectoryItem)
{
    let cel_location = cel_coords_to_location(&cel_bounds);

    match cel_location
    {
        CelLocation::Small(bound) =>
        {
            let mut entry_cels = SmallVec::new();

            for y in bound.y1..bound.y2 + 1
            {
                for x in bound.x1..bound.x2 + 1
                {
                    let cel = cels.small.entry((x, y)).or_insert(OptVec::new());
                    let id = cel.add(directory_id);

                    entry_cels.push(DirectoryCelInfo
                    {
                        cel : (x, y),
                        id
                    });
                }
            }

            entry.cel_id = CelId::Small(entry_cels)
        }
        CelLocation::Large(bound) =>
        {
            let mut entry_cels = SmallVec::new();

            for y in bound.y1..bound.y2 + 1
            {
                for x in bound.x1..bound.x2 + 1
                {
                    let cel = cels.large.entry((x, y)).or_insert(OptVec::new());
                    let id = cel.add(directory_id);

                    entry_cels.push(DirectoryCelInfo
                    {
                        cel : (x, y),
                        id
                    });
                }
            }

            entry.cel_id = CelId::Large(entry_cels)
        }
        CelLocation::Overflow =>
        {
            entry.cel_id = CelId::Overflow(cels.overflow.add(directory_id));
        }
    }

    entry.small_bound = cel_bounds;
}

fn remove_internal(cels : &mut CelStore, entry : &mut CollisionDirectoryItem)
{
    match &entry.cel_id
    {
        CelId::Small(entry_cels) =>
        {
            for cel_info in entry_cels.iter()
            {
                if let Some(cel) = cels.small.get_mut(&cel_info.cel)
                {
                    cel.remove(cel_info.id);

                    if cel.count() == 0
                    {
                        cels.small.remove(&cel_info.cel);
                    }
                }
            }
        }
        CelId::Large(entry_cels) =>
        {
            for cel_info in entry_cels.iter()
            {
                if let Some(cel) = cels.large.get_mut(&cel_info.cel)
                {
                    cel.remove(cel_info.id);

                    if cel.count() == 0
                    {
                        cels.large.remove(&cel_info.cel);
                    }
                }
            }
        }
        Overflow(cel_id) =>
        {
            cels.overflow.remove(*cel_id);
        }
    }

    entry.cel_id = Overflow(std::usize::MAX);
}

impl CollisionBroadStore
{
    pub fn new(cel_size : u16) -> CollisionBroadStore
    {
        if cel_size < MIN_CEL_GRID_SIZE
        {
            panic!("CollisionBroadStore cel size too small!");
        }

        CollisionBroadStore
        {
            directory : OptVec::new(),
            cels : CelStore
            {
                small : FxHashMap::default(),
                large : FxHashMap::default(),
                overflow : OptVec::new()
            },
            already_overlapping : RefCell::new(BitSet::with_capacity(1024)),
            cel_size : cel_size as f64,
            dirty_ids : Vec::new(),
            maintain_timer : 0
        }
    }

    pub fn add(&mut self, item : CollisionBroadItem) -> usize
    {
        self.update_dirty_bounds();

        let new_entry = CollisionDirectoryItem::new(item);
        let cel_bounds = find_cel_coords(&new_entry.item.positioned_bound, self.cel_size);

        let directory_id = self.directory.add(new_entry);

        let entry = self.directory.get_mut(directory_id).unwrap();
        add_internal(&mut self.cels, directory_id, cel_bounds, entry);

        directory_id
    }

    pub fn remove(&mut self, id : usize)
    {
        self.update_dirty_bounds();

        if let Some(mut entry) = self.directory.remove(id)
        {
            remove_internal(&mut self.cels, &mut entry);
        }
    }

    pub fn queue_update(&mut self, id : usize, positioned_bound : BakedRect<f64>)
    {
        if let Some(entry) = self.directory.get_mut(id)
        {
            entry.item.positioned_bound = positioned_bound;

            if !entry.dirty
            {
                self.dirty_ids.push(id);
                entry.dirty = true;
            }
        }
    }

    pub fn positioned_bound(&mut self, id : usize) -> Option<BakedRect<f64>>
    {
        if let Some(entry) = self.directory.get(id)
        {
            return Some(entry.item.positioned_bound.clone());
        }

        None
    }

    pub fn update_dirty_bounds(&mut self)
    {
        if self.dirty_ids.is_empty()
        {
            return;
        }

        for id in &self.dirty_ids
        {
            if let Some(entry) = self.directory.get_mut(*id)
            {
                entry.dirty = false;

                let new_cel_bounds = find_cel_coords(&entry.item.positioned_bound, self.cel_size);

                if new_cel_bounds != entry.small_bound
                {
                    // New cel bounds, remove and readd
                    remove_internal(&mut self.cels, entry);
                    add_internal(&mut self.cels, *id, new_cel_bounds, entry);
                }
            }
        }

        self.dirty_ids.clear();
    }

    pub fn query_overlapping(&mut self, id : usize, overlaps : &mut Vec<CollisionBroadItem>)
    {
        self.update_dirty_bounds();

        overlaps.clear();

        if let Some(entry) = self.directory.get(id)
        {
            match entry.cel_id
            {
                CelId::Small(_) | CelId::Large(_) =>
                {
                    let small_cel_bound = &entry.small_bound;

                    if small_cel_bound.x1 == small_cel_bound.x2 && small_cel_bound.y1 == small_cel_bound.y2
                    {
                        self.query_overlapping_single_cel(id, overlaps, &entry.item.positioned_bound, small_cel_bound.x1, small_cel_bound.y1);
                        self.query_overlapping_overflow_cels(id, overlaps, &entry.item.positioned_bound);
                    }
                    else
                    {
                        self.query_overlapping_multi_cel(id, overlaps, &entry.item.positioned_bound, &entry.small_bound);
                        self.query_overlapping_overflow_cels(id, overlaps, &entry.item.positioned_bound);
                    }
                },
                CelId::Overflow(_) =>
                {
                    self.query_overlapping_all(id, overlaps, &entry.item.positioned_bound);
                }
            }
        }
    }

    #[inline(always)]
    fn query_overlapping_single_cel(&self, id : usize, overlaps : &mut Vec<CollisionBroadItem>, bound : &BakedRect<f64>, small_cel_x : i32, small_cel_y: i32)
    {
        let large_cel_x = small_coord_to_large(small_cel_x);
        let large_cel_y = small_coord_to_large(small_cel_y);

        if let Some(cel) = self.cels.small.get(&(small_cel_x, small_cel_y))
        {
            self.check_other_cel_single(id, overlaps, bound, cel);
        }

        if let Some(cel) = self.cels.large.get(&(large_cel_x, large_cel_y))
        {
            self.check_other_cel_single(id, overlaps, bound, cel);
        }
    }

    #[inline(always)]
    fn check_other_cel_single(&self, id : usize, overlaps : &mut Vec<CollisionBroadItem>, bound : &BakedRect<f64>, cel : &OptVec<usize>)
    {
        for (_, other_id) in cel.iter()
        {
            if let Some(other) = self.directory.get(*other_id)
            {
                if id != *other_id && bound.overlaps(&other.item.positioned_bound)
                {
                    overlaps.push(other.item.clone());
                }
            }
        }
    }

    #[inline(always)]
    fn query_overlapping_multi_cel(&self, id : usize, overlaps : &mut Vec<CollisionBroadItem>,
                                                                   bound : &BakedRect<f64>, small_cel_bound : &BakedRect<i32>)
    {
        let mut already_overlapping_borrow = self.already_overlapping.borrow_mut();
        already_overlapping_borrow.clear();

        for y in small_cel_bound.y1..small_cel_bound.y2 + 1
        {
            for x in small_cel_bound.x1..small_cel_bound.x2 + 1
            {
                if let Some(cel) = self.cels.small.get(&(x, y))
                {
                    self.check_other_cel_multi(id, overlaps, bound, &mut already_overlapping_borrow, cel);
                }
            }
        }

        let large_bound = small_cel_coords_to_large(&small_cel_bound);

        for y in large_bound.y1..large_bound.y2 + 1
        {
            for x in large_bound.x1..large_bound.x2 + 1
            {
                if let Some(cel) = self.cels.large.get(&(x, y))
                {
                    self.check_other_cel_multi(id, overlaps, bound, &mut already_overlapping_borrow, cel);
                }
            }
        }
    }

    #[inline(always)]
    fn check_other_cel_multi(&self, id : usize, overlaps : &mut Vec<CollisionBroadItem>, bound : &BakedRect<f64>,
                       already_overlapping_borrow : &mut RefMut<BitSet>, cel : &OptVec<usize>)
    {
        for (_, other_id) in cel.iter()
        {
            if let Some(other) = self.directory.get(*other_id)
            {
                if id != *other_id && !already_overlapping_borrow.contains(*other_id)
                    && bound.overlaps(&other.item.positioned_bound)
                {
                    overlaps.push(other.item.clone());
                    already_overlapping_borrow.insert(*other_id);
                }
            }
        }
    }

    #[inline(always)]
    fn query_overlapping_overflow_cels(&self, id : usize, overlaps : &mut Vec<CollisionBroadItem>, bound : &BakedRect<f64>)
    {
        for (_, other_id) in self.cels.overflow.iter()
        {
            if let Some(other) = self.directory.get(*other_id)
            {
                if id != *other_id && bound.overlaps(&other.item.positioned_bound)
                {
                    overlaps.push(other.item.clone());
                }
            }
        }
    }

    #[inline(always)]
    fn query_overlapping_all(&self, id : usize, overlaps : &mut Vec<CollisionBroadItem>, bound : &BakedRect<f64>)
    {
        for (other_id, other) in self.directory.iter()
        {
            if id != other_id && bound.overlaps(&other.item.positioned_bound)
            {
                overlaps.push(other.item.clone());
            }
        }
    }

    pub fn maintain(&mut self)
    {
        self.maintain_timer = (self.maintain_timer + 1) % CEL_MAINTAIN_INTERVAL;
        self.directory.shrink();
        self.cels.overflow.shrink();

        for (pos, cel) in &mut self.cels.small
        {
            if (pos.0 + pos.1) % CEL_MAINTAIN_INTERVAL == self.maintain_timer
            {
                cel.shrink();
            }
        }

        for (pos, cel) in &mut self.cels.large
        {
            if (pos.0 + pos.1) % CEL_MAINTAIN_INTERVAL == self.maintain_timer
            {
                cel.shrink();
            }
        }

        let mut already_overlapping_borrow = self.already_overlapping.borrow_mut();

        already_overlapping_borrow.clear();
        already_overlapping_borrow.shrink_to_fit();
    }
}
