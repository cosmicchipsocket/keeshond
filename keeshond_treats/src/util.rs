use serde::{Serialize, Serializer};
use std::collections::{HashMap, BTreeMap};

pub(crate) fn sort_map<S, T : Serialize + Ord, U : Serialize>(map : &HashMap<T, U>, serializer : S) -> Result<S::Ok, S::Error> where S : Serializer
{
    let sorted : BTreeMap<_, _> = map.iter().collect();

    sorted.serialize(serializer)
}
