use keeshond::scene::{Component, ComponentCallbacks, ComponentControl, ComponentRef, ComponentRefMut, ComponentStore, Entity, SceneType, ThinkerArgs, ThinkerSystem};
use keeshond::util::{BakedRect, Circle};

use keeshond_derive::Component;

use crate::collision::broadstore::CollisionBroadStore;
use crate::spawnable::SpawnableTag;

pub use crate::collision::broadstore::CollisionBroadItem;

use std::cell::RefCell;
use std::rc::Rc;
use serde::{Deserialize, Serialize};
use keeshond::datapack::DataStore;
use crate::tilemap::{Tilemap, Tileset};
use crate::world::Transform;

mod optvec;
pub mod broadstore;

const GRID_DEFAULT_CEL_SIZE : u16 = 256;

pub struct CollisionResults
{
    pub bound_overlaps : Vec<CollisionBroadItem>,
    pub shape_overlaps : Vec<Entity>
}

impl CollisionResults
{
    pub fn new() -> CollisionResults
    {
        CollisionResults
        {
            bound_overlaps : Vec::new(),
            shape_overlaps : Vec::new()
        }
    }

    pub fn iter_overlapping_bound_tags<F, T>(&self, spawn_tag_store : &ComponentStore<SpawnableTag<T>>, mut func : F)
        where F : FnMut(&CollisionBroadItem, &SpawnableTag<T>), T : SceneType + 'static
    {
        for overlap in &self.bound_overlaps
        {
            if let Some(tag) = spawn_tag_store.get_entity(&overlap.entity)
            {
                func(overlap, tag);
            }
        }
    }

    pub fn iter_overlapping_shape_tags<F, T>(&self, spawn_tag_store : &ComponentStore<SpawnableTag<T>>, mut func : F)
        where F : FnMut(&Entity, &SpawnableTag<T>), T : SceneType + 'static
    {
        for entity in &self.shape_overlaps
        {
            if let Some(tag) = spawn_tag_store.get_entity(&entity)
            {
                func(entity, tag);
            }
        }
    }
}

#[derive(Clone, PartialEq, Serialize, Deserialize, Debug)]
pub enum CollisionShape
{
    None,
    Box(BakedRect<f64>),
    Circle(Circle<f64>)
}

impl Default for CollisionShape
{
    fn default() -> Self
    {
        CollisionShape::Box(BakedRect::default())
    }
}

impl CollisionShape
{
    fn overlaps_box_to_box(a : &BakedRect<f64>, b : &BakedRect<f64>) -> bool
    {
        a.overlaps(&b)
    }

    fn overlaps_circle_to_circle(a : &Circle<f64>, b : &Circle<f64>) -> bool
    {
        a.overlaps(&b)
    }

    fn overlaps_box_to_circle(a : &BakedRect<f64>, b : &Circle<f64>) -> bool
    {
        let rect_x = b.x.clamp(a.x1, a.x2);
        let rect_y = b.y.clamp(a.y1, a.y2);

        b.point_overlaps(rect_x, rect_y)
    }

    pub fn overlaps(&self, x : f64, y : f64, other : &CollisionShape, other_x : f64, other_y : f64) -> bool
    {
        match self
        {
            CollisionShape::Box(bound) =>
            {
                match other
                {
                    CollisionShape::Box(other_bound) =>
                    {
                        CollisionShape::overlaps_box_to_box(&bound.clone_translated(x, y), &other_bound.clone_translated(other_x, other_y))
                    }
                    CollisionShape::Circle(circle) =>
                    {
                        CollisionShape::overlaps_box_to_circle(&bound.clone_translated(x, y), &circle.clone_translated(other_x, other_y))
                    }
                    CollisionShape::None => { false }
                }
            }
            CollisionShape::Circle(circle) =>
            {
                match other
                {
                    CollisionShape::Box(bound) =>
                    {
                        CollisionShape::overlaps_box_to_circle(&bound.clone_translated(other_x, other_y), &circle.clone_translated(x, y))
                    }
                    CollisionShape::Circle(other_circle) =>
                    {
                        CollisionShape::overlaps_circle_to_circle(&circle.clone_translated(x, y), &other_circle.clone_translated(other_x, other_y))
                    }
                    CollisionShape::None => { false }
                }
            }
            CollisionShape::None => { false }
        }
    }

    pub fn make_bound(&self) -> BakedRect<f64>
    {
        match self
        {
            CollisionShape::Box(bound) => { bound.clone() }
            CollisionShape::Circle(circle) => { BakedRect
            {
                x1 : circle.x - circle.radius,
                y1 : circle.y - circle.radius,
                x2 : circle.x + circle.radius,
                y2 : circle.y + circle.radius
            }}
            CollisionShape::None => { BakedRect::default() }
        }
    }
}


pub struct Collidable
{
    pub(crate) x : f64,
    pub(crate) y : f64,
    pub(crate) bound : BakedRect<f64>,
    pub(crate) broad : Option<BroadStoreRef>,
    pub(crate) collision_shape : CollisionShape,
    pub(crate) want_indexed : bool,
    pub(crate) dirty : bool,
    pub(crate) teleported : bool
}

impl Collidable
{
    pub fn new(x : f64, y : f64, collision_shape : CollisionShape, indexed : bool) -> Collidable
    {
        let bound = collision_shape.make_bound();

        Collidable
        {
            x,
            y,
            bound,
            broad : None,
            collision_shape,
            want_indexed: indexed,
            dirty : false,
            teleported : true
        }
    }

    pub fn x(&self) -> f64 { self.x }
    pub fn y(&self) -> f64 { self.y }

    pub fn pos(&self) -> (f64, f64) { (self.x, self.y) }
    pub fn set_pos(&mut self, x : f64, y : f64)
    {
        self.teleported = false;

        if x != self.x || y != self.y
        {
            self.x = x;
            self.y = y;
            self.dirty = true;

            self.update_broad();
        }
    }
    pub fn teleport(&mut self, x : f64, y : f64)
    {
        self.teleported = true;

        if x != self.x || y != self.y
        {
            self.x = x;
            self.y = y;
            self.dirty = true;

            self.update_broad();
        }
    }

    pub fn bound(&self) -> &BakedRect<f64>
    {
        &self.bound
    }

    #[inline]
    pub fn positioned_bound(&self) -> BakedRect<f64>
    {
        let mut positioned = self.bound.clone();

        positioned.translate(self.x, self.y);

        positioned
    }

    pub fn set_collision_shape(&mut self, shape : CollisionShape)
    {
        if self.collision_shape != shape
        {
            self.collision_shape = shape;
            self.bound = self.collision_shape.make_bound();

            self.update_broad();
        }
    }

    pub fn overlaps_positioned_bound(&self, positioned_bound : &BakedRect<f64>) -> bool
    {
        let positioned = self.positioned_bound();

        positioned.overlaps(positioned_bound)
    }

    pub fn overlaps_other_bound(&self, other : &Collidable) -> bool
    {
        let positioned = self.positioned_bound();
        let other_positioned = other.positioned_bound();

        positioned.overlaps(&other_positioned)
    }

    pub fn overlaps_other_shape(&self, other : &Collidable) -> bool
    {
        self.collision_shape.overlaps(self.x, self.y, &other.collision_shape, other.x, other.y)
    }

    pub fn query_overlapping_bounds(&self, out_results : &mut CollisionResults)
    {
        if let Some(broad) = &self.broad
        {
            if let Some(store) = &broad.store
            {
                let mut broad_borrow = store.borrow_mut();

                broad_borrow.query_overlapping(broad.id, &mut out_results.bound_overlaps);
                out_results.shape_overlaps.clear();
            }
        }
    }

    pub fn query_overlapping_shapes(&self, collidable_store : &ComponentStore<Collidable>, out_results : &mut CollisionResults)
    {
        if let Some(broad) = &self.broad
        {
            if let Some(store) = &broad.store
            {
                let mut broad_borrow = store.borrow_mut();

                broad_borrow.query_overlapping(broad.id, &mut out_results.bound_overlaps);
                out_results.shape_overlaps.clear();

                for overlap in &out_results.bound_overlaps
                {
                    if let Some(other_collidable) = collidable_store.get_entity(&overlap.entity)
                    {
                        if self.overlaps_other_shape(other_collidable)
                        {
                            out_results.shape_overlaps.push(overlap.entity.clone());
                        }
                    }
                }
            }
        }
    }

    pub fn collision_indexed(&self) -> bool
    {
        self.broad.is_some()
    }

    pub fn update_tilemap_bound(&mut self, tilemap : &Tilemap, tileset_res : &mut DataStore<Tileset>) -> bool
    {
        if let Some(tileset) = tilemap.tileset().get_if_resolved(tileset_res)
        {
            let (tile_width, tile_height) = tileset.tile_size();
            let (grid_width, grid_height) = tilemap.grid().size();

            self.bound = BakedRect
            {
                x1 : 0.0,
                y1 : 0.0,
                x2 : tile_width * grid_width as f64,
                y2 : tile_height * grid_height as f64
            };
            self.collision_shape = CollisionShape::None;

            self.update_broad();

            return true;
        }

        false
    }

    #[inline]
    fn update_broad(&mut self)
    {
        if let Some(broad) = &mut self.broad
        {
            if let Some(store) = &mut broad.store
            {
                let mut broad_borrow = store.borrow_mut();
                let mut new_bound = self.bound.clone();
                new_bound.translate(self.x, self.y);

                broad_borrow.queue_update(broad.id, new_bound);
            }
        }
    }
}

impl Component for Collidable
{
    fn maintain_ordering() -> bool where Self : Sized { false }

    fn add_priority() -> i32 where Self: Sized
    {
        crate::COMPONENT_PRIORITY_PHYSICAL
    }
}

pub struct BorrowedMutCollidable<'a>
{
    pub(crate) reference : &'a mut Collidable
}

impl<'a> BorrowedMutCollidable<'a>
{
    #[inline]
    pub fn x(&self) -> f64 { self.reference.x() }
    #[inline]
    pub fn y(&self) -> f64 { self.reference.y() }

    #[inline]
    pub fn pos(&self) -> (f64, f64) { self.reference.pos() }
    #[inline]
    pub fn set_pos(&mut self, x : f64, y : f64)
    {
        self.reference.set_pos(x, y)
    }
    #[inline]
    pub fn teleport(&mut self, x : f64, y : f64)
    {
        self.reference.teleport(x, y)
    }

    #[inline]
    pub fn bound(&self) -> &BakedRect<f64>
    {
        self.reference.bound()
    }
    #[inline]
    pub fn positioned_bound(&self) -> BakedRect<f64>
    {
        self.reference.positioned_bound()
    }
    #[inline]
    pub fn set_collision_shape(&mut self, shape : CollisionShape)
    {
        self.reference.set_collision_shape(shape)
    }
    #[inline]
    pub fn bound_overlaps(&self, other : &Collidable) -> bool
    {
        self.reference.overlaps_other_bound(other)
    }
    #[inline]
    pub fn query_overlapping_bounds(&self, out_results : &mut CollisionResults)
    {
        self.reference.query_overlapping_bounds(out_results)
    }
    #[inline]
    pub fn collision_indexed(&self) -> bool
    {
        self.reference.collision_indexed()
    }
}

impl<'a> AsRef<Collidable> for BorrowedMutCollidable<'a>
{
    fn as_ref(&self) -> &Collidable
    {
        self.reference
    }
}

impl<'a> ComponentRef<'a> for Collidable
{
    type StoreRef = &'a Collidable;

    fn store_get(&'a self) -> Self::StoreRef
    {
        self
    }
}

impl<'a> ComponentRefMut<'a> for Collidable
{
    type StoreRefMut = BorrowedMutCollidable<'a>;

    fn store_get_mut(&'a mut self) -> Self::StoreRefMut
    {
        BorrowedMutCollidable
        {
            reference : self
        }
    }
}

pub struct CollidableTransformSyncThinker<T : SceneType + 'static>
{
    phantom : std::marker::PhantomData::<T>
}

impl<T : SceneType + 'static> CollidableTransformSyncThinker<T>
{
    pub fn new() -> CollidableTransformSyncThinker::<T>
    {
        CollidableTransformSyncThinker::<T>
        {
            phantom : std::marker::PhantomData
        }
    }
}

impl<T : SceneType + 'static> ThinkerSystem<T> for CollidableTransformSyncThinker<T>
{
    fn think(&mut self, args : ThinkerArgs<T>)
    {
        let mut transform_store = args.components.store_mut::<Transform>();
        let mut collidable_store = args.components.store_mut::<Collidable>();

        for (entity, collidable) in collidable_store.iter_all_mut()
        {
            if collidable.reference.dirty
            {
                if let Some(transform) = transform_store.get_entity_mut(&entity)
                {
                    transform.data.x = collidable.reference.x;
                    transform.data.y = collidable.reference.y;
                    transform.dirty = false;
                    transform.lerp_suspended = collidable.reference.teleported;
                }

                collidable.reference.dirty = false;
                collidable.reference.teleported = false;
            }
        }
    }

    fn priority(&self) -> i32 { crate::SYSTEM_PRIORITY_POST_PHYSICS }
}


pub(crate) struct BroadStoreRef
{
    pub(crate) id : usize,
    pub(crate) store : Option<Rc<RefCell<CollisionBroadStore>>>
}

#[derive(Component)]
pub(crate) struct CollisionControl
{
    broad : Rc<RefCell<CollisionBroadStore>>
}

impl CollisionControl
{
    pub fn new(cel_size : u16) -> CollisionControl
    {
        CollisionControl
        {
            broad : Rc::new(RefCell::new(CollisionBroadStore::new(cel_size)))
        }
    }
}

impl Default for CollisionControl
{
    fn default() -> Self
    {
        CollisionControl::new(GRID_DEFAULT_CEL_SIZE)
    }
}

pub struct CollisionMaintenanceThinker<T : SceneType + 'static>
{
    phantom : std::marker::PhantomData<T>
}

impl<T : SceneType + 'static> CollisionMaintenanceThinker<T>
{
    pub fn new() -> CollisionMaintenanceThinker<T>
    {
        CollisionMaintenanceThinker::<T>
        {
            phantom : std::marker::PhantomData
        }
    }
}

fn add_collision_item(entity : &Entity, components : &mut ComponentControl)
{
    let mut collidable_store = components.store_mut::<Collidable>();
    let mut collision_store = components.store_mut::<CollisionControl>();
    let control = collision_store.singleton_mut();

    if let Some(collidable) = collidable_store.get_entity_mut(entity)
    {
        if collidable.reference.want_indexed
        {
            add_to_broad_store(entity, collidable.reference, control);
        }
    }
    else
    {
        panic!("Missing Collidable for entity {}!", entity);
    }
}

fn add_to_broad_store(entity : &Entity, collidable : &mut Collidable,
                      control : &mut CollisionControl)
{
    let item = CollisionBroadItem
    {
        entity : entity.clone(),
        positioned_bound: collidable.positioned_bound()
    };

    collidable.broad = Some(BroadStoreRef
    {
        id : control.broad.borrow_mut().add(item),
        store : Some(control.broad.clone())
    });
}

fn remove_collision_item(entity : &Entity, components : &mut ComponentControl)
{
    let mut collidable_store = components.store_mut::<Collidable>();
    let mut collision_store = components.store_mut::<CollisionControl>();
    let control = collision_store.singleton_mut();

    if let Some(collidable) = collidable_store.get_entity_mut(entity)
    {
        if let Some(broad) = &collidable.reference.broad
        {
            control.broad.borrow_mut().remove(broad.id);
        }
    }
    else
    {
        panic!("Missing Collidable for entity {}!", entity);
    }
}

impl<T : SceneType + 'static> ThinkerSystem<T> for CollisionMaintenanceThinker<T>
{
    fn start(&mut self, args : ThinkerArgs<T>)
    {
        let mut callbacks = ComponentCallbacks::new();
        callbacks.push_add_component_callback(Box::new(add_collision_item));
        callbacks.push_remove_component_callback(Box::new(remove_collision_item));

        args.scene.register_callbacks_later::<Collidable>(callbacks);

        // Register existing Collidables too!

        let mut collidable_store = args.components.store_mut::<Collidable>();
        let mut collision_store = args.components.store_mut::<CollisionControl>();
        let control = collision_store.singleton_mut();

        for (entity, collidable) in collidable_store.iter_all_mut()
        {
            if collidable.reference.want_indexed
            {
                add_to_broad_store(&entity, collidable.reference, control);
            }
        }
    }

    fn think(&mut self, args : ThinkerArgs<T>)
    {
        let mut collision_store = args.components.store_mut::<CollisionControl>();
        let control = collision_store.singleton_mut();

        control.broad.borrow_mut().maintain();
    }

    fn priority(&self) -> i32
    {
        crate::SYSTEM_PRIORITY_POST_FRAME
    }
}
