extern crate rand;

use crate::collision::broadstore::CollisionBroadStore;
use crate::collision::CollisionBroadItem;

use keeshond::util::BakedRect;
use keeshond::scene::Entity;

use rand::Rng;
use bit_set::BitSet;

const GRID_DEFAULT_CEL_SIZE : u16 = 64;

fn vec_equal<T : PartialEq>(a : &Vec<T>, b : &Vec<T>) -> bool
{
    if a.len() != b.len()
    {
        return false;
    }

    for i in 0..a.len()
    {
        if a[i] != b[i]
        {
            return false;
        }
    }

    true
}

fn generate_random_item(entity : Entity, grid_snap : f64) -> CollisionBroadItem
{
    let mut rng = rand::thread_rng();

    let mut x1 = rng.gen_range(-5000.0..5000.0);
    let mut y1 = rng.gen_range(-5000.0..5000.0);

    let probability = rng.gen::<f32>();

    let mut x2;
    let mut y2;

    if probability <= 0.1
    {
        x2 = x1 + rng.gen_range(0.0..1_000.0);
        y2 = y1 + rng.gen_range(0.0..1_000.0);
    }
    else
    {
        x2 = x1 + rng.gen_range(0.0..grid_snap / 2.0);
        y2 = y1 + rng.gen_range(0.0..grid_snap / 2.0);
    }

    let probability = rng.gen::<f32>();

    x1 = (x1 / grid_snap).floor() * grid_snap;
    y1 = (y1 / grid_snap).floor() * grid_snap;
    x2 = (x2 / grid_snap).floor() * grid_snap;
    y2 = (y2 / grid_snap).floor() * grid_snap;

    if probability <= 0.25
    {
        x1 = x1.floor();
        y1 = y1.floor();
        x2 = x2.floor();
        y2 = y2.floor();
    }

    CollisionBroadItem
    {
        entity,
        positioned_bound : BakedRect
        {
            x1, y1, x2, y2
        }
    }
}

fn aligned_items(grid_size : f64, id_start : usize) -> Vec<CollisionBroadItem>
{
    let mut items = Vec::new();
    let mut i = 0;

    for y in (0..70).step_by(2)
    {
        for x in (0..70).step_by(2)
        {
            let x_float = x as f64;
            let y_float = y as f64;

            let positioned_bound = BakedRect
            {
                x1 : x_float * grid_size + x_float,
                y1 : y_float * grid_size + y_float,
                x2 : x_float * grid_size + x_float + grid_size,
                y2 : y_float * grid_size + y_float + grid_size
            };


            let item = CollisionBroadItem
            {
                entity : Entity::new(i + id_start, 1),
                positioned_bound
            };

            i += 1;

            items.push(item);
        }
    }

    items
}

fn random_items(grid_size : f64, id_start : usize) -> Vec<CollisionBroadItem>
{
    let mut items = Vec::new();

    for i in 0..500
    {
        let new_item = generate_random_item(Entity::new(i + id_start, 1), grid_size);

        items.push(new_item);
    }

    items
}

fn random_ids(max : usize) -> BitSet
{
    let mut rng = rand::thread_rng();
    let mut ids = BitSet::with_capacity(max);

    for i in 0..max
    {
        if rng.gen_bool(0.5)
        {
            ids.insert(i);
        }
    }

    ids
}

fn is_matching(expected : &mut Vec<CollisionBroadItem>, actual : &mut Vec<CollisionBroadItem>) -> bool
{
    expected.sort_by(|a, b|
    {
        a.entity.id().cmp(&b.entity.id())
    });

    actual.sort_by(|a, b|
    {
        a.entity.id().cmp(&b.entity.id())
    });

    vec_equal(&expected, &actual)
}

struct CollisionTestState
{
    reference : Vec<CollisionBroadItem>,
    broadstore : CollisionBroadStore,
    ids : Vec<usize>,
    cel_size : u16
}

impl CollisionTestState
{
    fn new(cel_size : u16) -> CollisionTestState
    {
        CollisionTestState
        {
            reference : Vec::new(),
            broadstore : CollisionBroadStore::new(cel_size),
            ids : Vec::new(),
            cel_size
        }
    }

    fn set_items(&mut self, items : &Vec<CollisionBroadItem>)
    {
        self.reference.clear();
        self.ids.clear();
        self.broadstore = CollisionBroadStore::new(self.cel_size);

        self.add_items(items);
    }

    fn add_items(&mut self, items : &Vec<CollisionBroadItem>)
    {
        for item in items.iter().cloned()
        {
            self.reference.push(item.clone());
            self.ids.push(self.broadstore.add(item));
        }
    }

    fn remove_items(&mut self, ids : BitSet)
    {
        let mut to_remove : Vec<usize> = ids.iter().collect();

        to_remove.sort_by(|a, b| { b.cmp(a) });

        for i in &to_remove
        {
            let id = self.ids.remove(*i);
            self.reference.remove(*i);
            self.broadstore.remove(id);
        }
    }

    fn move_items(&mut self, moved_items : &Vec<CollisionBroadItem>)
    {
        for (index, mut moved) in moved_items.iter().cloned().enumerate()
        {
            let old = &self.reference[index];
            moved.entity = old.entity.clone();

            self.reference[index] = moved.clone();
            self.broadstore.queue_update(self.ids[index], moved.positioned_bound);
        }
    }

    fn test_overlapping(&mut self, label : &str)
    {
        for (id, item) in self.reference.iter().enumerate()
        {
            let mut expected = Vec::new();

            for other in &self.reference
            {
                if item.entity != other.entity && item.positioned_bound.overlaps(&other.positioned_bound)
                {
                    expected.push(other.clone());
                }
            }

            let mut actual = Vec::new();

            self.broadstore.query_overlapping(self.ids[id], &mut actual);

            if !is_matching(&mut expected, &mut actual)
            {
                panic!("\nFailed at {}, id = {}\n\n   bound = {:?}\n\nexpected = {:?}\n\n  actual = {:?}\n", label, id, item.positioned_bound, expected, actual);
            }
        }
    }

    fn count(&self) -> usize
    {
        self.reference.len()
    }
}

#[test]
fn check_broadphase_aligned()
{
    for iter in 1..130
    {
        let mut state = CollisionTestState::new(GRID_DEFAULT_CEL_SIZE);
        state.set_items(&aligned_items(iter as f64, 0));
        state.test_overlapping(&format!("{}", iter));
    }
}

#[test]
fn check_broadphase_random()
{
    let mut rng = rand::thread_rng();

    // Init RNG for better entropy
    for _ in 0..1000
    {
        rng.gen::<f64>();
    }

    for grid_size in &[64.0, 256.0]
    {
        for iter in 0..100
        {
            let mut state = CollisionTestState::new(GRID_DEFAULT_CEL_SIZE);
            state.set_items(&random_items(*grid_size, 0));
            state.test_overlapping(&format!("{}", iter));
        }
    }
}

#[test]
fn check_broadphase_random_moved()
{
    let mut rng = rand::thread_rng();

    // Init RNG for better entropy
    for _ in 0..1000
    {
        rng.gen::<f64>();
    }

    for grid_size in &[64.0, 256.0]
    {
        for iter in 0..100
        {
            let mut state = CollisionTestState::new(GRID_DEFAULT_CEL_SIZE);
            state.set_items(&random_items(*grid_size, 0));
            state.move_items(&random_items(*grid_size, 0));
            state.test_overlapping(&format!("{}", iter));
        }
    }
}

#[test]
fn check_broadphase_random_moved_removed()
{
    let mut rng = rand::thread_rng();

    // Init RNG for better entropy
    for _ in 0..1000
    {
        rng.gen::<f64>();
    }

    for grid_size in &[64.0, 256.0]
    {
        for iter in 0..100
        {
            let mut state = CollisionTestState::new(GRID_DEFAULT_CEL_SIZE);
            state.set_items(&random_items(*grid_size, 0));
            state.move_items(&random_items(*grid_size, 0));
            state.test_overlapping(&format!("{}", iter));
            state.remove_items(random_ids(state.count()));
            state.test_overlapping(&format!("removed {}", iter));
        }
    }
}

#[test]
fn check_broadphase_random_moved_removed_added()
{
    let mut rng = rand::thread_rng();

    // Init RNG for better entropy
    for _ in 0..1000
    {
        rng.gen::<f64>();
    }

    for grid_size in &[64.0, 256.0]
    {
        for iter in 0..100
        {
            let mut state = CollisionTestState::new(GRID_DEFAULT_CEL_SIZE);
            state.set_items(&random_items(*grid_size, 0));
            state.move_items(&random_items(*grid_size, 0));
            state.test_overlapping(&format!("{}", iter));
            state.remove_items(random_ids(state.count()));
            state.test_overlapping(&format!("post-remove {}", iter));
            state.add_items(&random_items(*grid_size, 1000));
            state.test_overlapping(&format!("post-add {}", iter));
        }
    }
}
