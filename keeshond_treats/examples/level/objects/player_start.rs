use keeshond::gameloop::GameControl;
use keeshond::scene::{DynArgList, SpawnableConfig, SpawnControl};
use keeshond::util::SimpleTransform;

use keeshond_derive::Component;

#[derive(Component)]
pub struct PlayerStart {}

impl SpawnableConfig for PlayerStart
{
    fn spawn(spawn : &mut SpawnControl, _game : &mut GameControl,
             _transform : &SimpleTransform, _args : &DynArgList)
    {
        spawn.with(PlayerStart {});
    }
}
