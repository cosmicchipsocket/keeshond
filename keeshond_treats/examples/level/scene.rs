
use keeshond::gameloop::GameControl;
use keeshond::scene::{DynArgList, SceneConfig, SceneType, SpawnableConfig, SpawnControl, ThinkerArgs, ThinkerSystem};
use keeshond::util::{Rect, SimpleTransform};

use keeshond_treats::{DefaultSystemConfig, DefaultSystems, LoadErrorMode};
use keeshond_treats::level::{Level, LevelApplyOptions};
use keeshond_treats::spawnable::SpawnDefaultHelper;
use keeshond_treats::visual::PixelSnap;
use keeshond_treats::world::{Camera, Transform};

use crate::objects::player_start::PlayerStart;

const CAMERA_MOVE_SPEED : f64 = 8.0;

keeshond::spawnables!
{
    SpawnId;
    FarTiles,
    Tiles,
    PlayerStart : PlayerStart,
    Gem,
    Spring,
    SpringDiagRight,
    SpringDiagLeft
}

pub struct GameScene
{
    spawn_defaults : SpawnDefaultHelper<Self>
}

impl SceneType for GameScene
{
    type SpawnableIdType = SpawnId;

    fn new() -> Self where Self : Sized
    {
        GameScene
        {
            spawn_defaults : SpawnDefaultHelper::empty()
        }
    }
    fn config(&mut self, game : &mut GameControl) -> SceneConfig<Self>
    {
        let mut config = SceneConfig::new();

        config.default_systems(DefaultSystemConfig
        {
            load_error : LoadErrorMode::Fatal,
            pixel_snap : PixelSnap::SpriteAndCameraSnap,
            auto_create_camera : false,
            collision_cel_size : 64
        })
            .thinker(ExampleThinker::new());

        self.spawn_defaults = SpawnDefaultHelper::from_spawnables_lookup("level_example", "objects.json", game);

        config
    }
    fn spawn(&mut self, spawn : &mut SpawnControl, spawnable_id : SpawnId,
             game : &mut GameControl, transform : &SimpleTransform, args : &DynArgList)
    {
        self.spawn_defaults.spawn_defaults(spawn, spawnable_id, game, transform, args, LoadErrorMode::Fatal);
    }
}


////////////////////////////////////////

struct ExampleThinker
{

}

impl ExampleThinker
{
    fn new() -> ExampleThinker
    {
        ExampleThinker {}
    }
}

impl ThinkerSystem<GameScene> for ExampleThinker
{
    fn start(&mut self, mut args : ThinkerArgs<GameScene>)
    {
        Level::apply_from_path("level_example", "level.keelvl", &mut args, LevelApplyOptions
        {
            global_properties : true
        });
    }

    fn start_late(&mut self, args : ThinkerArgs<GameScene>)
    {
        let transform_store = args.components.store::<Transform>();
        let start_store = args.components.store::<PlayerStart>();

        if let Some((entity, _)) = start_store.first()
        {
            if let Some(start_pos) = transform_store.get_entity(&entity)
            {
                let (base_width, base_height) = args.game.renderer().base_size();
                let entity = args.scene.add_entity_later();

                args.scene.add_component_later(&entity, Camera::new(start_pos.x(), start_pos.y(), Rect
                {
                    x : 0.0,
                    y : 0.0,
                    w : base_width,
                    h : base_height
                }));
            }
        }
    }

    fn think(&mut self, args : ThinkerArgs<GameScene>)
    {
        let mut camera_store = args.components.store_mut::<Camera>();

        if let Some((_, camera)) = camera_store.first_mut()
        {
            let (axis_x, axis_y) = args.game.input().paired_axis_2d("left", "right", "up", "down");
            camera.x += axis_x * CAMERA_MOVE_SPEED;
            camera.y += axis_y * CAMERA_MOVE_SPEED;
        }
    }
}
