
#[macro_use] extern crate keeshond;
extern crate rand;

use keeshond::gameloop::{GameControl, GameInfo, Gameloop, GameStats};
use keeshond::scene::{DynArgList, SceneConfig, SceneControl, SceneType, SpawnControl, ThinkerArgs, ThinkerSystem};
use keeshond::renderer::{ViewportMode, WindowScaleMode};
use keeshond::util::SimpleTransform;

use keeshond::datapack::DataHandle;
use keeshond::datapack::source::{FilesystemSource, TrustLevel};

use keeshond_derive::Component;

use keeshond_treats::{DefaultSystemConfig, DefaultSystems, LoadErrorMode};
use keeshond_treats::visual::{Drawable, PixelSnap, Text};
use keeshond_treats::collision::{Collidable, CollisionResults};
use keeshond_treats::spawnable::SpawnDefaultHelper;

use rand::Rng;

const BLOB_SPEED_MIN : f64 = 2.5;
const BLOB_SPEED_MAX : f64 = 5.0;
const BLOB_CENTER_SPREAD : f64 = 4.0;


////////////////////////////////////////


keeshond::spawnables!
{
    SpawnId;
    Background,
    StatusText,
    BlobYellow
}

#[derive(Component)]
struct StatusText
{
    optimized : bool,
    blobs : usize,
    stats : GameStats
}

impl StatusText
{
    fn new() -> StatusText
    {
        StatusText
        {
            optimized : true,
            blobs : 0,
            stats : GameStats::new()
        }
    }

    fn update_text(&self, text : &mut Text)
    {
        let on_text;

        if self.optimized { on_text = "on"; }
        else { on_text = "off"; }

        text.set_text(&format!("optimized collision {} - press o to toggle\n\n\
            {} blobs\n{} draw fps\n{} think fps\n\nhold space for more objects\n\n\
            hold delete to despawn\nf to freeze\nr to reset\n\n\
            think {:.2} ms\ndraw {:.2} ms\npresent {:.2} ms\nwait {:.2} ms",
            on_text, self.blobs, self.stats.draw_fps, self.stats.think_fps,
            self.stats.frame_ms_think, self.stats.frame_ms_draw,
            self.stats.frame_ms_present, self.stats.frame_ms_wait));
    }
}

#[derive(Component)]
struct Blob
{
    vel_x : f64,
    vel_y : f64
}

impl Blob
{
    pub fn new(vel_x : f64, vel_y : f64) -> Blob
    {
        Blob
        {
            vel_x,
            vel_y
        }
    }
}

struct GameScene
{
    spawn_defaults : SpawnDefaultHelper<Self>
}

impl SceneType for GameScene
{
    type SpawnableIdType = SpawnId;

    fn new() -> Self where Self : Sized
    {
        GameScene
        {
            spawn_defaults : SpawnDefaultHelper::empty()
        }
    }
    fn config(&mut self, game : &mut GameControl) -> SceneConfig<Self>
    {
        let mut config = SceneConfig::new();

        // Collision grid size manually set to 16, enlarge this value if you are making your objects bigger than 8x8
        config.default_systems(DefaultSystemConfig
        {
            load_error : LoadErrorMode::Fatal,
            pixel_snap : PixelSnap::SpriteAndCameraSnap,
            auto_create_camera : true,
            collision_cel_size : 16
        })
            .thinker(ExampleThinker::new());

        self.spawn_defaults = SpawnDefaultHelper::from_spawnables_lookup("collision_example", "objects.json", game);

        config
    }
    fn spawn(&mut self, spawn : &mut SpawnControl, spawnable_id : SpawnId,
             game : &mut GameControl, transform : &SimpleTransform, args : &DynArgList)
    {
        match spawnable_id
        {
            SpawnId::Background =>
            {
                spawn.with(Drawable::with_background_color(0.0, 0.20, 0.26));
            }
            SpawnId::StatusText =>
            {
                let text = Text::new("", DataHandle::with_path("common", "font.png"), 32.0);

                spawn.with(StatusText::new());
                spawn.with(Drawable::new());
                spawn.with(text);
            }
            SpawnId::BlobYellow =>
            {
                spawn.with(Blob::new(args.get_float(0, 0.0), args.get_float(1, 0.0)));
            }
        }

        self.spawn_defaults.spawn_defaults(spawn, spawnable_id, game, transform, args, LoadErrorMode::Fatal);
    }
}


////////////////////////////////////////

struct ExampleThinker
{
    optimized : bool
}

impl ExampleThinker
{
    fn new() -> ExampleThinker
    {
        ExampleThinker
        {
            optimized : true
        }
    }

    fn spawn_blob(&self, scene : &mut SceneControl<GameScene>, game : &mut GameControl)
    {
        let (screen_width, screen_height) = game.renderer().base_size();

        let mut rng = rand::thread_rng();
        let speed = rng.gen_range(BLOB_SPEED_MIN..BLOB_SPEED_MAX);
        let angle = rng.gen_range(0.0..2.0 * std::f64::consts::PI);
        let vel_x = angle.cos() * speed;
        let vel_y = angle.sin() * speed;
        let center_offset = rng.gen_range(0.0..BLOB_CENTER_SPREAD);
        let x = (screen_width as f64 / 2.0) + vel_x * center_offset;
        let y = (screen_height as f64 / 2.0) + vel_y * center_offset;

        scene.spawn_later_with(SpawnId::BlobYellow, game, &SimpleTransform::with_xy(x, y), &darg![vel_x, vel_y]);
    }
}

impl ThinkerSystem<GameScene> for ExampleThinker
{
    fn start(&mut self, args : ThinkerArgs<GameScene>)
    {
        args.scene.spawn_later(SpawnId::Background, args.game, &SimpleTransform::with_xy(0.0, 0.0));
        args.scene.spawn_later(SpawnId::StatusText, args.game, &SimpleTransform::with_pos(8.0, 32.0, 1024.0));
    }
    fn think(&mut self, args : ThinkerArgs<GameScene>)
    {
        let mut collidable_store = args.components.store_mut::<Collidable>();
        let mut drawable_store = args.components.store_mut::<Drawable>();
        let mut blob_store = args.components.store_mut::<Blob>();
        let mut text_store = args.components.store_mut::<Text>();
        let mut status_text_store = args.components.store_mut::<StatusText>();

        let (screen_width, screen_height) = args.game.renderer().base_size();

        let mut freeze = false;

        if args.game.input().down_once("freeze")
        {
            freeze = true;
        }

        if args.game.input().down_once("toggle optimizations")
        {
            self.optimized = !self.optimized;
        }

        for (entity, blob) in blob_store.iter_mut()
        {
            if freeze
            {
                blob.vel_x = 0.0;
                blob.vel_y = 0.0;
            }

            if let Some(mut collidable) = collidable_store.get_entity_mut(&entity)
            {
                collidable.set_pos(collidable.x() + blob.vel_x, collidable.y() + blob.vel_y);

                if collidable.x() + collidable.bound().w() < 0.0 || collidable.x() - collidable.bound().w() >= screen_width as f64
                    || collidable.y() + collidable.bound().h() < 0.0 || collidable.y() - collidable.bound().h() >= screen_height as f64
                {
                    args.scene.remove_entity_later(&entity);
                }
            }
        }

        // CollisionResults object should be created once per frame, not once per query,
        //  for performance reasons.
        let mut collision_results = CollisionResults::new();

        for (entity, _) in blob_store.iter_mut()
        {
            if let Some(drawable) = drawable_store.get_entity_mut(&entity)
            {
                let mut alpha = 0.5;

                if let Some(collidable) = collidable_store.get_entity(&entity)
                {
                    if self.optimized
                    {
                        collidable.query_overlapping_bounds(&mut collision_results);

                        if collision_results.bound_overlaps.len() > 0
                        {
                            alpha = 1.0;
                        }
                    }
                    else
                    {
                        for (other_entity, other_collidable) in collidable_store.iter()
                        {
                            if entity != other_entity
                                && collidable.overlaps_other_bound(other_collidable)
                            {
                                alpha = 1.0;
                            }
                        }
                    }
                }

                drawable.item.modify_sprite(|inner|
                {
                    inner.alpha = alpha;
                });
            }
        }

        for _ in 0..5
        {
            self.spawn_blob(args.scene, args.game);
        }

        if args.game.input().held("spawn")
        {
            for _ in 0..35
            {
                self.spawn_blob(args.scene, args.game);
            }
        }

        if args.game.input().held("despawn")
        {
            let mut iter = blob_store.iter_mut();

            for _ in 0..30
            {
                if let Some((entity, _)) = iter.next()
                {
                    args.scene.remove_entity_later(&entity);
                }
            }
        }

        if args.game.input().down_once("reset")
        {
            args.game.goto_new_scene::<GameScene>();
        }

        for (entity, status) in status_text_store.iter_mut()
        {
            status.optimized = self.optimized;
            status.blobs = blob_store.count();
            status.stats = args.game.stats().clone();

            if let Some(text) = text_store.get_entity_mut(&entity)
            {
                status.update_text(text);
            }
        }
    }
}


////////////////////////////////////////


fn main()
{
    let gameinfo = GameInfo
    {
        package_name : "collision_index",
        friendly_name : "Keeshond Treats Collision Index Example",
        base_width : 1280,
        base_height : 720,
        texture_filtering : false,
        viewport_mode : ViewportMode::Pixel,
        default_window_scale : WindowScaleMode::PixelMultiplierDpiScaled(2),
        cursor_visible : false,
        .. Default::default()
    };
    
    let mut gameloop = Gameloop::new(gameinfo);

    let mut rng = rand::thread_rng();

    // Make first few numbers more random
    for _ in 0..100
    {
        rng.gen::<f64>();
    }

    let input = gameloop.control_mut().input_mut();

    input.add_bind("spawn", "space");
    input.add_bind("freeze", "f");
    input.add_bind("despawn", "delete");
    input.add_bind("toggle optimizations", "o");
    input.add_bind("reset", "r");
    
    let source_manager = gameloop.control_mut().source_manager();

    for path in &["examples_data", "keeshond_treats/examples_data", "keeshond/keeshond_treats/examples_data"]
    {
        let source = FilesystemSource::new(path, TrustLevel::TrustedSource);
        source_manager.borrow_mut().add_source(Box::new(source));
    }
    
    gameloop.control_mut().goto_new_scene::<GameScene>();
    gameloop.run();
}
