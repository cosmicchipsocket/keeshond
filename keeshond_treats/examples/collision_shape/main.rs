#[macro_use] extern crate keeshond;

use keeshond::datapack::LoadErrorMode;
use keeshond::datapack::source::{FilesystemSource, TrustLevel};
use keeshond::gameloop::{GameControl, GameInfo, Gameloop};
use keeshond::scene::{DynArgList, SceneConfig, SceneType, SpawnControl, ThinkerArgs, ThinkerSystem};
use keeshond::util::SimpleTransform;

use keeshond_derive::Component;

use keeshond_treats::{DefaultSystemConfig, DefaultSystems};
use keeshond_treats::collision::{Collidable, CollisionResults};
use keeshond_treats::spawnable::SpawnDefaultHelper;
use keeshond_treats::visual::{Drawable, PixelSnap};

keeshond::spawnables!
{
    SpawnId;
    Background,
    Rectangle,
    Circle
}

#[derive(Component)]
struct TestShape
{
    mouse_controlled : bool,
    mouse_offset_x : f64,
    mouse_offset_y : f64
}

struct GameScene
{
    spawn_defaults : SpawnDefaultHelper<Self>
}

impl SceneType for GameScene
{
    type SpawnableIdType = SpawnId;

    fn new() -> Self where Self : Sized
    {
        GameScene
        {
            spawn_defaults : SpawnDefaultHelper::empty()
        }
    }
    fn config(&mut self, game : &mut GameControl) -> SceneConfig<Self>
    {
        let mut config = SceneConfig::new();

        // Using a collision cel size of 256 based on the shapes used here. You may want a larger
        //  or smaller value here based on what you're using in your game.
        config.default_systems(DefaultSystemConfig
        {
            load_error : LoadErrorMode::Fatal,
            pixel_snap : PixelSnap::NoSnap,
            auto_create_camera : true,
            collision_cel_size : 256
        })
            .thinker(ExampleThinker::new());

        self.spawn_defaults = SpawnDefaultHelper::from_spawnables_lookup("collision_example", "objects.json", game);

        config
    }
    fn spawn(&mut self, spawn : &mut SpawnControl, spawnable_id : SpawnId,
             game : &mut GameControl, transform : &SimpleTransform, args : &DynArgList)
    {
        match spawnable_id
        {
            SpawnId::Background =>
            {
                spawn.with(Drawable::with_background_color(0.0, 0.20, 0.26));
            }
            SpawnId::Rectangle | SpawnId::Circle =>
            {
                spawn.with(TestShape
                {
                    mouse_controlled : args.get_bool_named("mouse_controlled", false),
                    mouse_offset_x : transform.x,
                    mouse_offset_y : transform.y
                });
            }
        }

        self.spawn_defaults.spawn_defaults(spawn, spawnable_id, game, transform, args, LoadErrorMode::Fatal);
    }
}

struct ExampleThinker
{

}

impl ExampleThinker
{
    fn new() -> ExampleThinker
    {
        ExampleThinker {}
    }
}

impl ThinkerSystem<GameScene> for ExampleThinker
{
    fn start(&mut self, args: ThinkerArgs<GameScene>)
    {
        args.scene.spawn_later(SpawnId::Background, args.game, &SimpleTransform::with_xy(0.0, 0.0));

        args.scene.spawn_later(SpawnId::Rectangle, args.game, &SimpleTransform::with_xy(480.0, 768.0));
        args.scene.spawn_later(SpawnId::Circle, args.game, &SimpleTransform::with_xy(1440.0, 768.0));

        args.scene.spawn_later_with(SpawnId::Rectangle, args.game, &SimpleTransform::with_xy(-192.0, 0.0), &darg_named!(; "mouse_controlled" => true));
        args.scene.spawn_later_with(SpawnId::Circle, args.game, &SimpleTransform::with_xy(192.0, 0.0), &darg_named!(; "mouse_controlled" => true));
    }
    fn think(&mut self, args : ThinkerArgs<GameScene>)
    {
        let shape_store = args.components.store::<TestShape>();
        let mut collidable_store = args.components.store_mut::<Collidable>();
        let mut drawable_store = args.components.store_mut::<Drawable>();

        let (mouse_x, mouse_y) = args.game.input().cursor_position();

        for (entity, shape) in shape_store.iter()
        {
            if shape.mouse_controlled
            {
                if let Some(mut collidable) = collidable_store.get_entity_mut(&entity)
                {
                    collidable.set_pos(shape.mouse_offset_x + mouse_x, shape.mouse_offset_y + mouse_y);
                }
            }
        }

        // CollisionResults object should be created once per frame, not once per query,
        //  for performance reasons.
        let mut collision_results = CollisionResults::new();

        for (entity, _) in shape_store.iter()
        {
            let mut is_overlapping = false;

            if let Some(collidable) = collidable_store.get_entity(&entity)
            {
                collidable.query_overlapping_shapes(&collidable_store, &mut collision_results);

                if collision_results.shape_overlaps.len() > 0
                {
                    is_overlapping = true;
                }
            }

            if let Some(drawable) = drawable_store.get_entity_mut(&entity)
            {
                let highlight = match is_overlapping
                {
                    true => { 2.0 }
                    false => { 1.0 }
                };

                drawable.item.modify_sprite(|sprite|
                {
                    sprite.r = highlight;
                    sprite.g = highlight;
                    sprite.b = highlight;
                });
            }
        }
    }
}

fn main()
{
    let gameinfo = GameInfo
    {
        package_name : "collision_shape",
        friendly_name : "Keeshond Treats Collision Shape Example",
        base_width : 1920,
        base_height : 1080,
        cursor_visible : true,
        .. Default::default()
    };

    let mut gameloop = Gameloop::new(gameinfo);

    let source_manager = gameloop.control_mut().source_manager();

    for path in &["examples_data", "keeshond_treats/examples_data", "keeshond/keeshond_treats/examples_data"]
    {
        let source = FilesystemSource::new(path, TrustLevel::TrustedSource);
        source_manager.borrow_mut().add_source(Box::new(source));
    }

    gameloop.control_mut().goto_new_scene::<GameScene>();
    gameloop.run();
}
