
extern crate keeshond;
extern crate rand;

use keeshond::gameloop::{GameControl, GameInfo, Gameloop};
use keeshond::scene::{DynArgList, SceneConfig, SceneType, SpawnControl, ThinkerArgs, ThinkerSystem};
use keeshond::renderer::{ViewportMode, WindowScaleMode};
use keeshond::util::SimpleTransform;

use keeshond::datapack::source::{FilesystemSource, TrustLevel};

use keeshond_derive::Component;

use keeshond_treats::{DefaultSystemConfig, DefaultSystems, LoadErrorMode};
use keeshond_treats::visual::{Actor, Costume, Drawable, PixelSnap};
use keeshond_treats::spawnable::SpawnDefaultHelper;


////////////////////////////////////////


keeshond::spawnables!
{
    SpawnId;
    Background,
    Skater
}

struct GameScene
{
    spawn_defaults : SpawnDefaultHelper<Self>
}

impl SceneType for GameScene
{
    type SpawnableIdType = SpawnId;
    
    fn new() -> Self where Self : Sized
    {
        GameScene
        {
            spawn_defaults : SpawnDefaultHelper::empty()
        }
    }
    fn config(&mut self, game : &mut GameControl) -> SceneConfig<Self>
    {
        let mut config = SceneConfig::new();

        config.default_systems(DefaultSystemConfig
        {
            load_error : LoadErrorMode::Fatal,
            pixel_snap : PixelSnap::SpriteAndCameraSnap,
            auto_create_camera : true,
            collision_cel_size : 64
        })
            .thinker(SkaterThinker {});

        self.spawn_defaults = SpawnDefaultHelper::from_spawnables_lookup("common", "objects.json", game);

        config
    }
    fn spawn(&mut self, spawn : &mut SpawnControl, spawnable_id : SpawnId,
             game : &mut GameControl, transform : &SimpleTransform, args : &DynArgList)
    {
        match spawnable_id
        {
            SpawnId::Background =>
            {
                spawn.with(Drawable::with_background_color(0.7, 0.83, 0.53));
            }
            SpawnId::Skater =>
            {
                spawn.with(Skater {});
            }
        }

        self.spawn_defaults.spawn_defaults(spawn, spawnable_id, game, transform, args, LoadErrorMode::Fatal);
    }
}


////////////////////////////////////////

#[derive(Component)]
struct Skater {}

struct SkaterThinker {}

impl ThinkerSystem<GameScene> for SkaterThinker
{
    fn start(&mut self, args : ThinkerArgs<GameScene>)
    {
        let (base_width, base_height) = args.game.renderer().base_size();

        args.scene.spawn_later(SpawnId::Background, args.game, &SimpleTransform::with_xy(0.0, 0.0));
        args.scene.spawn_later(SpawnId::Skater, args.game, &SimpleTransform::with_xy(base_width as f64 / 2.0, base_height as f64 / 2.0));
    }
    
    fn think(&mut self, args : ThinkerArgs<GameScene>)
    {
        let costume_store = args.game.res().store::<Costume>();
        let mut actor_store = args.components.store_mut::<Actor>();
        let mut skater_store = args.components.store_mut::<Skater>();

        let mut new_anim = "";

        if args.game.input().down_once("anim1") { new_anim = "idle"; }
        if args.game.input().down_once("anim2") { new_anim = "push"; }
        if args.game.input().down_once("anim3") { new_anim = "crouch"; }
        if args.game.input().down_once("anim4") { new_anim = "ollie"; }
        if args.game.input().down_once("anim5") { new_anim = "boardslide"; }
        if args.game.input().down_once("anim6") { new_anim = "bail"; }

        for (entity, _) in skater_store.iter_mut()
        {
            if let Some(actor) = actor_store.get_entity_mut(&entity)
            {
                if !new_anim.is_empty()
                {
                    actor.play_animation_name(new_anim, false, &costume_store);
                }
            }
        }
    }
}


////////////////////////////////////////


fn main()
{
    let gameinfo = GameInfo
    {
        package_name : "actor",
        friendly_name : "Keeshond Treats Actor Example",
        base_width : 384,
        base_height : 224,
        texture_filtering : false,
        viewport_mode : ViewportMode::Pixel,
        default_window_scale : WindowScaleMode::PixelMultiplierDpiScaled(2),
        cursor_visible : false,
        .. Default::default()
    };
    
    let mut gameloop = Gameloop::new(gameinfo);
    
    gameloop.control_mut().renderer_mut().set_window_title("Keeshond Treats Actor | Number keys to play animations");
    
    let input = gameloop.control_mut().input_mut();

    input.add_bind("anim1", "1");
    input.add_bind("anim2", "2");
    input.add_bind("anim3", "3");
    input.add_bind("anim4", "4");
    input.add_bind("anim5", "5");
    input.add_bind("anim6", "6");
    
    let source_manager = gameloop.control_mut().source_manager();

    for path in &["examples_data", "keeshond_treats/examples_data", "keeshond/keeshond_treats/examples_data"]
    {
        let source = FilesystemSource::new(path, TrustLevel::TrustedSource);
        source_manager.borrow_mut().add_source(Box::new(source));
    }
    
    gameloop.control_mut().goto_new_scene::<GameScene>();
    gameloop.run();
}
