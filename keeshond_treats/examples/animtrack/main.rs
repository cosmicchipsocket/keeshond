
extern crate keeshond;

use keeshond::gameloop::{GameControl, GameInfo, Gameloop};
use keeshond::scene::{DynArgList, SceneConfig, SceneType, SpawnControl, ThinkerArgs, ThinkerSystem};
use keeshond::renderer::{ViewportMode, WindowScaleMode};
use keeshond::util::{InputDirection8, SimpleTransform};

use keeshond::datapack::source::{FilesystemSource, TrustLevel};

use keeshond_derive::Component;

use keeshond_treats::{DefaultSystemConfig, DefaultSystems, LoadErrorMode};
use keeshond_treats::visual::{Actor, Costume, Drawable, PixelSnap};
use keeshond_treats::spawnable::SpawnDefaultHelper;
use keeshond_treats::collision::Collidable;


////////////////////////////////////////

const MOVE_SPEED : f64 = 3.0;
const IDLE_TIME_DELAY : u32 = 180;

keeshond::spawnables!
{
    SpawnId;
    Background,
    Kobold
}

struct GameScene
{
    spawn_defaults : SpawnDefaultHelper<Self>
}

impl SceneType for GameScene
{
    type SpawnableIdType = SpawnId;
    
    fn new() -> Self where Self : Sized
    {
        GameScene
        {
            spawn_defaults : SpawnDefaultHelper::empty()
        }
    }
    fn config(&mut self, game : &mut GameControl) -> SceneConfig<Self>
    {
        let mut config = SceneConfig::new();

        config.default_systems(DefaultSystemConfig
        {
            load_error : LoadErrorMode::Fatal,
            pixel_snap : PixelSnap::SpriteAndCameraSnap,
            auto_create_camera : true,
            collision_cel_size : 64
        })
            .thinker(KoboldThinker::new());

        self.spawn_defaults = SpawnDefaultHelper::from_spawnables_lookup("animtrack_example", "objects.json", game);

        config
    }
    fn spawn(&mut self, spawn : &mut SpawnControl, spawnable_id : SpawnId,
             game : &mut GameControl, transform : &SimpleTransform, args : &DynArgList)
    {
        match spawnable_id
        {
            SpawnId::Background =>
            {
                spawn.with(Drawable::with_background_color(0.7, 0.0, 0.5));
            }
            SpawnId::Kobold =>
            {
                spawn.with(Kobold::new());
            }
        }

        self.spawn_defaults.spawn_defaults(spawn, spawnable_id, game, transform, args, LoadErrorMode::Fatal);
    }
}


////////////////////////////////////////

#[derive(Component)]
struct Kobold
{
    idle_time : u32
}

impl Kobold
{
    fn new() -> Kobold
    {
        Kobold
        {
            idle_time : 0
        }
    }
}

struct KoboldThinker {}

impl ThinkerSystem<GameScene> for KoboldThinker
{
    fn start(&mut self, args : ThinkerArgs<GameScene>)
    {
        let (base_width, base_height) = args.game.renderer().base_size();

        args.scene.spawn_later(SpawnId::Background, args.game, &SimpleTransform::with_xy(0.0, 0.0));
        args.scene.spawn_later(SpawnId::Kobold, args.game, &SimpleTransform::with_xy(base_width as f64 / 2.0, base_height as f64 / 2.0));
    }
    
    fn think(&mut self, args : ThinkerArgs<GameScene>)
    {
        let costume_store = args.game.res().store::<Costume>();
        let mut collidable_store = args.components.store_mut::<Collidable>();
        let mut actor_store = args.components.store_mut::<Actor>();
        let mut kobold_store = args.components.store_mut::<Kobold>();

        let (window_w, window_h) = args.game.renderer().base_size();
        let mut walking = false;
        let mut new_anim = "";
        let mut new_speed = 1.0;
        let (axis_x, axis_y) = args.game.input().paired_axis_2d("left", "right", "up", "down");

        if axis_x != 0.0 || axis_y != 0.0
        {
            walking = true;
            new_anim = "walk";
            new_speed = 1.0 + keeshond::util::xy_to_distance_clamped(axis_x, axis_y) * 0.5;
        }

        let new_track = self.new_direction(axis_x, axis_y);

        for (entity, kobold) in kobold_store.iter_mut()
        {
            if !walking
            {
                kobold.idle_time += 1;

                if kobold.idle_time > IDLE_TIME_DELAY
                {
                    new_anim = "idle";
                }
                else
                {
                    new_anim = "stand";
                }
            }
            else
            {
                kobold.idle_time = 0;
            }

            if let Some(mut collidable) = collidable_store.get_entity_mut(&entity)
            {
                let mut new_x = collidable.x() + axis_x * MOVE_SPEED;
                let mut new_y = collidable.y() + axis_y * MOVE_SPEED;

                new_x = new_x.clamp(16.0, window_w as f64 - 16.0);
                new_y = new_y.clamp(80.0, window_h as f64);

                collidable.set_pos(new_x, new_y);
            }

            if let Some(actor) = actor_store.get_entity_mut(&entity)
            {
                if actor.is_animation_name_finished("idle", &costume_store)
                {
                    new_anim = "stand";
                    kobold.idle_time = 0;
                }

                if !new_anim.is_empty()
                {
                    actor.play_animation_name(new_anim, false, &costume_store);
                }
                if !new_track.is_empty()
                {
                    actor.set_track_by_name(new_track, &["down"], &costume_store);
                }
                actor.set_animation_speed(new_speed);
            }
        }
    }
}

impl KoboldThinker
{
    fn new() -> KoboldThinker
    {
        KoboldThinker {}
    }

    fn new_direction(&self, axis_x : f64, axis_y : f64) -> &'static str
    {
        let angle_opt = keeshond::util::xy_to_angle(axis_x, axis_y);

        if let Some(angle) = angle_opt
        {
            if let Some(dir) = keeshond::util::angle_to_eight_way_direction(angle)
            {
                return match dir
                {
                    InputDirection8::Left => { "left" }
                    InputDirection8::UpLeft => { "upleft" }
                    InputDirection8::Up => { "up" }
                    InputDirection8::UpRight => { "upright" }
                    InputDirection8::Right => { "right" }
                    InputDirection8::DownRight => { "downright" }
                    InputDirection8::Down => { "down" }
                    InputDirection8::DownLeft => { "downleft" }
                };
            }
        }

        ""
    }
}


////////////////////////////////////////


fn main()
{
    let gameinfo = GameInfo
    {
        package_name : "animtrack",
        friendly_name : "Keeshond Treats Animation Track Example",
        base_width : 640,
        base_height : 360,
        texture_filtering : false,
        viewport_mode : ViewportMode::Pixel,
        default_window_scale : WindowScaleMode::PixelMultiplierDpiScaled(2),
        cursor_visible : false,
        .. Default::default()
    };
    
    let mut gameloop = Gameloop::new(gameinfo);
    
    gameloop.control_mut().renderer_mut().set_window_title("Keeshond Treats Animation Track | Arrow keys to move");
    
    let input = gameloop.control_mut().input_mut();

    input.add_bind("up", "up");
    input.add_bind("up", "gamepad up");
    input.add_bind("up", "gamepad lstick up");
    input.add_bind("down", "down");
    input.add_bind("down", "gamepad down");
    input.add_bind("down", "gamepad lstick down");
    input.add_bind("left", "left");
    input.add_bind("left", "gamepad left");
    input.add_bind("left", "gamepad lstick left");
    input.add_bind("right", "right");
    input.add_bind("right", "gamepad right");
    input.add_bind("right", "gamepad lstick right");
    
    let source_manager = gameloop.control_mut().source_manager();

    for path in &["examples_data", "keeshond_treats/examples_data", "keeshond/keeshond_treats/examples_data"]
    {
        let source = FilesystemSource::new(path, TrustLevel::TrustedSource);
        source_manager.borrow_mut().add_source(Box::new(source));
    }
    
    gameloop.control_mut().goto_new_scene::<GameScene>();
    gameloop.run();
}
