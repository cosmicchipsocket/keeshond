# 🐶 KEESHOND Treats 🐶

## About

Keeshond Treats is a crate that provides useful components, systems, and other supplemental materials that help you make your games quicker. It includes functionality that might not necessarily be needed by all games, and thus is not part of the core:

- Graphics:
    - Lerpable and Drawable primitives
    - Animations through Costume dataobject and Actor component
    - Sprite-based text through Text component
    - Tileset dataobject and Tilemaps optimized for large scrolling levels
    - Scrolling through Camera component
- Logic:
    - Position component
    - Fast object-to-object collision through Collidable component
- Level Editing:
    - Data-based object definition through Spawnables
    - Level data format and visual level editor


## License

Licensed under either of

 * Apache License, Version 2.0
   ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license
   ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.


## Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.
