# Keeshond TODOs

## Todo

### Core/Data

- [ ] Modularize into separate packages
- [ ] Multilingual support
- [ ] Configuration file support
- [ ] Datapack support for file streaming
- [ ] "Headless with timing" mode

### Input

- [ ] Last pressed input name for in-game rebinding
- [ ] Legacy joystick input
- [ ] Multiplayer gamepad input
- [ ] Touch events

### Rendering

- [ ] Custom window icon on non-Windows OSes
- [ ] Set window scale through code at runtime
- [ ] Forced integer scaling option for pixel viewport mode
- [ ] Shape drawing
- [ ] Improved framepacing for 30hz on 60hz display
- [ ] Material system
- [ ] Z-buffering

### Audio

- [ ] Music streaming playback
- [ ] Deterministic audio (decouple play length/position state from backend)
- [ ] Music stack - crossfading, jingles, multitrack

### Scene/ECS

- [ ] Disallow duplicate Systems/callbacks
- [ ] ECS QoL improvements
- [ ] Object culling
- [ ] System deactivation
- [ ] System auto-add

### Gameplay

- [ ] Object-to-tilemap collision
- [ ] Collision shapes
- [ ] Collision raycasts

### Editing

- [ ] Editor close confirmation
- [ ] Editor undo/redo
- [ ] Object selection handles
- [ ] Reorder objects
- [ ] Objects to different layer
- [ ] Lock object selection
- [ ] Edit properties on multi-object selection
- [ ] Select tiles within tilemap
- [ ] Unified graphics view between editors
- [ ] Oriented bounding box selection in level editor
- [ ] Ability to find/click on objects accidentally resized too small

### Documentation

- [ ] Update The Keeshond Book
- [ ] Update the Bat And Ball Tutorial

### Bugs

- [ ] Workaround init problems with default OpenAL device (ensure determinism first)
- [ ] Fix window pause when dragging/resizing on Windows
- [ ] Audio device pausing (Fix apps inhibiting sleep on Windows)
- [ ] Text component currently does not support joined graphemes or RTL layout
- [ ] Fixed rate imgui timing when using frame lerping
- [ ] Occasional framepacing issues on web builds


## Todo Feature Dig-down

- [ ] Tilemap collision
    - [ ] Basic axis-aligned movement (move X, test X, move Y, test Y)
    - [ ] Test overlap with tilemap objects, then get surface data from those tilemaps
    - [ ] Determine axis collision using surface data
        - [ ] Filter out unwanted surfaces when handling slopes
- [ ] Collision features
    - [ ] query_overlapping() allows non-stored collision
    - [ ] Collision layers
    - [ ] CollisionShape resolution
    - [ ] Function for clipping object given collision normals
    - [ ] Function for raycasting broad phase
    - [ ] Raycasting narrow phase with CollisionShape
- [ ] Object culling and deactivation
    - [ ] System for performing deactivation
    - [ ] Fast spatial lookup for reactivation
- [ ] Object groups
    - [ ] Multi-object spawnables
    - [ ] Entity groups in ECS
    - [ ] Transform hierarchies
- [ ] Music streaming
    - [ ] Datapack streaming
        - [ ] Direct loading, orthogonal to DataStore system
        - [ ] Basic system for requesting more from a file buffer?
    - [ ] New data type "Music"
        - [ ] Treated like a voice in the audio subsystem, but starting playback is different
        - [ ] Create separate thread to update any necessary streaming audio
- [ ] More drawing primitives
    - [ ] Point clouds
    - [ ] Line polygons
    - [ ] Closed shapes
    - [ ] Distance field text
    - [ ] Render-to-texture
        - [ ] Will require ability to make temp resources in Datapack
    - [ ] Custom shader parameters
    - [ ] Multitexturing


## Done for 0.28

Level format has changed to accomodate the new transform capabilities. Use
keeshond_migrator to upgrade old level files.

Multiple Cameras are now supported. If you were using the singleton Camera, that will
no longer work. Instead, use `camera_store.first()`.

Additionally, if you are using SpawnDefaults, you should move your call to
`self.spawn_defaults.spawn_defaults()` to the bottom of your `spawn()` function.

- [x] Updated to Rust 2021 Edition
- [x] Angle and scale in Transform
- [x] Z coordinate, angle, and scale in spawn function
- [x] Refactor level/representation/code component spawning
- [x] Custom sheets/costumes in loaded levels
- [x] Multiple cameras
- [x] Camera screen region
- [x] User-defined render scissor region
- [x] Z-buffering
- [x] Depth sorting in the core renderer
- [x] Automatic Z-buffer/depth sort mode
- [ ] Customizable depth range
- [ ] Rework Treats drawing infrastructure
- [ ] NamedItemStore::get_name() returns reference to string instead of new allocation
- [ ] Rework singletons
- [ ] Collision layers
- [ ] query_overlapping() allows non-stored collision
- [ ] Tilemap collision

## Done for 0.27

If you have AutoImport items from a previous version of Keeshond Editor, you will need to
manually go in and reenable AutoImport for any items due to timestamps being moved to a
separate, local TOML file.

You should also be repositioning Collidable directly instead of modifying Position and
syncing manually during your collision checks.

- [x] Position renamed to Transform
- [x] Reverted to previous style of Position <-> Collidable sync.
- [x] Drawable lerping rework
- [x] Collision API rework
- [x] Collision shapes
- [x] AutoImport only performs import if source file has different hash
- [x] Frame interpolation pacing to keep lerped motion smooth at the cost of some FPS
- [x] iter_flags() functionality moved into iter_filtered
- [x] Drawable fixes and performance tweaks
- [x] New entity mapping structure to heavily reduced memory bloat

## Done for 0.26

Really sorry about all the breakage this release! Some important API holes had to be closed.

- [x] New derive macro for Components
- [x] Support for defining newtype reference wrappers for Components
- [x] SpawnableTag for identifying objects for use in fast match statements
- [x] Reworked Position and Collidable for correctness and ergonomics
- [x] Changing Position now updates collision backend automatically
- [x] Manual control over which components are added first when spawning
- [x] New Prop representation for Spawnables that don't need the built-in animation handling
- [x] Gameloop timing fixes (allow short hitches but not continuous lag)
- [x] Drawable component set to use fast removal for a speed boost
- [x] Fix REASON_HIDDEN freeze flag
- [x] Fix freezing Actor/Prop/Tilemap causing Drawable position to not update
- [x] Fix correctness hole in memory assign/swap on Position/Collidable
- [x] Fix BorrowMutError after panic on web builds
- [x] Renderer no longer optional feature (for now)
- [x] Removed set_entity() (you can assign by mutable ref anyway)

## Done for 0.25.1

- [x] Fixed build for stable Rust

## Done for 0.25

- [x] Fix memory bloat in OpenGL renderer
- [x] Removed the WIP Vulkan renderer
- [x] Texture size and slice coordinates now exposed in shaders
- [x] Audio moved into separate keeshond_audio module
- [x] Undo some of the timing/sleep changes made in 0.24 (they caused problems)
- [x] Ability to freeze/thaw components to disable behaviors or save on processing time
- [x] DynArg now supports strings
- [x] Recompute layout when setting sheet in Text
- [x] Support for creating only a Sheet from the sprite importer
- [x] Revert cgmath to 0.17 for performance reasons
- [x] Rect/BakedRect overlap now uses symmetrical comparisons
- [x] Update base Keeshond examples to use DataHandle instead of DataId
- [x] Remove more DataId functionality that's no longer in step with the new DataHandle model
- [x] Additional OpenGL renderer tweaks
- [x] API and documentation cleanup
- [x] Remove try_or_else!()
- [x] keeshond_datapack now re-exported

## Done for 0.24

- [x] (Data breakage) Animation tracks for Costumes
- [x] New migrator program for converting data files to the latest format
- [x] Auto-importer support for animation tracks
- [x] Animation tracks example
- [x] High-DPI support
- [x] New logic for initial window size
- [x] Lag spikes now properly detected as lag
- [x] Timing no longer jumps too far ahead after lag spike
- [x] Additional timing fixes
- [x] Frame lerping example
- [x] Replace scancodes with keycodes
- [x] Reduce memory allocations when spawning
- [x] Batched component add optimizations
- [x] Performance/logic tweaks to component sorting
- [x] Add paired_axis_2d() and angle_to_n_way_direction() functions
- [x] Faster (but less accurate?) pixel snapping
- [x] Editor keyboard shortcuts
- [x] Scroll wheel to zoom in Sheet and Costume editors
- [x] Improved editor package tree view
- [x] Sheet/Costume thumbnails in package listing
- [x] Fix Level editor not refreshing drawables when reverting document
- [x] Level editor not refreshing drawables when edited elsewhere
- [x] Editor console window tweaks
- [x] Auto unload packages only after calling end() on previous scene.
- [x] Fix examples broken by auto-unload changes
- [x] Removed goto_constructed_scene()

## Done for 0.23

- [x] PackageUseToken for selectively unloading only unused packages
- [x] Unused packages now unload automatically between scenes (can be disabled in GameInfo)
- [x] DataId is now typed by the target resource type to discourage use on wrong type
- [x] DataHandle can now hold an ID without a path, for when the extra I/O isn't needed
- [x] DataHandle::new() renamed to DataHandle::with_path()
- [x] package_source_id() for getting the SourceId associated with a package
- [x] get_package_source() renamed to package_source()
- [x] Improve error message in DataHandle resolve
- [x] Add keeshond_datapack to list of default log filters
- [x] Level::apply() takes Level reference instead of DataHandle
- [x] keeshond_treats API cleanup in Actor, Text, Tilemap
- [x] Set current animation cel in Actor
- [x] Select all/none in level editor
- [x] Spawnables list now shows icon previews for Actors
- [x] Click to select tilemap within level editor Tile Tool
- [x] Make level editor object drag start distance zoom-aware
- [x] Crosshair for placing spawnable in level editor
- [x] Fix level editor incorrectly warning on blank auto layer field in SpawnableSet
- [x] Tool selection in context menu now using button controls
- [x] Update to imgui 0.8.1

## Done for 0.22

- [x] New spawnables! macro for defining spawnables in code
- [x] Per-Spawnable spawn functions now easily defined in separate source files
- [x] Keeshond Treats default systems now use struct for configuration
- [x] start_late() for first-frame initializations after all objects are created
- [x] get_nth() for direct indexing into component stores
- [x] More predictable component purge
- [x] Non-deferred spawns now use same component ordering as deferred
- [x] strum/strum_macros/enum-map packages no longer need to be manually included
- [x] Entity get_id() and get_generation() renamed to id() and generation()
- [x] General ECS ergonomic improvements and performance optimizations

## Done for 0.21

- [x] Auto-import for sprites, tilesets, and backgrounds!
- [x] Emscripten (web) support
- [x] Mouse input
- [x] .pk3 loading
- [x] Heightmap collision mode for tiles
- [x] In-game console history
- [x] Clipboard functions
- [x] Level editor zoom
- [x] Tile preview for level editor tile drawing
- [x] Boxed up GameControl to avoid stack overflow
- [x] Custom package filters in DogLog via GameInfo object
- [x] GameStats object for getting basic performance stats
- [x] Force purge components at end of frame
- [x] Queue keys pressed and released on the same frame
- [x] Suspend frame interpolation during logic slowdown to fix jumpy motion
- [x] Updated collidable example to show comparison with/without optimizations
- [x] Updated imgui package to 0.8
- [x] Other editor tweaks

## Done for 0.20

- [x] Level editor
- [x] Level data format
- [x] DataHandle for serializable caching of data path lookups
- [x] (Data breakage!) Costume, Tileset, and Spawnable data formats now using DataHandle
- [x] Thinker arguments rolled into single structs (ThinkerArgs, DrawerArgs)
- [x] SpawnArg and sparg!() renamed to DynArg and darg!()
- [x] Named arguments in DynArgList
- [x] Auto-enable/disable interpolation based on refresh rate
- [x] Fix mixed usage of entity/spawnable adds
- [x] Fix offset tilemap scrolling
- [x] Fix tileset sheet slicing
- [x] Auto-create folder for new package files
- [x] Updated imgui package to 0.7.0
- [x] New default font for imgui
- [x] Icons in editor
- [x] Tabbed documents for editor
- [x] Other editor UI tweaks

## Done for 0.19

- [x] Spawnable data format
- [x] Spawnable data loading
- [x] Gamepad per-stick/trigger axis deadzone support
- [x] Removed buggy axis priority system (multiple axes on same action are now cumulative)
- [x] Gamepad axis-to-key deadzone adjusted to 0.5
- [x] Scroll rate snap to camera
- [x] Visual filtering by tile flags in tileset editor

## Done for 0.18

- [x] Fix automatic row/columns in tileset gen
- [x] Object-to-object collision via custom broadphase
- [x] Reworked scene config
- [x] Singleton initialization in scene config
- [x] Reworked spawn arguments
- [x] Add/remove component callbacks
- [x] Show/hide mouse cursor
- [x] X/Y position now in root Drawable
- [x] Per-Drawable scrolling rate
- [x] More ergonomic DrawableItem access
- [x] New goto_new_scene() convenience function
- [x] Fix component tracking for entity removals
- [x] Easy slice adding in the Costume editor
- [x] Auto-scroll lists during operations in the editor
- [x] Fix origin calculation for paletted images to round down
- [x] Fix behavior of --interpolate commandline switch
- [x] Update The Keeshond Book Part 1

## Done for 0.17

- [x] Actor update fixes
- [x] Improved cache performance of sorted Drawable by no longer cross-referencing components during draw
- [x] Physical component replaced with Position for better cache-friendliness
- [x] Lerpable type
- [x] (Data breakage!) JSON filesize savings (shorter field names; costumes need manual update)
- [x] Tilesets and tileset editor
- [x] Tilemaps
- [x] Create new resources within editor
- [x] Fixed texture wrapping function to be repeat
- [x] Fixed error on drawing padded slice in sheet editor
- [x] Fixed right-click drag in sheet editor
- [x] ImGui bindings updated to 0.6.0
- [x] Other editor tweaks
- [x] A bunch of other fixes

## Done for 0.16

- [x] Reworked Text in Keeshond Treats
- [x] New Keeshond Treats Text example
- [x] Added new GameControl::lookup_res() convenience function. Please please use responsibly.
- [x] Mixed ordering of drawables in Keeshond Treats
- [x] Trust policy in keeshond_datapack to avoid loading possibly dangerous data from untrusted sources
- [x] Fixed OpenGL busy wait on multiple draw batches
- [x] Fixed build with graphical panic disabled
- [x] Polished window initialization

## Done for 0.15

- [x] (Data breakage!) Sheet JSON changed, manual update of existing JSON needed.
- [x] Registerable custom datapack types
- [x] Drawn slices generate integral origins for paletted images
- [x] Slice view in slice editor for lining up slice origins
- [x] Actor/Costume system
- [x] Data resources can have generations for tracking modifications
- [x] New default icon for window
- [x] Editor theming tweaks, including two new themes, Milk Chocolate and Cookies & Cream.
- [x] OpenGL renderer can now recalc viewport if changed in imgui thinker
- [x] 1:1 viewports
- [x] Fixed Escape key in imgui

## Done for 0.14

- [x] ImGui systems made more ergonomic
- [x] Texture filtering option part of sheet JSON metadata
- [x] Datapack support for writing files
- [x] ImGui "wants input" and texture integration
- [x] Option to allow computer to sleep during application runtime
- [x] Datapack repreparing of resources
- [x] Low energy mode for applications such as editors
- [x] Sprite slice editing

## Done for 0.13

- [x] Sprite slices
- [x] Gamepad input
- [x] Input axes
- [x] Four-way, eight-way, angle + distance directional input helpers
- [x] Ignore Enter when using Alt + Enter shortcut

## Done for 0.12

- [x] Entity now available as SpawnArg
- [x] spawn_later() returns an Entity
- [x] Entity is now cloneable
- [x] Entity now properly nullable
- [x] Fix readding entity with unpurged slot
- [x] Purge entries on mutable store access, not just mutable iterator
- [x] Basic support for global singletons

## Done for 0.11

- [x] Fix slow resource loading
- [x] ECS components now auto-register!
- [x] Update The Keeshond Book Part 1

## Done for 0.10

- [x] ECS QoL improvements
- [x] Fix game skipping initial scene frames due to loading lag
- [x] Fix playing sound on same frame as loading sound resource

## Done for 0.9

- [x] Component sorting
- [x] Frame interpolation improvements (frame cap, CLI switch)
- [x] Warning when forgetting to set initial scene
- [x] Spawn arguments
- [x] Fixed issue with doing component adds and removals on the same frame

## Done for 0.8

- [x] ECS QoL enhancements
- [x] Package listing support in keeshond_datapack
- [x] The Keeshond Book Part 1

## Done for 0.7

- [x] Object prefabs/prototypes
- [x] User-friendly GUI panic
- [x] Pixel viewports
- [x] Texture filtering made optional

## Done for 0.6

- [x] Faster transform matrix
- [x] Game properties
- [x] Kiosk mode
- [x] Internal commandline arguments

## Done for 0.5

- [x] Fullscreen mode
- [x] Sound playback
- [x] Sound voice controls (stop, adjust volume/pan/pitch)
