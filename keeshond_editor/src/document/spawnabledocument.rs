use crate::document::{Document, DocumentState, ActionHistory};
use crate::editorgui::{Packages, EditorConfig, ProjectConfig};

use std::any::Any;

use keeshond::gameloop::GameControl;
use keeshond::datapack::{DataId, DataHandle, DataStore};
use keeshond::renderer::Sheet;
use keeshond::util::{BakedRect, Circle};

use keeshond_treats::collision::CollisionShape;
use keeshond_treats::spawnable::{Spawnable, SpawnableSet, Representation, CollisionSettings};
use keeshond_treats::visual::Costume;

use crate::util::zoom::{EDITOR_ZOOM_LEVELS, ZoomControl};

pub const COLLISION_SHAPE_NAMES : [&'static str; 3] = ["None", "Box", "Circle"];

fn collision_shape_index(shape : &CollisionShape) -> usize
{
    match shape
    {
        CollisionShape::None => { 0 }
        CollisionShape::Box(..) => { 1 }
        CollisionShape::Circle(..) => { 2 }
    }
}

fn collision_shape_from_index(index : usize) -> CollisionShape
{
    match index
    {
        0 => CollisionShape::None,
        1 => CollisionShape::Box(BakedRect::default()),
        2 => CollisionShape::Circle(Circle::default()),
        _ => panic!("Unknown CollisionShape index")
    }
}

enum SpawnableSetAction
{
    AddSpawnable(usize, Spawnable),
    RenameSpawnable(usize, String),
    SetSpawnable(usize, Spawnable),
    MoveSpawnable(usize, usize),
    RemoveSpawnable(usize),
}

pub struct SpawnableSetDocument
{
    action_history : ActionHistory,
    data_id : DataId<SpawnableSet>,
    current_spawnable : usize,
    scroll_spawn_list : bool,
    focus_spawnable_name_input : bool,
    show_origin : bool,
    transparent_color : [f32; 3],
    zoom_control : ZoomControl<f32>,
    dpi_factor : f32,
    tics : u64
}

impl SpawnableSetDocument
{
    fn adjusted_zoom(&self) -> f32
    {
        self.zoom_control.zoom() / self.dpi_factor
    }

    fn scaled_round(&self, value : f32) -> f32
    {
        crate::util::scaled_round(value, 1.0 / self.dpi_factor)
    }

    fn draw_crosshair(&mut self, draw_list : &imgui::DrawListMut, x : f32, y : f32)
    {
        let pulse = (((self.tics as f64 * 0.1).sin() + 1.0) / 2.0) as f32;

        let cross_size = 4.1 + self.adjusted_zoom().min(8.0);
        let cross_color = [pulse, pulse, pulse, 1.0];

        draw_list.add_line([x - cross_size, y], [x + cross_size, y], imgui::ImColor32::from(cross_color)).thickness(1.0 / self.dpi_factor).build();
        draw_list.add_line([x, y - cross_size], [x, y + cross_size], imgui::ImColor32::from(cross_color)).thickness(1.0 / self.dpi_factor).build();
    }

    fn add_spawnable(&mut self, index : usize, spawnables : &mut SpawnableSet, spawnable : Spawnable)
    {
        let mut target_spawnable = 0;

        if spawnables.spawnable_count() > 0
        {
            target_spawnable = index + 1;
        }

        spawnables.insert_spawnable(target_spawnable, spawnable);

        self.current_spawnable += 1;
    }

    fn move_spawnable(&mut self, spawnables : &mut SpawnableSet, old : usize, new : usize) -> bool
    {
        if new >= spawnables.spawnable_count()
        {
            return false;
        }

        if let Some(spawnable) = spawnables.spawnable(old).cloned()
        {
            let old_name = spawnables.spawnable_name(old);

            spawnables.remove_spawnable(old);
            spawnables.insert_spawnable(new, spawnable);

            if let Some(name) = old_name
            {
                spawnables.rename_spawnable(new, &name);
            }

            self.current_spawnable = new;

            return true;
        }

        false
    }

    fn basic_tab(&mut self, packages : &mut Packages, ui : &imgui::Ui,
                 spawnables : &SpawnableSet, spawnable : &Spawnable, sheet_store : &DataStore<Sheet>)
    {
        ui.text(format!("Editing Spawnable {}", self.current_spawnable));
        ui.spacing();

        let mut spawnable_name = spawnables.spawnable_name(self.current_spawnable).unwrap_or_default();

        if self.focus_spawnable_name_input
        {
            ui.set_keyboard_focus_here_with_offset(imgui::FocusedWidget::Offset(0));
            self.focus_spawnable_name_input = false;
        }

        if ui.input_text("Name###spawnable_name", &mut spawnable_name).enter_returns_true(true).build()
        {
            self.queue_action(Box::new(SpawnableSetAction::RenameSpawnable(self.current_spawnable, spawnable_name.to_string())));
        }

        ui.spacing();

        let mut index = spawnable.representation().index();

        if ui.combo_simple_string("Representation", &mut index, &crate::util::widget::REPRESENTATION_NAMES)
        {
            let representation = Representation::from_index(index);

            let mut spawnable_clone = spawnable.clone();
            spawnable_clone.set_representation(representation);

            self.queue_action(Box::new(SpawnableSetAction::SetSpawnable(self.current_spawnable, spawnable_clone)));
        }

        imgui::ChildWindow::new("representation_controls").size([0.0, 116.0]).draw_background(false).border(false).build(ui, ||
        {
            ui.indent_by(10.0);

            match spawnable.representation()
            {
                Representation::None => {}
                Representation::Prop { sheet, slice } =>
                {
                    let mut update_prop = false;
                    let mut package_name = sheet.package().clone();
                    let mut sheet_name = sheet.path().clone();
                    let mut slice_name = slice.clone();

                    ui.spacing();

                    if let Some((package, sheet, slice)) = crate::util::widget::sheet_slice_popup(packages, ui, &sheet_store)
                    {
                        update_prop = true;
                        package_name = package;
                        sheet_name = sheet;
                        slice_name = slice;
                    }

                    let style_var = ui.push_style_var(imgui::StyleVar::ItemSpacing([-1.0, -1.0]));
                    let width = ui.push_item_width(ui.column_width(0) * 0.6);

                    update_prop |= ui.input_text("Sheet Package", &mut package_name).enter_returns_true(true).build();
                    update_prop |= ui.input_text("Sheet Name", &mut sheet_name).enter_returns_true(true).build();
                    update_prop |= ui.input_text("Slice Name", &mut slice_name).enter_returns_true(true).build();

                    width.pop(ui);
                    style_var.pop();

                    if update_prop
                    {
                        let mut spawnable_clone = spawnable.clone();

                        spawnable_clone.set_representation(Representation::Prop
                        {
                            sheet : DataHandle::with_path(&package_name, &sheet_name),
                            slice : slice_name
                        });

                        self.queue_action(Box::new(SpawnableSetAction::SetSpawnable(self.current_spawnable, spawnable_clone)));
                    }
                }
                Representation::Actor { costume } =>
                {
                    let mut update_actor = false;
                    let mut package_name = costume.package().clone();
                    let mut costume_name = costume.path().clone();

                    ui.spacing();

                    if let Some((package, costume)) = crate::util::widget::costume_popup(packages, ui)
                    {
                        update_actor = true;
                        package_name = package;
                        costume_name = costume;
                    }

                    let style_var = ui.push_style_var(imgui::StyleVar::ItemSpacing([-1.0, -1.0]));
                    let width = ui.push_item_width(ui.column_width(0) * 0.6);

                    update_actor |= ui.input_text("Costume Package", &mut package_name).enter_returns_true(true).build();
                    update_actor |= ui.input_text("Costume Name", &mut costume_name).enter_returns_true(true).build();

                    width.pop(ui);
                    style_var.pop();

                    if update_actor
                    {
                        let mut spawnable_clone = spawnable.clone();

                        spawnable_clone.set_representation(Representation::Actor
                        {
                            costume : DataHandle::with_path(&package_name, &costume_name)
                        });

                        self.queue_action(Box::new(SpawnableSetAction::SetSpawnable(self.current_spawnable, spawnable_clone)));
                    }
                }
                Representation::Tilemap => {}
            }

            ui.unindent_by(10.0);
        });

        let mut auto_layer = spawnable.auto_layer_name().clone();

        if ui.input_text("Auto-Set Layer", &mut auto_layer).enter_returns_true(true).build()
        {
            let mut spawnable_clone = spawnable.clone();

            spawnable_clone.set_auto_layer_name(auto_layer);

            self.queue_action(Box::new(SpawnableSetAction::SetSpawnable(self.current_spawnable, spawnable_clone)));
        }
    }

    fn collision_tab(&mut self, ui : &imgui::Ui, spawnable : &Spawnable)
    {
        ui.text(format!("Editing Spawnable {}", self.current_spawnable));
        ui.spacing();

        let mut collision_enabled = spawnable.collision().is_some();

        if ui.checkbox("Collision Enabled", &mut collision_enabled)
        {
            let mut spawnable_clone = spawnable.clone();
            let new_collision = match collision_enabled
            {
                true =>
                {
                    Some(CollisionSettings
                    {
                        shape : Default::default(),
                        indexed : true
                    })
                }
                false => { None }
            };

            spawnable_clone.set_collision(new_collision);

            self.queue_action(Box::new(SpawnableSetAction::SetSpawnable(self.current_spawnable, spawnable_clone)));
        }

        crate::util::widget::hover_hint("Gives this object a Collidable component. When moving this object around,\n\
                be sure to use the position in Collidable rather than the Transform.", ui);

        imgui::ChildWindow::new("collision_controls").size([0.0, 116.0]).draw_background(false).border(false).build(ui, ||
        {
            if let Some(mut collision) = spawnable.collision().cloned()
            {
                let mut update_collision = false;

                update_collision |= ui.checkbox("Collision Indexing", &mut collision.indexed);

                crate::util::widget::hover_hint("Registers the object in the collision lookup backend, allowing it to\n\
                        show up in results from query_overlapping_bounds(). If you're colliding\n\
                        many objects, you probably want this enabled so you can perform fast lookups.\n\
                        If you don't need to use query_overlapping_bounds() for this object, disable\n\
                        this for a speed boost.", ui);

                let mut index = collision_shape_index(&collision.shape);

                if ui.combo_simple_string("Collision Shape", &mut index, &COLLISION_SHAPE_NAMES)
                {
                    collision.shape = collision_shape_from_index(index);

                    let mut spawnable_clone = spawnable.clone();
                    spawnable_clone.set_collision(Some(collision.clone()));

                    self.queue_action(Box::new(SpawnableSetAction::SetSpawnable(self.current_spawnable, spawnable_clone)));
                }

                match &mut collision.shape
                {
                    CollisionShape::None => {}
                    CollisionShape::Box(bound) =>
                    {
                        let mut x_bounds = [bound.x1, bound.x2];
                        let mut y_bounds = [bound.y1, bound.y2];

                        update_collision |= imgui::Drag::new("X Bounds")
                            .range(-65536.0, 65536.0).display_format("%.4f").build_array(ui, &mut x_bounds);
                        update_collision |= imgui::Drag::new("Y Bounds")
                            .range(-65536.0, 65536.0).display_format("%.4f").build_array(ui, &mut y_bounds);

                        bound.x1 = x_bounds[0];
                        bound.y1 = y_bounds[0];
                        bound.x2 = x_bounds[1];
                        bound.y2 = y_bounds[1];

                        bound.make_positive();
                    }
                    CollisionShape::Circle(circle) =>
                    {
                        let mut center = [circle.x, circle.y];

                        update_collision |= imgui::Drag::new("Center Pos")
                            .range(-65536.0, 65536.0).display_format("%.4f").build_array(ui, &mut center);
                        update_collision |= imgui::Drag::new("Radius")
                            .range(0.0, 65536.0).display_format("%.4f").build(ui, &mut circle.radius);

                        circle.x = center[0];
                        circle.y = center[1];
                    }
                }

                if update_collision
                {
                    let mut spawnable_clone = spawnable.clone();

                    spawnable_clone.set_collision(Some(collision));

                    self.queue_action(Box::new(SpawnableSetAction::SetSpawnable(self.current_spawnable, spawnable_clone)));
                }
            }
        });
    }

    fn spawnable_list_view(&mut self, ui : &imgui::Ui, spawnables : &mut SpawnableSet,
                           costume_store : &mut DataStore<Costume>, sheet_store : &mut DataStore<Sheet>,
                           packages : &mut Packages)
    {
        let main_style = ui.clone_style();

        self.current_spawnable = self.current_spawnable.min(spawnables.spawnable_count().max(1) - 1);

        if ui.button("Add")
        {
            self.queue_action(Box::new(SpawnableSetAction::AddSpawnable(self.current_spawnable, Spawnable::new())));
        }
        ui.same_line_with_spacing(0.0, 4.0);
        if ui.button("Up") && self.current_spawnable > 0
        {
            self.queue_action(Box::new(SpawnableSetAction::MoveSpawnable(self.current_spawnable, self.current_spawnable - 1)));
        }
        ui.same_line_with_spacing(0.0, 4.0);
        if ui.button("Down")
        {
            self.queue_action(Box::new(SpawnableSetAction::MoveSpawnable(self.current_spawnable, self.current_spawnable + 1)));
        }
        ui.same_line_with_spacing(0.0, 4.0);
        if ui.button("Remove")
        {
            self.queue_action(Box::new(SpawnableSetAction::RemoveSpawnable(self.current_spawnable)));
        }

        let style_spawnable_list = ui.push_style_var(imgui::StyleVar::WindowPadding([3.0, 2.0]));
        let colors_spawnable_list = ui.push_style_color(imgui::StyleColor::ChildBg, main_style.colors[imgui::StyleColor::FrameBg as usize]);

        if let Some(spawnable_list) = imgui::ChildWindow::new("spawnable_list").border(true).draw_background(true).begin(ui)
        {
            crate::util::widget::ui_spawnable_list(spawnables, &mut self.current_spawnable, &mut self.scroll_spawn_list, costume_store, sheet_store, packages, ui);

            spawnable_list.end();
        }

        colors_spawnable_list.pop();
        style_spawnable_list.pop();
    }

    fn spawnable_image(&mut self, packages : &mut Packages, ui : &imgui::Ui, sheet_store : &mut DataStore<Sheet>, costume_store : &mut DataStore<Costume>, spawnable : &Spawnable)
    {
        let mut image_info = None;

        match spawnable.representation()
        {
            Representation::None => {}
            Representation::Prop { sheet, slice } =>
            {
                image_info = crate::util::drawable::sheet_slice_image(sheet, slice, sheet_store, packages);
            }
            Representation::Actor { costume } =>
            {
                image_info = crate::util::drawable::costume_image(costume, costume_store, sheet_store, packages);
            }
            Representation::Tilemap => {}
        }

        let style_var = ui.push_style_var(imgui::StyleVar::WindowPadding([1.0, 1.0]));
        let style_color = ui.push_style_color(imgui::StyleColor::ChildBg, [self.transparent_color[0], self.transparent_color[1], self.transparent_color[2], 1.0]);

        imgui::ChildWindow::new("texture_view").size([0.0, 0.0])
            .scrollable(false).scroll_bar(false).draw_background(true).border(true).build(ui, ||
        {
            if ui.is_window_hovered_with_flags(imgui::WindowHoveredFlags::CHILD_WINDOWS)
            {
                self.zoom_control.update_mouse_wheel(ui.io().mouse_wheel);
            }

            let slice_space = ui.window_size();
            let center_x = self.scaled_round(slice_space[0] / 2.0);
            let center_y = self.scaled_round(slice_space[1] / 2.0);

            if let Some((image, rect)) = image_info
            {
                let image_x = self.scaled_round(center_x - (rect.x) * self.adjusted_zoom());
                let image_y = self.scaled_round(center_y - (rect.y) * self.adjusted_zoom());

                ui.set_cursor_pos([image_x, image_y]);
                crate::util::widget::round_cursor_screen_pos(ui);

                image.size([self.scaled_round(rect.w * self.adjusted_zoom()),
                    self.scaled_round(rect.h * self.adjusted_zoom())]).build(ui);
            }

            ui.set_cursor_pos([center_x, center_y]);
            crate::util::widget::round_cursor_screen_pos(ui);

            let draw_list = ui.get_window_draw_list();
            let cursor_pos = ui.cursor_screen_pos();

            if let Some(collision) = spawnable.collision()
            {
                match &collision.shape
                {
                    CollisionShape::None => {}
                    CollisionShape::Box(bound) =>
                    {
                        let x1 = cursor_pos[0] + (bound.x1 as f32 * self.adjusted_zoom());
                        let y1 = cursor_pos[1] + (bound.y1 as f32 * self.adjusted_zoom());
                        let x2 = cursor_pos[0] + (bound.x2 as f32 * self.adjusted_zoom());
                        let y2 = cursor_pos[1] + (bound.y2 as f32 * self.adjusted_zoom());

                        draw_list.add_rect([x1, y1], [x2, y2], [1.0, 1.0, 1.0, 0.25]).filled(true).build();
                        draw_list.add_rect([x1, y1], [x2, y2], [1.0, 1.0, 1.0, 1.0]).thickness(1.0).build();
                    }
                    CollisionShape::Circle(circle) =>
                    {
                        let center_x = cursor_pos[0] + (circle.x as f32 * self.adjusted_zoom());
                        let center_y = cursor_pos[1] + (circle.y as f32 * self.adjusted_zoom());
                        let circle_radius = circle.radius as f32 * self.adjusted_zoom();

                        draw_list.add_circle([center_x, center_y], circle_radius, [1.0, 1.0, 1.0, 0.25]).filled(true).build();
                        draw_list.add_circle([center_x, center_y], circle_radius, [1.0, 1.0, 1.0, 1.0]).thickness(1.0).build();
                    }
                }
            }

            if self.show_origin
            {
                self.draw_crosshair(&draw_list, cursor_pos[0], cursor_pos[1]);
            }
        });

        style_color.pop();
        style_var.pop();
    }
}

impl Document for SpawnableSetDocument
{
    fn load(game : &mut GameControl, package_name : &str, item_name : &str) -> Self where Self : Sized
    {
        let mut data_id = DataId::new(0);

        if let Ok(id) = game.res().store_mut::<SpawnableSet>().get_id_mut(package_name, item_name)
        {
            data_id = id;
        }

        let dpi_factor = game.renderer().current_display_scale_factor();

        SpawnableSetDocument
        {
            action_history : ActionHistory::new(),
            data_id,
            current_spawnable : 0,
            scroll_spawn_list : false,
            focus_spawnable_name_input : false,
            show_origin : true,
            transparent_color : [0.5, 0.5001, 0.5002],
            zoom_control : ZoomControl::new(EDITOR_ZOOM_LEVELS.to_vec(), (1.0 * dpi_factor).round()),
            dpi_factor,
            tics : 0
        }
    }

    fn do_ui(&mut self, packages : &mut Packages, _config : &mut EditorConfig,
             _project : &mut ProjectConfig, game : &mut GameControl, ui : &imgui::Ui) -> DocumentState
    {
        self.tics = self.tics.wrapping_add(1);
        self.dpi_factor = game.renderer().current_display_scale_factor();

        let mut result = DocumentState::new();

        ui.menu_bar(||
        {
            ui.menu("View", ||
            {
                if imgui::MenuItem::new("Show Origin").selected(self.show_origin).build(ui)
                {
                    self.show_origin = !self.show_origin;
                }

                ui.menu("Background Color", ||
                {
                    imgui::ColorPicker::new("##background_color_picker", imgui::EditableColor::Float3(&mut self.transparent_color)).build(ui);
                });
            });
            ui.menu("Zoom", ||
            {
                for level in &EDITOR_ZOOM_LEVELS
                {
                    if imgui::MenuItem::new(format!("{}%", (level * 100.0) as i32)).selected(self.zoom_control.zoom() == *level).build(ui)
                    {
                        self.zoom_control.set_zoom(*level);
                    }
                }
            });
        });

        result.active = ui.is_window_focused();

        let mut sheet_store = game.res().store_mut::<Sheet>();
        let mut costume_store = game.res().store_mut::<Costume>();
        let mut spawnable_store = game.res().store_mut::<SpawnableSet>();

        if let Some(spawnables) = spawnable_store.get_mut(self.data_id)
        {
            ui.columns(2, "##spawnable_columns", false);

            if let Some(spawnable_controls) = imgui::ChildWindow::new("spawnable_controls")
                .size([320.0, 0.0]).draw_background(true).border(false).horizontal_scrollbar(true).begin(ui)
            {
                if let Some(spawnable) = spawnables.spawnable(self.current_spawnable)
                {
                    if let Some(tab_bar) = imgui::TabBar::new("spawnable_tab_bar")
                        .flags(imgui::TabBarFlags::FITTING_POLICY_SCROLL).begin(ui)
                    {
                        if let Some(tab) = imgui::TabItem::new("Basic").begin(ui)
                        {
                            self.basic_tab(packages, ui, spawnables, spawnable, &sheet_store);

                            tab.end();
                        }

                        if let Some(tab) = imgui::TabItem::new("Collision").begin(ui)
                        {
                            self.collision_tab(ui, spawnable);

                            tab.end();
                        }

                        tab_bar.end();
                    }
                }

                ui.spacing();
                ui.spacing();

                if let Some(spawnable_list_view) = imgui::ChildWindow::new("spawnable_list_view").size([0.0, 0.0]).movable(false).begin(ui)
                {
                    self.spawnable_list_view(ui, spawnables, &mut costume_store, &mut sheet_store, packages);

                    spawnable_list_view.end();
                }

                spawnable_controls.end();
            }

            ui.set_current_column_width(ui.item_rect_size()[0] + 10.0);
            ui.next_column();

            if let Some(spawnable) = spawnables.spawnable(self.current_spawnable)
            {
                self.spawnable_image(packages, ui, &mut sheet_store, &mut costume_store, spawnable)
            }
        }

        result
    }

    fn action_history(&mut self) -> &mut ActionHistory
    {
        &mut self.action_history
    }

    fn apply_action(&mut self, game : &mut GameControl, action : &Box<dyn Any>) -> bool
    {
        if let Some(action) = action.downcast_ref::<SpawnableSetAction>()
        {
            let mut spawnable_store = game.res().store_mut::<SpawnableSet>();

            if let Some(spawnables) = spawnable_store.get_mut(self.data_id)
            {
                match action
                {
                    SpawnableSetAction::AddSpawnable(index, spawnable) =>
                    {
                        self.scroll_spawn_list = true;
                        self.focus_spawnable_name_input = true;
                        self.add_spawnable(*index, spawnables, spawnable.clone());
                        return true;
                    }
                    SpawnableSetAction::RenameSpawnable(index, name) =>
                    {
                        self.scroll_spawn_list = true;
                        return spawnables.rename_spawnable(*index, name);
                    }
                    SpawnableSetAction::SetSpawnable(index, spawnable) =>
                    {
                        self.scroll_spawn_list = true;
                        return spawnables.set_spawnable(*index, spawnable.clone());
                    }
                    SpawnableSetAction::MoveSpawnable(old_index, new_index) =>
                    {
                        self.scroll_spawn_list = true;
                        return self.move_spawnable(spawnables, *old_index, *new_index);
                    }
                    SpawnableSetAction::RemoveSpawnable(index) =>
                    {
                        self.scroll_spawn_list = true;
                        return spawnables.remove_spawnable(*index).is_some();
                    }
                }
            }
        }

        false
    }
}
