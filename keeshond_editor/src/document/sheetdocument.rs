use crate::document::{Document, DocumentState, ActionHistory};
use crate::editorgui::{Packages, EditorConfig, ProjectConfig};
use crate::import::sheet::TilesetGenInfo;

use keeshond::gameloop::GameControl;
use keeshond::renderer::{Sheet, SpriteSlice};
use keeshond::datapack::DataId;

use std::any::Any;

use image::{RgbaImage, GrayImage};
use crate::util::zoom::{EDITOR_ZOOM_LEVELS, ZoomControl};

const MAX_DRAW_POINTS : usize = 32768;

enum SheetAction
{
    SetPixels(Vec<u8>),
    SetPixelsWithSize(Vec<u8>, u32, u32),
    SetTextureFiltering(bool, bool),
    AddSlice(usize, SpriteSlice),
    AddSlices(usize, Vec<SpriteSlice>),
    RenameSlice(usize, String),
    SetSlice(usize, SpriteSlice),
    MoveSlice(usize, usize),
    RemoveSlice(usize),
    RemoveSlices(usize, usize)
}

pub struct SheetDocument
{
    action_history : ActionHistory,
    data_id : DataId<Sheet>,
    show_origin : bool,
    transparent_color : [f32; 3],
    zoom_control : ZoomControl<f32>,
    dpi_factor : f32,
    current_slice : usize,
    tics : u64,
    draw_tool_on : bool,
    draw_tool_padding : bool,
    focus_slice_name_input : bool,
    scroll_slice_list : bool,
    mouse_drawing_texture : bool,
    draw_points : Vec<(f32, f32)>,
    sheet_scroll_x : f32,
    sheet_scroll_y : f32,
    update_sheet_scroll : bool,
    dragging : bool,
    tilegen : TilesetGenInfo,
    tilegen_preview : bool,
    on_tilegen : bool
}

impl SheetDocument
{
    fn adjusted_zoom(&self) -> f32
    {
        self.zoom_control.zoom() / self.dpi_factor
    }

    fn scaled_floor(&self, value : f32) -> f32
    {
        crate::util::scaled_floor(value, 1.0 / self.dpi_factor)
    }

    fn scaled_round(&self, value : f32) -> f32
    {
        crate::util::scaled_round(value, 1.0 / self.dpi_factor)
    }

    fn opaque_pixels(&self, sheet : &Sheet) -> toodee::TooDee<bool>
    {
        let mut pixels = toodee::TooDee::new(sheet.width() as usize, sheet.height() as usize);

        for y in 0..sheet.height()
        {
            for x in 0..sheet.width()
            {
                if let Some(color) = sheet.get_pixel(x, y)
                {
                    if color.a > 0
                    {
                        pixels[(x as usize, y as usize)] = true;
                    }
                }
            }
        }

        pixels
    }

    fn draw_slice_bounds(&mut self, pos: [f32; 2], draw_list: &imgui::DrawListMut, slice: &SpriteSlice, active : bool)
    {
        let mut x = self.scaled_round(slice.texture_x * self.adjusted_zoom() + pos[0]);
        let mut y = self.scaled_round(slice.texture_y * self.adjusted_zoom() + pos[1]);
        let mut x2 = self.scaled_round(x + slice.texture_w * self.adjusted_zoom());
        let mut y2 = self.scaled_round(y + slice.texture_h * self.adjusted_zoom());

        let color;
        let thickness;

        if active
        {
            x -= 1.0;
            y -= 1.0;
            x2 += 1.0;
            y2 += 1.0;

            let pulse = (((self.tics as f64 * 0.1).sin() + 1.0) / 2.0) as f32;

            let origin_x = x + (slice.origin_x * self.adjusted_zoom()) + 1.0;
            let origin_y = y + (slice.origin_y * self.adjusted_zoom()) + 1.0;

            color = [1.0, pulse, pulse, 1.0];
            thickness = ((self.tics as f64 * 0.05).sin() + 1.0) as f32;

            if self.show_origin
            {
                self.draw_crosshair(draw_list, origin_x, origin_y);
            }
        }
        else
        {
            thickness = 1.0;
            color = [1.0, 1.0, 1.0, 1.0];
        }

        draw_list.add_rect([x, y], [x2, y2], imgui::ImColor32::from(color)).thickness(thickness / self.dpi_factor).build();
    }

    fn draw_crosshair(&mut self, draw_list : &imgui::DrawListMut, x : f32, y : f32)
    {
        let pulse = (((self.tics as f64 * 0.1).sin() + 1.0) / 2.0) as f32;

        let cross_size = 4.1 + self.adjusted_zoom().min(8.0);
        let cross_color = [pulse, pulse, pulse, 1.0];

        draw_list.add_line([x - cross_size, y], [x + cross_size, y], imgui::ImColor32::from(cross_color)).thickness(1.0 / self.dpi_factor).build();
        draw_list.add_line([x, y - cross_size], [x, y + cross_size], imgui::ImColor32::from(cross_color)).thickness(1.0 / self.dpi_factor).build();
    }

    fn defringe_sheet(&mut self, sheet : &mut Sheet)
    {
        let mut pixels = sheet.data();
        let mut texture = RgbaImage::from_raw(sheet.width(), sheet.height(), pixels).expect("Raw data to image failed");

        crate::util::sheet::defringe_image(&mut texture, true);

        pixels = texture.into_raw();

        self.queue_action(Box::new(SheetAction::SetPixels(pixels)));
    }

    fn select_slice(&mut self, sheet: &mut Sheet, point : (f32, f32))
    {
        if sheet.slice_count() <= 0
        {
            return;
        }

        let (x, y) = point;
        let old_slice = self.current_slice;
        let mut i = (old_slice + 1) % sheet.slice_count();

        while i != old_slice
        {
            if let Some(slice) = sheet.slice(i)
            {
                if x >= slice.texture_x && y >= slice.texture_y
                    && x < slice.texture_x + slice.texture_w
                    && y < slice.texture_y + slice.texture_h
                {
                    self.current_slice = i;
                    return;
                }
            }

            i = (i + 1) % sheet.slice_count();
        }
    }

    fn add_slice(&mut self, index : usize, sheet: &mut Sheet, slice : SpriteSlice)
    {
        let mut target_slice = 0;

        if sheet.slice_count() > 0
        {
            target_slice = index + 1;
        }

        sheet.insert_slice(target_slice, slice);

        self.current_slice += 1;
    }

    fn add_slices(&mut self, index : usize, sheet: &mut Sheet, slices : &Vec<SpriteSlice>)
    {
        let mut target_slice = 0;

        if sheet.slice_count() > 0
        {
            target_slice = index + 1;
        }

        for slice in slices
        {
            sheet.insert_slice(target_slice, slice.clone());
            target_slice += 1;
        }

        self.current_slice += slices.len();
    }

    fn move_slice(&mut self, sheet : &mut Sheet, old : usize, new : usize) -> bool
    {
        if new >= sheet.slice_count()
        {
            return false;
        }

        if let Some(slice) = sheet.slice(old).cloned()
        {
            let old_name = sheet.slice_name(old);

            sheet.remove_slice(old);
            sheet.insert_slice(new, slice);

            if let Some(name) = old_name
            {
                sheet.rename_slice(new, &name);
            }

            self.current_slice = new;

            return true;
        }

        false
    }

    fn make_slice_from_draw_points(&mut self, sheet : &mut Sheet) -> bool
    {
        let mut success = false;
        let mut left = sheet.width() as usize;
        let mut right = 0;
        let mut top = sheet.height() as usize;
        let mut bottom = 0;
        let pixels = self.opaque_pixels(sheet);

        for (point_x, point_y) in &self.draw_points
        {
            let x = *point_x as usize;
            let y = *point_y as usize;

            self.adjust_box_for_pixels(&mut left, &mut right, &mut top, &mut bottom, x, y, pixels.clone());
        }

        let mut left_tex = left as f32;
        let mut right_tex = right as f32 + 1.0;
        let mut top_tex = top as f32;
        let mut bottom_tex = bottom as f32 + 1.0;

        if self.draw_tool_padding
        {
            left_tex -= 1.0;
            top_tex -= 1.0;
            right_tex += 1.0;
            bottom_tex += 1.0;
        }

        if right_tex >= left_tex && bottom_tex >= top_tex
        {
            let width = right_tex - left_tex;
            let height = bottom_tex - top_tex;
            let mut origin_x = width / 2.0;
            let mut origin_y = height / 2.0;

            if sheet.has_palette()
            {
                origin_x = origin_x.floor();
                origin_y = origin_y.floor();
            }

            let slice = SpriteSlice
            {
                texture_x: left_tex,
                texture_y: top_tex,
                texture_w: width,
                texture_h: height,
                origin_x,
                origin_y
            };

            self.queue_action(Box::new(SheetAction::AddSlice(self.current_slice, slice)));

            success = true;
        }

        self.draw_points.clear();

        success
    }

    fn adjust_box_for_pixels(&self, left : &mut usize, right : &mut usize, top : &mut usize, bottom : &mut usize,
                             x : usize, y : usize, mut pixels: toodee::TooDee<bool>)
    {
        if x > *left && x < *right && y > *top && y < *bottom
        {
            return;
        }

        crate::util::sheet::flood_fill(&mut pixels, x, y, true, |tiles, x, y|
        {
            tiles[(x, y)] == true
        },
            |tiles, x, y|
            {
                tiles[(x, y)] = false;

                *left = (*left).min(x);
                *right = (*right).max(x);
                *top = (*top).min(y);
                *bottom = (*bottom).max(y);
            });
    }

    fn draw_tileset_preview(&self, pos: [f32; 2], draw_list : &imgui::DrawListMut, sheet : &Sheet)
    {
        if self.tilegen.width as f32 * self.adjusted_zoom() < 8.0
            || self.tilegen.height as f32 * self.adjusted_zoom() < 8.0
        {
            return;
        }

        let tileset_info = crate::import::sheet::tileset_slice_info(sheet.width(), sheet.height(), &self.tilegen);

        let pulse = (((self.tics as f64 * 0.06).sin() + 1.0) / 2.0) as f32;
        let color = [pulse, pulse, pulse, 1.0];
        let thickness = 1.0;

        for y in 0..tileset_info.rows
        {
            for x in 0..tileset_info.columns
            {
                let x1 = pos[0] + (self.tilegen.x as f32 + (tileset_info.tile_walk_x * x) as f32) * self.adjusted_zoom();
                let y1 = pos[1] + (self.tilegen.y as f32 + (tileset_info.tile_walk_y * y) as f32) * self.adjusted_zoom();
                let x2 = x1 + (self.tilegen.width as f32) * self.adjusted_zoom();
                let y2 = y1 + (self.tilegen.height as f32) * self.adjusted_zoom();

                draw_list.add_rect([x1, y1], [x2, y2], imgui::ImColor32::from(color)).thickness(thickness / self.dpi_factor).build();
            }
        }
    }

    fn create_tileset_from_sheet(&mut self, sheet : &Sheet)
    {
        let pixels = sheet.data();
        let slices;

        if sheet.has_palette()
        {
            let texture = GrayImage::from_raw(sheet.width(), sheet.height(), pixels).expect("Raw data to image failed");
            let (new_texture, new_slices) = crate::import::sheet::create_tileset_texture_and_slices(&texture, &mut self.tilegen);
            let (width, height) = (new_texture.width(), new_texture.height());

            self.queue_action(Box::new(SheetAction::SetPixelsWithSize(new_texture.into_raw(), width, height)));
            slices = new_slices;
        }
        else
        {
            let texture = RgbaImage::from_raw(sheet.width(), sheet.height(), pixels).expect("Raw data to image failed");
            let (new_texture, new_slices) = crate::import::sheet::create_tileset_texture_and_slices(&texture, &mut self.tilegen);
            let (width, height) = (new_texture.width(), new_texture.height());

            self.queue_action(Box::new(SheetAction::SetPixelsWithSize(new_texture.into_raw(), width, height)));
            slices = new_slices;
        }

        if slices.len() > 0
        {
            self.queue_action(Box::new(SheetAction::RemoveSlices(0, sheet.slice_count())));
            self.queue_action(Box::new(SheetAction::AddSlices(0, slices)));
        }
    }

    fn apply_zoom(&mut self, level : f32)
    {
        self.zoom_control.set_zoom(level);
        self.update_sheet_scroll = true;
    }

    fn sheet_tab(&mut self, ui : &imgui::Ui, sheet : &mut Sheet)
    {
        let (mut force, mut force_on) = crate::util::option_bool_to_double_bool(sheet.texture_filtering());
        let mut update_filtering = false;

        ui.text_wrapped(format!("Size: {}x{}", sheet.width(), sheet.height()));

        update_filtering |= ui.checkbox("Force texture filter setting", &mut force);

        if force
        {
            ui.indent();
            update_filtering |= ui.checkbox("Enabled", &mut force_on);
            ui.unindent();
        }

        if update_filtering
        {
            self.queue_action(Box::new(SheetAction::SetTextureFiltering(force, force_on)));
        }

        if ui.button("De-fringe")
        {
            if sheet.has_palette()
            {
                ui.open_popup("Can't Defringe");
            }
            else
            {
                self.defringe_sheet(sheet);
            }
        }

        ui.popup_modal("Can't Defringe").resizable(false).build(ui, ||
        {
            ui.text("De-fringing requires averaging pixels,\nwhich can't be done for paletted images!");
            if ui.button("Oh well!")
            {
                ui.close_current_popup();
            }
        });

        crate::util::widget::hover_hint("Use this if you see an undesired halo around the edges of your graphics\nwhen zooming and texture filtering is enabled.", ui);

        ui.separator();
        ui.spacing();
        ui.text(format!("Editing Slice {}", self.current_slice));
        ui.spacing();

        let mut slice_name = sheet.slice_name(self.current_slice).unwrap_or_default();

        self.current_slice = self.current_slice.min(sheet.slice_count().max(1) - 1);

        if let Some(slice) = sheet.slice(self.current_slice)
        {
            let mut update_slice = false;

            if self.focus_slice_name_input
            {
                ui.set_keyboard_focus_here_with_offset(imgui::FocusedWidget::Offset(0));
                self.focus_slice_name_input = false;
            }

            if ui.input_text("Name", &mut slice_name).enter_returns_true(true).build()
            {
                self.queue_action(Box::new(SheetAction::RenameSlice(self.current_slice, slice_name.to_string())));
            }

            let mut pos = [slice.texture_x, slice.texture_y];
            let mut size = [slice.texture_w, slice.texture_h];
            let mut origin = [slice.origin_x, slice.origin_y];

            let style_var = ui.push_style_var(imgui::StyleVar::ItemSpacing([-1.0, -1.0]));

            update_slice |= imgui::Drag::new("Pos").display_format("%.4f").build_array(ui, &mut pos);
            update_slice |= imgui::Drag::new("Size").display_format("%.4f").build_array(ui, &mut size);

            style_var.pop();

            update_slice |= imgui::Drag::new("Origin").display_format("%.4f").build_array(ui, &mut origin);

            if update_slice
            {
                let mut slice = slice.clone();

                slice.texture_x = pos[0];
                slice.texture_y = pos[1];
                slice.texture_w = size[0];
                slice.texture_h = size[1];
                slice.origin_x = origin[0];
                slice.origin_y = origin[1];

                self.queue_action(Box::new(SheetAction::SetSlice(self.current_slice, slice)));
            }
        }

        ui.spacing();
        ui.separator();
        ui.spacing();

        ui.checkbox("Draw Tool", &mut self.draw_tool_on);

        if self.draw_tool_on
        {
            ui.indent();
            ui.checkbox("Add Padding", &mut self.draw_tool_padding);
            ui.unindent();
        }

        if ui.button("Add")
        {
            self.queue_action(Box::new(SheetAction::AddSlice(self.current_slice, SpriteSlice
            {
                texture_x: 0.0,
                texture_y: 0.0,
                texture_w: 32.0,
                texture_h: 32.0,
                origin_x: 16.0,
                origin_y: 16.0
            })));
        }
        ui.same_line_with_spacing(0.0, 4.0);
        if ui.button("Up") && self.current_slice > 0
        {
            self.queue_action(Box::new(SheetAction::MoveSlice(self.current_slice, self.current_slice - 1)));
        }
        ui.same_line_with_spacing(0.0, 4.0);
        if ui.button("Down")
        {
            self.queue_action(Box::new(SheetAction::MoveSlice(self.current_slice, self.current_slice + 1)));
        }
        ui.same_line_with_spacing(0.0, 4.0);
        if ui.button("Remove")
        {
            self.queue_action(Box::new(SheetAction::RemoveSlice(self.current_slice)));
        }

        let main_style = ui.clone_style();
        let style_slice_list = ui.push_style_var(imgui::StyleVar::WindowPadding([3.0, 2.0]));
        let colors_slice_list = ui.push_style_color(imgui::StyleColor::ChildBg, main_style.colors[imgui::StyleColor::FrameBg as usize]);

        imgui::ChildWindow::new("slice_list").border(true).draw_background(true).build(ui, ||
        {
            let num_slices = sheet.slice_count();

            for i in 0..num_slices
            {
                let slice_name;

                if let Some(name) = sheet.slice_name(i)
                {
                    slice_name = name;
                }
                else
                {
                    slice_name = format!("<slice {}>", i);
                }

                if imgui::Selectable::new(&slice_name).selected(self.current_slice == i).build(ui)
                {
                    self.current_slice = i;
                }

                if self.current_slice == i && self.scroll_slice_list
                {
                    ui.set_scroll_here_y();
                    self.scroll_slice_list = false;
                }
            }
        });

        colors_slice_list.pop();
        style_slice_list.pop();
    }

    fn tilegen_tab(&mut self, ui : &imgui::Ui, sheet : &mut Sheet)
    {
        self.tilegen.tilegen_ui(ui);

        ui.spacing();
        ui.spacing();

        ui.checkbox("Preview", &mut self.tilegen_preview);

        ui.spacing();
        ui.spacing();
        ui.separator();
        ui.spacing();
        ui.spacing();

        if ui.button("Create Slices and Optimize Texture")
        {
            self.create_tileset_from_sheet(sheet);
        }

        ui.text_wrapped("Click this to get the sheet ready for use as a tileset. \
                                Warning: This will modify the texture and replace any existing slices on this sheet!");

        ui.spacing();
        ui.spacing();

        if ui.button("Create Slices Only")
        {
            let slices = crate::import::sheet::create_tileset_slices(&self.tilegen,
                &crate::import::sheet::tileset_slice_info(sheet.width(), sheet.height(), &self.tilegen));

            if slices.len() > 0
            {
                self.queue_action(Box::new(SheetAction::RemoveSlices(0, sheet.slice_count())));
                self.queue_action(Box::new(SheetAction::AddSlices(0, slices)));
            }
        }

        ui.text_wrapped("Only use this if you know what you are doing.");
    }

    fn texture_sheet_tab(&mut self, ui : &imgui::Ui, sheet : &mut Sheet)
    {
        let style_var_outer = ui.push_style_var(imgui::StyleVar::WindowPadding([0.0, 0.0]));

        imgui::ChildWindow::new("texture_view").flags(imgui::WindowFlags::NO_SCROLL_WITH_MOUSE)
            .size([0.0, 0.0]).draw_background(true).border(true)
            .horizontal_scrollbar(true).movable(false).build(ui, ||
        {
            let window_pos = ui.window_pos();
            let size = ui.window_size();

            let mut real_scroll_x = ui.scroll_x();
            let mut real_scroll_y = ui.scroll_y();

            if ui.is_window_hovered_with_flags(imgui::WindowHoveredFlags::CHILD_WINDOWS)
            {
                if self.zoom_control.update_mouse_wheel(ui.io().mouse_wheel)
                {
                    self.apply_zoom(self.zoom_control.zoom());
                }
            }

            let mut mouse_x_ratio = 0.5;
            let mut mouse_y_ratio = 0.5;

            if ui.is_window_hovered_with_flags(imgui::WindowHoveredFlags::CHILD_WINDOWS)
            {
                mouse_x_ratio = ((ui.io().mouse_pos[0] - window_pos[0]) / size[0]).clamp(0.0, 1.0);
                mouse_y_ratio = ((ui.io().mouse_pos[1] - window_pos[1]) / size[1]).clamp(0.0, 1.0);
            }

            if self.update_sheet_scroll
            {
                self.update_sheet_scroll = false;

                let real_scroll_x_max = (sheet.width() as f32 * self.adjusted_zoom() - size[0]).max(0.0);
                let real_scroll_y_max = (sheet.height() as f32 * self.adjusted_zoom() - size[1]).max(0.0);

                real_scroll_x = (self.sheet_scroll_x * self.adjusted_zoom() - size[0] * mouse_x_ratio)
                    .clamp(0.0, real_scroll_x_max);
                real_scroll_y = (self.sheet_scroll_y * self.adjusted_zoom() - size[1] * mouse_y_ratio)
                    .clamp(0.0, real_scroll_y_max);
                ui.set_scroll_x(real_scroll_x);
                ui.set_scroll_y(real_scroll_y);
            }

            self.sheet_scroll_x = (real_scroll_x + size[0] * mouse_x_ratio + 0.5) / self.adjusted_zoom();
            self.sheet_scroll_y = (real_scroll_y + size[1] * mouse_y_ratio + 0.5) / self.adjusted_zoom();

            let mut delta = [0.0, 0.0];

            let width = (sheet.width() as f32 * self.adjusted_zoom()).max(1.0);
            let height = (sheet.height() as f32 * self.adjusted_zoom()).max(1.0);

            let pos_min = [
                self.scaled_round(window_pos[0] - real_scroll_x),
                self.scaled_round(window_pos[1] - real_scroll_y)];
            let pos_max = [
                self.scaled_round(pos_min[0] + width),
                self.scaled_round(pos_min[1] + height)];

            ui.invisible_button("texture_drag", [width, height]);

            let hovering = ui.is_item_hovered();

            if ui.is_mouse_clicked(imgui::MouseButton::Left) && hovering && !self.on_tilegen
            {
                if self.draw_tool_on
                {
                    self.mouse_drawing_texture = true;
                }
                else
                {
                    let mouse_pos = ui.io().mouse_pos;
                    let new_point = (
                        self.scaled_floor((mouse_pos[0] - pos_min[0]) / self.adjusted_zoom()),
                        self.scaled_floor((mouse_pos[1] - pos_min[1]) / self.adjusted_zoom())
                    );

                    self.select_slice(sheet, new_point);
                }
            }

            if self.mouse_drawing_texture && ui.is_mouse_down(imgui::MouseButton::Left) && hovering
            {
                let mouse_pos = ui.io().mouse_pos;
                let new_point = (
                    self.scaled_floor((mouse_pos[0] - pos_min[0]) / self.adjusted_zoom()),
                    self.scaled_floor((mouse_pos[1] - pos_min[1]) / self.adjusted_zoom())
                );
                let mut push = true;

                if let Some(last) = self.draw_points.last()
                {
                    push = *last != new_point;
                }

                if push
                {
                    self.draw_points.push(new_point);

                    if self.draw_points.len() > MAX_DRAW_POINTS
                    {
                        self.draw_points.remove(0);
                    }
                }
            }
            else if !ui.is_mouse_down(imgui::MouseButton::Left)
            {
                self.mouse_drawing_texture = false;

                if self.draw_points.len() > 0
                {
                    self.focus_slice_name_input = self.make_slice_from_draw_points(sheet);
                }
            }

            if ui.is_mouse_clicked(imgui::MouseButton::Right) && hovering
            {
                self.dragging = true;
            }

            if self.dragging && ui.is_mouse_down(imgui::MouseButton::Right)
            {
                delta = ui.io().mouse_delta;
            }
            else if !ui.is_mouse_down(imgui::MouseButton::Right)
            {
                self.dragging = false;
            }

            ui.same_line_with_pos(1.0);
            let draw_list = ui.get_window_draw_list();

            draw_list.add_rect(pos_min, pos_max, [self.transparent_color[0], self.transparent_color[1], self.transparent_color[2], 1.0])
                .filled(true).build();
            draw_list.add_rect(pos_min, pos_max, ui.style_color(imgui::StyleColor::Border)).build();
            draw_list.add_image(imgui::TextureId::from(self.data_id.index()), pos_min, pos_max).build();

            if !self.on_tilegen
            {
                for i in 0..sheet.slice_count()
                {
                    if let Some(slice) = sheet.slice(i)
                    {
                        self.draw_slice_bounds(pos_min, &draw_list, slice, false);
                    }
                }

                if let Some(slice) = sheet.slice(self.current_slice)
                {
                    self.draw_slice_bounds(pos_min, &draw_list, slice, true);
                }

                for point in 1..self.draw_points.len()
                {
                    let (mut x1, mut y1) = self.draw_points[point - 1];
                    let (mut x2, mut y2) = self.draw_points[point];

                    if self.zoom_control.zoom() >= 3.0
                    {
                        x1 += 0.5;
                        y1 += 0.5;
                        x2 += 0.5;
                        y2 += 0.5;
                    }

                    draw_list.add_line([self.scaled_round(x1 * self.adjusted_zoom()) + pos_min[0],
                                           self.scaled_round(y1 * self.adjusted_zoom()) + pos_min[1]],
                                       [self.scaled_round(x2 * self.adjusted_zoom()) + pos_min[0],
                                           self.scaled_round(y2 * self.adjusted_zoom()) + pos_min[1]],
                                       imgui::ImColor32::from([1.0, 1.0, 1.0, 1.0]))
                        .thickness(1.0 / self.dpi_factor).build();
                }
            }
            else if self.tilegen_preview
            {
                self.draw_tileset_preview(pos_min, &draw_list, sheet);
            }

            if delta[0] != 0.0 || delta[1] != 0.0
            {
                ui.set_scroll_x(ui.scroll_x() - delta[0]);
                ui.set_scroll_y(ui.scroll_y() - delta[1]);
            }
        });

        style_var_outer.pop();
    }

    fn texture_slice_tab(&mut self, ui : &imgui::Ui, sheet : &mut Sheet)
    {
        let style_var = ui.push_style_var(imgui::StyleVar::WindowPadding([1.0, 1.0]));
        let style_color = ui.push_style_color(imgui::StyleColor::ChildBg, [self.transparent_color[0], self.transparent_color[1], self.transparent_color[2], 1.0]);

        imgui::ChildWindow::new("texture_slice_item").size([0.0, 0.0])
            .scrollable(false).scroll_bar(false).draw_background(true).border(true).build(ui, ||
        {
            if ui.is_window_hovered_with_flags(imgui::WindowHoveredFlags::CHILD_WINDOWS)
            {
                if self.zoom_control.update_mouse_wheel(ui.io().mouse_wheel)
                {
                    self.apply_zoom(self.zoom_control.zoom());
                }
            }

            if let Some(slice) = sheet.slice(self.current_slice)
            {
                let base_width = sheet.width() as f32;
                let base_height = sheet.height() as f32;
                let x1 = slice.texture_x / base_width;
                let y1 = slice.texture_y / base_height;
                let x2 = (slice.texture_x + slice.texture_w) / base_width;
                let y2 = (slice.texture_y + slice.texture_h) / base_height;

                let slice_space = ui.window_size();
                let center_x = self.scaled_round(slice_space[0] / 2.0);
                let center_y = self.scaled_round(slice_space[1] / 2.0);
                let image_x = self.scaled_round(center_x - slice.origin_x * self.adjusted_zoom());
                let image_y = self.scaled_round(center_y - slice.origin_y * self.adjusted_zoom());

                ui.set_cursor_pos([image_x, image_y]);
                crate::util::widget::round_cursor_screen_pos(ui);

                imgui::Image::new(imgui::TextureId::from(self.data_id.index()),
                                  [self.scaled_round(slice.texture_w * self.adjusted_zoom()),
                                      self.scaled_round(slice.texture_h * self.adjusted_zoom())])
                    .uv0([x1, y1]).uv1([x2, y2]).build(ui);

                ui.set_cursor_pos([center_x, center_y]);
                crate::util::widget::round_cursor_screen_pos(ui);

                let draw_list = ui.get_window_draw_list();
                let cursor_pos = ui.cursor_screen_pos();

                if self.show_origin
                {
                    self.draw_crosshair(&draw_list, cursor_pos[0], cursor_pos[1]);
                }
            }
            else
            {
                ui.text("No slice selected!");
            }
        });

        style_color.pop();
        style_var.pop();
    }
}

impl Document for SheetDocument
{
    fn load(game : &mut GameControl, package_name : &str, item_name: &str) -> Self where Self : Sized
    {
        let mut data_id = DataId::new(0);

        if let Ok(id) = game.res().store_mut::<Sheet>().get_id_mut(package_name, item_name)
        {
            data_id = id;
        }

        let dpi_factor = game.renderer().current_display_scale_factor();

        SheetDocument
        {
            action_history : ActionHistory::new(),
            data_id,
            show_origin : true,
            transparent_color : [0.5, 0.5001, 0.5002],
            zoom_control : ZoomControl::new(EDITOR_ZOOM_LEVELS.to_vec(), (1.0 * dpi_factor).round()),
            dpi_factor,
            current_slice : 0,
            tics : 0,
            draw_tool_on : false,
            draw_tool_padding : false,
            focus_slice_name_input : false,
            scroll_slice_list : false,
            mouse_drawing_texture : false,
            draw_points : Vec::new(),
            sheet_scroll_x : 0.0,
            sheet_scroll_y : 0.0,
            update_sheet_scroll: false,
            dragging : false,
            tilegen : TilesetGenInfo
            {
                width : 32,
                height : 32,
                x : 0,
                y : 0,
                space_x : 0,
                space_y : 0,
                rows : 0,
                columns : 0
            },
            tilegen_preview : true,
            on_tilegen : false
        }
    }

    fn action_history(&mut self) -> &mut ActionHistory
    {
        &mut self.action_history
    }

    fn apply_action(&mut self, game: &mut GameControl, action: &Box<dyn Any>) -> bool
    {
        if let Some(action) = action.downcast_ref::<SheetAction>()
        {
            let mut sheet_store = game.res().store_mut::<Sheet>();

            if let Some(sheet) = sheet_store.get_mut(self.data_id)
            {
                match action
                {
                    SheetAction::SetPixels(pixels) =>
                    {
                        if sheet.set_data(pixels.clone())
                        {
                            sheet_store.reprepare(self.data_id);
                            return true;
                        }
                    }
                    SheetAction::SetPixelsWithSize(pixels, width, height) =>
                    {
                        let palette_opt = sheet.palette();
                        let success;

                        if let Some(palette) = palette_opt
                        {
                            success = sheet.set_indexed_data_with_size(pixels.clone(), *width, *height, palette);
                        }
                        else
                        {
                            success = sheet.set_rgba_data_with_size(pixels.clone(), *width, *height);
                        }

                        if success
                        {
                            sheet_store.reprepare(self.data_id);
                        }

                        return success;
                    }
                    SheetAction::SetTextureFiltering(force, force_on) =>
                    {
                        sheet.set_texture_filtering(crate::util::double_bool_to_option_bool(*force, *force_on));
                        sheet_store.reprepare(self.data_id);
                        return true;
                    }
                    SheetAction::AddSlice(index, slice) =>
                    {
                        self.scroll_slice_list = true;
                        self.add_slice(*index, sheet, slice.clone());
                        sheet_store.reprepare(self.data_id);
                        return true;
                    }
                    SheetAction::AddSlices(index, slices) =>
                    {
                        self.scroll_slice_list = true;
                        self.add_slices(*index, sheet, slices);
                        sheet_store.reprepare(self.data_id);
                        return true;
                    }
                    SheetAction::RenameSlice(index, name) =>
                    {
                        self.scroll_slice_list = true;

                        let result = sheet.rename_slice(*index, name);
                        sheet_store.reprepare(self.data_id);

                        return result;
                    }
                    SheetAction::SetSlice(index, slice) =>
                    {
                        self.scroll_slice_list = true;

                        let result = sheet.set_slice(*index, slice.clone());
                        sheet_store.reprepare(self.data_id);

                        return result;
                    }
                    SheetAction::MoveSlice(old_index, new_index) =>
                    {
                        self.scroll_slice_list = true;

                        let result = self.move_slice(sheet, *old_index, *new_index);
                        sheet_store.reprepare(self.data_id);

                        return result;
                    }
                    SheetAction::RemoveSlice(index) =>
                    {
                        self.scroll_slice_list = true;

                        let result = sheet.remove_slice(*index).is_some();
                        sheet_store.reprepare(self.data_id);

                        return result;
                    }
                    SheetAction::RemoveSlices(index, count) =>
                    {
                        let mut removed = false;

                        for _ in 0..*count
                        {
                            removed |= sheet.remove_slice(*index).is_some();
                        }

                        self.scroll_slice_list = true;

                        sheet_store.reprepare(self.data_id);

                        return removed;
                    }
                }
            }
        }

        false
    }

    fn do_ui(&mut self, _packages : &mut Packages, _config : &mut EditorConfig,
             _project : &mut ProjectConfig, game : &mut GameControl, ui : &imgui::Ui) -> DocumentState
    {
        self.tics = self.tics.wrapping_add(1);
        self.dpi_factor = game.renderer().current_display_scale_factor();

        let mut result = DocumentState::new();

        ui.menu_bar(||
        {
            ui.menu("View", ||
            {
                if imgui::MenuItem::new("Show Origin").selected(self.show_origin).build(ui)
                {
                    self.show_origin = !self.show_origin;
                }

                ui.menu("Background Color", ||
                {
                    imgui::ColorPicker::new("##background_color_picker", imgui::EditableColor::Float3(&mut self.transparent_color)).build(ui);
                });
            });
            ui.menu("Zoom", ||
            {
                for level in &EDITOR_ZOOM_LEVELS
                {
                    if imgui::MenuItem::new(format!("{}%", (level * 100.0) as i32))
                        .selected(self.zoom_control.zoom() == *level).build(ui)
                    {
                        self.apply_zoom(*level);
                    }
                }
            });
        });

        result.active = ui.is_window_focused();

        let mut sheet_store = game.res().store_mut::<Sheet>();

        if let Some(sheet) = sheet_store.get_mut(self.data_id)
        {
            ui.columns(2, "##sheet_columns", false);

            if let Some(sheet_controls) = imgui::ChildWindow::new("sheet_controls").size([240.0, 0.0]).draw_background(true).border(false).begin(ui)
            {
                if let Some(tab_bar) = imgui::TabBar::new("controls_tabbar")
                    .flags(imgui::TabBarFlags::FITTING_POLICY_SCROLL).begin(ui)
                {
                    if let Some(tab) = imgui::TabItem::new("Sheet").begin(ui)
                    {
                        self.sheet_tab(ui, sheet);
                        tab.end();
                    }

                    self.on_tilegen = false;

                    if let Some(tab) = imgui::TabItem::new("Tileset Gen").begin(ui)
                    {
                        self.on_tilegen = true;

                        self.tilegen_tab(ui, sheet);
                        tab.end();
                    }

                    tab_bar.end();
                }

                sheet_controls.end();
            }

            ui.set_current_column_width(ui.item_rect_size()[0] + 10.0);
            ui.next_column();

            if let Some(texture_tab_bar) = imgui::TabBar::new("texture_tab_bar").begin(ui)
            {
                if let Some(tab) = imgui::TabItem::new("Sheet View").begin(ui)
                {
                    self.texture_sheet_tab(ui, sheet);

                    tab.end();
                }

                if !self.on_tilegen
                {
                    if let Some(tab) = imgui::TabItem::new("Slice View").begin(ui)
                    {
                        self.texture_slice_tab(ui, sheet);

                        tab.end();
                    }
                }

                texture_tab_bar.end();
            }
        }

        result
    }
}
