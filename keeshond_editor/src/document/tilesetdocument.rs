use crate::document::{Document, DocumentState, ActionHistory};
use crate::editorgui::{Packages, ResourceType, EditorConfig, ProjectConfig};

use std::collections::HashSet;
use std::any::Any;

use keeshond::gameloop::GameControl;
use keeshond::renderer::Sheet;

use keeshond::datapack::{DataId, DataHandle};
use keeshond_treats::tilemap::{Tileset, TilesetTile, TileCollision, TileBoundingBoxData, TileHeightmapData};
use keeshond_treats::visual::{SliceIndexHandle};

const MAX_HEIGHTMAP_POINTS : usize = 256;

enum TilesetAction
{
    SetSheet(String, String),
    SetTileSize(f64, f64),
    AddTile(usize, TilesetTile),
    AddTiles(usize, Vec<TilesetTile>),
    SetTile(usize, TilesetTile),
    MoveTile(usize, usize),
    RemoveTile(usize),
    RenameFlag(usize, String)
}

pub struct TilesetDocument
{
    action_history : ActionHistory,
    data_id : DataId<Tileset>,
    current_tile : usize,
    extra_selection : HashSet<usize>,
    left_column_set : bool,
    transparent_color : [f32; 3],
    zoom_level : f32,
    show_colliders_in_list : bool,
    scroll_tile_list : bool,
    filter_flags : u32
}

impl TilesetDocument
{
    fn add_tile(&mut self, index : usize, tileset : &mut Tileset, tile : TilesetTile) -> bool
    {
        let mut target_tile = 0;

        if tileset.tile_count() > 0
        {
            target_tile = index + 1;
        }

        if tileset.insert_tile(target_tile, tile.clone())
        {
            self.current_tile += 1;
            return true;
        }

        false
    }

    fn add_tiles(&mut self, index : usize, tileset : &mut Tileset, tiles : &Vec<TilesetTile>) -> bool
    {
        if tiles.is_empty()
        {
            return false;
        }

        let mut target_tile = 0;
        let mut result = false;

        if tileset.tile_count() > 0
        {
            target_tile = index + 1;
        }

        for tile in tiles
        {
            if tileset.insert_tile(target_tile, tile.clone())
            {
                target_tile += 1;
                self.current_tile += 1;

                result = true;
            }
        }

        result
    }

    fn move_tile(&mut self, tileset : &mut Tileset, old : usize, new : usize) -> bool
    {
        if new >= tileset.tile_count()
        {
            return false;
        }

        if let Some(tile) = tileset.tile(old).cloned()
        {
            tileset.remove_tile(old);
            tileset.insert_tile(new, tile);

            self.current_tile = new;

            return true;
        }

        false
    }

    fn graph_points(&self, x1 : f64, x2 : f64, y : &Vec<f64>) -> Vec<(f64, f64)>
    {
        let mut points = Vec::with_capacity(y.len() + 1);

        if y.len() == 1
        {
            points.push((x1, y[0]));
            points.push((x2, y[0]));
        }
        else
        {
            let xstep = (x2 - x1) / ((y.len() - 1) as f64);

            for i in 0..y.len()
            {
                points.push((x1 + xstep * i as f64, y[i]));
            }
        }

        return points;
    }

    fn draw_collision_shape(&mut self, pos : [f32; 2], size : [f32; 2], zoom : f32, draw_list : &imgui::DrawListMut, tile : &TilesetTile)
    {
        draw_list.add_rect([pos[0], pos[1]], [pos[0] + size[0], pos[1] + size[1]], imgui::ImColor32::from([0.0, 0.0, 0.0, 0.25])).filled(true).build();

        let adjust_x = |x| { (pos[0] + x as f32 * zoom).max(pos[0]).min(pos[0] + size[0]).round() };
        let adjust_y = |y| { (pos[1] + y as f32 * zoom).max(pos[1]).min(pos[1] + size[1]).round() };
        let adjust_x_poly = |x| { (pos[0] + x as f32 * zoom).max(pos[0]).min(pos[0] + size[0] - 1.0) };
        let adjust_y_poly = |y| { (pos[1] + y as f32 * zoom).max(pos[1]).min(pos[1] + size[1] - 1.0) };

        match tile.collision()
        {
            TileCollision::None => {}
            TileCollision::BoundingBox(bounding_box) =>
            {
                let x1 = adjust_x(bounding_box.x1);
                let y1 = adjust_y(bounding_box.y1);
                let x2 = adjust_x(bounding_box.x2);
                let y2 = adjust_y(bounding_box.y2);

                draw_list.add_rect([x1, y1], [x2, y2], [1.0, 1.0, 1.0, 0.25]).filled(true).build();
                draw_list.add_rect([x1, y1], [x2, y2], [1.0, 1.0, 1.0, 1.0]).thickness(1.0).build();
            }
            TileCollision::Heightmap(heightmap) =>
            {
                if !heightmap.valid()
                {
                    return;
                }

                let draw_lines_xy = |line : &[(f64, f64)]|
                {
                    draw_list.add_line([adjust_x_poly(line[0].0), adjust_y_poly(line[0].1)],
                                       [adjust_x_poly(line[1].0), adjust_y_poly(line[1].1)],
                                       [1.0, 1.0, 0.0, 1.0]).thickness(1.0).build();
                };
                let draw_lines_yx = |line : &[(f64, f64)]|
                {
                    draw_list.add_line([adjust_x_poly(line[0].1), adjust_y_poly(line[0].0)],
                                       [adjust_x_poly(line[1].1), adjust_y_poly(line[1].0)],
                                       [1.0, 0.0, 0.0, 1.0]).thickness(1.0).build();
                };

                let (top_x1, top_x2) = heightmap.top_x_range();
                let (bottom_x1, bottom_x2) = heightmap.bottom_x_range();
                let (left_y1, left_y2) = heightmap.left_y_range();
                let (right_y1, right_y2) = heightmap.right_y_range();

                for line in self.graph_points(top_x1, top_x2, &heightmap.top).windows(2)
                {
                    draw_lines_xy(line);
                }
                for line in self.graph_points(bottom_x1, bottom_x2, &heightmap.bottom).windows(2)
                {
                    draw_lines_xy(line);
                }

                for line in self.graph_points(left_y1, left_y2, &heightmap.left).windows(2)
                {
                    draw_lines_yx(line);
                }
                for line in self.graph_points(right_y1, right_y2, &heightmap.right).windows(2)
                {
                    draw_lines_yx(line);
                }
            }
        }
    }

    fn slices_tab(&mut self, ui : &imgui::Ui, tileset : &mut Tileset, sheet_opt : Option<&Sheet>)
    {
        let main_style = ui.clone_style();
        let source = tileset.sheet();
        let mut package_name = source.package().clone();
        let mut sheet_name = source.path().clone();

        let mut update_sheet = false;

        update_sheet |= ui.input_text("Package", &mut package_name).enter_returns_true(true).build();
        update_sheet |= ui.input_text("Sheet", &mut sheet_name).enter_returns_true(true).build();

        if update_sheet
        {
            self.queue_action(Box::new(TilesetAction::SetSheet(package_name.to_string(), sheet_name.to_string())));
        }

        let (width, height) = tileset.tile_size();
        let mut sizes = [width, height];

        if imgui::Drag::new("Tile Size").display_format("%.4f").build_array(ui, &mut sizes)
        {
            self.queue_action(Box::new(TilesetAction::SetTileSize(sizes[0], sizes[1])));
        }

        if let Some(sheet) = sheet_opt
        {
            if let Some(slice) = sheet.slice(0)
            {
                if width as f32 != slice.texture_w || height as f32 != slice.texture_h
                {
                    ui.same_line();

                    if ui.button("!###tile_size_auto")
                    {
                        self.queue_action(Box::new(TilesetAction::SetTileSize(slice.texture_w as f64, slice.texture_h as f64)));
                    }
                }
            }
        }

        ui.spacing();
        ui.separator();
        ui.spacing();
        ui.text(format!("Editing Tile {}", self.current_tile));

        ui.spacing();

        if let Some(sheet) = sheet_opt
        {
            if self.current_tile < tileset.tile_count()
            {
                let index_opt = tileset.updated_slice_id(self.current_tile, sheet);
                let mut has_slice = index_opt.is_some();

                if ui.checkbox("Has Slice", &mut has_slice)
                {
                    let mut tile = tileset.tile(self.current_tile).expect("Current tile vanished!").clone();

                    if has_slice
                    {
                        tile.set_slice_handle(Some(SliceIndexHandle::with_index(0)));
                    }
                    else
                    {
                        tile.set_slice_handle(None);
                    }

                    self.queue_action(Box::new(TilesetAction::SetTile(self.current_tile, tile)));
                }

                if let Some(index) = index_opt
                {
                    let mut index_slider = index as u32;
                    let mut slice_name = tileset.slice_name(self.current_tile).cloned().unwrap_or_default();

                    let width = ui.push_item_width(48.0);
                    if imgui::Drag::new("Index###slice_index").build(ui, &mut index_slider)
                    {
                        let mut tile = tileset.tile(self.current_tile).expect("Current tile vanished!").clone();
                        tile.set_slice_handle(Some(SliceIndexHandle::with_index(index_slider as usize)));

                        self.queue_action(Box::new(TilesetAction::SetTile(self.current_tile, tile)));
                    }
                    width.pop(ui);

                    ui.same_line();

                    let width = ui.push_item_width(128.0);
                    if ui.input_text("Name###slice_name", &mut slice_name).build()
                    {
                        let mut tile = tileset.tile(self.current_tile).expect("Current tile vanished!").clone();
                        tile.set_slice_handle(Some(SliceIndexHandle::with_name(&slice_name)));

                        self.queue_action(Box::new(TilesetAction::SetTile(self.current_tile, tile)));
                    }
                    width.pop(ui);
                }
                else
                {
                    ui.invisible_button("###slice_placeholder", [100.0, ui.current_font_size() + main_style.frame_padding[1] * 2.0]);
                }
            }

            if ui.button("Create Tiles From Unused Slices")
            {
                let mut tiles = Vec::new();
                let mut used = HashSet::new();

                let num_slices = sheet.slice_count();
                let num_tiles = tileset.tile_count();

                for i in 0..num_tiles
                {
                    if let Some(id) = tileset.updated_slice_id(i, sheet)
                    {
                        used.insert(id);
                    }
                }

                for i in 0..num_slices
                {
                    if !used.contains(&i)
                    {
                        tiles.push(TilesetTile::new(Some(SliceIndexHandle::with_index(i))));
                    }
                }

                self.queue_action(Box::new(TilesetAction::AddTiles(self.current_tile.max(1) - 1, tiles)));
            }
        }
    }

    fn collision_tab(&mut self, ui : &imgui::Ui, tileset : &mut Tileset)
    {
        ui.text(format!("Editing Tile {}", self.current_tile));

        if self.extra_selection.len() > 0
        {
            ui.same_line();

            if ui.small_button(format!("Copy To {} Others###collision_copy", self.extra_selection.len()))
            {
                if let Some(tile) = tileset.tile(self.current_tile)
                {
                    let collision = tile.collision().clone();

                    for i in &self.extra_selection.clone()
                    {
                        if let Some(mut other_tile) = tileset.tile(*i).cloned()
                        {
                            other_tile.set_collision(collision.clone());

                            self.queue_action(Box::new(TilesetAction::SetTile(*i, other_tile)));
                        }
                    }
                }
            }
        }

        ui.spacing();

        let remaining = ui.content_region_avail();

        if let Some(token) = imgui::ChildWindow::new("tileset_collision_controls").size([0.0, remaining[1] * 0.3]).draw_background(false).border(false).begin(ui)
        {
            if let Some(tile) = tileset.tile(self.current_tile)
            {
                let mut index = match tile.collision()
                {
                    TileCollision::None => { 0 }
                    TileCollision::BoundingBox(_) => { 1 }
                    TileCollision::Heightmap(_) => { 2 }
                };

                if ui.combo_simple_string("Collision Type",
                    &mut index, &["None", "Bounding Box", "Heightmap"])
                {
                    let mut new_tile = tile.clone();
                    let (width, height) = tileset.tile_size();
                    let collision = match index
                    {
                        0 => TileCollision::None,
                        1 => TileCollision::BoundingBox(TileBoundingBoxData::from_width_height(width, height)),
                        2 => TileCollision::Heightmap(TileHeightmapData::from_width_height(width, height)),
                        _ => panic!("Unknown collision tile index")
                    };

                    new_tile.set_collision(collision);

                    self.queue_action(Box::new(TilesetAction::SetTile(self.current_tile, new_tile)));
                }

                let (tile_width, tile_height) = tileset.tile_size();

                match tile.collision()
                {
                    TileCollision::None => {},
                    TileCollision::BoundingBox(bounding_box) =>
                    {
                        let mut new_box = bounding_box.clone();
                        let mut modified = false;
                        let mut x_bounds = [new_box.x1, new_box.x2];
                        let mut y_bounds = [new_box.y1, new_box.y2];

                        modified |= imgui::Drag::new("X Bounds")
                            .range(0.0, tile_width).display_format("%.4f").build_array(ui, &mut x_bounds);
                        modified |= imgui::Drag::new("Y Bounds")
                            .range(0.0, tile_height).display_format("%.4f").build_array(ui, &mut y_bounds);

                        ui.spacing();
                        ui.spacing();
                        ui.text("Solidity");

                        ui.spacing();

                        modified |= ui.checkbox("Top###solid_top", &mut new_box.solid_top);
                        ui.same_line();
                        modified |= ui.checkbox("Bottom###solid_bottom", &mut new_box.solid_bottom);
                        ui.same_line();
                        modified |= ui.checkbox("Left###solid_left", &mut new_box.solid_left);
                        ui.same_line();
                        modified |= ui.checkbox("Right###solid_right", &mut new_box.solid_right);

                        if modified
                        {
                            let mut new_tile = tile.clone();
                            new_box.x1 = x_bounds[0];
                            new_box.x2 = x_bounds[1];
                            new_box.y1 = y_bounds[0];
                            new_box.y2 = y_bounds[1];
                            new_tile.set_collision(TileCollision::BoundingBox(new_box));
                            self.queue_action(Box::new(TilesetAction::SetTile(self.current_tile, new_tile)));
                        }
                    }
                    TileCollision::Heightmap(heightmap) =>
                    {
                        self.heightmap_controls(ui, tile, heightmap.clone(), tile_width, tile_height);
                    }
                }
            }

            token.end();
        }
    }

    fn heightmap_side_list(&mut self, ui : &imgui::Ui, side : &mut Vec<f64>, side_name : &str, max_height : f64, default_height : f64) -> bool
    {
        let mut modified = false;

        ui.indent();
        ui.same_line_with_pos(60.0);

        let item_width = ui.push_item_width(120.0);
        let mut num_points = side.len() as i32;

        if imgui::InputInt::new(ui, format!("Points###{} Points", side_name), &mut num_points).build()
        {
            side.resize((num_points as usize).clamp(1, MAX_HEIGHTMAP_POINTS), side.last().cloned().unwrap_or(default_height));
            modified = true;
        }

        let style_point_spacing = ui.push_style_var(imgui::StyleVar::ItemSpacing([0.0, 0.0]));
        let style_point_padding = ui.push_style_var(imgui::StyleVar::FramePadding([3.0, 3.0]));

        for i in 0..side.len()
        {
            modified |= imgui::Drag::new(format!("[{}]###{} {}", i, side_name, i))
                .range(0.0, max_height).display_format("%.4f").build(ui, &mut side[i]);
        }

        style_point_padding.pop();
        style_point_spacing.pop();

        item_width.pop(ui);

        ui.unindent();
        ui.spacing();

        modified
    }

    fn heightmap_controls(&mut self, ui : &imgui::Ui, tile : &TilesetTile, mut heightmap : TileHeightmapData, tile_width : f64, tile_height : f64)
    {
        let mut modified = false;

        ui.align_text_to_frame_padding();
        ui.text("Top");
        modified |= self.heightmap_side_list(ui, &mut heightmap.top, "Top", tile_height, 0.0);

        ui.align_text_to_frame_padding();
        ui.text("Bottom");
        modified |= self.heightmap_side_list(ui, &mut heightmap.bottom, "Bottom", tile_height, tile_height);

        ui.align_text_to_frame_padding();
        ui.text("Left");
        modified |= self.heightmap_side_list(ui, &mut heightmap.left, "Left", tile_width, 0.0);

        ui.align_text_to_frame_padding();
        ui.text("Right");
        modified |= self.heightmap_side_list(ui, &mut heightmap.right, "Right", tile_width, tile_width);

        ui.spacing();
        ui.spacing();
        ui.text("Solidity");

        ui.spacing();

        modified |= ui.checkbox("Top###solid_top", &mut heightmap.solid_top);
        ui.same_line();
        modified |= ui.checkbox("Bottom###solid_bottom", &mut heightmap.solid_bottom);
        ui.same_line();
        modified |= ui.checkbox("Left###solid_left", &mut heightmap.solid_left);
        ui.same_line();
        modified |= ui.checkbox("Right###solid_right", &mut heightmap.solid_right);

        if modified
        {
            let mut new_tile = tile.clone();
            new_tile.set_collision(TileCollision::Heightmap(heightmap));
            self.queue_action(Box::new(TilesetAction::SetTile(self.current_tile, new_tile)));
        }
    }

    fn flags_tab(&mut self, ui : &imgui::Ui, tileset : &mut Tileset)
    {
        ui.text(format!("Editing Tile {}", self.current_tile));

        if self.extra_selection.len() > 0
        {
            ui.same_line();

            if ui.small_button(format!("Copy To {} Others###flag_copy", self.extra_selection.len()))
            {
                if let Some(tile) = tileset.tile(self.current_tile)
                {
                    let flags = tile.flags();

                    for i in &self.extra_selection.clone()
                    {
                        if let Some(mut other_tile) = tileset.tile(*i).cloned()
                        {
                            other_tile.set_flags(flags);

                            self.queue_action(Box::new(TilesetAction::SetTile(*i, other_tile)));
                        }
                    }
                }
            }
        }

        ui.spacing();

        imgui::ChildWindow::new("tileset_flag_controls").size([0.0, 240.0]).draw_background(false).border(false).build(ui, ||
        {
            for i in 0..32
            {
                if let Some(tile) = tileset.tile(self.current_tile)
                {
                    let mut flag_name = imgui::ImString::with_capacity(4096);

                    if let Some(name) = tileset.flag_name(i)
                    {
                        flag_name.push_str(name);
                    }

                    if flag_name.is_empty()
                    {
                        flag_name.push_str(&format!("<flag {}>", i));
                    }

                    let mut flag_value = tile.flag(i);

                    if ui.checkbox(&flag_name, &mut flag_value)
                    {
                        let mut new_tile = tile.clone();

                        new_tile.set_flag(i, flag_value);

                        self.queue_action(Box::new(TilesetAction::SetTile(self.current_tile, new_tile)));
                    }

                    ui.same_line_with_pos(ui.window_content_region_max()[0] - 70.0);

                    ui.checkbox_flags(format!("Filter###filter_flag_{}", i), &mut self.filter_flags, 1 << i);
                }
            }
        });
    }

    fn flag_names_tab(&mut self, ui : &imgui::Ui, tileset : &mut Tileset)
    {
        for i in 0..32
        {
            if let Some(name) = tileset.flag_name(i)
            {
                let mut flag_name = name.clone();

                if ui.input_text(format!("Flag {}", i), &mut flag_name).enter_returns_true(true).build()
                {
                    self.queue_action(Box::new(TilesetAction::RenameFlag(i, flag_name.to_string())));
                }
            }
        }
    }

    fn tile_view(&mut self, ui : &imgui::Ui, tileset : &mut Tileset, texture_id : usize, sheet_opt : Option<&Sheet>, collision_open : bool, flags_open : bool)
    {
        let main_style = ui.clone_style();

        if ui.button("Add")
        {
            let new_tile;

            if let Some(tile) = tileset.tile(self.current_tile)
            {
                new_tile = tile.clone();
            }
            else
            {
                new_tile = TilesetTile::new(None);
            }

            self.queue_action(Box::new(TilesetAction::AddTile(self.current_tile, new_tile)));
        }
        ui.same_line_with_spacing(0.0, 4.0);
        if ui.button("Up") && self.current_tile > 0
        {
            self.queue_action(Box::new(TilesetAction::MoveTile(self.current_tile, self.current_tile - 1)));
        }
        ui.same_line_with_spacing(0.0, 4.0);
        if ui.button("Down")
        {
            self.queue_action(Box::new(TilesetAction::MoveTile(self.current_tile, self.current_tile + 1)));
        }
        ui.same_line_with_spacing(0.0, 4.0);
        if ui.button("Remove")
        {
            self.queue_action(Box::new(TilesetAction::RemoveTile(self.current_tile)));
        }

        let style_slice_list = ui.push_style_var(imgui::StyleVar::WindowPadding([1.0, 1.0]));
        let colors_slice_list = ui.push_style_color(imgui::StyleColor::ChildBg, [self.transparent_color[0], self.transparent_color[1], self.transparent_color[2], 1.0]);

        imgui::ChildWindow::new("tile_list").always_vertical_scrollbar(true).border(true).draw_background(true).build(ui, ||
        {
            let style_slice = ui.push_style_var(imgui::StyleVar::ItemSpacing([0.0, 0.0]));
            let num_tiles = tileset.tile_count();

            let start_pos = ui.cursor_screen_pos();

            let (orig_tile_width, orig_tile_height) = tileset.tile_size();
            let mut tile_width = orig_tile_width as f32 * self.zoom_level;
            let mut tile_height = orig_tile_height as f32 * self.zoom_level;
            let base_width = tile_width.max(1.0);

            tile_width = tile_width.max(8.0).min(128.0);
            tile_height = tile_height.max(1.0);

            if tile_width != base_width
            {
                tile_height *= tile_width / base_width;
                tile_height = tile_height.max(1.0);
            }

            let button_size = [tile_width, tile_height];
            let layout_width = ui.column_width(0) - 2.0;
            let columns = (layout_width / tile_width).floor().max(1.0) as usize;

            for i in 0..num_tiles
            {
                if i % columns > 0
                {
                    ui.same_line();
                }

                let cursor_pos = ui.cursor_pos();
                crate::util::drawable::tileset_image_from_slice(tileset, texture_id, sheet_opt, i, button_size).build(ui);

                ui.set_cursor_pos([cursor_pos[0], cursor_pos[1]]);

                if imgui::Selectable::new(format!("###tile_{}", i))
                    .selected(self.current_tile == i || self.extra_selection.contains(&i)).size(button_size).build(ui)
                {
                    if ui.io().key_ctrl
                    {
                        if self.current_tile != i
                        {
                            if self.extra_selection.contains(&i)
                            {
                                self.extra_selection.remove(&i);
                            }
                            else
                            {
                                self.extra_selection.insert(i);
                            }
                        }
                    }
                    else
                    {
                        self.extra_selection.clear();
                        self.current_tile = i;
                    }
                }

                if self.current_tile == i && self.scroll_tile_list
                {
                    ui.set_scroll_here_y();
                    self.scroll_tile_list = false;
                }

                if ui.is_item_hovered()
                {
                    ui.tooltip_text(format!("Tile {}", i));
                }
            }

            let draw_list = ui.get_window_draw_list();

            if collision_open && self.show_colliders_in_list
            {
                for i in 0..num_tiles
                {
                    let x = start_pos[0] + (i % columns) as f32 * tile_width;
                    let y = start_pos[1] + (i / columns) as f32 * tile_height;
                    let zoom = tile_width / orig_tile_width as f32;

                    if let Some(tile) = tileset.tile(i)
                    {
                        self.draw_collision_shape([x, y], button_size, zoom, &draw_list, tile)
                    }
                }
            }
            else if flags_open && self.filter_flags > 0
            {
                for i in 0..num_tiles
                {
                    if let Some(tile) = tileset.tile(i)
                    {
                        if tile.flags() & self.filter_flags != self.filter_flags
                        {
                            let x = start_pos[0] + (i % columns) as f32 * tile_width;
                            let y = start_pos[1] + (i / columns) as f32 * tile_height;
                            draw_list.add_rect([x, y], [x + button_size[0], y + button_size[1]],
                                               imgui::ImColor32::from([0.0, 0.0, 0.0, 0.5])).filled(true).build();
                        }
                    }
                }
            }

            let x1 = start_pos[0] + (self.current_tile % columns) as f32 * tile_width;
            let y1 = start_pos[1] + (self.current_tile / columns) as f32 * tile_height;
            let x2 = x1 + tile_width;
            let y2 = y1 + tile_height;

            draw_list.add_rect([x1, y1], [x2, y2], main_style.colors[imgui::StyleColor::Button as usize]).thickness(2.0).build();

            style_slice.pop();
        });

        colors_slice_list.pop();
        style_slice_list.pop();
    }
}

impl Document for TilesetDocument
{
    fn load(game : &mut GameControl, package_name : &str, item_name : &str) -> Self where Self : Sized
    {
        let mut data_id = DataId::new(0);

        if let Ok(id) = game.res().store_mut::<Tileset>().get_id_mut(package_name, item_name)
        {
            data_id = id;
        }

        TilesetDocument
        {
            action_history : ActionHistory::new(),
            data_id,
            current_tile : 0,
            extra_selection : HashSet::new(),
            left_column_set : false,
            transparent_color : [0.5, 0.5001, 0.5002],
            zoom_level : 1.0,
            show_colliders_in_list : true,
            scroll_tile_list : false,
            filter_flags : 0
        }
    }

    fn action_history(&mut self) -> &mut ActionHistory
    {
        &mut self.action_history
    }

    fn apply_action(&mut self, game : &mut GameControl, action : &Box<dyn Any>) -> bool
    {
        if let Some(action) = action.downcast_ref::<TilesetAction>()
        {
            let mut tileset_store = game.res().store_mut::<Tileset>();

            if let Some(tileset) = tileset_store.get_mut(self.data_id)
            {
                match action
                {
                    TilesetAction::SetSheet(package, name) =>
                    {
                        tileset.set_sheet(DataHandle::with_path(&package, &name));
                        return true;
                    },
                    TilesetAction::SetTileSize(width, height) =>
                    {
                        self.scroll_tile_list = true;
                        tileset.set_tile_size(*width, *height);
                        return true;
                    },
                    TilesetAction::AddTile(index, tile) =>
                    {
                        self.scroll_tile_list = true;
                        return self.add_tile(*index, tileset, tile.clone());
                    },
                    TilesetAction::AddTiles(index, tiles) =>
                    {
                        self.scroll_tile_list = true;
                        return self.add_tiles(*index, tileset, tiles);
                    }
                    TilesetAction::SetTile(index, tile) =>
                    {
                        self.scroll_tile_list = true;
                        return tileset.set_tile(*index, tile.clone());
                    },
                    TilesetAction::MoveTile(old_index, new_index) =>
                    {
                        self.scroll_tile_list = true;
                        return self.move_tile(tileset, *old_index, *new_index);
                    },
                    TilesetAction::RemoveTile(index) =>
                    {
                        self.scroll_tile_list = true;
                        return tileset.remove_tile(*index).is_some();
                    },
                    TilesetAction::RenameFlag(flag, name) =>
                    {
                        return tileset.set_flag_name(*flag, name);
                    }
                }
            }
        }

        false
    }

    fn do_ui(&mut self, packages : &mut Packages, _config : &mut EditorConfig,
             _project : &mut ProjectConfig, game : &mut GameControl, ui : &imgui::Ui) -> DocumentState
    {
        let mut result = DocumentState::new();
        let sheet_store = game.res().store_mut::<Sheet>();
        let mut tileset_store = game.res().store_mut::<Tileset>();

        ui.menu_bar(||
        {
            ui.menu("View", ||
            {
                if imgui::MenuItem::new("Show Collision Shapes in Tile List").selected(self.show_colliders_in_list).build(ui)
                {
                    self.show_colliders_in_list = !self.show_colliders_in_list;
                }

                ui.menu("Background Color", ||
                {
                    imgui::ColorPicker::new("##background_color_picker", imgui::EditableColor::Float3(&mut self.transparent_color)).build(ui);
                });
            });
            ui.menu("Zoom", ||
            {
                for level in &[0.25, 0.5, 1.0, 2.0, 3.0, 4.0, 6.0, 8.0, 10.0, 12.0, 16.0]
                {
                    if imgui::MenuItem::new(format!("{}%", (level * 100.0) as i32)).selected(self.zoom_level == *level).build(ui)
                    {
                        self.zoom_level = *level;
                    }
                }
            });
        });

        result.active = ui.is_window_focused();

        if let Some(tileset) = tileset_store.get_mut(self.data_id)
        {
            let mut texture_id = 0;
            let source = tileset.sheet();
            let sheet_opt = match sheet_store.get_id(source.package(), source.path())
            {
                Ok(id) =>
                {
                    texture_id = id.index();
                    sheet_store.get(id)
                }
                Err(_) =>
                {
                    packages.load_package_later(ResourceType::Sheet, source.package());
                    None
                }
            };

            ui.columns(2, "##tileset_columns", true);

            if !self.left_column_set
            {
                ui.set_current_column_width(290.0);
                self.left_column_set = true;
            }

            let mut collision_open = false;
            let mut flags_open = false;
            let mut flag_names_open = false;

            if let Some(tileset_controls) = imgui::ChildWindow::new("tileset_controls").size([0.0, 0.0]).draw_background(true).border(false).horizontal_scrollbar(true).begin(ui)
            {
                self.current_tile = self.current_tile.min(tileset.tile_count().max(1) - 1);

                if let Some(tab_bar) = imgui::TabBar::new("tileset_tab_bar")
                    .flags(imgui::TabBarFlags::FITTING_POLICY_SCROLL).begin(ui)
                {
                    if let Some(tab) = imgui::TabItem::new("Slices").begin(ui)
                    {
                        self.slices_tab(ui, tileset, sheet_opt);

                        tab.end();
                    }

                    if let Some(tab) = imgui::TabItem::new("Collision").begin(ui)
                    {
                        collision_open = true;
                        self.collision_tab(ui, tileset);

                        tab.end();
                    }

                    if let Some(tab) = imgui::TabItem::new("Flags").begin(ui)
                    {
                        flags_open = true;
                        self.flags_tab(ui, tileset);

                        tab.end();
                    }

                    if let Some(tab) = imgui::TabItem::new("Flag Names").begin(ui)
                    {
                        flag_names_open = true;

                        self.flag_names_tab(ui, tileset);

                        tab.end();
                    }

                    tab_bar.end();
                }

                if !flag_names_open
                {
                    ui.spacing();
                    ui.separator();
                    ui.spacing();

                    if let Some(tile_view) = imgui::ChildWindow::new("tile_view").size([0.0, 0.0]).movable(false).begin(ui)
                    {
                        self.tile_view(ui, tileset, texture_id, sheet_opt, collision_open, flags_open);

                        tile_view.end();
                    }
                }

                tileset_controls.end();
            }

            ui.next_column();

            let remaining = ui.content_region_avail();
            let (tile_width, tile_height) = tileset.tile_size();
            let zoom = (remaining[0] / tile_width as f32).min(remaining[1] / tile_height as f32).floor().max(1.0).min(16.0);
            let size = [zoom * tile_width as f32, zoom * tile_height as f32];

            let style_tile_view = ui.push_style_var(imgui::StyleVar::WindowPadding([1.0, 1.0]));
            let colors_tile_view = ui.push_style_color(imgui::StyleColor::ChildBg, [self.transparent_color[0], self.transparent_color[1], self.transparent_color[2], 1.0]);

            imgui::ChildWindow::new("tileset_tile_view").size([size[0] + 2.0, size[1] + 2.0])
                .draw_background(true).border(true).build(ui, ||
            {
                crate::util::widget::round_cursor_screen_pos(ui);

                let pos = ui.cursor_screen_pos();
                let draw_list = ui.get_window_draw_list();

                crate::util::drawable::tileset_image_from_slice(tileset, texture_id, sheet_opt, self.current_tile, size).build(ui);

                if collision_open
                {
                    if let Some(tile) = tileset.tile(self.current_tile)
                    {
                        self.draw_collision_shape(pos, size, zoom, &draw_list, tile);
                    }
                }
            });

            colors_tile_view.pop();
            style_tile_view.pop();
        }

        result
    }
}
