use crate::document::{ActionHistory, Document, DocumentState};
use crate::editorgui::{ARRANGE_PACKAGES_WIDTH, EditorConfig, Packages, ProjectConfig, ResourceType};
use crate::util::zoom::ZoomControl;

use std::any::Any;
use std::collections::{HashMap, HashSet};

use keeshond::crate_reexport::strum::IntoEnumIterator;
use keeshond::crate_reexport::strum_macros::EnumIter;
use keeshond::gameloop::GameControl;
use keeshond::renderer::{DrawBackgroundColorInfo, DrawControl, DrawOptions, DrawOrdering, DrawTransform, Sheet};
use keeshond::scene::DynArg;
use keeshond::util::{BakedRect, OrientedRect, Rect, SimpleTransform};

use keeshond::datapack::{DataHandle, DataId, DataMultistore, DataObject, DataStore};
use keeshond_treats::LoadErrorMode;
use keeshond_treats::visual::{Costume, Drawable, DrawableDrawer, DrawableItem, Lerpable, LerpedTransform, PixelSnap, Sprite};
use keeshond_treats::spawnable::{Representation, Spawnable, SpawnableSet};
use keeshond_treats::tilemap::{Tilemap, TilemapTile, Tileset};
use keeshond_treats::level::{BackgroundInfo, Level, LevelLayer, LevelRepresentation, LevelSpawnable};

use imgui::ColorStackToken;

const SCROLL_TARGET_BUTTON_WIDTH : f32 = 100.0;
const SCROLL_TARGET_PADDING_OFFSET : f64 = 128.0;
const TOOLTIP_DELAY : f32 = 0.5;
const TOOLTIP_FULL_DELAY : f32 = 1.5;
const TOOLTIP_DELAY_MAX : f32 = 2.0;
const BAD_DRAWABLE_SELECTABLE_WIDTH : f64 = 32.0;
const DRAG_START_DISTANCE : f64 = 8.0;
const DRAG_START_DISTANCE_MIN_ZOOM : f64 = 4.0;
const SPAWNABLE_CROSSHAIR_SIZE : f32 = 10.0;

const ZOOM_LEVELS : [f64 ; 11] = [0.25, 0.333, 0.5, 0.75, 1.0, 1.5, 2.0, 3.0, 4.0, 6.0, 8.0];

#[derive(Clone, Serialize, Deserialize)]
pub struct LevelEditorConfig
{
    #[serde(default)]
    pub snap_to_grid : bool,
    #[serde(default)]
    pub grid_w : f64,
    #[serde(default)]
    pub grid_h : f64,
    #[serde(default)]
    pub grid_offset_x : f64,
    #[serde(default)]
    pub grid_offset_y : f64,
    #[serde(default)]
    pub grid_offset_x_actor : f64,
    #[serde(default)]
    pub grid_offset_y_actor : f64,
    #[serde(default)]
    pub select_opaque_tilemap_only : bool
}

impl Default for LevelEditorConfig
{
    fn default() -> Self
    {
        LevelEditorConfig
        {
            snap_to_grid : false,
            grid_w : 16.0,
            grid_h : 16.0,
            grid_offset_x : 0.0,
            grid_offset_y : 0.0,
            grid_offset_x_actor : 0.0,
            grid_offset_y_actor : 0.0,
            select_opaque_tilemap_only : false
        }
    }
}

#[derive(Eq, PartialEq)]
enum ScrollTarget
{
    None,
    TopLeft,
    Top,
    TopRight,
    Left,
    Center,
    Right,
    BottomLeft,
    Bottom,
    BottomRight
}

struct MouseMovedSpawnable
{
    id : LevelSpawnableId,
    x : f64,
    y : f64
}

struct ChangedTile
{
    tile : TilemapTile,
    x : usize,
    y : usize
}

enum LevelAction
{
    AddSpawnable
    {
        position : LevelSpawnableId,
        spawnable : LevelSpawnable,
        spawnable_name : String
    },
    RemoveSpawnables(Vec<LevelSpawnableId>),
    RenameSpawnable
    {
        id : LevelSpawnableId,
        spawnable_name : String
    },
    MoveSpawnable
    {
        id : LevelSpawnableId,
        transform : SimpleTransform
    },
    MouseMoveSpawnables(Vec<MouseMovedSpawnable>),
    SetSpawnableRepresentation
    {
        id : LevelSpawnableId,
        representation : LevelRepresentation
    },
    AddNumberedArg
    {
        spawnable_id : LevelSpawnableId,
        arg_pos : usize,
        arg : DynArg
    },
    SetNumberedArg
    {
        spawnable_id : LevelSpawnableId,
        arg_pos : usize,
        arg : DynArg
    },
    RemoveNumberedArg
    {
        spawnable_id : LevelSpawnableId,
        arg_pos : usize
    },
    MoveNumberedArg
    {
        spawnable_id : LevelSpawnableId,
        arg_pos_old : usize,
        arg_pos_new : usize
    },
    AddNamedArg
    {
        spawnable_id : LevelSpawnableId,
        arg_name : String,
        arg : DynArg
    },
    RemoveNamedArg
    {
        spawnable_id : LevelSpawnableId,
        arg_name : String
    },
    SetTileset
    {
        spawnable_id : LevelSpawnableId,
        tileset : DataHandle<Tileset>
    },
    SetTilemapTiles
    {
        spawnable_id : LevelSpawnableId,
        changed_tiles : Vec<ChangedTile>
    },
    ResizeTilemap
    {
        spawnable_id : LevelSpawnableId,
        new_w : usize,
        new_h : usize,
        nudge_x : i32,
        nudge_y : i32,
        position_x : i32,
        position_y : i32
    },
    AddLayer
    {
        pos : usize,
        layer : LevelLayer
    },
    RemoveLayer(usize),
    RenameLayer
    {
        id : usize,
        name : String
    },
    MoveLayer
    {
        old_pos : usize,
        new_pos : usize
    },
    SetLayerVisible
    {
        id : usize,
        visible : bool
    },
    SetLayerLocked
    {
        id : usize,
        locked : bool
    },
    SetLayerParameters
    {
        id : usize,
        z_offset : f64,
        scroll_rate : Option<(f32, f32)>
    },
    SetSpawnableSet(DataHandle<SpawnableSet>),
    SetBackground(Option<BackgroundInfo>),
    SetBounds(BakedRect<f64>)
}

#[derive(EnumIter, Eq, PartialEq, Copy, Clone, Debug)]
#[strum(crate = "keeshond::crate_reexport::strum")]
enum ToolMode
{
    Select,
    Spawnable,
    Tile,
    Layer,
    Level
}

impl ToolMode
{
    fn tool_icon(&self) -> &'static str
    {
        match self
        {
            ToolMode::Select => { crate::util::widget::ICON_CURSOR }
            ToolMode::Spawnable => { crate::util::widget::ICON_DRAW }
            ToolMode::Tile => { crate::util::widget::ICON_TILES }
            ToolMode::Layer => { crate::util::widget::ICON_LAYER }
            ToolMode::Level => { crate::util::widget::ICON_LEVEL }
        }
    }

    fn tool_name(&self) -> &'static str
    {
        match self
        {
            ToolMode::Select => { "Select Tool" }
            ToolMode::Spawnable => { "Spawnable Tool" }
            ToolMode::Tile => { "Tile Tool" }
            ToolMode::Layer => { "Layer Tool" }
            ToolMode::Level => { "Level Properties" }
        }
    }

    fn tool_description(&self) -> &'static str
    {
        match self
        {
            ToolMode::Select => { "Select, move, and modify Spawnables within the Level." }
            ToolMode::Spawnable => { "Place new Spawnables in the level." }
            ToolMode::Tile => { "Edit the tiles in the currently selected Tilemap." }
            ToolMode::Layer => { "Manage layers, and hide or lock specific layers for easy editing." }
            ToolMode::Level => { "View and change properties like the level size and SpawnableSet to use." }
        }
    }

    fn keyboard_shortcut(&self) -> Option<sdl2::keyboard::Keycode>
    {
        match self
        {
            ToolMode::Select => { Some(sdl2::keyboard::Keycode::S) }
            ToolMode::Spawnable => { Some(sdl2::keyboard::Keycode::P) }
            ToolMode::Tile => { Some(sdl2::keyboard::Keycode::T) }
            ToolMode::Layer => { Some(sdl2::keyboard::Keycode::L) }
            ToolMode::Level => { None }
        }
    }

    fn auto_layout(&self) -> bool
    {
        match self
        {
            ToolMode::Layer => { false }
            _ => { true }
        }
    }

    fn quick_select(&self) -> bool
    {
        match self
        {
            ToolMode::Select | ToolMode::Spawnable | ToolMode::Tile => { true }
            _ => { false }
        }
    }
}

#[derive(EnumIter, Eq, PartialEq, Copy, Clone, Debug)]
#[strum(crate = "keeshond::crate_reexport::strum")]
enum SelectionDisplayMode
{
    Full,
    Outline,
    None
}

#[derive(Copy, Clone, Default, Debug, Eq, PartialEq, Ord, PartialOrd, Hash)]
struct LevelSpawnableId
{
    layer : usize,
    spawnable : usize
}

#[derive(Debug)]
struct Selectable
{
    bounds : BakedRect<f64>,
    fill : bool,
    locked : bool
}

impl Selectable
{
    fn line_thickness(&self) -> f32
    {
        if self.fill { return 1.0; }

        2.0
    }
}

#[derive(Debug)]
struct Moving
{
    spawnable_id : LevelSpawnableId,
    offset_x : f64,
    offset_y : f64,
    is_actor : bool
}

#[derive(Debug)]
struct ScrollingDrawable
{
    drawable_id : usize,
    spawnable_id : LevelSpawnableId
}

fn repr_has_properties(representation : &LevelRepresentation) -> bool
{
    match representation
    {
        LevelRepresentation::Actor { .. } => { true }
        LevelRepresentation::Tilemap { .. } => { true }
        _ => { false }
    }
}

fn bad_drawable(spawnable : &LevelSpawnable, layer : &LevelLayer) -> Drawable
{
    Drawable
    {
        item: DrawableItem::Sprite(Sprite
        {
            sheet_id : DataId::new(0),
            slice_id : 0,
            shader_id : DataId::new(0),
            offset_x : 0.0,
            offset_y : 0.0,
            r : 1.0,
            g : 1.0,
            b : 1.0,
            alpha : 1.0
        }),
        transform : LerpedTransform
        {
            x : Lerpable::new(spawnable.transform.x as f32),
            y : Lerpable::new(spawnable.transform.y as f32),
            angle : Lerpable::new(spawnable.transform.angle as f32),
            scale_x : Lerpable::new(spawnable.transform.scale_x as f32),
            scale_y : Lerpable::new(spawnable.transform.scale_y as f32)
        },
        scroll_rate_x : 1.0,
        scroll_rate_y : 1.0,
        depth : spawnable.transform.z as f32 + layer.z_offset() as f32,
        visible : true,
        lerp_enabled : false
    }
}

struct InputData
{
    was_focused : bool,
    dragging_left : bool,
    dragging_right : bool,
    mouse_x : f64,
    mouse_y : f64,
    drag_left_origin_x : f64,
    drag_left_origin_y : f64,
    drag_right_origin_x : f64,
    drag_right_origin_y : f64,
    box_select_active : bool,
    down_object : bool,
    moving_objects : Vec<Moving>,
    panning_view : bool,
}

impl InputData
{
    fn new() -> InputData
    {
        InputData
        {
            was_focused : false,
            dragging_left : false,
            dragging_right : false,
            mouse_x : 0.0,
            mouse_y : 0.0,
            drag_left_origin_x : 0.0,
            drag_left_origin_y : 0.0,
            drag_right_origin_x : 0.0,
            drag_right_origin_y : 0.0,
            box_select_active : false,
            down_object : false,
            moving_objects : Vec::new(),
            panning_view : false
        }
    }
}

struct SelectToolData
{
    selectables : Vec<Vec<Selectable>>,
    selected : Vec<LevelSpawnableId>,
    selected_set : HashSet<LevelSpawnableId>,
    display : SelectionDisplayMode,
    hovered : Option<LevelSpawnableId>,
    cached_arg_id : LevelSpawnableId,
    cached_args_numbered : Vec<DynArg>,
    cached_args_named : Vec<(String, DynArg)>,
    selected_arg_numbered : usize,
    selected_arg_named : String,
    new_arg_name : String,
    keep_selection : bool
}

impl SelectToolData
{
    fn new() -> SelectToolData
    {
        SelectToolData
        {
            selectables : Vec::new(),
            selected : Vec::new(),
            selected_set : HashSet::new(),
            display : SelectionDisplayMode::Full,
            hovered : None,
            cached_arg_id : LevelSpawnableId::default(),
            cached_args_numbered : Vec::new(),
            cached_args_named : Vec::new(),
            new_arg_name : String::new(),
            selected_arg_numbered : 0,
            selected_arg_named : String::new(),
            keep_selection : false
        }
    }
}

struct HoveredTileBounds
{
    rect : OrientedRect<f64>,
    offset_x : f64,
    offset_y : f64
}

struct TilesToolData
{
    current : u32,
    scroll_list : bool,
    hovered_tile_bounds : Option<HoveredTileBounds>,
    tile_ghost_drawable : Drawable,
    new_tilemap_size : [u32; 2],
    new_tilemap_nudge : [i32; 2],
    new_tilemap_position : [i32; 2]
}

impl TilesToolData
{
    fn new() -> TilesToolData
    {
        TilesToolData
        {
            current : 0,
            scroll_list : false,
            hovered_tile_bounds : None,
            tile_ghost_drawable : Drawable::new(),
            new_tilemap_size : [0, 0],
            new_tilemap_nudge : [0, 0],
            new_tilemap_position : [0, 0]
        }
    }
}

pub struct LevelDocument
{
    action_history : ActionHistory,
    data_id : DataId<Level>,
    level_gen : u64,
    tics : u64,
    scroll_x : f64,
    scroll_y : f64,
    zoom_control : ZoomControl<f64>,
    cached_zoom : f64,
    applied_zoom : f64,
    dpi_factor : f32,
    input : InputData,
    bg_r : Lerpable<f32>,
    bg_g : Lerpable<f32>,
    bg_b : Lerpable<f32>,
    bg_tween : f32,
    initial_bg : bool,
    tool_mode : ToolMode,
    tool_button_hovered : bool,
    tool_button_hover : f32,
    spawnable_to_place : usize,
    spawnables_gen : u64,
    drawables : Vec<Drawable>,
    scrolling_drawables : Vec<ScrollingDrawable>,
    tool_drawables : Vec<Drawable>,
    cached_data_dirty : bool,
    old_viewport : Rect<f64>,
    drawer : DrawableDrawer,
    current_layer : usize,
    scroll_layer_list : bool,
    select : SelectToolData,
    tiles : TilesToolData
}

impl LevelDocument
{
    fn mark_dirty(&mut self)
    {
        self.cached_data_dirty = true;
        self.select.cached_arg_id = LevelSpawnableId
        {
            layer : usize::MAX,
            spawnable : usize::MAX
        };
        self.select.cached_args_numbered.clear();
        self.select.cached_args_named.clear();
    }

    fn check_generation_dirty(&mut self, level : &mut Level, game : &GameControl)
    {
        let level_gen = level.generation();

        if self.level_gen != level_gen
        {
            self.level_gen = level_gen;
            self.mark_dirty();
        }
        else
        {
            let spawnables_store = game.res().store_mut::<SpawnableSet>();

            if let Some(spawnables) = spawnables_store.get(level.spawnable_set().id())
            {
                let spawnables_gen = spawnables.generation();

                if self.spawnables_gen != spawnables_gen
                {
                    self.spawnables_gen = spawnables_gen;
                    self.mark_dirty();
                }
            }
        }
    }

    fn add_selection(&mut self, id : LevelSpawnableId)
    {
        if !self.select.selected_set.contains(&id)
        {
            self.select.selected.push(id);
            self.select.selected_set.insert(id);
        }
    }

    fn subtract_selection(&mut self, id : LevelSpawnableId)
    {
        if self.select.selected_set.contains(&id)
        {
            for i in 0..self.select.selected.len()
            {
                if self.select.selected[i] == id
                {
                    self.select.selected.remove(i);
                    break;
                }
            }
            self.select.selected_set.remove(&id);
        }
    }

    fn select_all(&mut self, include_locked : bool)
    {
        self.clear_selection();

        for i in 0..self.select.selectables.len()
        {
            for j in 0..self.select.selectables[i].len()
            {
                if include_locked || !self.select.selectables[i][j].locked
                {
                    self.add_selection(LevelSpawnableId
                    {
                        layer : i,
                        spawnable : j
                    });
                }
            }
        }
    }

    fn clear_selection(&mut self)
    {
        self.select.selected.clear();
        self.select.selected_set.clear();
    }

    fn snap_to_grid(&self, x : f64, y : f64, actor : bool, project : &ProjectConfig) -> (f64, f64)
    {
        if !project.level_editor.snap_to_grid
        {
            return (x, y);
        }

        let mut new_x = x;
        let mut new_y = y;
        let mut offset_x = project.level_editor.grid_offset_x;
        let mut offset_y = project.level_editor.grid_offset_y;

        if actor
        {
            offset_x += project.level_editor.grid_offset_x_actor;
            offset_y += project.level_editor.grid_offset_y_actor;
        }

        new_x = ((new_x - offset_x) / project.level_editor.grid_w + 0.5).floor() * project.level_editor.grid_w + offset_x;
        new_y = ((new_y - offset_y) / project.level_editor.grid_h + 0.5).floor() * project.level_editor.grid_h + offset_y;

        (new_x, new_y)
    }

    fn scroll_to(&mut self, target : ScrollTarget, level : &Level, window_w : f64, window_h : f64)
    {
        let mut bounds = level.bounds().clone();
        bounds.scale(self.applied_zoom, self.applied_zoom);

        let x;
        let y;
        let padding = SCROLL_TARGET_PADDING_OFFSET.min(bounds.w().min(bounds.h()));
        let left_x = bounds.x1 + padding;
        let right_x = bounds.x2 - padding;
        let top_y = bounds.y1 + padding;
        let bottom_y = bounds.y2 - padding;
        let center_x = (bounds.x2 + bounds.x1) / 2.0;
        let center_y = (bounds.y2 + bounds.y1) / 2.0;

        match target
        {
            ScrollTarget::None => { return; }
            ScrollTarget::TopLeft => { x = left_x; y = top_y; }
            ScrollTarget::Top => { x = center_x; y = top_y; }
            ScrollTarget::TopRight => { x = right_x; y = top_y; }
            ScrollTarget::Left => { x = left_x; y = center_y; }
            ScrollTarget::Center => { x = center_x; y = center_y; }
            ScrollTarget::Right => { x = right_x; y = center_y; }
            ScrollTarget::BottomLeft => { x = left_x; y = bottom_y; }
            ScrollTarget::Bottom => { x = center_x; y = bottom_y; }
            ScrollTarget::BottomRight => { x = right_x; y = bottom_y; }
        }

        self.scroll_x = (x - (window_w / 2.0)).round();
        self.scroll_y = (y - (window_h / 2.0)).round();
    }

    fn draw_bounds(&mut self, ui: &imgui::Ui, level : &mut Level, game : &GameControl)
    {
        let (width, height) = game.renderer().base_size();
        let mut bounds = level.bounds().clone();
        let style = ui.clone_style();
        let mut outside_color = style.colors[imgui::StyleColor::MenuBarBg as usize];
        let mut border_color = style.colors[imgui::StyleColor::Border as usize];
        outside_color[3] = 0.4;
        border_color[3] = 1.0;
        let draw_list = ui.get_background_draw_list();

        bounds.scale(self.applied_zoom, self.applied_zoom);
        bounds.translate(-self.scroll_x, -self.scroll_y);

        draw_list.add_rect([0.0, 0.0], [width, bounds.y1 as f32], outside_color).filled(true).build();
        draw_list.add_rect([0.0, bounds.y2 as f32], [width, height], outside_color).filled(true).build();
        draw_list.add_rect([0.0, bounds.y1 as f32], [bounds.x1 as f32, bounds.y2 as f32], outside_color).filled(true).build();
        draw_list.add_rect([bounds.x2 as f32, bounds.y1 as f32], [width, bounds.y2 as f32], outside_color).filled(true).build();

        draw_list.add_rect([bounds.x1 as f32, bounds.y1 as f32], [bounds.x2 as f32, bounds.y2 as f32], border_color)
            .thickness(1.0).filled(false).build();
    }

    fn animate_bg_color(&mut self, level: &mut Level, delta : f32)
    {
        let mut r = 0.5;
        let mut g = 0.5;
        let mut b = 0.5;

        if let Some(bg) = level.background()
        {
            r = bg.r;
            g = bg.g;
            b = bg.b;
        }

        if r != self.bg_r.value || g != self.bg_g.value || b != self.bg_b.value
        {
            self.bg_r.old = self.bg_r.lerped(self.bg_tween);
            self.bg_g.old = self.bg_g.lerped(self.bg_tween);
            self.bg_b.old = self.bg_b.lerped(self.bg_tween);
            self.bg_r.value = r;
            self.bg_g.value = g;
            self.bg_b.value = b;

            if self.initial_bg
            {
                self.bg_tween = 0.0;
            }
            else
            {
                self.bg_tween = -1.0;
            }
        }

        self.initial_bg = false;
        self.bg_tween = (self.bg_tween + delta).min(0.0);
    }

    fn get_selectable(&self, selectable_id : LevelSpawnableId) -> Option<&Selectable>
    {
        if let Some(layer) = self.select.selectables.get(selectable_id.layer)
        {
            return layer.get(selectable_id.spawnable);
        }

        None
    }

    fn selectable_contains_mouse(&self, selectable_id : LevelSpawnableId) -> bool
    {
        if let Some(layer) = self.select.selectables.get(selectable_id.layer)
        {
            if let Some(selectable) = layer.get(selectable_id.spawnable)
            {
                return selectable.bounds.contains(self.input.mouse_x, self.input.mouse_y);
            }
        }

        false
    }

    fn layer_name(&self, level : &Level, id : usize) -> String
    {
        level.layer_name(id).unwrap_or(String::from("<unnamed>"))
    }

    fn level_context_menu(&mut self, ui : &imgui::Ui, level : &mut Level)
    {
        ui.popup("Level Context Menu", ||
        {
            if ui.io().key_shift
            {
                if imgui::MenuItem::new("Select All Objects (Including Locked)").build(ui)
                {
                    self.select_all(true);
                }
            }
            else
            {
                if imgui::MenuItem::new("Select All Objects").build(ui)
                {
                    self.select_all(false);
                }
            }
            if imgui::MenuItem::new("Clear Selection").build(ui)
            {
                self.clear_selection();
            }

            ui.separator();

            let mut first_button = true;

            for tool in ToolMode::iter()
            {
                if !tool.quick_select()
                {
                    continue;
                }

                if first_button
                {
                    first_button = false;
                }
                else
                {
                    ui.same_line_with_spacing(0.0, 2.0);
                }

                self.tool_selector_button(ui, tool);
            }

            ui.separator();

            crate::util::widget::window_placeholder_text("Layers", ui);
            self.layer_list(ui, level, [200.0, 150.0]);
        });
    }

    fn selection_hit_test(&self, layer_id : usize, spawnable_id : usize, x : f64, y : f64, project : &ProjectConfig,
                          level : &mut Level, tileset_store : &DataStore<Tileset>) -> bool
    {
        if let Some(spawnable) = level.spawnable(layer_id, spawnable_id)
        {
            match &spawnable.representation
            {
                LevelRepresentation::None => {}
                LevelRepresentation::Actor { .. } => {}
                LevelRepresentation::Tilemap { tiles } =>
                {
                    if !project.level_editor.select_opaque_tilemap_only
                    {
                        return true;
                    }

                    if let Some(tileset) = tiles.tileset().get_if_resolved(tileset_store)
                    {
                        if let Some((tile_x, tile_y)) = tiles.grid().world_to_tile_pos_int(x, y, &spawnable.transform,
                                                    tiles.offset_x(), tiles.offset_y(), tileset)
                        {
                            if let Some(tile) = tiles.grid().tile(tile_x, tile_y)
                            {
                                if let Some(tileset_tile) = tileset.tile(tile.index as usize)
                                {
                                    return tileset_tile.slice_handle().is_some();
                                }
                            }
                        }

                        return false;
                    }
                }
                LevelRepresentation::Prop { .. } => {}
            }
        }

        true
    }

    fn selection_box_test(&self, layer_id : usize, spawnable_id : usize, hitbox : &BakedRect<f64>,
                          project : &ProjectConfig, level : &mut Level, tileset_store : &DataStore<Tileset>) -> bool
    {
        if let Some(spawnable) = level.spawnable(layer_id, spawnable_id)
        {
            match &spawnable.representation
            {
                LevelRepresentation::None => {}
                LevelRepresentation::Actor { .. } => {}
                LevelRepresentation::Tilemap { tiles } =>
                {
                    if !project.level_editor.select_opaque_tilemap_only
                    {
                        return true;
                    }

                    if let Some(tileset) = tiles.tileset().get_if_resolved(tileset_store)
                    {
                        if spawnable.transform.angle == 0.0
                        {
                            return self.tilemap_box_test_axis_aligned(hitbox, spawnable, tiles, tileset);
                        }
                        else
                        {
                            return self.tilemap_box_test_rotated(hitbox, spawnable, tiles, tileset);
                        }
                    }
                }
                LevelRepresentation::Prop { .. } => {}
            }
        }

        true
    }

    fn tilemap_box_test_axis_aligned(&self, hitbox : &BakedRect<f64>, spawnable : &LevelSpawnable,
                        tiles : &Tilemap, tileset : &Tileset) -> bool
    {
        let (tile_x1, tile_y1) = tiles.grid().world_to_tile_pos_int_clamped(hitbox.x1, hitbox.y1,
                &spawnable.transform, tiles.offset_x(), tiles.offset_y(), tileset);
        let (tile_x2, tile_y2) = tiles.grid().world_to_tile_pos_int_clamped(hitbox.x2, hitbox.y2,
                &spawnable.transform, tiles.offset_x(), tiles.offset_y(), tileset);

        for tile_y in tile_y1..tile_y2 + 1
        {
            for tile_x in tile_x1..tile_x2 + 1
            {
                if let Some(tile) = tiles.grid().tile(tile_x, tile_y)
                {
                    if let Some(tileset_tile) = tileset.tile(tile.index as usize)
                    {
                        if tileset_tile.slice_handle().is_some()
                        {
                            return true;
                        }
                    }
                }
            }
        }

        false
    }

    fn tilemap_box_test_rotated(&self, hitbox : &BakedRect<f64>, spawnable : &LevelSpawnable,
                                     tiles : &Tilemap, tileset : &Tileset) -> bool
    {
        // TODO: Very rough, upoptimized and approximate but mostly functional for now
        let box_x1 = hitbox.x1.ceil() as u32;
        let box_y1 = hitbox.y1.ceil() as u32;
        let box_x2 = hitbox.x2.floor() as u32;
        let box_y2 = hitbox.y2.floor() as u32;

        for y in (box_y1..box_y2).step_by((tileset.tile_width() / 2.0) as usize)
        {
            for x in (box_x1..box_x2).step_by((tileset.tile_height() / 2.0) as usize)
            {
                if let Some((tile_x, tile_y)) = tiles.grid().world_to_tile_pos_int(x as f64, y as f64,
                        &spawnable.transform, tiles.offset_x(), tiles.offset_y(), tileset)
                {
                    if let Some(tile) = tiles.grid().tile(tile_x, tile_y)
                    {
                        if let Some(tileset_tile) = tileset.tile(tile.index as usize)
                        {
                            if tileset_tile.slice_handle().is_some()
                            {
                                return true;
                            }
                        }
                    }
                }
            }
        }

        false
    }

    fn handle_input(&mut self, project : &ProjectConfig, ui : &imgui::Ui, packages : &mut Packages, level : &mut Level, game : &GameControl) -> bool
    {
        let mut make_focused = false;
        let mut spawnables_store = game.res().store_mut::<SpawnableSet>();
        let avail = ui.content_region_avail();

        ui.invisible_button("level_drag", [avail[0], avail[1]]);

        let hovering_window = ui.is_item_hovered();
        let focused_window = ui.is_window_focused_with_flags(imgui::WindowFocusedFlags::CHILD_WINDOWS);
        let mut delta = [0.0, 0.0];
        let origin = ui.io().mouse_pos;

        self.input.mouse_x = (origin[0] as f64 + self.scroll_x) / self.applied_zoom;
        self.input.mouse_y = (origin[1] as f64 + self.scroll_y) / self.applied_zoom;

        if ui.is_mouse_clicked(imgui::MouseButton::Left) && hovering_window
        {
            self.input.dragging_left = true;

            self.input.drag_left_origin_x = self.input.mouse_x;
            self.input.drag_left_origin_y = self.input.mouse_y;

            make_focused = true;
        }
        if ui.is_mouse_clicked(imgui::MouseButton::Right) && hovering_window
        {
            self.input.panning_view = false;
            self.input.dragging_right = true;

            self.input.drag_right_origin_x = self.input.mouse_x;
            self.input.drag_right_origin_y = self.input.mouse_y;

            make_focused = true;
        }

        if self.input.dragging_right && ui.is_mouse_down(imgui::MouseButton::Right)
        {
            if (self.input.mouse_x - self.input.drag_right_origin_x).abs() >= DRAG_START_DISTANCE / self.applied_zoom ||
                (self.input.mouse_y - self.input.drag_right_origin_y).abs() >= DRAG_START_DISTANCE / self.applied_zoom
            {
                self.input.panning_view = true;
            }

            if self.input.panning_view
            {
                delta = ui.io().mouse_delta;
            }

            make_focused = true;
        }

        if !ui.is_mouse_down(imgui::MouseButton::Left)
        {
            self.input.dragging_left = false;
        }
        if !ui.is_mouse_down(imgui::MouseButton::Right)
        {
            if self.input.dragging_right && !self.input.panning_view
            {
                self.scroll_layer_list = true;
                ui.open_popup("Level Context Menu");
            }

            self.input.dragging_right = false;
        }

        if delta[0] != 0.0 || delta[1] != 0.0
        {
            self.scroll_x -= delta[0] as f64;
            self.scroll_y -= delta[1] as f64;
        }

        let mut tileset_store = game.res().store_mut::<Tileset>();
        let mut sheet_store = game.res().store_mut::<Sheet>();

        match self.tool_mode
        {
            ToolMode::Select =>
            {
                self.input_tool_select(level, &tileset_store, project, ui, hovering_window);
            }
            ToolMode::Spawnable =>
            {
                self.input_tool_spawnable(level, &mut spawnables_store, &tileset_store, project, ui, hovering_window);
            }
            ToolMode::Tile =>
            {
                self.input_tool_tiles(level, &mut tileset_store, &mut sheet_store, project, packages, ui, hovering_window);
            }
            ToolMode::Layer =>
            {
                self.input_tool_select(level, &tileset_store, project, ui, hovering_window);
            }
            ToolMode::Level =>
            {
                self.input_tool_select(level, &tileset_store, project, ui, hovering_window);
            }
        }

        self.input_tool_move(level, project, ui, hovering_window);
        self.level_context_menu(ui, level);

        if !self.tool_button_hovered
        {
            self.tool_button_hover -= ui.io().delta_time * 2.0;
        }

        self.tool_button_hover = self.tool_button_hover.clamp(0.0, TOOLTIP_DELAY_MAX);

        if focused_window
        {
            self.input_keyboard(ui);
        }

        if hovering_window
        {
            if self.zoom_control.update_mouse_wheel(ui.io().mouse_wheel)
            {
                self.apply_zoom(game, self.zoom_control.zoom());
            }
        }

        make_focused
    }

    fn input_keyboard(&mut self, ui : &imgui::Ui)
    {
        if !ui.io().key_ctrl && !ui.io().key_shift && !ui.io().key_alt
        {
            for tool in ToolMode::iter()
            {
                if let Some(keycode) = tool.keyboard_shortcut()
                {
                    if crate::util::keycode_pressed(keycode, false, ui)
                    {
                        self.tool_mode = tool;
                        break;
                    }
                }
            }
        }

        if ui.is_key_pressed_no_repeat(imgui::Key::A) && ui.io().key_ctrl
        {
            self.select_all(ui.io().key_shift);
        }
        if ui.is_key_pressed_no_repeat(imgui::Key::Escape)
        {
            self.clear_selection();
        }
    }

    fn input_tool_move(&mut self, level : &mut Level, project : &ProjectConfig, ui : &imgui::Ui, hovering_window : bool)
    {
        if ui.is_mouse_down(imgui::MouseButton::Left) && self.input.down_object && hovering_window
        {
            let drag_start = (DRAG_START_DISTANCE / self.applied_zoom.max(1.0)).max(DRAG_START_DISTANCE_MIN_ZOOM / self.applied_zoom);

            if ((self.input.mouse_x - self.input.drag_left_origin_x).abs() >= drag_start ||
                (self.input.mouse_y - self.input.drag_left_origin_y).abs() >= drag_start)
                && self.input.moving_objects.is_empty()
            {
                if !ui.io().key_ctrl
                {
                    self.start_moving_selection(level);
                }
                else
                {
                    self.start_copying_selection(level);
                }
            }

            let mut move_action_list = Vec::new();

            for moving in &self.input.moving_objects
            {
                let (x, y) = self.snap_to_grid(self.input.mouse_x + moving.offset_x, self.input.mouse_y + moving.offset_y, moving.is_actor, project);

                move_action_list.push(MouseMovedSpawnable
                {
                    id : moving.spawnable_id,
                    x,
                    y
                });
            }

            if !move_action_list.is_empty()
            {
                self.queue_action(Box::new(LevelAction::MouseMoveSpawnables(move_action_list)));
            }
        }
        else if !ui.is_mouse_down(imgui::MouseButton::Left)
        {
            self.input.down_object = false;
            self.input.moving_objects.clear();
        }
    }

    fn start_moving_selection(&mut self, level: &mut Level)
    {
        for selected in &self.select.selected
        {
            if let Some(layer) = level.layer(selected.layer)
            {
                if let Some(spawnable) = layer.spawnable(selected.spawnable)
                {
                    let is_actor = match spawnable.representation
                    {
                        LevelRepresentation::Actor { .. } => { true }
                        _ => { false }
                    };

                    self.input.moving_objects.push(Moving
                    {
                        spawnable_id: *selected,
                        offset_x : spawnable.transform.x - self.input.drag_left_origin_x,
                        offset_y : spawnable.transform.y - self.input.drag_left_origin_y,
                        is_actor
                    })
                }
            }
        }
    }

    fn start_copying_selection(&mut self, level: &mut Level)
    {
        let old_selection = self.select.selected.clone();
        self.clear_selection();

        // Hack to stop AddSpawnable action from clobbering selection
        self.select.keep_selection = true;

        let mut new_id_on_layer : HashMap<usize, usize> = HashMap::new();

        for selected in &old_selection
        {
            if let Some(layer) = level.layer(selected.layer)
            {
                if let Some(spawnable) = layer.spawnable(selected.spawnable)
                {
                    let is_actor = match spawnable.representation
                    {
                        LevelRepresentation::Actor { .. } => { true }
                        _ => { false }
                    };

                    let entry = new_id_on_layer.entry(selected.layer).or_insert(level.layer_spawnable_count(selected.layer));

                    let target = LevelSpawnableId
                    {
                        layer : selected.layer,
                        spawnable : *entry
                    };

                    *entry += 1;

                    let spawnable_name = level.spawnable_name_for_id(spawnable.spawn_name_id).cloned().unwrap_or(String::new());

                    self.queue_action(Box::new(LevelAction::AddSpawnable
                    {
                        position : target.clone(),
                        spawnable : spawnable.clone(),
                        spawnable_name
                    }));

                    self.input.moving_objects.push(Moving
                    {
                        spawnable_id: target.clone(),
                        offset_x : spawnable.transform.x - self.input.drag_left_origin_x,
                        offset_y : spawnable.transform.y - self.input.drag_left_origin_y,
                        is_actor
                    });

                    self.add_selection(target);
                }
            }
        }
    }

    fn input_tool_select(&mut self, level : &mut Level, tileset_store : &DataStore<Tileset>, project : &ProjectConfig, ui : &imgui::Ui, hovering_window : bool)
    {
        self.select.hovered = None;

        if self.input.moving_objects.is_empty() && hovering_window
        {
            self.select.hovered = self.selectable_under_mouse(level, tileset_store, project);
        }

        if ui.is_mouse_clicked(imgui::MouseButton::Left) && hovering_window
        {
            self.tool_mode = ToolMode::Select;

            if !ui.io().key_shift && !ui.io().key_ctrl
            {
                match self.select.hovered
                {
                    None => { self.clear_selection(); }
                    Some(id) =>
                    {
                        if !self.select.selected_set.contains(&id)
                        {
                            self.clear_selection();
                        }
                    }
                }
            }

            if let Some(id) = self.select.hovered
            {
                if !ui.io().key_ctrl
                {
                    self.add_selection(id);
                }

                self.input.down_object = true;
            }
            else
            {
                self.input.box_select_active = true;
            }
        }

        if !ui.is_mouse_down(imgui::MouseButton::Left)
        {
            if self.input.box_select_active
            {
                let mut bounds = BakedRect
                {
                    x1 : self.input.drag_left_origin_x,
                    y1 : self.input.drag_left_origin_y,
                    x2 : self.input.mouse_x,
                    y2 : self.input.mouse_y
                };

                bounds.make_positive();
                bounds.translate(0.5, 0.5);

                for i in 0..self.select.selectables.len()
                {
                    for j in 0..self.select.selectables[i].len()
                    {
                        if !self.select.selectables[i][j].locked
                            && self.select.selectables[i][j].bounds.overlaps(&bounds)
                        {
                            let mut intersection = bounds.clone();
                            intersection.intersection(&self.select.selectables[i][j].bounds);

                            if !self.selection_box_test(i, j, &intersection, project, level, tileset_store)
                            {
                                continue;
                            }

                            if !ui.io().key_ctrl
                            {
                                self.add_selection(LevelSpawnableId
                                {
                                    layer : i,
                                    spawnable : j
                                });
                            }
                            else
                            {
                                self.subtract_selection(LevelSpawnableId
                                {
                                    layer : i,
                                    spawnable : j
                                });
                            }
                        }
                    }
                }

                self.input.box_select_active = false;
            }
            else if self.input.down_object && self.input.moving_objects.is_empty() && ui.io().key_ctrl
            {
                if let Some(id) = self.select.hovered
                {
                    self.subtract_selection(id);
                }
            }
        }

        if ui.is_key_down(imgui::Key::Delete) && !self.input.down_object
        {
            self.queue_action(Box::new(LevelAction::RemoveSpawnables(self.select.selected.clone())));
        }
    }

    fn selectable_under_mouse(&mut self, level : &mut Level, tileset_store : &DataStore<Tileset>, project : &ProjectConfig) -> Option<LevelSpawnableId>
    {
        let mouse_x = self.input.mouse_x + 0.5;
        let mouse_y = self.input.mouse_y + 0.5;

        for i in (0..self.select.selectables.len()).rev()
        {
            let selectable_layer = &self.select.selectables[i];

            for j in (0..selectable_layer.len()).rev()
            {
                let selectable = &selectable_layer[j];

                if !selectable.locked && selectable.bounds.contains(mouse_x, mouse_y)
                    && self.selection_hit_test(i, j, self.input.mouse_x, self.input.mouse_y, project, level, tileset_store)
                {
                    return Some(LevelSpawnableId
                    {
                        layer : i,
                        spawnable : j
                    });
                }
            }
        }
        
        None
    }

    fn set_named_spawnable_to_place(&mut self, name : &str, level : &Level, spawnables_store : &DataStore<SpawnableSet>) -> Option<usize>
    {
        if let Some(spawnables) = spawnables_store.get(level.spawnable_set().id())
        {
            if let Some(id) = spawnables.spawnable_id(name)
            {
                self.spawnable_to_place = id;
                return Some(id);
            }
        }

        None
    }

    fn auto_layer_for_spawnable(&mut self, id: usize, level: &Level, spawnables_store : &DataStore<SpawnableSet>) -> Option<usize>
    {
        if let Some(spawnables) = spawnables_store.get(level.spawnable_set().id())
        {
            if let Some(spawnable) = spawnables.spawnable(id)
            {
                let layer_name = spawnable.auto_layer_name();

                if !layer_name.is_empty()
                {
                    return level.layer_id(layer_name);
                }
            }
        }

        None
    }

    fn input_tool_spawnable(&mut self, level : &mut Level, spawnables_store : &mut DataStore<SpawnableSet>,
                            tileset_store: &DataStore<Tileset>, project : &ProjectConfig,
                            ui : &imgui::Ui, hovering_window : bool)
    {
        let mut locked = false;

        if let Some(layer) = level.layer(self.current_layer)
        {
            locked = layer.locked()
        }

        if hovering_window && !locked
        {
            if ui.is_mouse_clicked(imgui::MouseButton::Left)
            {
                if let Some(spawnables) = spawnables_store.get(level.spawnable_set().id())
                {
                    if let Some(spawnable_data) = spawnables.spawnable(self.spawnable_to_place)
                    {
                        let spawnable_name = spawnables.spawnable_name(self.spawnable_to_place).unwrap_or(String::new());
                        let representation = self.make_spawnable_repr(spawnable_data.representation().clone());
                        let is_actor = match representation
                        {
                            LevelRepresentation::Actor { .. } => { true }
                            _ => { false }
                        };
                        let (x, y) = self.snap_to_grid(self.input.drag_left_origin_x, self.input.drag_left_origin_y, is_actor, project);

                        let spawnable = LevelSpawnable
                        {
                            spawn_name_id : 0,
                            transform : SimpleTransform::with_xy(x, y),
                            args : None,
                            representation : self.make_spawnable_repr(spawnable_data.representation().clone())
                        };

                        let target = LevelSpawnableId
                        {
                            layer : self.current_layer,
                            spawnable : level.layer_spawnable_count(self.current_layer)
                        };

                        self.queue_action(Box::new(LevelAction::AddSpawnable
                        {
                            position : target.clone(),
                            spawnable,
                            spawnable_name
                        }));
                        self.input.moving_objects.push(Moving
                        {
                            spawnable_id : target,
                            offset_x : 0.0,
                            offset_y : 0.0,
                            is_actor
                        });

                        self.input.down_object = true;
                        self.clear_selection();
                    }
                }
            }
            if ui.is_mouse_clicked(imgui::MouseButton::Middle)
            {
                if let Some(selectable) = self.selectable_under_mouse(level, tileset_store, project)
                {
                    if let Some(spawnable) = level.spawnable(selectable.layer, selectable.spawnable)
                    {
                        if let Some(name) = level.spawnable_name_for_id(spawnable.spawn_name_id)
                        {
                            if let Some(id) = self.set_named_spawnable_to_place(name, level, spawnables_store)
                            {
                                if let Some(auto_layer) = self.auto_layer_for_spawnable(id, level, spawnables_store)
                                {
                                    self.current_layer = auto_layer;
                                }
                                else
                                {
                                    self.current_layer = selectable.layer;
                                }
                            }
                        }
                    }
                }
            }
        }

        if hovering_window
        {
            if !locked
            {
                if self.spawnable_to_place == usize::MAX
                {
                    ui.tooltip_text("Please choose a Spawnable from the Sidebar to place in the level,\n\
                        or middle-click an already-placed Spawnable to place more of that type.");
                }
                else
                {
                    let mut spawnable_name = String::from("<unknown>");
                    let layer_name = self.layer_name(level, self.current_layer);
                    let mut expected_layer_name = String::new();
                    let mut correct_layer = true;

                    if let Some(spawnables) = spawnables_store.get(level.spawnable_set().id())
                    {
                        if let Some(name) = spawnables.spawnable_name(self.spawnable_to_place)
                        {
                            spawnable_name = name;
                        }

                        if let Some(spawnable) = spawnables.spawnable(self.spawnable_to_place)
                        {
                            expected_layer_name = spawnable.auto_layer_name().clone();

                            if !expected_layer_name.is_empty() && layer_name != expected_layer_name
                            {
                                correct_layer = false;
                            }
                        }
                    }

                    ui.tooltip(||
                    {
                        ui.text(format!("Place {}\non Layer {}: {}", spawnable_name, self.current_layer, layer_name));

                        if !correct_layer
                        {
                            ui.text_colored(crate::util::widget::error_text_color(ui), format!("Different layer selected, expected layer {}", expected_layer_name));
                        }
                    });
                }
            }
            else
            {
                ui.tooltip_text(format!("Layer {}: {} is locked!", self.current_layer, self.layer_name(level, self.current_layer)));
            }
        }
    }

    fn input_tool_tiles(&mut self, level : &mut Level, tileset_store : &mut DataStore<Tileset>, sheet_store : &mut DataStore<Sheet>,
                        project : &ProjectConfig, packages : &mut Packages, ui : &imgui::Ui, hovering_window : bool)
    {
        self.tiles.hovered_tile_bounds = None;

        if self.select.selected.len() != 1 || self.select.selectables.is_empty()
            || !self.selectable_contains_mouse(self.select.selected[0])
        {
            if ui.is_mouse_clicked(imgui::MouseButton::Left) && hovering_window
            {
                if let Some(id) = self.selectable_under_mouse(level, tileset_store, project)
                {
                    if let Some(spawnable) = level.spawnable(id.layer, id.spawnable)
                    {
                        if let LevelRepresentation::Tilemap { .. } = &spawnable.representation
                        {
                            self.clear_selection();
                            self.add_selection(id);

                            self.input.dragging_left = false;
                        }
                    }
                }
            }

            return;
        }

        let spawnable_id = self.select.selected[0];

        if hovering_window
        {
            if let Some(spawnable) = level.spawnable(spawnable_id.layer, spawnable_id.spawnable)
            {
                if let Some(layer) = level.layer(spawnable_id.spawnable)
                {
                    if let LevelRepresentation::Tilemap { tiles } = &spawnable.representation
                    {
                        // TODO tileset/tileset store stuff is messy
                        let (tile_w, tile_h) = crate::util::drawable::tile_size(tiles, tileset_store);
                        let (tile_x, tile_y) = tiles.world_to_tile_pos_int_clamped(self.input.mouse_x, self.input.mouse_y,
                                                                                   &spawnable.transform, tileset_store,
                                                                                   LoadErrorMode::Warning).unwrap_or_default();

                        let tile_world_x = (tile_x as f64 + tiles.offset_x()) * tile_w;
                        let tile_world_y = (tile_y as f64 + tiles.offset_y()) * tile_h;

                        self.tiles.hovered_tile_bounds = Some(HoveredTileBounds
                        {
                            rect : OrientedRect
                            {
                                rect : BakedRect
                                {
                                    x1 : tile_world_x,
                                    y1 : tile_world_y,
                                    x2 : tile_world_x + tile_w,
                                    y2 : tile_world_y + tile_h
                                },
                                angle : spawnable.transform.angle,
                                scale_x : spawnable.transform.scale_x,
                                scale_y : spawnable.transform.scale_y
                            },
                            offset_x : spawnable.transform.x,
                            offset_y : spawnable.transform.y
                        });


                        let mut item = crate::util::drawable::tile_drawable_item(tiles.tileset(), self.tiles.current, tileset_store, sheet_store, packages);

                        if item.is_some()
                        {
                            item.modify_tile(|tile|
                            {
                                tile.offset_x = tile_x as f32 + tiles.offset_x() as f32;
                                tile.offset_y = tile_y as f32 + tiles.offset_y() as f32;
                                tile.alpha = 0.5;
                            });
                        }

                        self.tiles.tile_ghost_drawable = Drawable
                        {
                            item,
                            transform : LerpedTransform
                            {
                                x : Lerpable::new(spawnable.transform.x as f32),
                                y : Lerpable::new(spawnable.transform.y as f32),
                                angle : Lerpable::new(spawnable.transform.angle as f32),
                                scale_x : Lerpable::new(spawnable.transform.scale_x as f32),
                                scale_y : Lerpable::new(spawnable.transform.scale_y as f32)
                            },
                            scroll_rate_x : 1.0,
                            scroll_rate_y : 1.0,
                            depth : spawnable.transform.z as f32 + layer.z_offset() as f32,
                            visible : true,
                            lerp_enabled : false
                        };

                        if ui.is_mouse_down(imgui::MouseButton::Left) && self.input.dragging_left
                        {
                            self.queue_action(Box::new(LevelAction::SetTilemapTiles
                            {
                                spawnable_id : self.select.selected[0],
                                changed_tiles : vec![ChangedTile
                                {
                                    tile : TilemapTile
                                    {
                                        index : self.tiles.current,
                                        flags : 0
                                    },
                                    x : tile_x,
                                    y : tile_y
                                }]
                            }));
                        }
                        else if ui.is_mouse_clicked(imgui::MouseButton::Middle)
                        {
                            if let Some(tile) = tiles.grid().tile(tile_x, tile_y)
                            {
                                self.tiles.current = tile.index;
                                self.tiles.scroll_list = true;
                            }
                        }
                    }
                }
            }
        }
    }

    fn make_spawnable_repr(&self, representation : Representation) -> LevelRepresentation
    {
        match representation
        {
            Representation::None => { LevelRepresentation::None }
            Representation::Prop { .. } =>
            {
                LevelRepresentation::Prop
                {
                    custom_sheet : None,
                    custom_slice : None
                }
            }
            Representation::Actor { .. } =>
            {
                LevelRepresentation::Actor
                {
                    custom_costume : None
                }
            }
            Representation::Tilemap =>
            {
                let mut tiles = Tilemap::new(4, 4, DataHandle::with_path("common", "default.json"));

                for y in 0..4
                {
                    for x in 0..4
                    {
                        tiles.grid_mut().set_tile(x, y, TilemapTile
                        {
                            index : (x / 2 + (y / 2) * 2) as u32,
                            flags : 0
                        });
                    }
                }

                LevelRepresentation::Tilemap
                {
                    tiles
                }
            }
        }
    }

    fn apply_zoom(&mut self, game : &GameControl, new_zoom : f64)
    {
        let (width, height) = game.renderer().base_size();
        let dpi_factor = game.renderer().current_display_scale_factor() as f64;
        let half_width = width as f64 / 2.0 / dpi_factor;
        let half_height = height as f64 / 2.0 / dpi_factor;

        self.scroll_x += half_width;
        self.scroll_y += half_height;

        self.scroll_x /= self.cached_zoom;
        self.scroll_y /= self.cached_zoom;
        self.scroll_x *= new_zoom;
        self.scroll_y *= new_zoom;

        self.scroll_x -= half_width;
        self.scroll_y -= half_height;

        self.cached_zoom = new_zoom;
        self.zoom_control.set_zoom(self.cached_zoom);
        self.applied_zoom = self.cached_zoom / dpi_factor;

        self.scroll_x = self.scroll_x.round();
        self.scroll_y = self.scroll_y.round();
    }

    fn tool_selector(&mut self, game : &GameControl, level : &Level, config : &mut ProjectConfig, ui : &imgui::Ui)
    {
        let mut first_button = true;

        self.tool_button_hovered = false;

        for tool in ToolMode::iter()
        {
            if !tool.auto_layout()
            {
                continue;
            }

            if first_button
            {
                first_button = false;
            }
            else
            {
                ui.same_line_with_spacing(0.0, 2.0);
            }

            self.tool_selector_button(ui, tool);
        }

        ui.same_line_with_spacing(0.0, 10.0);

        ui.popup("View Menu", ||
        {
            ui.menu("Zoom", ||
            {
                for level in &ZOOM_LEVELS
                {
                    if imgui::MenuItem::new(format!("{}%", (level * 100.0) as i32)).selected(self.applied_zoom == *level).build(ui)
                    {
                        self.apply_zoom(game, *level);
                    }
                }
            });

            ui.menu("Scroll To", ||
            {
                let mut target = ScrollTarget::None;

                if ui.button_with_size("Top left", [SCROLL_TARGET_BUTTON_WIDTH, 0.0]) { target = ScrollTarget::TopLeft; }
                ui.same_line();
                if ui.button_with_size("Top", [SCROLL_TARGET_BUTTON_WIDTH, 0.0]) { target = ScrollTarget::Top; }
                ui.same_line();
                if ui.button_with_size("Top right", [SCROLL_TARGET_BUTTON_WIDTH, 0.0]) { target = ScrollTarget::TopRight; }

                if ui.button_with_size("Left", [SCROLL_TARGET_BUTTON_WIDTH, 0.0]) { target = ScrollTarget::Left; }
                ui.same_line();
                if ui.button_with_size("Center", [SCROLL_TARGET_BUTTON_WIDTH, 0.0]) { target = ScrollTarget::Center; }
                ui.same_line();
                if ui.button_with_size("Right", [SCROLL_TARGET_BUTTON_WIDTH, 0.0]) { target = ScrollTarget::Right; }

                if ui.button_with_size("Bottom left", [SCROLL_TARGET_BUTTON_WIDTH, 0.0]) { target = ScrollTarget::BottomLeft; }
                ui.same_line();
                if ui.button_with_size("Bottom", [SCROLL_TARGET_BUTTON_WIDTH, 0.0]) { target = ScrollTarget::Bottom; }
                ui.same_line();
                if ui.button_with_size("Bottom right", [SCROLL_TARGET_BUTTON_WIDTH, 0.0]) { target = ScrollTarget::BottomRight; }

                if target != ScrollTarget::None
                {
                    let (window_w, window_h) = game.renderer().base_size();

                    self.scroll_to(target, level, window_w as f64, window_h as f64);
                    ui.close_current_popup();
                }
            });
        });

        ui.popup("Grid Menu", ||
        {
            ui.checkbox("Snap to Grid", &mut config.level_editor.snap_to_grid);

            let mut grid_size = [config.level_editor.grid_w, config.level_editor.grid_h];
            let mut grid_offset = [config.level_editor.grid_offset_x, config.level_editor.grid_offset_y];
            let mut grid_offset_actor = [config.level_editor.grid_offset_x_actor, config.level_editor.grid_offset_y_actor];

            if imgui::Drag::new("Grid Size").display_format("%.2f").build_array(ui, &mut grid_size)
            {
                config.level_editor.grid_w = grid_size[0].clamp(1.0, 65536.0);
                config.level_editor.grid_h = grid_size[1].clamp(1.0, 65536.0);
            };
            if imgui::Drag::new("Grid Offset").display_format("%.2f").build_array(ui, &mut grid_offset)
            {
                config.level_editor.grid_offset_x = grid_offset[0];
                config.level_editor.grid_offset_y = grid_offset[1];
            };
            if imgui::Drag::new("Grid Actor Offset").display_format("%.2f").build_array(ui, &mut grid_offset_actor)
            {
                config.level_editor.grid_offset_x_actor = grid_offset_actor[0];
                config.level_editor.grid_offset_y_actor = grid_offset_actor[1];
            };
        });

        if ui.button("View")
        {
            ui.open_popup("View Menu");
        }

        ui.same_line_with_spacing(0.0, 2.0);

        if ui.button("Grid...")
        {
            ui.open_popup("Grid Menu");
        }

        self.tool_selector_button(ui, ToolMode::Layer);

        ui.same_line();

        if level.layer_count() > 0
        {
            let layer_name = level.layer_name(self.current_layer).unwrap_or(String::from("<unnamed>"));
            ui.text(format!("Layer {}: {}", self.current_layer, layer_name));
        }
        else
        {
            ui.text_colored(crate::util::widget::error_text_color(ui), "No layers defined");
        }
    }

    fn tool_selector_button(&mut self, ui : &imgui::Ui, tool : ToolMode)
    {
        let main_style = ui.clone_style();
        let icon_font = crate::util::widget::get_icon_font(ui);
        let tool_button_w = crate::util::widget::ICON_FONT_SIZE + main_style.frame_padding[0] * 2.0;
        let tool_button_h = crate::util::widget::ICON_FONT_SIZE + main_style.frame_padding[1] * 2.0;
        let mut button_color = main_style.colors[imgui::StyleColor::Button as usize];

        if self.tool_mode != tool
        {
            button_color = main_style.colors[imgui::StyleColor::FrameBg as usize];
        }

        let color_token = ui.push_style_color(imgui::StyleColor::Button, button_color);
        let font_token = ui.push_font(icon_font);

        if ui.button_with_size(tool.tool_icon(), [tool_button_w, tool_button_h])
        {
            self.tool_mode = tool;
            self.tool_button_hover = 0.0;

            ui.close_current_popup();
        }

        font_token.pop();
        color_token.pop();

        if ui.is_item_hovered()
        {
            self.tool_button_hovered = true;
            self.tool_button_hover += ui.io().delta_time;

            if self.tool_button_hover >= TOOLTIP_DELAY
            {
                let tooltip = ui.begin_tooltip();

                if let Some(scancode) = tool.keyboard_shortcut()
                {
                    ui.text(&format!("{} (shortcut: {})", tool.tool_name(), scancode));
                }
                else
                {
                    ui.text(tool.tool_name());
                }

                if self.tool_button_hover >= TOOLTIP_FULL_DELAY
                {
                    ui.text(tool.tool_description());
                }

                tooltip.end();
            }
        }
    }

    fn layer_button_colors<'a>(&mut self, active : bool, ui : &'a imgui::Ui<'a>) -> (ColorStackToken<'a>, ColorStackToken<'a>)
    {
        let main_style = ui.clone_style();
        let mut text_color = main_style.colors[imgui::StyleColor::Text as usize];
        let mut button_color = main_style.colors[imgui::StyleColor::Button as usize];

        button_color[3] = 0.0;

        if !active
        {
            text_color[3] *= 0.3;
        }

        (ui.push_style_color(imgui::StyleColor::Button, button_color),
            ui.push_style_color(imgui::StyleColor::Text, text_color))
    }

    fn cache_spawnable_args(&mut self, id : LevelSpawnableId, spawnable : &LevelSpawnable)
    {
        if self.select.cached_arg_id == id
        {
            return;
        }

        self.select.cached_args_numbered.clear();
        self.select.cached_args_named.clear();

        if let Some(args) = &spawnable.args
        {
            let (arg_numbered, arg_named) = args.to_vec_named();

            self.select.cached_args_numbered = arg_numbered;
            self.select.cached_args_named = arg_named;
        }

        self.select.cached_arg_id = id;
    }

    fn sidebar_select_tool(&mut self, ui : &imgui::Ui,
                           level : &mut Level, config : &mut ProjectConfig,
                           spawnables_store : &DataStore<SpawnableSet>)
    {
        if let Some(tree_item) = imgui::TreeNode::new("Select Options")
            .flags(imgui::TreeNodeFlags::SPAN_AVAIL_WIDTH)
            .frame_padding(true).default_open(true).push(ui)
        {
            ui.unindent();

            ui.checkbox("Tilemap Select Opaque Only", &mut config.level_editor.select_opaque_tilemap_only);

            ui.indent();
            tree_item.pop();
        }

        ui.spacing();
        ui.separator();
        ui.spacing();

        if self.select.selected.len() != 1
        {
            ui.text(format!("{} objects selected", self.select.selected.len()));

            if self.select.selected.is_empty()
            {
                ui.spacing();
                ui.text_disabled("Left click: Replace selection.");
                ui.text_disabled("Shift + Left click: Add to selection.");
                ui.text_disabled("Ctrl + Left click: Subtract from selection.");
                ui.spacing();
                ui.text_disabled("Ctrl + Left click drag: Clone selected objects.");
            }
        }
        else
        {
            let id = self.select.selected[0];

            ui.text_disabled(format!("Properties for Object {} on Layer {}", id.spawnable, id.layer));
            ui.spacing();

            if let Some(spawnable) = level.spawnable(id.layer, id.spawnable)
            {
                if let Some(tree_item) = imgui::TreeNode::new("Basic")
                    .flags(imgui::TreeNodeFlags::SPAN_AVAIL_WIDTH)
                    .frame_padding(true).default_open(true).push(ui)
                {
                    ui.unindent();

                    self.select_header_basic(ui, level, id, &spawnable, spawnables_store);

                    ui.indent();
                    tree_item.pop();
                }

                if repr_has_properties(&spawnable.representation)
                {
                    if let Some(tree_item) = imgui::TreeNode::new("Representation")
                        .label::<String, String>(spawnable.representation.to_string())
                        .flags(imgui::TreeNodeFlags::SPAN_AVAIL_WIDTH)
                        .frame_padding(true).default_open(true).push(ui)
                    {
                        ui.unindent();

                        self.select_header_repr(ui, id, &spawnable);

                        ui.indent();
                        tree_item.pop();
                    }
                }

                if let Some(tree_item) = imgui::TreeNode::new("Arguments")
                    .flags(imgui::TreeNodeFlags::SPAN_AVAIL_WIDTH)
                    .frame_padding(true).default_open(true).push(ui)
                {
                    ui.unindent();

                    self.select_header_args(ui, id, &spawnable);

                    ui.indent();
                    tree_item.pop();
                }
            }
        }
    }

    fn select_header_basic(&mut self, ui : &imgui::Ui, level : &Level, id : LevelSpawnableId,
                              spawnable : &LevelSpawnable, spawnables_store : &DataStore<SpawnableSet>)
    {
        let mut spawnable_type_input = level.spawnable_name_for_id(spawnable.spawn_name_id).cloned().unwrap_or_default();
        let spawnable_type = spawnable_type_input.clone();

        if ui.input_text("Type Name", &mut spawnable_type_input).enter_returns_true(true).build()
        {
            self.queue_action(Box::new(LevelAction::RenameSpawnable
            {
                id,
                spawnable_name : spawnable_type_input.to_string()
            }))
        }

        if let Some(spawnables) = level.spawnable_set().get_if_resolved(spawnables_store)
        {
            if spawnables.spawnable_by_name(&spawnable_type).is_none()
            {
                self.sidebar_error_text(ui, "Type name not in SpawnableSet!");
            }
        }

        let mut index = spawnable.representation.index();
        let old_index = index;

        let width = ui.push_item_width(ui.window_content_region_width() * 0.55);

        if ui.combo_simple_string("Representation", &mut index, &crate::util::widget::REPRESENTATION_NAMES)
        {
            if index != old_index
            {
                let representation = self.make_spawnable_repr(Representation::from_index(index));

                self.queue_action(Box::new(LevelAction::SetSpawnableRepresentation
                {
                    id,
                    representation
                }));
            }
        }

        width.pop(ui);

        if let Some(spawnables) = level.spawnable_set().get_if_resolved(spawnables_store)
        {
            if let Some(spawnable_data) = spawnables.spawnable_by_name(&spawnable_type)
            {
                if !spawnable_data.representation().matches_with_level(&spawnable.representation)
                {
                    self.sidebar_error_text(ui, "Representation does not match SpawnableSet definition!");
                }
            }
        }

        let mut position = [spawnable.transform.x, spawnable.transform.y];
        let mut z_position = spawnable.transform.z;
        let mut angle = spawnable.transform.angle.to_degrees();
        let mut scale = [spawnable.transform.scale_x, spawnable.transform.scale_y];
        let mut update_transform = false;
        let mut update_angle = false;

        let style_var = ui.push_style_var(imgui::StyleVar::ItemSpacing([1.0, 1.0]));

        update_transform |= imgui::Drag::new("Position").display_format("%.2f").build_array(ui, &mut position);
        update_transform |= imgui::Drag::new("Z Position").display_format("%.2f").build(ui, &mut z_position);
        update_angle |= imgui::Drag::new("Rotation").display_format("%.2f").build(ui, &mut angle);

        style_var.pop();

        update_transform |= imgui::Drag::new("Scale").display_format("%.2f").build_array(ui, &mut scale);

        if update_transform || update_angle
        {
            let mut transform = SimpleTransform
            {
                x : position[0],
                y : position[1],
                z : z_position,
                angle : spawnable.transform.angle,
                scale_x : scale[0],
                scale_y : scale[1]
            };

            if update_angle
            {
                transform.angle = (angle % 360.0).to_radians();
            }

            if !ui.io().key_ctrl
            {
                transform.scale_x = transform.scale_x.clamp(0.015625, 65536.0);
                transform.scale_y = transform.scale_y.clamp(0.015625, 65536.0);
            }

            self.queue_action(Box::new(LevelAction::MoveSpawnable
            {
                id,
                transform
            }));
        }
    }

    fn select_header_repr(&mut self, ui : &imgui::Ui, id : LevelSpawnableId,
                           spawnable : &LevelSpawnable)
    {
        match &spawnable.representation
        {
            LevelRepresentation::Actor { .. } =>
            {
                self.select_header_actor(ui, id, spawnable);
            }
            LevelRepresentation::Tilemap { .. } =>
            {
                self.select_header_tilemap(ui, id, spawnable);
            }
            _ =>
            {
                let color = ui.push_style_color(imgui::StyleColor::Text, ui.style_color(imgui::StyleColor::TextDisabled));
                ui.text_wrapped("No additional properties for this representation type.");
                color.pop();
            }
        }
    }

    fn select_header_actor(&mut self, ui : &imgui::Ui, id : LevelSpawnableId, spawnable : &LevelSpawnable)
    {
        match &spawnable.representation
        {
            LevelRepresentation::Actor { custom_costume } =>
            {
                let mut update_path = false;
                let mut custom_costume_package = String::new();
                let mut custom_costume_path = String::new();

                if let Some(custom_some) = &custom_costume
                {
                    custom_costume_package.push_str(custom_some.package());
                    custom_costume_path.push_str(custom_some.path());
                }

                let width = ui.push_item_width(ui.window_content_region_width() * 0.55);
                update_path |= ui.input_text("Package", &mut custom_costume_package).enter_returns_true(true).build();
                update_path |= ui.input_text("Custom Costume", &mut custom_costume_path).enter_returns_true(true).build();
                width.pop(ui);

                if update_path
                {
                    let mut custom_costume = None;

                    if !custom_costume_package.is_empty() || !custom_costume_path.is_empty()
                    {
                        custom_costume = Some(DataHandle::with_path(&custom_costume_package, &custom_costume_path));
                    }

                    let representation = LevelRepresentation::Actor
                    {
                        custom_costume
                    };

                    self.queue_action(Box::new(LevelAction::SetSpawnableRepresentation
                    {
                        id,
                        representation
                    }));
                }
            }
            _ => {}
        }
    }

    fn select_header_tilemap(&mut self, ui : &imgui::Ui, id : LevelSpawnableId, spawnable : &LevelSpawnable)
    {
        match &spawnable.representation
        {
            LevelRepresentation::Tilemap { tiles } =>
            {
                let (offset_x, offset_y) = tiles.offset();
                let mut offset = [offset_x, offset_y];

                if imgui::Drag::new("Offset").display_format("%.2f").build_array(ui, &mut offset)
                {
                    let mut new_tiles = tiles.clone();

                    new_tiles.set_offset(offset[0], offset[1]);

                    let representation = LevelRepresentation::Tilemap
                    {
                        tiles : new_tiles
                    };

                    self.queue_action(Box::new(LevelAction::SetSpawnableRepresentation
                    {
                        id,
                        representation
                    }));
                }
            },
            _ => {}
        }
    }

    fn select_header_args(&mut self, ui : &imgui::Ui, id : LevelSpawnableId, spawnable : &LevelSpawnable)
    {
        let style = ui.clone_style();

        self.cache_spawnable_args(id, spawnable);

        self.select.selected_arg_numbered = self.select.selected_arg_numbered.min(self.select.cached_args_numbered.len().max(1) - 1);

        ui.align_text_to_frame_padding();
        ui.text("Numbered");
        ui.same_line_with_spacing(0.0, 8.0);

        if ui.button("Add")
        {
            self.queue_action(Box::new(LevelAction::AddNumberedArg
            {
                spawnable_id : id,
                arg_pos : (self.select.selected_arg_numbered + 1).clamp(0, self.select.cached_args_numbered.len()),
                arg : DynArg::Empty
            }));

            self.select.selected_arg_numbered += 1;
        }
        ui.same_line_with_spacing(0.0, 4.0);
        if ui.button("Up") && self.select.cached_args_numbered.len() > 0
            && self.select.selected_arg_numbered > 0
        {
            self.queue_action(Box::new(LevelAction::MoveNumberedArg
            {
                spawnable_id : id,
                arg_pos_old : self.select.selected_arg_numbered,
                arg_pos_new : self.select.selected_arg_numbered - 1
            }));

            self.select.selected_arg_numbered -= 1;
        }
        ui.same_line_with_spacing(0.0, 4.0);
        if ui.button("Down") && self.select.cached_args_numbered.len() > 0
            && self.select.selected_arg_numbered < self.select.cached_args_numbered.len() - 1
        {
            self.queue_action(Box::new(LevelAction::MoveNumberedArg
            {
                spawnable_id : id,
                arg_pos_old : self.select.selected_arg_numbered,
                arg_pos_new : self.select.selected_arg_numbered + 1
            }));

            self.select.selected_arg_numbered += 1;
        }
        ui.same_line_with_spacing(0.0, 4.0);
        if ui.button("Remove") && self.select.cached_args_numbered.len() > 0
        {
            self.queue_action(Box::new(LevelAction::RemoveNumberedArg
            {
                spawnable_id : id,
                arg_pos : self.select.selected_arg_numbered,
            }));
        }

        ui.columns(2, "columns_args_numbered", false);
        ui.set_column_width(0, ui.window_content_region_width() * 0.4);

        for i in 0..self.select.cached_args_numbered.len()
        {
            let mut arg_string = self.select.cached_args_numbered[i].to_value_string();

            crate::util::widget::right_aligned_label(&format!("Arg {}", i), true, ui);

            if self.select.selected_arg_numbered == i
            {
                crate::util::widget::underline_current_item(2.0, 4.0, 1.0, ui);
            }

            ui.next_column();

            let width = ui.push_item_width(ui.current_column_width() - style.item_spacing[0] * 2.0);
            if ui.input_text(format!("##numbered_arg {}", i), &mut arg_string).enter_returns_true(true).build()
            {
                self.queue_action(Box::new(LevelAction::SetNumberedArg
                {
                    spawnable_id : id,
                    arg_pos : i,
                    arg : DynArg::from_value_str(&arg_string)
                }));
            }
            width.pop(ui);
            if ui.is_item_focused()
            {
                self.select.selected_arg_numbered = i;
            }

            ui.next_column();
        }

        ui.columns(1, "", false);

        ui.spacing();

        ui.align_text_to_frame_padding();
        ui.text("Named");
        ui.same_line_with_spacing(0.0, 8.0);

        let width = ui.push_item_width(ui.window_content_region_width() * 0.4);
        ui.input_text("##new_arg_name", &mut self.select.new_arg_name).build();
        width.pop(ui);
        ui.same_line_with_spacing(0.0, 4.0);
        if ui.button("Add##Add Named")
        {
            let new_arg_name = self.select.new_arg_name.to_string();

            self.queue_action(Box::new(LevelAction::AddNamedArg
            {
                spawnable_id : id,
                arg_name : new_arg_name.clone(),
                arg : DynArg::Empty
            }));

            self.select.selected_arg_named = new_arg_name;
        }
        ui.same_line_with_spacing(0.0, 4.0);

        let mut focus_next_named = false;

        if ui.button("Remove##Remove Named")
        {
            self.queue_action(Box::new(LevelAction::RemoveNamedArg
            {
                spawnable_id : id,
                arg_name : self.select.selected_arg_named.clone(),
            }));

            focus_next_named = true;
        }

        ui.columns(2, "columns_args_named", false);
        ui.set_current_column_width(ui.window_content_region_width() * 0.4);

        for i in 0..self.select.cached_args_named.len()
        {
            let mut arg_string = self.select.cached_args_named[i].1.to_value_string();
            let arg_name = self.select.cached_args_named[i].0.clone();

            crate::util::widget::right_aligned_label(&arg_name, true, ui);
            if self.select.selected_arg_named == arg_name
            {
                crate::util::widget::underline_current_item(2.0, 4.0, 1.0, ui);
            }
            if ui.is_item_hovered()
            {
                ui.tooltip_text(&arg_name);
            }

            ui.next_column();

            if focus_next_named && arg_name == self.select.selected_arg_named
            {
                if i == self.select.cached_args_named.len() - 1
                {
                    ui.set_keyboard_focus_here_with_offset(imgui::FocusedWidget::Previous);
                }
                else
                {
                    ui.set_keyboard_focus_here_with_offset(imgui::FocusedWidget::Next);
                }
            }

            let width = ui.push_item_width(ui.current_column_width() - style.item_spacing[0] * 2.0);
            if ui.input_text(format!("##named_arg {}", arg_name), &mut arg_string).enter_returns_true(true).build()
            {
                self.queue_action(Box::new(LevelAction::AddNamedArg
                {
                    spawnable_id : id,
                    arg_name : arg_name.clone(),
                    arg : DynArg::from_value_str(&arg_string)
                }));
            }
            width.pop(ui);

            if ui.is_item_focused()
            {
                self.select.selected_arg_named = arg_name;
            }

            ui.next_column();
        }

        ui.columns(1, "", false);
    }

    fn sidebar_error_text(&self, ui : &imgui::Ui, text : &str)
    {
        let color = ui.push_style_color(imgui::StyleColor::Text, crate::util::widget::error_text_color(ui));
        ui.indent();
        ui.text_wrapped(text);
        ui.unindent();
        color.pop();

        ui.spacing();
    }

    fn sidebar_spawnables_tool(&mut self, ui : &imgui::Ui,
                               level : &mut Level,
                               spawnables_store : &DataStore<SpawnableSet>,
                               costume_store : &DataStore<Costume>, sheet_store : &DataStore<Sheet>,
                               packages : &mut Packages)
    {
        if let Some(spawnables) = spawnables_store.get(level.spawnable_set().id())
        {
            let main_style = ui.clone_style();
            let style_spawnables_list = ui.push_style_var(imgui::StyleVar::WindowPadding([3.0, 2.0]));
            let colors_spawnables_list = ui.push_style_color(imgui::StyleColor::ChildBg, main_style.colors[imgui::StyleColor::FrameBg as usize]);

            if let Some(spawnables_list) = imgui::ChildWindow::new("spawnables_list").border(true).draw_background(true).begin(ui)
            {
                let mut dummy = false;

                ui.spacing();

                if crate::util::widget::ui_spawnable_list(spawnables, &mut self.spawnable_to_place, &mut dummy, costume_store, sheet_store, packages, ui)
                {
                    if let Some(auto_layer) = self.auto_layer_for_spawnable(self.spawnable_to_place, level, spawnables_store)
                    {
                        self.current_layer = auto_layer;
                    }
                }

                ui.spacing();

                spawnables_list.end();
            }

            colors_spawnables_list.pop();
            style_spawnables_list.pop();
        }
        else
        {
            crate::util::widget::window_placeholder_text("No SpawnableSet loaded.", ui);
        }
    }

    fn texture_id_for_tileset(&self, packages : &mut Packages, tileset : &Tileset, sheet_store : &DataStore<Sheet>) -> DataId<Sheet>
    {
        let source = tileset.sheet();

        match sheet_store.get_id(source.package(), source.path())
        {
            Ok(id) =>
            {
                id
            }
            Err(_) =>
            {
                packages.load_package_later(ResourceType::Sheet, source.package());
                DataId::new(0)
            }
        }
    }

    fn sidebar_tiles_tool(&mut self, ui : &imgui::Ui, packages : &mut Packages, level : &Level,
                          tileset_store : &mut DataStore<Tileset>, sheet_store : &DataStore<Sheet>)
    {
        let mut show_placeholder = true;

        if self.select.selected.len() == 1
        {
            let id = self.select.selected[0];

            if let Some(spawnable) = level.spawnable(id.layer, id.spawnable)
            {
                if let LevelRepresentation::Tilemap { tiles } = &spawnable.representation
                {
                    show_placeholder = false;
                    self.sidebar_tiles_tilemap(ui, packages, id, level, tiles, tileset_store, sheet_store);
                }
            }
        }

        if show_placeholder
        {
            ui.text_wrapped("Please select one Tilemap object to edit.");
        }
    }

    fn sidebar_tiles_tilemap(&mut self, ui : &imgui::Ui, packages : &mut Packages, spawnable_id : LevelSpawnableId,
                             level : &Level, tilemap : &Tilemap, tileset_store : &mut DataStore<Tileset>,
                             sheet_store : &DataStore<Sheet>)
    {
        if let Some(tree_item) = imgui::TreeNode::new("Basic")
            .flags(imgui::TreeNodeFlags::SPAN_AVAIL_WIDTH)
            .frame_padding(true).default_open(true).push(ui)
        {
            ui.unindent();

            self.tiles_header_basic(ui, spawnable_id, level, tilemap, tileset_store);

            ui.indent();
            tree_item.pop();
        }

        if let Some(tree_item) = imgui::TreeNode::new("Tile Selection")
            .flags(imgui::TreeNodeFlags::SPAN_AVAIL_WIDTH)
            .frame_padding(true).default_open(true).push(ui)
        {
            ui.unindent();

            if let Some(tileset) = tilemap.tileset().get_mut_if_resolved(tileset_store)
            {
                self.tiles_header_tiles(ui, packages, tileset, sheet_store);
            }
            else
            {
                self.sidebar_error_text(ui, "No Tileset loaded.");
            }

            ui.indent();
            tree_item.pop();
        }
    }

    fn set_autocrop(&mut self, tilemap : &Tilemap)
    {
        let mut empty = true;
        let (width, height) = tilemap.grid().size();
        let mut x1 = width - 1;
        let mut y1 = height - 1;
        let mut x2 = 0;
        let mut y2 = 0;

        for y in 0..height
        {
            for x in 0..width
            {
                if let Some(tile) = tilemap.grid().tile(x, y)
                {
                    if tile.index > 0
                    {
                        empty = false;
                        x1 = x1.min(x);
                        y1 = y1.min(y);
                        x2 = x2.max(x);
                        y2 = y2.max(y);
                    }
                }
            }
        }

        if empty
        {
            self.tiles.new_tilemap_size = [1, 1];
            self.tiles.new_tilemap_nudge = [0, 0];
            self.tiles.new_tilemap_position = [0, 0];
        }
        else
        {
            self.tiles.new_tilemap_size = [(x2 - x1 + 1) as u32, (y2 - y1 + 1) as u32];
            self.tiles.new_tilemap_position = [x1 as i32, y1 as i32];
            self.tiles.new_tilemap_nudge = [0 - self.tiles.new_tilemap_position[0], 0 - self.tiles.new_tilemap_position[1]];
        }
    }

    fn tiles_header_basic(&mut self, ui : &imgui::Ui, spawnable_id : LevelSpawnableId, level : &Level,
                          tilemap : &Tilemap, tileset_store : &mut DataStore<Tileset>)
    {
        let mut update_path = false;
        let mut tileset_package = tilemap.tileset().package().clone();
        let mut tileset_path = tilemap.tileset().path().clone();

        let width = ui.push_item_width(160.0);
        update_path |= ui.input_text("Package", &mut tileset_package).enter_returns_true(true).build();
        update_path |= ui.input_text("Tileset", &mut tileset_path).enter_returns_true(true).build();
        width.pop(ui);

        if update_path
        {
            let mut new_handle = tilemap.tileset().clone();

            new_handle.set_path(&tileset_package, &tileset_path);

            self.queue_action(Box::new(LevelAction::SetTileset
            {
                spawnable_id,
                tileset : new_handle
            }));
        }

        ui.popup("Resize Menu", ||
        {
            imgui::Drag::new("New Size").build_array(ui, &mut self.tiles.new_tilemap_size);
            imgui::Drag::new("Nudge Tiles").build_array(ui, &mut self.tiles.new_tilemap_nudge);
            imgui::Drag::new("Reposition Tilemap").build_array(ui, &mut self.tiles.new_tilemap_position);

            self.tiles.new_tilemap_size[0] = self.tiles.new_tilemap_size[0].clamp(1, 65536);
            self.tiles.new_tilemap_size[1] = self.tiles.new_tilemap_size[1].clamp(1, 65536);

            let (tile_w, tile_h) = crate::util::drawable::tile_size(tilemap, tileset_store);

            if ui.button("Autocrop")
            {
                self.set_autocrop(tilemap);
            }
            ui.same_line();
            if ui.button("To Level Size")
            {
                self.tiles.new_tilemap_size[0] = (level.bounds().w() / tile_w).round() as u32;
                self.tiles.new_tilemap_size[1] = (level.bounds().h() / tile_h).round() as u32;

                if let Some(spawnable) = level.spawnable(spawnable_id.layer, spawnable_id.spawnable)
                {
                    self.tiles.new_tilemap_nudge[0] = (spawnable.transform.x / tile_w) as i32;
                    self.tiles.new_tilemap_nudge[1] = (spawnable.transform.y / tile_h) as i32;
                    self.tiles.new_tilemap_position[0] = 0 - self.tiles.new_tilemap_nudge[0];
                    self.tiles.new_tilemap_position[1] = 0 - self.tiles.new_tilemap_nudge[1];
                }
            }

            ui.spacing();

            if ui.button("Apply Resize")
            {
                self.queue_action(Box::new(LevelAction::ResizeTilemap
                {
                    spawnable_id,
                    new_w : self.tiles.new_tilemap_size[0] as usize,
                    new_h : self.tiles.new_tilemap_size[1] as usize,
                    nudge_x : self.tiles.new_tilemap_nudge[0],
                    nudge_y : self.tiles.new_tilemap_nudge[1],
                    position_x : self.tiles.new_tilemap_position[0],
                    position_y : self.tiles.new_tilemap_position[1]
                }));
                ui.close_current_popup();
            }
        });

        let (grid_width, grid_height) = tilemap.grid().size();

        ui.align_text_to_frame_padding();
        ui.indent();
        ui.text(format!("Tilemap Size: {}x{}", grid_width, grid_height));
        ui.same_line_with_spacing(0.0, 16.0);

        if ui.button("Resize...")
        {
            self.tiles.new_tilemap_size = [grid_width as u32, grid_height as u32];
            self.tiles.new_tilemap_nudge = [0, 0];
            self.tiles.new_tilemap_position = [0, 0];

            ui.open_popup("Resize Menu");
        }

        ui.unindent();
    }

    fn tiles_header_tiles(&mut self, ui : &imgui::Ui, packages : &mut Packages, tileset : &mut Tileset, sheet_store : &DataStore<Sheet>)
    {
        let style_slice_list = ui.push_style_var(imgui::StyleVar::WindowPadding([1.0, 1.0]));
        let colors_slice_list = ui.push_style_color(imgui::StyleColor::ChildBg, [self.bg_r.value, self.bg_g.value, self.bg_b.value, 1.0]);

        if let Some(tile_list) = imgui::ChildWindow::new("tile_list").always_vertical_scrollbar(true).border(true).draw_background(true).begin(ui)
        {
            let texture_id = self.texture_id_for_tileset(packages, tileset, sheet_store);
            let style_slice = ui.push_style_var(imgui::StyleVar::ItemSpacing([0.0, 0.0]));
            let num_tiles = tileset.tile_count();

            let start_pos = ui.cursor_screen_pos();
            let layout_width = ui.column_width(0) - 2.0;
            let (orig_tile_width, orig_tile_height) = tileset.tile_size();
            let zoom_level = (layout_width / 8.0 / orig_tile_width as f32).floor();
            let mut tile_width = orig_tile_width as f32 * zoom_level;
            let mut tile_height = orig_tile_height as f32 * zoom_level;
            let base_width = tile_width.max(1.0);

            tile_width = tile_width.max(8.0).min(128.0);
            tile_height = tile_height.max(1.0);

            if tile_width != base_width
            {
                tile_height *= tile_width / base_width;
                tile_height = tile_height.max(1.0);
            }

            let button_size = [tile_width, tile_height];
            let columns = (layout_width / tile_width).floor().max(1.0) as u32;

            for i in 0..num_tiles as u32
            {
                if i % columns > 0
                {
                    ui.same_line();
                }

                let cursor_pos = ui.cursor_pos();
                crate::util::drawable::tileset_image_from_slice(tileset, texture_id.index(), sheet_store.get(texture_id), i as usize, button_size).build(ui);

                ui.set_cursor_pos([cursor_pos[0], cursor_pos[1]]);

                if imgui::Selectable::new(format!("###tile_{}", i))
                    .selected(self.tiles.current == i).size(button_size).build(ui)
                {
                    self.tiles.current = i;
                }

                if self.tiles.current == i && self.tiles.scroll_list
                {
                    ui.set_scroll_here_y();
                    self.tiles.scroll_list = false;
                }

                if ui.is_item_hovered()
                {
                    ui.tooltip_text(format!("Tile {}", i));
                }
            }

            let draw_list = ui.get_window_draw_list();

            let x1 = start_pos[0] + (self.tiles.current % columns) as f32 * tile_width;
            let y1 = start_pos[1] + (self.tiles.current / columns) as f32 * tile_height;
            let x2 = x1 + tile_width;
            let y2 = y1 + tile_height;

            draw_list.add_rect([x1, y1], [x2, y2], ui.style_color(imgui::StyleColor::Button)).thickness(2.0).build();

            style_slice.pop();

            tile_list.end();
        }

        colors_slice_list.pop();
        style_slice_list.pop();
    }

    fn sidebar_layer_tool(&mut self, ui : &imgui::Ui, level : &mut Level)
    {
        let num_layers = level.layer_count();
        self.current_layer = self.current_layer.min(num_layers.max(1) - 1);

        if let Some(layer) = level.layer(self.current_layer)
        {
            let mut layer_name = level.layer_name(self.current_layer).unwrap_or_default();

            if ui.input_text("Layer Name", &mut layer_name).enter_returns_true(true).build()
            {
                self.queue_action(Box::new(LevelAction::RenameLayer
                {
                    id : self.current_layer,
                    name : layer_name.to_string()
                }));
            }

            let mut visible = layer.visible();
            let mut locked = layer.locked();
            let mut z_offset = layer.z_offset();

            let scroll_rate = layer.scroll_rate();
            let mut scroll_rate_enabled = scroll_rate.is_some();
            let mut scroll_rate = match scroll_rate
            {
                None => { [1.0, 1.0] }
                Some((x, y)) => { [x, y] }
            };

            if ui.checkbox("Visible", &mut visible)
            {
                self.queue_action(Box::new(LevelAction::SetLayerVisible
                {
                    id : self.current_layer,
                    visible
                }));
            }

            ui.same_line_with_spacing(0.0, 10.0);

            if ui.checkbox("Locked", &mut locked)
            {
                self.queue_action(Box::new(LevelAction::SetLayerLocked
                {
                    id : self.current_layer,
                    locked
                }));
            }

            ui.spacing();

            let mut update_draw_settings = false;

            let width = ui.push_item_width(ui.window_content_region_width() * 0.4);
            update_draw_settings |= imgui::Drag::new("Z Offset").display_format("%.2f").build(ui, &mut z_offset);
            width.pop(ui);

            update_draw_settings |= ui.checkbox("Set Scroll Rate", &mut scroll_rate_enabled);

            if scroll_rate_enabled
            {
                ui.same_line();
                let width = ui.push_item_width(ui.window_content_region_width() * 0.4);
                update_draw_settings |= imgui::Drag::new("##scroll_rate").display_format("%.2f").build_array(ui, &mut scroll_rate);
                width.pop(ui);
            }

            if update_draw_settings
            {
                self.queue_action(Box::new(LevelAction::SetLayerParameters
                {
                    id : self.current_layer,
                    z_offset,
                    scroll_rate : match scroll_rate_enabled
                    {
                        true => { Some((scroll_rate[0], scroll_rate[1])) }
                        false => { None}
                    }
                }));
            }
        }

        ui.separator();

        if ui.button("Add")
        {
            self.queue_action(Box::new(LevelAction::AddLayer
            {
                pos : (self.current_layer + 1).clamp(0, num_layers),
                layer : LevelLayer::default()
            }));

            self.current_layer += 1;
        }
        ui.same_line_with_spacing(0.0, 4.0);
        if ui.button("Raise") && self.current_layer + 1 < num_layers
        {
            self.queue_action(Box::new(LevelAction::MoveLayer
            {
                old_pos : self.current_layer,
                new_pos : self.current_layer + 1
            }));

            self.current_layer += 1;
        }
        ui.same_line_with_spacing(0.0, 4.0);
        if ui.button("Lower") && self.current_layer > 0
        {
            self.queue_action(Box::new(LevelAction::MoveLayer
            {
                old_pos : self.current_layer,
                new_pos : self.current_layer - 1
            }));

            self.current_layer -= 1;
        }
        ui.same_line_with_spacing(0.0, 4.0);
        if ui.button("Remove")
        {
            self.queue_action(Box::new(LevelAction::RemoveLayer(self.current_layer)));
        }

        self.layer_list(ui, level, [0.0, 0.0]);
    }

    fn sidebar_level_tool(&mut self, ui : &imgui::Ui, level : &mut Level, spawnables_source : &DataHandle<SpawnableSet>)
    {
        let mut update_path = false;
        let mut spawnable_set_package = spawnables_source.package().clone();
        let mut spawnable_set_path = spawnables_source.path().clone();

        let width = ui.push_item_width(160.0);
        update_path |= ui.input_text("Package", &mut spawnable_set_package).enter_returns_true(true).build();
        update_path |= ui.input_text("SpawnableSet", &mut spawnable_set_path).enter_returns_true(true).build();
        width.pop(ui);

        if update_path
        {
            let mut new_handle = spawnables_source.clone();

            new_handle.set_path(&spawnable_set_package, &spawnable_set_path);

            self.queue_action(Box::new(LevelAction::SetSpawnableSet(new_handle)));
        }

        ui.separator();

        let bg = level.background();
        let mut has_bg = bg.is_some();

        if ui.checkbox("Background color", &mut has_bg)
        {
            let mut new_bg = None;

            if has_bg
            {
                new_bg = Some(BackgroundInfo
                {
                    r: 0.0,
                    g: 0.5,
                    b: 1.0
                });
            }

            self.queue_action(Box::new(LevelAction::SetBackground(new_bg)));
        }

        if let Some(bg_some) = bg
        {
            let mut color = [bg_some.r, bg_some.g, bg_some.b];
            let avail = ui.content_region_avail();

            let item_width = ui.push_item_width(avail[0]);

            if imgui::ColorEdit::new("###level_bg_color", &mut color).build(ui)
            {
                self.queue_action(Box::new(LevelAction::SetBackground(Some(BackgroundInfo
                {
                    r: color[0],
                    g: color[1],
                    b: color[2]
                }))));
            }

            item_width.pop(ui);
        }

        let bounds = level.bounds();
        let mut update = false;
        let mut bounds_min = [bounds.x1, bounds.y1];
        let mut bounds_max = [bounds.x2, bounds.y2];

        update |= imgui::Drag::new("Bounds Min").display_format("%.2f").build_array(ui, &mut bounds_min);
        update |= imgui::Drag::new("Bounds Max").display_format("%.2f").build_array(ui, &mut bounds_max);

        if update
        {
            self.queue_action(Box::new(LevelAction::SetBounds(BakedRect
            {
                x1: bounds_min[0],
                y1: bounds_min[1],
                x2: bounds_max[0],
                y2: bounds_max[1]
            })));
        }
    }

    fn layer_list(&mut self, ui : &imgui::Ui, level : &mut Level, size : [f32; 2])
    {
        let num_layers = level.layer_count();
        let main_style = ui.clone_style();

        let style_layer_list = ui.push_style_var(imgui::StyleVar::WindowPadding([3.0, 2.0]));
        let colors_layer_list = ui.push_style_color(imgui::StyleColor::ChildBg, main_style.colors[imgui::StyleColor::FrameBg as usize]);

        imgui::ChildWindow::new("layer_list").border(true).draw_background(true).size(size).build(ui, ||
        {
            let right = ui.window_content_region_max()[0];
            let button_width = crate::util::widget::ICON_FONT_SIZE + 4.0;
            let button_height = ui.current_font_size();
            let button_spacing = main_style.item_spacing[0];
            let locked_pos = right - button_width;
            let visibility_pos = locked_pos - button_width;

            for i in (0..num_layers).rev()
            {
                let mut visible = false;
                let mut locked = false;
                let layer_name;

                if let Some(layer) = level.layer(i)
                {
                    visible = layer.visible();
                    locked = layer.locked();
                }

                if let Some(name) = level.layer_name(i)
                {
                    layer_name = name;
                }
                else
                {
                    layer_name = format!("<layer {}>", i);
                }

                if imgui::Selectable::new(&layer_name).size([visibility_pos - button_spacing / 2.0 - 5.0, 0.0]).selected(self.current_layer == i).build(ui)
                {
                    self.current_layer = i;
                    ui.close_current_popup();
                }

                if self.current_layer == i && self.scroll_layer_list
                {
                    ui.set_scroll_here_y();
                    self.scroll_layer_list = false;
                }

                let font = ui.push_font(crate::util::widget::get_icon_font(ui));
                let style_button_border = ui.push_style_var(imgui::StyleVar::FrameBorderSize(0.0));
                let style_button_padding = ui.push_style_var(imgui::StyleVar::FramePadding([2.0, 0.0]));

                let cursor_pos = ui.cursor_pos();
                ui.same_line_with_pos(visibility_pos);

                let (button_color, button_text_color) = self.layer_button_colors(visible, ui);
                if ui.button_with_size(format!("{}##visible_layer_{}", crate::util::widget::ICON_VISIBLE, i), [button_width, button_height])
                {
                    self.queue_action(Box::new(LevelAction::SetLayerVisible
                    {
                        id : i,
                        visible : !visible
                    }));
                }
                button_color.pop();
                button_text_color.pop();

                ui.same_line_with_pos(locked_pos);

                let mut icon_lock = crate::util::widget::ICON_UNLOCKED;

                if locked
                {
                    icon_lock = crate::util::widget::ICON_LOCKED;
                }

                let (button_color, button_text_color) = self.layer_button_colors(!locked, ui);
                if ui.button_with_size(format!("{}##locked_layer_{}", icon_lock, i), [button_width, button_height])
                {
                    self.queue_action(Box::new(LevelAction::SetLayerLocked
                    {
                        id : i,
                        locked : !locked
                    }));
                }
                button_color.pop();
                button_text_color.pop();

                ui.set_cursor_pos(cursor_pos);

                style_button_padding.pop();
                style_button_border.pop();

                font.pop();
            }
        });

        colors_layer_list.pop();
        style_layer_list.pop();
    }

    fn cache_bad_selectable(&mut self, x : f64, y : f64, layer : &LevelLayer)
    {
        let radius = BAD_DRAWABLE_SELECTABLE_WIDTH / 2.0;

        if let Some(layer_selectables) = self.select.selectables.last_mut()
        {
            layer_selectables.push(Selectable
            {
                bounds : BakedRect
                {
                    x1 : x - radius,
                    y1 : y - radius,
                    x2 : x + radius,
                    y2 : y + radius
                },
                fill : true,
                locked : layer.locked() || !layer.visible()
            });
        }
    }

    fn cache_bad_object(&mut self, spawnable : &LevelSpawnable, layer : &LevelLayer, update_drawables : bool)
    {
        if update_drawables && layer.visible()
        {
            self.drawables.push(bad_drawable(spawnable, layer));
        }

        self.cache_bad_selectable(spawnable.transform.x, spawnable.transform.y, layer);
    }

    fn push_sprite_selectable(&mut self, transform : &SimpleTransform, layer : &LevelLayer, sheet_id : DataId<Sheet>, slice_id : usize, sheet_store : &DataStore<Sheet>)
    {
        if let Some(sheet) = sheet_store.get(sheet_id)
        {
            if let Some(slice) = sheet.slice(slice_id)
            {
                if let Some(layer_selectables) = self.select.selectables.last_mut()
                {
                    let mut bounds = BakedRect
                    {
                        x1 : -slice.origin_x as f64,
                        y1 : -slice.origin_y as f64,
                        x2 : (-slice.origin_x + slice.texture_w) as f64,
                        y2 : (-slice.origin_y + slice.texture_h) as f64
                    };

                    bounds.scale(transform.scale_x, transform.scale_y);
                    bounds.grow_for_angle(transform.angle);
                    bounds.translate(transform.x, transform.y);

                    layer_selectables.push(Selectable
                    {
                        bounds,
                        fill : true,
                        locked : layer.locked() || !layer.visible()
                    });
                }

                return;
            }
        }

        self.cache_bad_selectable(transform.x, transform.y, layer);
    }

    fn cache_object(&mut self, packages : &mut Packages, game : &GameControl,
                    spawnable : &LevelSpawnable, spawnable_def : &Spawnable,
                    layer : &LevelLayer, update_drawables : bool)
    {
        let mut costume_store = game.res().store_mut::<Costume>();
        let mut tileset_store = game.res().store_mut::<Tileset>();
        let mut sheet_store = game.res().store_mut::<Sheet>();

        match &spawnable.representation
        {
            LevelRepresentation::None =>
            {
                self.cache_bad_object(spawnable, layer, update_drawables);
            }
            LevelRepresentation::Prop { custom_sheet, custom_slice } =>
            {
                if let Representation::Prop { sheet, slice } = spawnable_def.representation()
                {
                    let sheet = custom_sheet.as_ref().unwrap_or(sheet);
                    let slice = custom_slice.as_ref().unwrap_or(slice);

                    if let Some((sheet_id, slice_id)) = crate::util::drawable::sheet_slice(
                        sheet, &slice, &mut sheet_store, packages)
                    {
                        if update_drawables && layer.visible()
                        {
                            self.drawables.push(Drawable
                            {
                                item: DrawableItem::Sprite(Sprite
                                {
                                    sheet_id,
                                    slice_id,
                                    shader_id : DataId::new(0),
                                    offset_x : 0.0,
                                    offset_y : 0.0,
                                    r : 1.0,
                                    g : 1.0,
                                    b : 1.0,
                                    alpha : 1.0
                                }),
                                transform : LerpedTransform
                                {
                                    x : Lerpable::new(spawnable.transform.x as f32),
                                    y : Lerpable::new(spawnable.transform.y as f32),
                                    angle : Lerpable::new(spawnable.transform.angle as f32),
                                    scale_x : Lerpable::new(spawnable.transform.scale_x as f32),
                                    scale_y : Lerpable::new(spawnable.transform.scale_y as f32)
                                },
                                scroll_rate_x : 1.0,
                                scroll_rate_y : 1.0,
                                depth : spawnable.transform.z as f32 + layer.z_offset() as f32,
                                visible : true,
                                lerp_enabled : false
                            });
                        }

                        self.push_sprite_selectable(&spawnable.transform, layer, sheet_id, slice_id, &mut sheet_store);
                    }
                    else
                    {
                        self.cache_bad_object(spawnable, layer, update_drawables);
                    }
                }
                else
                {
                    self.cache_bad_object(spawnable, layer, update_drawables);
                }
            }
            LevelRepresentation::Actor { custom_costume } =>
            {
                if let Representation::Actor { costume } = spawnable_def.representation()
                {
                    let costume = custom_costume.as_ref().unwrap_or(costume);

                    if let Some((sheet_id, slice_id)) = crate::util::drawable::costume_sheet_slice(
                        costume, &mut costume_store, &mut sheet_store, packages)
                    {
                        if update_drawables && layer.visible()
                        {
                            self.drawables.push(Drawable
                            {
                                item: DrawableItem::Sprite(Sprite
                                {
                                    sheet_id,
                                    slice_id,
                                    shader_id : DataId::new(0),
                                    offset_x : 0.0,
                                    offset_y : 0.0,
                                    r : 1.0,
                                    g : 1.0,
                                    b : 1.0,
                                    alpha : 1.0
                                }),
                                transform: LerpedTransform
                                {
                                    x : Lerpable::new(spawnable.transform.x as f32),
                                    y : Lerpable::new(spawnable.transform.y as f32),
                                    angle : Lerpable::new(spawnable.transform.angle as f32),
                                    scale_x : Lerpable::new(spawnable.transform.scale_x as f32),
                                    scale_y : Lerpable::new(spawnable.transform.scale_y as f32)
                                },
                                scroll_rate_x : 1.0,
                                scroll_rate_y : 1.0,
                                depth : spawnable.transform.z as f32 + layer.z_offset() as f32,
                                visible : true,
                                lerp_enabled : false
                            });
                        }

                        self.push_sprite_selectable(&spawnable.transform, layer, sheet_id, slice_id, &mut sheet_store);
                    }
                    else
                    {
                        self.cache_bad_object(spawnable, layer, update_drawables);
                    }
                }
                else
                {
                    self.cache_bad_object(spawnable, layer, update_drawables);
                }
            }
            LevelRepresentation::Tilemap { tiles } =>
            {
                let (width, height) = game.renderer().base_size();

                if update_drawables && layer.visible()
                {
                    let transform = LerpedTransform
                    {
                        x : Lerpable::new(spawnable.transform.x as f32),
                        y : Lerpable::new(spawnable.transform.y as f32),
                        angle : Lerpable::new(spawnable.transform.angle as f32),
                        scale_x : Lerpable::new(spawnable.transform.scale_x as f32),
                        scale_y : Lerpable::new(spawnable.transform.scale_y as f32)
                    };

                    let item = crate::util::drawable::tilemap_drawable_item(tiles, &transform, BakedRect
                    {
                        x1 : self.scroll_x / self.applied_zoom,
                        y1 : self.scroll_y / self.applied_zoom,
                        x2 : (width as f64 + self.scroll_x) / self.applied_zoom,
                        y2 : (height as f64 + self.scroll_y) / self.applied_zoom
                    }, &mut tileset_store, &mut sheet_store, packages);

                    if item.is_some()
                    {
                        self.drawables.push(Drawable
                        {
                            item,
                            transform,
                            scroll_rate_x : 1.0,
                            scroll_rate_y : 1.0,
                            depth : spawnable.transform.z as f32 + layer.z_offset() as f32,
                            visible : true,
                            lerp_enabled : false
                        });

                        if let Some(layer_selectables) = self.select.selectables.last()
                        {
                            self.scrolling_drawables.push(ScrollingDrawable
                            {
                                drawable_id : self.drawables.len() - 1,
                                spawnable_id : LevelSpawnableId
                                {
                                    layer : self.select.selectables.len() - 1,
                                    spawnable : layer_selectables.len()
                                }
                            });
                        }
                    }
                    else
                    {
                        self.drawables.push(bad_drawable(spawnable, layer));
                    }
                }

                if let Some(tileset) = tileset_store.get(tiles.tileset().resolve_if_loaded(&tileset_store))
                {
                    let (tilegrid_w, tilegrid_h) = tiles.grid().size();
                    let (tile_w, tile_h) = tileset.tile_size();

                    if let Some(layer_selectables) = self.select.selectables.last_mut()
                    {
                        let mut bounds = BakedRect
                        {
                            x1 : 0.0,
                            y1 : 0.0,
                            x2 : (tilegrid_w as f64 * tile_w as f64),
                            y2 : (tilegrid_h as f64 * tile_h as f64)
                        };

                        bounds.grow_for_angle(spawnable.transform.angle);
                        bounds.scale(spawnable.transform.scale_x, spawnable.transform.scale_y);
                        bounds.translate(spawnable.transform.x, spawnable.transform.y);

                        layer_selectables.push(Selectable
                        {
                            bounds,
                            fill : false,
                            locked : layer.locked() || !layer.visible()
                        })
                    }
                }
                else
                {
                    self.cache_bad_selectable(spawnable.transform.x, spawnable.transform.y, layer);
                }
            }
        }
    }

    fn update_cached_object_data(&mut self, update_drawables : bool, packages : &mut Packages, level : &mut Level, game : &GameControl)
    {
        if !self.cached_data_dirty
        {
            return;
        }

        let mut spawnables_store = game.res().store_mut::<SpawnableSet>();

        if let Some(spawnables) = spawnables_store.get_mut(level.spawnable_set().id())
        {
            if update_drawables
            {
                self.drawables.clear();
                self.scrolling_drawables.clear();
            }

            self.select.selectables.clear();

            for i in 0..level.layer_count()
            {
                let layer = level.layer(i).unwrap();
                let placed_count = layer.spawnable_count();

                self.select.selectables.push(Vec::new());

                for j in 0..placed_count
                {
                    let spawnable = layer.spawnable(j).unwrap();
                    let empty = String::new();
                    let name = level.spawnable_name_for_id(spawnable.spawn_name_id).unwrap_or(&empty);

                    if let Some(spawnable_def) = spawnables.spawnable_by_name(&name)
                    {
                        self.cache_object(packages, game, &spawnable, spawnable_def, layer, update_drawables);
                    }
                    else
                    {
                        self.cache_bad_object(spawnable, layer, update_drawables);
                    }
                }
            }
        }
    }

    fn scroll_cached_object_data(&mut self, packages : &mut Packages, level : &mut Level, game : &GameControl)
    {
        let mut tileset_store = game.res().store_mut::<Tileset>();
        let mut sheet_store = game.res().store_mut::<Sheet>();

        let (width, height) = game.renderer().base_size();

        let viewport = Rect
        {
            x : (self.scroll_x) / self.applied_zoom,
            y : (self.scroll_y) / self.applied_zoom,
            w : (width as f64) / self.applied_zoom,
            h : (height as f64) / self.applied_zoom
        };

        if viewport == self.old_viewport
        {
            return;
        }

        self.old_viewport = viewport;

        for entry in &self.scrolling_drawables
        {
            if let Some(drawable) = self.drawables.get_mut(entry.drawable_id)
            {
                if let Some(layer) = level.layer(entry.spawnable_id.layer)
                {
                    if let Some(spawnable) = layer.spawnable(entry.spawnable_id.spawnable)
                    {
                        if let LevelRepresentation::Tilemap { tiles } = &spawnable.representation
                        {
                            let transform = LerpedTransform
                            {
                                x : Lerpable::new(spawnable.transform.x as f32),
                                y : Lerpable::new(spawnable.transform.y as f32),
                                angle : Lerpable::new(spawnable.transform.angle as f32),
                                scale_x : Lerpable::new(spawnable.transform.scale_x as f32),
                                scale_y : Lerpable::new(spawnable.transform.scale_y as f32)
                            };

                            let item = crate::util::drawable::tilemap_drawable_item(tiles, &transform, BakedRect
                            {
                                x1 : self.scroll_x / self.applied_zoom,
                                y1 : self.scroll_y / self.applied_zoom,
                                x2 : (width as f64 + self.scroll_x) / self.applied_zoom,
                                y2 : (height as f64 + self.scroll_y) / self.applied_zoom
                            }, &mut tileset_store, &mut sheet_store, packages);

                            if item.is_some()
                            {
                                *drawable = Drawable
                                {
                                    item,
                                    transform,
                                    scroll_rate_x : 1.0,
                                    scroll_rate_y : 1.0,
                                    depth : spawnable.transform.z as f32 + layer.z_offset() as f32,
                                    visible : true,
                                    lerp_enabled : false
                                };
                            }
                            else
                            {
                                *drawable = bad_drawable(spawnable, layer);
                            }
                        }
                    }
                }
            }
        }
    }

    fn draw_selection(&self, ui : &imgui::Ui, level : &mut Level)
    {
        if self.select.display == SelectionDisplayMode::None
        {
            return;
        }

        let style = ui.clone_style();
        let mut fill_color = style.colors[imgui::StyleColor::TextSelectedBg as usize];
        let mut fill_hover_color = fill_color.clone();
        let mut border_color = style.colors[imgui::StyleColor::TextSelectedBg as usize];
        fill_color[3] = 0.3;
        fill_hover_color[3] = 0.2;
        border_color[3] = 1.0;
        let draw_list = ui.get_background_draw_list();

        if self.select.display == SelectionDisplayMode::Full
        {
            if let Some(hovered) = self.select.hovered
            {
                let selectable = &self.get_selectable(hovered).unwrap();
                let mut bounds = selectable.bounds.clone();
                bounds.scale(self.applied_zoom, self.applied_zoom);
                bounds.translate(-self.scroll_x, -self.scroll_y);

                if selectable.fill
                {
                    draw_list.add_rect([bounds.x1 as f32, bounds.y1 as f32], [bounds.x2 as f32, bounds.y2 as f32], fill_hover_color)
                        .filled(true).build();
                }

                draw_list.add_rect([bounds.x1 as f32, bounds.y1 as f32], [bounds.x2 as f32, bounds.y2 as f32], border_color)
                    .thickness(selectable.line_thickness()).filled(false).build();
            }
        }

        if self.select.display != SelectionDisplayMode::None
        {
            if let Some(hovered) = self.select.hovered
            {
                let mut name = String::new();

                if let Some(spawnable) = level.spawnable(hovered.layer, hovered.spawnable)
                {
                    if let Some(spawnable_name) = level.spawnable_name_for_id(spawnable.spawn_name_id)
                    {
                        name.push_str(&format!("{}\n", spawnable_name));
                    }
                }

                ui.tooltip_text(format!("{}Layer {}: {}\nObject {}", name, hovered.layer, self.layer_name(level, hovered.layer), hovered.spawnable));
            }
        }

        for id in &self.select.selected
        {
            if let Some(selectable) = self.get_selectable(*id)
            {
                let mut bounds = selectable.bounds.clone();
                bounds.scale(self.applied_zoom, self.applied_zoom);
                bounds.translate(-self.scroll_x, -self.scroll_y);

                if self.select.display == SelectionDisplayMode::Full && selectable.fill
                {
                    draw_list.add_rect([bounds.x1 as f32, bounds.y1 as f32], [bounds.x2 as f32, bounds.y2 as f32], fill_color)
                        .filled(true).build();
                }
                else
                {
                    bounds.extrude(1.0);
                }

                draw_list.add_rect([bounds.x1 as f32, bounds.y1 as f32], [bounds.x2 as f32, bounds.y2 as f32], border_color)
                    .thickness(selectable.line_thickness()).filled(false).build();
            }
        }

        if self.input.box_select_active
        {
            let mut bounds = BakedRect
            {
                x1 : self.input.drag_left_origin_x,
                y1 : self.input.drag_left_origin_y,
                x2 : self.input.mouse_x,
                y2 : self.input.mouse_y
            };

            bounds.make_positive();

            if bounds.w() >= 1.0 || bounds.h() >= 1.0
            {
                bounds.scale(self.applied_zoom, self.applied_zoom);
                bounds.translate(-self.scroll_x, -self.scroll_y);
                bounds.x2 += 1.0;
                bounds.y2 += 1.0;

                draw_list.add_rect([bounds.x1 as f32, bounds.y1 as f32], [bounds.x2 as f32, bounds.y2 as f32], fill_color)
                    .filled(true).build();
                draw_list.add_rect([bounds.x1 as f32, bounds.y1 as f32], [bounds.x2 as f32, bounds.y2 as f32], border_color)
                    .thickness(1.0).filled(false).build();
            }
        }
    }

    fn draw_tool(&mut self, project : &ProjectConfig, ui : &imgui::Ui, level : &mut Level, game : &GameControl)
    {
        self.tool_drawables.clear();

        match self.tool_mode
        {
            ToolMode::Select => {}
            ToolMode::Spawnable =>
            {
                let spawnables_store = game.res().store::<SpawnableSet>();

                if let Some(spawnables) = spawnables_store.get(level.spawnable_set().id())
                {
                    if let Some(spawnable_data) = spawnables.spawnable(self.spawnable_to_place)
                    {
                        let is_actor = match spawnable_data.representation()
                        {
                            Representation::Actor { .. } => { true }
                            _ => { false }
                        };

                        let (mut x, mut y) = self.snap_to_grid(self.input.mouse_x, self.input.mouse_y, is_actor, project);
                        let pulse = (((self.tics as f64 * 0.1).sin() + 1.0) / 2.0) as f32;
                        let color = [pulse, pulse, pulse, 1.0];
                        x = (x * self.applied_zoom) - self.scroll_x;
                        y = (y * self.applied_zoom) - self.scroll_y;

                        let draw_list = ui.get_background_draw_list();

                        draw_list.add_line([x as f32 - SPAWNABLE_CROSSHAIR_SIZE, y as f32],
                                           [x as f32 + SPAWNABLE_CROSSHAIR_SIZE, y as f32],
                                           color).thickness(1.0).build();
                        draw_list.add_line([x as f32, y as f32 - SPAWNABLE_CROSSHAIR_SIZE],
                                           [x as f32, y as f32 + SPAWNABLE_CROSSHAIR_SIZE],
                                           color).thickness(1.0).build();
                    }
                }
            }
            ToolMode::Tile =>
            {
                if let Some(bounds) = &self.tiles.hovered_tile_bounds
                {
                    let pulse = (((self.tics as f64 * 0.1).sin() + 1.0) / 2.0) as f32;
                    let color = [pulse, pulse, pulse, 1.0];
                    let draw_list = ui.get_background_draw_list();

                    self.tool_drawables.push(self.tiles.tile_ghost_drawable.clone());

                    let mut points = bounds.rect.points();

                    for point in &mut points
                    {
                        point.0 += bounds.offset_x;
                        point.1 += bounds.offset_y;
                        point.0 *= self.applied_zoom;
                        point.1 *= self.applied_zoom;
                        point.0 -= self.scroll_x;
                        point.1 -= self.scroll_y;
                    }

                    for i in 0..points.len()
                    {
                        let j = (i + 1) % 4;
                        draw_list.add_line([points[i].0 as f32, points[i].1 as f32], [points[j].0 as f32, points[j].1 as f32], color).build();
                    }
                }
            }
            ToolMode::Layer => {}
            ToolMode::Level => {}
        }
    }
}

impl Document for LevelDocument
{
    fn load(game : &mut GameControl, package_name : &str, item_name : &str) -> Self where Self : Sized
    {
        let mut level_store = game.res().store_mut::<Level>();
        let mut data_id = DataId::new(0);

        let mut scroll_x = -64.0 - ARRANGE_PACKAGES_WIDTH as f64;
        let mut scroll_y = -128.0;
        let mut current_layer = 0;
        let dpi_factor = game.renderer().current_display_scale_factor();
        let applied_zoom = 1.0 / dpi_factor as f64;

        if let Ok(id) = level_store.get_id_mut(package_name, item_name)
        {
            data_id = id;

            if let Some(level) = level_store.get_mut(data_id)
            {
                scroll_x += level.bounds().x1;
                scroll_y += level.bounds().y1;

                let num_layers = level.layer_count();

                if num_layers > 0
                {
                    current_layer = level.layer_count() - 1;
                }
            }
        }

        LevelDocument
        {
            action_history: ActionHistory::new(),
            data_id,
            level_gen : 0,
            tics : 0,
            scroll_x,
            scroll_y,
            zoom_control : ZoomControl::new(ZOOM_LEVELS.to_vec(), 1.0),
            cached_zoom : 1.0,
            applied_zoom,
            dpi_factor,
            input : InputData::new(),
            bg_r : Lerpable::new(0.5),
            bg_g : Lerpable::new(0.5),
            bg_b : Lerpable::new(0.5),
            bg_tween : 0.0,
            initial_bg : true,
            tool_mode : ToolMode::Select,
            tool_button_hover : 0.0,
            tool_button_hovered : false,
            spawnable_to_place : usize::MAX,
            spawnables_gen : 0,
            drawables : Vec::new(),
            scrolling_drawables : Vec::new(),
            tool_drawables : Vec::new(),
            cached_data_dirty : true,
            old_viewport : Rect::default(),
            drawer : DrawableDrawer::new(PixelSnap::SpriteAndCameraSnap),
            current_layer,
            scroll_layer_list : false,
            select : SelectToolData::new(),
            tiles : TilesToolData::new()
        }
    }

    fn do_ui(&mut self, packages : &mut Packages, _config : &mut EditorConfig,
             project : &mut ProjectConfig, game : &mut GameControl, ui : &imgui::Ui) -> DocumentState
    {
        self.tics = self.tics.wrapping_add(1);
        self.select.keep_selection = false;
        self.dpi_factor = game.renderer().current_display_scale_factor();
        self.applied_zoom = self.cached_zoom / self.dpi_factor as f64;

        let mut result = DocumentState::new();
        result.active = ui.is_window_focused_with_flags(imgui::WindowFocusedFlags::CHILD_WINDOWS);

        if result.active && !self.input.was_focused
        {
            self.mark_dirty();
        }

        self.input.was_focused = result.active;

        let mut level_store = game.res().store_mut::<Level>();

        if let Some(level) = level_store.get_mut(self.data_id)
        {
            {
                let spawnables_store = game.res().store_mut::<SpawnableSet>();
                let spawnables_source = level.spawnable_set();

                match spawnables_store.get_id(spawnables_source.package(), spawnables_source.path())
                {
                    Ok(_) =>
                    {
                        level.spawnable_set().resolve_if_loaded(&*spawnables_store);
                    }
                    Err(_) =>
                    {
                        packages.load_package_later(ResourceType::SpawnableSet, spawnables_source.package());
                    }
                };
            }

            self.check_generation_dirty(level, game);
            self.update_cached_object_data(false, packages, level, game); // Update selectables for input
            result.make_active = self.handle_input(project, ui, packages, level, game);
            self.animate_bg_color(level, ui.io().delta_time * 1.5);
            self.draw_bounds(ui, level, game);
            self.update_cached_object_data(true, packages, level, game); // Update selectables again and drawables once
            self.scroll_cached_object_data(packages, level, game);
            self.draw_selection(ui, level);
            self.draw_tool(project, ui, level, game);
        }

        if self.cached_data_dirty && !packages.package_load_queued()
        {
            self.cached_data_dirty = false;
        }

        result
    }

    fn do_detached_ui(&mut self, id : &str, packages : &mut Packages, _config : &mut EditorConfig,
                      project : &mut ProjectConfig, game : &mut GameControl, ui : &imgui::Ui) -> bool
    {
        if id != "level_tab"
        {
            return false;
        }

        let mut level_store = game.res().store_mut::<Level>();
        let spawnables_store = game.res().store_mut::<SpawnableSet>();

        if let Some(level) = level_store.get_mut(self.data_id)
        {
            let spawnables_source = level.spawnable_set().clone();

            self.tool_selector(game, level, project, ui);

            ui.separator();

            crate::util::widget::window_placeholder_text(self.tool_mode.tool_name(), ui);

            ui.spacing();

            if let Some(tool_region) = imgui::ChildWindow::new("tool_region").begin(ui)
            {
                match self.tool_mode
                {
                    ToolMode::Select =>
                    {
                        self.sidebar_select_tool(ui, level, project, &spawnables_store);
                        self.select.display = SelectionDisplayMode::Full;
                    }
                    ToolMode::Spawnable =>
                    {
                        let costume_store = game.res().store_mut::<Costume>();
                        let sheet_store = game.res().store_mut::<Sheet>();

                        self.sidebar_spawnables_tool(ui, level, &spawnables_store, &costume_store, &sheet_store, packages);
                        self.select.display = SelectionDisplayMode::None;
                    }
                    ToolMode::Tile =>
                    {
                        let mut tileset_store = game.res().store_mut::<Tileset>();
                        let sheet_store = game.res().store_mut::<Sheet>();

                        self.sidebar_tiles_tool(ui, packages, level, &mut tileset_store, &sheet_store);
                        self.select.display = SelectionDisplayMode::Outline;
                    }
                    ToolMode::Layer =>
                    {
                        self.sidebar_layer_tool(ui, level);
                        self.select.display = SelectionDisplayMode::Outline;
                    }
                    ToolMode::Level =>
                    {
                        self.sidebar_level_tool(ui, level, &spawnables_source);
                        self.select.display = SelectionDisplayMode::Outline;
                    }
                }

                tool_region.end();
            }
        }

        true
    }

    fn do_draw(&self, _packages : &Packages, _resources : &DataMultistore, drawing : &mut Box<dyn DrawControl>, transform : &DrawTransform, _interpolation : f32)
    {
        let mut transform = transform.clone();

        transform.translate((-self.scroll_x as f32 * self.dpi_factor).round(), (-self.scroll_y as f32 * self.dpi_factor).round());
        transform.scale(self.cached_zoom as f32, self.cached_zoom as f32);

        drawing.draw_background_color(&DrawBackgroundColorInfo
        {
            r : self.bg_r.lerped(self.bg_tween),
            g : self.bg_g.lerped(self.bg_tween),
            b : self.bg_b.lerped(self.bg_tween),
            alpha : 1.0,
            depth : 0.0,
            shader_id : DataId::new(0)
        },
        &DrawOptions
        {
            ordering : DrawOrdering::Painter
        });

        for drawable in &self.drawables
        {
            self.drawer.draw_drawable(drawing, 0.0, &transform, drawable);
        }

        for drawable in &self.tool_drawables
        {
            self.drawer.draw_drawable(drawing, 0.0, &transform, drawable);
        }
    }

    fn action_history(&mut self) -> &mut ActionHistory
    {
        &mut self.action_history
    }

    fn apply_action(&mut self, game : &mut GameControl, action : &Box<dyn Any>) -> bool
    {
        if let Some(action) = action.downcast_ref::<LevelAction>()
        {
            let mut level_store = game.res().store_mut::<Level>();
            let tileset_store = game.res().store_mut::<Tileset>();

            if let Some(level) = level_store.get_mut(self.data_id)
            {
                match action
                {
                    LevelAction::AddSpawnable{ position, spawnable, spawnable_name} =>
                    {
                        let mut spawnable = spawnable.clone();
                        spawnable.spawn_name_id = level.spawnable_name_id_mut(spawnable_name);

                        self.mark_dirty();
                        let success = level.insert_spawnable(position.layer, position.spawnable, spawnable);

                        if success
                        {
                            self.select.hovered = None;

                            if !self.select.keep_selection
                            {
                                self.clear_selection();
                            }
                        }

                        return success;
                    }
                    LevelAction::RemoveSpawnables(ids) =>
                    {
                        let mut to_remove = ids.clone();
                        to_remove.sort_by(|a, b| { b.cmp(a) });

                        for id in &to_remove
                        {
                            level.remove_spawnable(id.layer, id.spawnable);
                        }

                        self.clear_selection();
                        self.select.hovered = None;
                        self.mark_dirty();

                        return true;
                    }
                    LevelAction::RenameSpawnable { id, spawnable_name } =>
                    {
                        self.mark_dirty();

                        let new_id = level.spawnable_name_id_mut(spawnable_name);

                        return level.modify_spawnable(id.layer, id.spawnable, |spawnable|
                        {
                            spawnable.spawn_name_id = new_id;
                        });
                    }
                    LevelAction::MoveSpawnable { id, transform } =>
                    {
                        self.mark_dirty();

                        return level.modify_spawnable(id.layer, id.spawnable, |spawnable|
                        {
                            spawnable.transform = transform.clone();
                        });
                    }
                    LevelAction::MouseMoveSpawnables(spawnables) =>
                    {
                        self.mark_dirty();

                        for moved in spawnables
                        {
                            level.modify_spawnable(moved.id.layer, moved.id.spawnable, |spawnable|
                            {
                                spawnable.transform.x = moved.x;
                                spawnable.transform.y = moved.y;
                            });
                        }

                        return true;
                    }
                    LevelAction::SetSpawnableRepresentation { id, representation } =>
                    {
                        self.mark_dirty();

                        return level.modify_spawnable(id.layer, id.spawnable, |spawnable|
                        {
                            spawnable.representation = representation.clone();
                        });
                    }
                    LevelAction::AddNumberedArg { spawnable_id, arg_pos, arg } =>
                    {
                        return level.modify_spawnable(spawnable_id.layer, spawnable_id.spawnable, |spawnable|
                        {
                            spawnable.modify_args(|args|
                            {
                                let mut new_args = self.select.cached_args_numbered.clone();
                                new_args.insert(*arg_pos, arg.clone());

                                args.set_numbered(new_args);
                            });

                            self.mark_dirty();
                        });
                    }
                    LevelAction::SetNumberedArg { spawnable_id, arg_pos, arg } =>
                    {
                        return level.modify_spawnable(spawnable_id.layer, spawnable_id.spawnable, |spawnable|
                        {
                            spawnable.modify_args(|args|
                            {
                                let mut new_args = self.select.cached_args_numbered.clone();

                                if let Some(old_arg) = new_args.get_mut(*arg_pos)
                                {
                                    *old_arg = arg.clone();
                                }

                                args.set_numbered(new_args);
                            });

                            self.mark_dirty();
                        });
                    }
                    LevelAction::RemoveNumberedArg { spawnable_id, arg_pos } =>
                    {
                        return level.modify_spawnable(spawnable_id.layer, spawnable_id.spawnable, |spawnable|
                        {
                            spawnable.modify_args(|args|
                            {
                                let mut new_args = self.select.cached_args_numbered.clone();
                                new_args.remove(*arg_pos);

                                args.set_numbered(new_args);
                            });

                            if spawnable.args_empty()
                            {
                                spawnable.args = None;
                            }

                            self.mark_dirty();
                        });
                    }
                    LevelAction::MoveNumberedArg { spawnable_id, arg_pos_old, arg_pos_new } =>
                    {
                        return level.modify_spawnable(spawnable_id.layer, spawnable_id.spawnable, |spawnable|
                        {
                            spawnable.modify_args(|args|
                            {
                                let mut new_args = self.select.cached_args_numbered.clone();
                                let popped = new_args.remove(*arg_pos_old);
                                new_args.insert(*arg_pos_new, popped);

                                args.set_numbered(new_args);
                            });

                            self.mark_dirty();
                        });
                    }
                    LevelAction::AddNamedArg { spawnable_id, arg_name, arg } =>
                    {
                        return level.modify_spawnable(spawnable_id.layer, spawnable_id.spawnable, |spawnable|
                        {
                            spawnable.modify_args(|args|
                            {
                                args.push_named(arg_name.clone(), arg.clone());
                            });

                            self.mark_dirty();
                        });
                    }
                    LevelAction::RemoveNamedArg { spawnable_id, arg_name } =>
                    {
                        return level.modify_spawnable(spawnable_id.layer, spawnable_id.spawnable, |spawnable|
                        {
                            spawnable.modify_args(|args|
                            {
                                args.remove_named(arg_name);
                            });

                            if spawnable.args_empty()
                            {
                                spawnable.args = None;
                            }

                            self.mark_dirty();
                        });
                    }
                    LevelAction::SetTileset { spawnable_id, tileset } =>
                    {
                        return level.modify_spawnable(spawnable_id.layer, spawnable_id.spawnable, |spawnable|
                        {
                            if let LevelRepresentation::Tilemap { tiles } = &mut spawnable.representation
                            {
                                tiles.set_tileset(tileset.clone());
                            }

                            self.mark_dirty();
                        });
                    }
                    LevelAction::SetTilemapTiles { spawnable_id, changed_tiles } =>
                    {
                        return level.modify_spawnable(spawnable_id.layer, spawnable_id.spawnable, |spawnable|
                        {
                            if let LevelRepresentation::Tilemap { tiles } = &mut spawnable.representation
                            {
                                for tile in changed_tiles
                                {
                                    tiles.grid_mut().set_tile(tile.x, tile.y, tile.tile);
                                }
                            }

                            self.mark_dirty();
                        });
                    }
                    LevelAction::ResizeTilemap { spawnable_id, new_w, new_h, nudge_x, nudge_y, position_x, position_y } =>
                    {
                        return level.modify_spawnable(spawnable_id.layer, spawnable_id.spawnable, |spawnable|
                        {
                            if let LevelRepresentation::Tilemap { tiles } = &mut spawnable.representation
                            {
                                (*tiles.grid_mut()) = tiles.grid().to_resized(*new_w, *new_h, *nudge_x as usize, *nudge_y as usize);

                                let (tile_w, tile_h) = crate::util::drawable::tile_size(tiles, &tileset_store);

                                spawnable.transform.x += *position_x as f64 * tile_w;
                                spawnable.transform.y += *position_y as f64 * tile_h;
                            }

                            self.mark_dirty();
                        });
                    }
                    LevelAction::AddLayer { pos, layer } =>
                    {
                        let success = level.insert_layer(*pos, layer.clone());

                        if success
                        {
                            self.clear_selection();
                            self.select.hovered = None;
                            self.scroll_layer_list = true;
                            self.mark_dirty();
                        }

                        return success;
                    }
                    LevelAction::RemoveLayer(id) =>
                    {
                        let old_layer = level.remove_layer(*id);

                        if old_layer.is_some()
                        {
                            self.clear_selection();
                            self.select.hovered = None;
                            self.scroll_layer_list = true;
                            self.mark_dirty();
                        }

                        return old_layer.is_some();
                    }
                    LevelAction::RenameLayer { id, name } =>
                    {
                        let success = level.rename_layer(*id, name);

                        self.mark_dirty();

                        return success;
                    }
                    LevelAction::MoveLayer { old_pos, new_pos } =>
                    {
                        let old_name = level.layer_name(*old_pos);

                        if let Some(old_layer) = level.remove_layer(*old_pos)
                        {
                            if level.insert_layer(*new_pos, old_layer)
                            {
                                if let Some(name) = old_name
                                {
                                    level.rename_layer(*new_pos, &name);
                                }

                                self.clear_selection();
                                self.select.hovered = None;
                                self.scroll_layer_list = true;
                                self.mark_dirty();

                                return true;
                            }
                        }

                        return false;
                    }
                    LevelAction::SetLayerVisible { id, visible } =>
                    {
                        let success = level.modify_layer(*id, |layer|
                        {
                            layer.set_visible(*visible);
                            self.mark_dirty();
                        });

                        return success;
                    }
                    LevelAction::SetLayerLocked { id, locked } =>
                    {
                        let success = level.modify_layer(*id, |layer|
                        {
                            layer.set_locked(*locked);
                            self.mark_dirty();
                        });

                        return success;
                    }
                    LevelAction::SetLayerParameters { id, z_offset, scroll_rate } =>
                    {
                        let success = level.modify_layer(*id, |layer|
                        {
                            layer.set_z_offset(*z_offset);
                            layer.set_scroll_rate(*scroll_rate);
                            self.mark_dirty();
                        });

                        return success;
                    }
                    LevelAction::SetSpawnableSet(handle) =>
                    {
                        level.set_spawnable_set(handle.clone());
                        self.mark_dirty();
                        return true;
                    }
                    LevelAction::SetBackground(bg) =>
                    {
                        level.set_background(bg.clone());
                        return true;
                    }
                    LevelAction::SetBounds(bounds) =>
                    {
                        level.set_bounds(bounds.clone());
                        return true;
                    }
                }
            }
        }

        false
    }
}
