use keeshond::renderer::Sheet;
use keeshond::util::{BakedRect, Rect};
use keeshond::datapack::{DataHandle, DataId, DataStore};
use keeshond_treats::tilemap::{Tilemap, TilemapGrid, Tileset};
use keeshond_treats::visual::{AnimationCel, Costume, DrawableItem, LerpedTransform, MultiTileSprite, TileSprite};

use crate::editorgui::{Packages, ResourceType};
use crate::util::BoxFitResult;

pub fn sheet_slice_image(sheet : &DataHandle<Sheet>, slice : &str, sheet_store : &DataStore<Sheet>,
                         packages : &mut Packages) -> Option<(imgui::Image, Rect<f32>)>
{
    let (package, sheet_name) = (sheet.package(), sheet.path());

    if package.is_empty() || sheet_name.is_empty()
    {
        return None;
    }

    if let Ok(sheet_id) = sheet_store.get_id(package, sheet_name)
    {
        if let Some(sheet) = sheet_store.get(sheet_id)
        {
            if let Some(slice_id) = sheet.slice_id(slice)
            {
                if let Some(slice) = sheet.slice(slice_id)
                {
                    let (w, h) = (slice.texture_w, slice.texture_h);
                    let rect = crate::util::sheet::slice_to_uv(sheet, slice);

                    return Some((imgui::Image::new(imgui::TextureId::from(sheet_id.index()), [w, h])
                                     .uv0([rect.x1, rect.y1]).uv1([rect.x2, rect.y2]), Rect
                    {
                        x : slice.origin_x, y : slice.origin_y, w, h
                    }));
                }
            }
        }
    }
    else
    {
        packages.load_package_later(ResourceType::Sheet, package);
    }

    None
}

pub fn sheet_slice(sheet : &DataHandle<Sheet>, slice : &str, sheet_store : &mut DataStore<Sheet>,
                   packages : &mut Packages) -> Option<(DataId<Sheet>, usize)>
{
    let (package, sheet_name) = (sheet.package(), sheet.path());

    if package.is_empty() || sheet_name.is_empty()
    {
        return None;
    }

    if let Ok(sheet_id) = sheet_store.get_id(package, sheet_name)
    {
        if let Some(sheet) = sheet_store.get(sheet_id)
        {
            if let Some(slice_id) = sheet.slice_id(slice)
            {
                return Some((sheet_id, slice_id));
            }
        }
    }
    else
    {
        packages.load_package_later(ResourceType::Sheet, package);
    }

    None
}

pub fn cel_image(cel : &AnimationCel, sheet_store : &DataStore<Sheet>, packages : &mut Packages) -> Option<(imgui::Image, Rect<f32>)>
{
    if let Some((image, mut rect)) = sheet_slice_image(&cel.sheet, cel.slice.slice(), sheet_store, packages)
    {
        rect.x -= cel.x;
        rect.y -= cel.y;

        return Some((image, rect));
    }

    None
}

pub fn cel_sheet_slice(cel : &AnimationCel, sheet_store : &mut DataStore<Sheet>, packages : &mut Packages) -> Option<(DataId<Sheet>, usize)>
{
    let (package, sheet_name) = (cel.sheet.package(), cel.sheet.path());

    if package.is_empty() || sheet_name.is_empty()
    {
        return None;
    }

    if let Ok(sheet_id) = sheet_store.get_id(package, sheet_name)
    {
        if let Some(sheet) = sheet_store.get(sheet_id)
        {
            if let Some(slice_id) = sheet.slice_id(&cel.slice.slice())
            {
                return Some((sheet_id, slice_id));
            }
        }
    }
    else
    {
        packages.load_package_later(ResourceType::Sheet, package);
    }

    None
}

pub fn cel_image_fit(cel : &AnimationCel, width : f32, height : f32,
                     sheet_store : &mut DataStore<Sheet>, packages : &mut Packages) -> Option<(imgui::Image, BoxFitResult<f32>)>
{
    if let Some((mut image, rect)) = cel_image(cel, sheet_store, packages)
    {
        let fit = crate::util::box_fit(rect.w, rect.h, width, height);

        image = image.size([fit.width, fit.height]);
        return Some((image, fit));
    }

    None
}

pub fn costume_image(costume : &DataHandle<Costume>, costume_store : &DataStore<Costume>,
                     sheet_store : &DataStore<Sheet>, packages : &mut Packages) -> Option<(imgui::Image, Rect<f32>)>
{
    if costume_store.package_loaded(costume.package())
    {
        let costume_id = costume.resolve_if_loaded(costume_store);

        if let Some(costume) = costume_store.get(costume_id)
        {
            if let Some(anim) = costume.animation(0)
            {
                if let Some(cel) = anim.cel(0, 0)
                {
                    return cel_image(cel, sheet_store, packages);
                }
            }
        }
    }
    else
    {
        packages.load_package_later(ResourceType::Costume, costume.package());
    }

    None
}

pub fn costume_sheet_slice(costume : &DataHandle<Costume>, costume_store : &mut DataStore<Costume>,
                           sheet_store : &mut DataStore<Sheet>, packages : &mut Packages) -> Option<(DataId<Sheet>, usize)>
{
    if costume_store.package_loaded(costume.package())
    {
        let costume_id = costume.resolve_if_loaded(costume_store);

        if let Some(costume) = costume_store.get(costume_id)
        {
            if let Some(anim) = costume.animation(0)
            {
                if let Some(cel) = anim.cel(0, 0)
                {
                    return cel_sheet_slice(cel, sheet_store, packages);
                }
            }
        }
    }
    else
    {
        packages.load_package_later(ResourceType::Costume, costume.package());
    }

    None
}

pub fn tileset_image_from_slice(tileset : &mut Tileset, texture_id : usize, sheet_opt : Option<&Sheet>, slice : usize, image_size : [f32; 2]) -> imgui::Image
{
    if let Some(sheet) = sheet_opt
    {
        if let Some(index) = tileset.updated_slice_id(slice, sheet)
        {
            if let Some(slice) = sheet.slice(index)
            {
                return imgui::Image::new(imgui::TextureId::from(texture_id), image_size)
                    .uv0([slice.texture_x / sheet.width() as f32,
                        slice.texture_y / sheet.height() as f32])
                    .uv1([(slice.texture_x + slice.texture_w) / sheet.width() as f32,
                        (slice.texture_y + slice.texture_h) / sheet.height() as f32]);
            }
            else
            {
                return imgui::Image::new(imgui::TextureId::from(0), image_size);
            }
        }
        else
        {
            return imgui::Image::new(imgui::TextureId::from(usize::MAX), image_size)
                .uv0([0.0, 0.0]).uv1([0.0, 0.0]);
        }
    }

    return imgui::Image::new(imgui::TextureId::from(0), image_size);
}

pub fn tile_drawable_item(tileset : &DataHandle<Tileset>, tile_index : u32, tileset_store : &mut DataStore<Tileset>,
                          sheet_store : &mut DataStore<Sheet>, packages : &mut Packages) -> DrawableItem
{
    if tileset_store.package_loaded(tileset.package())
    {
        if let Some(tileset) = tileset_store.get_mut(tileset.resolve_if_loaded(tileset_store))
        {
            if sheet_store.package_loaded(tileset.sheet().package())
            {
                if let Ok(sheet_id) = sheet_store.get_id(tileset.sheet().package(), tileset.sheet().path())
                {
                    if let Some(sheet) = sheet_store.get(sheet_id)
                    {
                        if let Some(tile) = TilemapGrid::drawable_positioned_tile(tileset, sheet, 0.0, 0.0, tile_index)
                        {
                            let (tile_w, tile_h) = tileset.tile_size();

                            return DrawableItem::TileSprite(TileSprite
                            {
                                texture_x : tile.texture_x,
                                texture_y : tile.texture_y,
                                texture_w : tile.texture_w,
                                texture_h : tile.texture_h,
                                tile_w : tile_w as f32,
                                tile_h : tile_h as f32,
                                offset_x : 0.0,
                                offset_y : 0.0,
                                sheet_id,
                                shader_id : DataId::new(0),
                                r : 1.0,
                                g : 1.0,
                                b : 1.0,
                                alpha : 1.0
                            });
                        }
                    }
                }
            }
            else
            {
                packages.load_package_later(ResourceType::Sheet, tileset.sheet().package());
            }
        }
    }
    else
    {
        packages.load_package_later(ResourceType::Tileset, tileset.package());
    }

    return DrawableItem::None;
}

pub fn tilemap_drawable_item(tilemap : &Tilemap, transform : &LerpedTransform<f32>, screen_rect : BakedRect<f64>,
                             tileset_store : &mut DataStore<Tileset>, sheet_store : &mut DataStore<Sheet>,
                             packages : &mut Packages) -> DrawableItem
{
    if tileset_store.package_loaded(tilemap.tileset().package())
    {
        if let Some(tileset) = tileset_store.get_mut(tilemap.tileset().resolve_if_loaded(tileset_store))
        {
            if sheet_store.package_loaded(tileset.sheet().package())
            {
                if let Ok(sheet_id) = sheet_store.get_id(tileset.sheet().package(), tileset.sheet().path())
                {
                    let mut sprite = MultiTileSprite::new();
                    sprite.sheet_id = sheet_id;
                    sprite.offset_x = tilemap.offset_x() as f32;
                    sprite.offset_y = tilemap.offset_y() as f32;

                    if let Some(sheet) = sheet_store.get(sheet_id)
                    {
                        let tilemap_rect = tilemap.grid().tile_range_for_screen_bounds(transform,
                                                tilemap.offset_x(), tilemap.offset_y(), screen_rect, tileset);
                        tilemap.grid().update_sprite_layout(tileset, sheet, &mut sprite, &tilemap_rect);

                        return DrawableItem::MultiTileSprite(sprite);
                    }
                }
            }
            else
            {
                packages.load_package_later(ResourceType::Sheet, tileset.sheet().package());
            }
        }
    }
    else
    {
        packages.load_package_later(ResourceType::Tileset, tilemap.tileset().package());
    }

    return DrawableItem::None;
}

pub fn tile_size(tilemap : &Tilemap, tileset_store : &DataStore<Tileset>) -> (f64, f64)
{
    let mut tile_w = 16.0;
    let mut tile_h = 16.0;

    if let Some(tileset) = tilemap.tileset().get_if_resolved(tileset_store)
    {
        let (w, h) = tileset.tile_size();
        tile_w = w as f64;
        tile_h = h as f64;
    }
    (tile_w, tile_h)
}
