const MOUSE_WHEEL_ZOOM_DISTANCE : f32 = 1.0;
pub const EDITOR_ZOOM_LEVELS : [f32 ; 11] = [0.25, 0.5, 1.0, 2.0, 3.0, 4.0, 6.0, 8.0, 10.0, 12.0, 16.0];

pub struct ZoomControl<T : Copy + PartialOrd>
{
    zoom_levels : Vec<T>,
    zoom : T,
    mouse_wheel_buffer : f32
}

impl<T : Copy + PartialOrd> ZoomControl<T>
{
    pub fn new(zoom_levels : Vec<T>, default : T) -> ZoomControl<T>
    {
        ZoomControl
        {
            zoom_levels,
            zoom : default,
            mouse_wheel_buffer : 0.0
        }
    }

    pub fn update_mouse_wheel(&mut self, wheel : f32) -> bool
    {
        let mut zoom_changed = false;
        self.mouse_wheel_buffer += wheel;

        while self.mouse_wheel_buffer >= MOUSE_WHEEL_ZOOM_DISTANCE
        {
            if self.zoom_in()
            {
                zoom_changed = true;
                self.mouse_wheel_buffer -= MOUSE_WHEEL_ZOOM_DISTANCE;
            }
            else
            {
                self.mouse_wheel_buffer = 0.0;
            }
        }
        while self.mouse_wheel_buffer <= -MOUSE_WHEEL_ZOOM_DISTANCE
        {
            if self.zoom_out()
            {
                zoom_changed = true;
                self.mouse_wheel_buffer += MOUSE_WHEEL_ZOOM_DISTANCE;
            }
            else
            {
                self.mouse_wheel_buffer = 0.0;
            }
        }

        zoom_changed
    }

    pub fn zoom(&self) -> T
    {
        self.zoom
    }

    pub fn set_zoom(&mut self, zoom : T)
    {
        self.zoom = zoom;
    }

    pub fn zoom_in(&mut self) -> bool
    {
        for zoom_level in &self.zoom_levels
        {
            if *zoom_level > self.zoom
            {
                self.zoom = *zoom_level;
                return true;
            }
        }

        return false;
    }

    pub fn zoom_out(&mut self) -> bool
    {
        let mut lower_zoom = self.zoom_levels[0];

        if self.zoom <= lower_zoom
        {
            return false;
        }

        for zoom_level in &self.zoom_levels
        {
            if *zoom_level >= self.zoom
            {
                self.zoom = lower_zoom;
                return true;
            }

            lower_zoom = *zoom_level;
        }

        return false;
    }
}