use keeshond::renderer::Sheet;
use keeshond::datapack::{DataHandle, DataId, DataStore};
use keeshond_treats::spawnable::{Representation, Spawnable, SpawnableSet};
use keeshond_treats::visual::Costume;
use log::Level;

use crate::editorgui::{Packages, ResourceType};

pub const ICON_PACKAGE : &'static str = "a";
pub const ICON_SHEET : &'static str = "b";
pub const ICON_COSTUME : &'static str = "c";
pub const ICON_ACTOR : &'static str = "d";
pub const ICON_TILES : &'static str = "e";
#[allow(dead_code)] pub const ICON_SHADER : &'static str = "f";
#[allow(dead_code)] pub const ICON_SOUND : &'static str = "g";
#[allow(dead_code)] pub const ICON_MUSIC : &'static str = "h";
pub const ICON_SPAWNABLE : &'static str = "i";
pub const ICON_LEVEL : &'static str = "j";
#[allow(dead_code)] pub const ICON_PLAY : &'static str = "k";
#[allow(dead_code)] pub const ICON_STOP : &'static str = "l";
#[allow(dead_code)] pub const ICON_PAUSE : &'static str = "m";
#[allow(dead_code)] pub const ICON_SEEK_BACK : &'static str = "n";
#[allow(dead_code)] pub const ICON_SEEK_NEXT : &'static str = "o";
#[allow(dead_code)] pub const ICON_VISIBLE : &'static str = "p";
#[allow(dead_code)] pub const ICON_SETTINGS : &'static str = "q";
#[allow(dead_code)] pub const ICON_FOLDER : &'static str = "r";
pub const ICON_MARKER : &'static str = "s";
#[allow(dead_code)] pub const ICON_STRING : &'static str = "t";
#[allow(dead_code)] pub const ICON_ADD : &'static str = "u";
#[allow(dead_code)] pub const ICON_REMOVE : &'static str = "v";
#[allow(dead_code)] pub const ICON_LEFT : &'static str = "w";
#[allow(dead_code)] pub const ICON_RIGHT : &'static str = "x";
#[allow(dead_code)] pub const ICON_UP : &'static str = "y";
#[allow(dead_code)] pub const ICON_DOWN : &'static str = "z";
#[allow(dead_code)] pub const ICON_GRID : &'static str = "A";
#[allow(dead_code)] pub const ICON_SNAP : &'static str = "B";
#[allow(dead_code)] pub const ICON_PATH : &'static str = "C";
#[allow(dead_code)] pub const ICON_STAR : &'static str = "D";
pub const ICON_CURSOR : &'static str = "E";
pub const ICON_DRAW : &'static str = "F";
pub const ICON_LAYER : &'static str = "G";
pub const ICON_LOCKED : &'static str = "H";
#[allow(dead_code)] pub const ICON_UNLOCKED : &'static str = "I";

pub const ICON_FONT_SIZE : f32 = 16.0;
pub const REPRESENTATION_ICON_SIZE : f32 = 32.0;

pub const REPRESENTATION_NAMES : [&'static str; 4] = ["None", "Actor", "Tilemap", "Prop"];

pub fn representation_font_icon(representation : &Representation) -> &'static str
{
    match representation
    {
        Representation::None => { ICON_MARKER }
        Representation::Prop { .. } => { ICON_SHEET }
        Representation::Actor { .. } => { ICON_ACTOR }
        Representation::Tilemap => { ICON_TILES }
    }
}

pub fn round_cursor_screen_pos(ui : &imgui::Ui)
{
    let cursor_screen_pos = ui.cursor_screen_pos();

    ui.set_cursor_screen_pos(
    [
        crate::util::scaled_round(cursor_screen_pos[0], 1.0 / ui.io().display_framebuffer_scale[0]),
        crate::util::scaled_round(cursor_screen_pos[1], 1.0 / ui.io().display_framebuffer_scale[1])
    ]);
}

pub fn hover_hint(text : &str, ui : &imgui::Ui)
{
    ui.same_line();
    ui.text_disabled("(?)");

    if ui.is_item_hovered()
    {
        ui.tooltip_text(text);
    }
}

pub fn window_placeholder_text(text : &str, ui : &imgui::Ui)
{
    let size = ui.calc_text_size(text);
    let avail = ui.content_region_avail();
    let mut cursor_pos = ui.cursor_pos();
    cursor_pos[0] += avail[0] / 2.0 - size[0] / 2.0;

    ui.set_cursor_pos(cursor_pos);
    ui.text_disabled(text);
}

pub fn right_aligned_label(text : &str, frame_padding : bool, ui : &imgui::Ui)
{
    let size = ui.calc_text_size(text);
    let style = ui.clone_style();

    let cursor_pos = ui.cursor_pos();
    let mut advance = 0.0;
    let avail_width = ui.current_column_width() - style.item_spacing[0] - 1.0;

    if size[0] < avail_width
    {
        advance = avail_width - size[0];
    }

    ui.set_cursor_pos([cursor_pos[0] + advance, cursor_pos[1]]);

    if frame_padding
    {
        ui.align_text_to_frame_padding();
    }

    ui.text(text);
}

pub fn underline_current_item(drop : f32, extend : f32, thickness : f32, ui : &imgui::Ui)
{
    let draw_list = ui.get_window_draw_list();
    let color = ui.style_color(imgui::StyleColor::ButtonActive);
    let size_min = ui.item_rect_min();
    let size_max = ui.item_rect_max();
    draw_list.add_line([size_min[0] - extend, size_max[1] + drop],
                       [size_max[0] + extend, size_max[1] + drop], color).thickness(thickness).build();
}

pub fn column_separator(ui : &imgui::Ui)
{
    let draw_list = ui.get_window_draw_list();
    let color = ui.style_color(imgui::StyleColor::Separator);
    let pos = ui.cursor_screen_pos();
    let width = ui.window_content_region_width();
    draw_list.add_line([pos[0], pos[1]],
                       [pos[0] + width, pos[1]], color).thickness(1.0).build();
}

pub fn error_text_color(ui : &imgui::Ui) -> [f32; 4]
{
    let mut color = ui.style_color(imgui::StyleColor::Text);

    color[0] = (color[0] + 2.0) / 3.0;
    color[1] /= 3.0;
    color[2] /= 3.0;
    color[3] = 1.0;

    color
}

pub fn label_color_for_log_level(level : log::Level, ui : &imgui::Ui) -> [f32; 4]
{
    let mut color = ui.style_color(imgui::StyleColor::Text);

    match level
    {
        Level::Error =>
        {
            color[0] = (color[0] + 2.0) / 3.0;
            color[1] /= 3.0;
            color[2] /= 3.0;
        }
        Level::Warn =>
        {
            color[0] = (color[0] + 2.0) / 3.0;
            color[1] = (color[1] + 1.0) / 3.0;
            color[2] /= 3.0;
        }
        Level::Info =>
        {
            color[0] /= 3.0;
            color[1] = (color[1] + 1.0) / 3.0;
            color[2] = (color[2] + 2.0) / 3.0;
        }
        Level::Debug =>
        {
            color[0] = (color[0] + 1.5) / 3.0;
            color[1] /= 3.0;
            color[2] = (color[2] + 2.0) / 3.0;
        }
        Level::Trace =>
        {
            color[0] /= 3.0;
            color[1] = (color[1] + 2.0) / 3.0;
            color[2] /= 3.0;
        }
    }

    color[3] = 1.0;

    color
}

pub fn text_color_for_log_level(level : log::Level, ui : &imgui::Ui) -> [f32; 4]
{
    let mut color = ui.style_color(imgui::StyleColor::Text);

    match level
    {
        Level::Error | Level::Warn =>
        {
            color[3] = 1.0;
        }
        _ =>
        {
            color[3] = 0.7;
        }
    }

    color
}

pub fn get_icon_font(ui : &imgui::Ui) -> imgui::FontId
{
    ui.fonts().fonts()[ui.fonts().fonts().len() - 1]
}

pub fn icon_label(icon : &str, name : &str, ui : &imgui::Ui)
{
    let old_cursor = ui.cursor_pos();
    let height = ui.current_font().font_size;
    let icon_font = get_icon_font(ui);

    let font = ui.push_font(icon_font);
    let icon_size = ui.calc_text_size(icon);
    ui.set_cursor_pos([old_cursor[0] + ((ICON_FONT_SIZE - icon_size[0]) / 2.0), old_cursor[1]]);

    ui.text(icon);
    font.pop();

    ui.same_line();
    ui.set_cursor_pos([old_cursor[0] + ICON_FONT_SIZE + 6.0, old_cursor[1] + ((icon_size[1] - height) / 2.0)]);
    ui.text(name);
}

pub fn sheet_icon_label_fit(sheet : &DataHandle<Sheet>, sheet_store : &DataStore<Sheet>, packages : &mut Packages, ui : &imgui::Ui)
{
    let old_cursor = ui.cursor_pos();
    let height = ui.current_font().font_size;
    let icon_size = ui.item_rect_size()[1] - 2.0;

    ui.set_cursor_pos([old_cursor[0] - 3.0, old_cursor[1]]);
    sheet_icon(sheet, sheet_store, packages, ui);

    ui.same_line();
    ui.set_cursor_pos([old_cursor[0] + icon_size + 6.0, old_cursor[1] + ((icon_size - height) / 2.0)]);
    ui.text(sheet.path());
}

fn sheet_icon(sheet : &DataHandle<Sheet>, sheet_store : &DataStore<Sheet>, packages : &mut Packages, ui : &imgui::Ui)
{
    let (icon_min, icon_max) = (ui.item_rect_min(), ui.item_rect_max());
    let icon_size = icon_max[1] - icon_min[1] - 2.0;

    if ui.is_rect_visible(icon_min, icon_max)
    {
        if sheet_store.package_loaded(sheet.package())
        {
            let sheet_id = sheet.resolve_if_loaded(sheet_store);

            if let Some(sheet) = sheet_store.get(sheet_id)
            {
                let fit = crate::util::box_fit(sheet.width() as f32, sheet.height() as f32, icon_size, icon_size);
                let cursor_pos = ui.cursor_pos();
                ui.set_cursor_pos([cursor_pos[0] + fit.x + 1.0, cursor_pos[1] + fit.y + 1.0]);

                imgui::Image::new(imgui::TextureId::from(sheet_id.index()), [fit.width, fit.height]).build(ui);
            }
        }
        else
        {
            packages.load_package_later(ResourceType::Sheet, sheet.package());
        }
    }
}

pub fn costume_icon_label_fit(costume : &DataHandle<Costume>, costume_store : &DataStore<Costume>,
                              sheet_store : &DataStore<Sheet>, packages : &mut Packages, ui : &imgui::Ui)
{
    let old_cursor = ui.cursor_pos();
    let height = ui.current_font().font_size;
    let icon_size = ui.item_rect_size()[1] - 2.0;

    ui.set_cursor_pos([old_cursor[0] - 3.0, old_cursor[1]]);
    costume_icon(costume, costume_store, sheet_store, packages, ui);

    ui.same_line();
    ui.set_cursor_pos([old_cursor[0] + icon_size + 6.0, old_cursor[1] + ((icon_size - height) / 2.0)]);
    ui.text(costume.path());
}

fn costume_icon(costume : &DataHandle<Costume>, costume_store : &DataStore<Costume>,
                           sheet_store : &DataStore<Sheet>, packages : &mut Packages, ui : &imgui::Ui)
{
    let (icon_min, icon_max) = (ui.item_rect_min(), ui.item_rect_max());
    let icon_size = icon_max[1] - icon_min[1] - 2.0;

    if ui.is_rect_visible(icon_min, icon_max)
    {
        if let Some((mut image, rect)) = crate::util::drawable::costume_image(costume, costume_store, sheet_store, packages)
        {
            let fit = crate::util::box_fit(rect.w, rect.h, icon_size, icon_size);
            let cursor_pos = ui.cursor_pos();
            ui.set_cursor_pos([cursor_pos[0] + fit.x + 1.0, cursor_pos[1] + fit.y + 1.0]);

            image = image.size([fit.width, fit.height]);
            image.build(ui);
        }
    }
}

pub fn representation_icon_label(spawnable_opt : Option<&Spawnable>, name : &str, costume_store : &DataStore<Costume>,
                                 sheet_store : &DataStore<Sheet>, packages : &mut Packages, ui : &imgui::Ui)
{
    let old_cursor = ui.cursor_pos();
    let height = ui.current_font().font_size;

    ui.set_cursor_pos([old_cursor[0], old_cursor[1]]);

    if let Some(spawnable) = spawnable_opt
    {
        representation_icon(spawnable, costume_store, sheet_store, packages, ui);
    }

    ui.same_line();
    ui.set_cursor_pos([old_cursor[0] + REPRESENTATION_ICON_SIZE + 8.0, old_cursor[1] + ((REPRESENTATION_ICON_SIZE - height) / 2.0)]);
    ui.text(name);
}

pub fn representation_icon(spawnable : &Spawnable, costume_store : &DataStore<Costume>,
                           sheet_store : &DataStore<Sheet>, packages : &mut Packages, ui : &imgui::Ui)
{
    let (icon_min, icon_max) = (ui.item_rect_min(), ui.item_rect_max());
    let mut image_info = None;

    if ui.is_rect_visible(icon_min, icon_max)
    {
        match spawnable.representation()
        {
            Representation::Prop { sheet, slice } =>
            {
                image_info = crate::util::drawable::sheet_slice_image(sheet, slice, sheet_store, packages);
            }
            Representation::Actor { costume } =>
            {
                image_info = crate::util::drawable::costume_image(costume, costume_store, sheet_store, packages);
            }
            _ => {}
        }

        if let Some((mut image, rect)) = image_info
        {
            let fit = crate::util::box_fit(rect.w, rect.h, REPRESENTATION_ICON_SIZE, REPRESENTATION_ICON_SIZE);
            let cursor_pos = ui.cursor_pos();
            ui.set_cursor_pos([cursor_pos[0] + fit.x, cursor_pos[1] + fit.y]);

            image = image.size([fit.width, fit.height]);
            image.build(ui);
        }
        else
        {
            let draw_list = ui.get_window_draw_list();
            let old_cursor = ui.cursor_pos();
            let screen_cursor = ui.cursor_screen_pos();

            let style = ui.clone_style();
            let outside_color = style.colors[imgui::StyleColor::Button as usize];
            let start = [screen_cursor[0], screen_cursor[1]];
            let end = [screen_cursor[0] + REPRESENTATION_ICON_SIZE, screen_cursor[1] + REPRESENTATION_ICON_SIZE];

            draw_list.add_rect(start, end, outside_color).filled(true).rounding(2.0).build();
            draw_list.add_rect(start, end, outside_color).filled(false).thickness(1.0).rounding(2.0).build();

            let icon = representation_font_icon(spawnable.representation());
            let icon_font = get_icon_font(ui);
            let font = ui.push_font(icon_font);
            let icon_size = ui.calc_text_size(icon);

            ui.set_cursor_pos([old_cursor[0] + ((REPRESENTATION_ICON_SIZE - icon_size[0]) / 2.0),
                old_cursor[1] + ((REPRESENTATION_ICON_SIZE - icon_size[1]) / 2.0)]);

            ui.text(icon);

            font.pop();
        }
    }
}

pub fn data_path_edit(ui : &imgui::Ui, label : &str, package : &mut String, name : &mut String) -> bool
{
    let mut changed = false;

    let width = ui.push_item_width(120.0);
    changed |= ui.input_text(format!("/###{} Package", label), package).build();
    width.pop(ui);

    ui.same_line_with_spacing(0.0, 4.0);

    let width = ui.push_item_width(160.0);
    changed |= ui.input_text(format!("{} Package/Name", label), name).build();
    width.pop(ui);

    changed
}

pub fn sheet_slice_popup(packages : &mut Packages, ui : &imgui::Ui, sheet_store : &DataStore<Sheet>) -> Option<(String, String, String)>
{
    let mut picked_sheet = None;

    ui.popup("Pick Sheet Slice Menu", ||
    {
        let mut package_to_load: Option<String> = None;

        for package in packages.package_iter()
        {
            ui.menu(&package, ||
            {
                if !packages.package_loaded(ResourceType::Sheet, package)
                {
                    package_to_load = Some(package.clone());
                }

                for sheet_name in packages.package_contents(ResourceType::Sheet, package)
                {
                    ui.menu(&sheet_name, ||
                    {
                        let sheet_id = sheet_store.get_id(package, sheet_name).unwrap_or(DataId::new(0));

                        if let Some(sheet) = sheet_store.get(sheet_id)
                        {
                            for slice_id in 0..sheet.slice_count()
                            {
                                if let Some(slice_name) = sheet.slice_name(slice_id)
                                {
                                    if imgui::Selectable::new(&slice_name).build(ui)
                                    {
                                        picked_sheet = Some((package.clone(), sheet_name.clone(), slice_name.clone()));
                                    }
                                }
                            }
                        }
                    });
                }
            });
        }

        if let Some(package) = package_to_load
        {
            packages.load_package_later(ResourceType::Sheet, &package);
        }
    });

    if ui.button("Pick Sheet")
    {
        ui.open_popup("Pick Sheet Slice Menu");
    }

    picked_sheet
}

pub fn costume_popup(packages : &mut Packages, ui : &imgui::Ui) -> Option<(String, String)>
{
    let mut picked_costume = None;

    ui.popup("Pick Costume Menu", ||
    {
        let mut package_to_load: Option<String> = None;

        for package in packages.package_iter()
        {
            ui.menu(package, ||
            {
                if !packages.package_loaded(ResourceType::Costume, package)
                {
                    package_to_load = Some(package.clone());
                }

                for costume in packages.package_contents(ResourceType::Costume, package)
                {
                    if imgui::Selectable::new(costume).build(ui)
                    {
                        picked_costume = Some((package.clone(), costume.clone()));
                    }
                }
            });
        }

        if let Some(package) = package_to_load
        {
            packages.load_package_later(ResourceType::Costume, &package);
        }
    });

    if ui.button("Pick Costume")
    {
        ui.open_popup("Pick Costume Menu");
    }

    picked_costume
}

pub fn ui_spawnable_list(spawnables : &SpawnableSet, index : &mut usize, scroll : &mut bool,
                         costume_store : &DataStore<Costume>, sheet_store : &DataStore<Sheet>,
                         packages : &mut Packages, ui : &imgui::Ui) -> bool
{
    let mut activated = false;
    let num_spawnables = spawnables.spawnable_count();

    for i in 0..num_spawnables
    {
        let spawnable_name;

        if imgui::Selectable::new(format!("##spawnable {}", i)).size([0.0, REPRESENTATION_ICON_SIZE]).selected(*index == i).build(ui)
        {
            *index = i;
            activated = true;
        }

        if let Some(name) = spawnables.spawnable_name(i)
        {
            spawnable_name = name;
        }
        else
        {
            spawnable_name = format!("<spawnable {}>", i);
        }

        let old_cursor = ui.cursor_pos();
        ui.same_line();
        representation_icon_label(spawnables.spawnable(i), &spawnable_name, costume_store, sheet_store, packages, ui);
        ui.set_cursor_pos(old_cursor);

        if *index == i && *scroll
        {
            ui.set_scroll_here_y();
            *scroll = false;
        }
    }

    activated
}
