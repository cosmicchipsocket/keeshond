use keeshond::gameloop::GameControl;

use crate::editorgui::{Packages};
use keeshond::datapack::DataObject;

pub mod sheetwizard;
pub mod costumewizard;
pub mod tilesetwizard;
pub mod spawnablewizard;
pub mod levelwizard;

pub struct WizardState
{
    pub open : bool
}

impl WizardState
{
    pub fn new() -> WizardState
    {
        WizardState
        {
            open : true
        }
    }
}

pub trait Wizard
{
    fn window_title(&self) -> &'static str;
    fn load(game : &mut GameControl) -> Self where Self : Sized;
    fn do_ui(&mut self, packages : &mut Packages, game: &mut GameControl, window_builder : imgui::Window<&'static str>, ui : &imgui::Ui) -> WizardState;

    fn make_resource<D : DataObject>(&self, mut data : D, package_name : &str, item : &str, packages : &mut Packages, game: &mut GameControl)
        -> bool where Self : Sized
    {
        if let Some(source) = game.source_manager().borrow_mut().source(packages.source_id())
        {
            let name = format!("{}/{}", D::folder_name(), item);

            match data.write(package_name, &name, source)
            {
                Ok(_) =>
                {
                    packages.reload();
                    return true;
                },
                Err(error) =>
                {
                    let package_error = format!("Failed to create \"{}/{}\": {}",
                                                package_name, name, error.to_string());
                    error!("{}", package_error);
                }
            }
        }

        false
    }
}
