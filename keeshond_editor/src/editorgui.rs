use keeshond::crate_reexport::strum::{EnumCount, IntoEnumIterator};
use keeshond::crate_reexport::strum_macros::{Display, EnumCount, EnumIter};
use keeshond::gameloop::GameControl;
use keeshond::scene::{ImGuiSystem, ThinkerSystem, DrawerSystem, ThinkerArgs, DrawerArgs, SceneType, NullSpawnable, SceneConfig};
use keeshond::renderer::Sheet;
use keeshond::util::Rect;

use keeshond::datapack::{DataError, DataHandle, DataStoreOk};
use keeshond::datapack::source::{SourceId, FilesystemSource, TrustLevel};

use keeshond_derive::Component;

use keeshond_treats::visual::{Costume, DrawableDrawer, Drawable, PixelSnap};
use keeshond_treats::tilemap::Tileset;
use keeshond_treats::spawnable::SpawnableSet;
use keeshond_treats::level::Level;

use std::collections::{HashMap, HashSet};
use std::string::ToString;
use std::slice::{Iter};

use sdl2::keyboard::Keycode;
use imgui::StyleColor;

use palette::{Srgb, Hsv, Hue, Saturate};
use keeshond_treats::world::CameraThinker;

use crate::document::Document;
use crate::document::sheetdocument::SheetDocument;
use crate::document::costumedocument::CostumeDocument;
use crate::document::tilesetdocument::TilesetDocument;
use crate::document::spawnabledocument::SpawnableSetDocument;
use crate::wizard::Wizard;
use crate::wizard::sheetwizard::SheetWizard;
use crate::wizard::costumewizard::CostumeWizard;
use crate::wizard::tilesetwizard::TilesetWizard;
use crate::wizard::spawnablewizard::SpawnableSetWizard;
use crate::document::leveldocument::{LevelDocument, LevelEditorConfig};
use crate::wizard::levelwizard::LevelWizard;
use crate::import::{AutoImport, ImportPendingResult};

const PACKAGE_SELECTABLE_PADDING : f32 = 4.0;
const ARRANGE_PADDING : f32 = 8.0;
pub const ARRANGE_PACKAGES_WIDTH : f32 = 300.0;
const AUTO_IMPORT_DIALOG_DELAY : u32 = 4;

pub const KEESHOND_CONFIG_FILENAME : &str = "Keeshond.toml";

struct EditorScene {}

impl SceneType for EditorScene
{
    type SpawnableIdType = NullSpawnable;

    fn new() -> Self where Self : Sized { EditorScene {} }
    fn config(&mut self, _game: &mut GameControl) -> SceneConfig<Self>
    {
        let mut config = SceneConfig::new();

        config.thinker(EditorThinker::new())
            .thinker(CameraThinker::new(true))
            .drawer(DrawableDrawer::new(PixelSnap::SpriteAndCameraSnap))
            .drawer(EditorDrawer::new())
            .imgui(Some(EditorGui::new()));

        config
    }
}

#[derive(Copy, Clone, EnumCount, EnumIter, Eq, PartialEq, Serialize, Deserialize, Display)]
#[strum(crate = "keeshond::crate_reexport::strum")]
pub enum FlavorMain
{
    Vanilla,
    #[strum(to_string="Cookies & Cream")]
    CookiesNCream,
    #[strum(to_string="Milk Chocolate")]
    MilkChocolate,
    #[strum(to_string="Dark Chocolate")]
    DarkChocolate
}

#[derive(Copy, Clone, EnumCount, EnumIter, Eq, PartialEq, Serialize, Deserialize, Display)]
#[strum(crate = "keeshond::crate_reexport::strum")]
pub enum FlavorAccent
{
    Raspberry,
    Orange,
    Lime,
    Blueberry,
    Grape
}

#[derive(Copy, Clone, EnumCount, EnumIter, Eq, PartialEq, Serialize, Deserialize, Display)]
#[strum(crate = "keeshond::crate_reexport::strum")]
pub enum FlavorFont
{
    Chewy,
    Crispy
}

#[derive(Clone, Serialize, Deserialize)]
pub struct ProjectConfig
{
    #[serde(default = "default_source_path")]
    pub source_path : String,
    #[serde(default = "default_auto_import_path")]
    pub auto_import_path : String,
    #[serde(default = "default_auto_refresh_import")]
    pub auto_refresh_import : bool,
    #[serde(default)]
    pub level_editor : LevelEditorConfig
}

impl Default for ProjectConfig
{
    fn default() -> ProjectConfig
    {
        ProjectConfig
        {
            source_path: default_source_path(),
            auto_import_path: default_auto_import_path(),
            auto_refresh_import : true,
            level_editor: Default::default()
        }
    }
}

fn default_source_path() -> String { "data/".to_string() }
fn default_auto_import_path() -> String { "data_src/".to_string() }
fn default_auto_refresh_import() -> bool { true }

#[derive(Clone, Serialize, Deserialize)]
pub struct EditorConfig
{
    #[serde(default)]
    pub project_path : String,
    #[serde(default)]
    pub skip_project_dialog : bool,
    pub flavor_main : FlavorMain,
    pub flavor_accent : FlavorAccent,
    pub flavor_font : FlavorFont,
    #[serde(default)]
    pub keyboard_navigation : bool,
    #[serde(default)]
    pub drag_window_from_anywhere : bool,
    #[serde(default)]
    pub force_arrange_windows : bool
}

impl Default for EditorConfig
{
    fn default() -> EditorConfig
    {
        EditorConfig
        {
            project_path : "".to_string(),
            skip_project_dialog : false,
            flavor_main : FlavorMain::MilkChocolate,
            flavor_accent : FlavorAccent::Raspberry,
            flavor_font : FlavorFont::Chewy,
            keyboard_navigation : false,
            drag_window_from_anywhere : true,
            force_arrange_windows : false
        }
    }
}

#[derive(Copy, Clone, EnumCount, EnumIter, Display)]
#[strum(crate = "keeshond::crate_reexport::strum")]
pub enum ResourceType
{
    Sheet,
    Costume,
    Tileset,
    SpawnableSet,
    Level
}

impl ResourceType
{
    pub fn folder_name(&self) -> &'static str
    {
        match self
        {
            ResourceType::Sheet => { "sheets" }
            ResourceType::Costume => { "costumes" }
            ResourceType::Tileset => { "tilesets" }
            ResourceType::SpawnableSet => { "spawnables" }
            ResourceType::Level => { "levels" }
        }
    }

    pub fn friendly_name(&self) -> &'static str
    {
        match self
        {
            ResourceType::Sheet => { "Sheets" }
            ResourceType::Costume => { "Costumes" }
            ResourceType::Tileset => { "Tilesets" }
            ResourceType::SpawnableSet => { "Spawnables" }
            ResourceType::Level => { "Levels" }
        }
    }

    pub fn icon_string(&self) -> &'static str
    {
        match self
        {
            ResourceType::Sheet => { crate::util::widget::ICON_SHEET }
            ResourceType::Costume => { crate::util::widget::ICON_COSTUME }
            ResourceType::Tileset => { crate::util::widget::ICON_TILES }
            ResourceType::SpawnableSet => { crate::util::widget::ICON_SPAWNABLE }
            ResourceType::Level => { crate::util::widget::ICON_LEVEL }
        }
    }

    pub fn load_package(&self, game : &mut GameControl, package_name : &str) -> Result<DataStoreOk, DataError>
    {
        match self
        {
            ResourceType::Sheet => { game.res().store_mut::<Sheet>().load_package(package_name) }
            ResourceType::Costume => { game.res().store_mut::<Costume>().load_package(package_name) }
            ResourceType::Tileset => { game.res().store_mut::<Tileset>().load_package(package_name) }
            ResourceType::SpawnableSet => { game.res().store_mut::<SpawnableSet>().load_package(package_name) }
            ResourceType::Level => { game.res().store_mut::<Level>().load_package(package_name) }
        }
    }

    pub fn list_package_contents(&self, game : &mut GameControl, package_name : &str) -> Vec<String>
    {
        match self
        {
            ResourceType::Sheet => { game.res().store_mut::<Sheet>().list_package_contents(package_name) }
            ResourceType::Costume => { game.res().store_mut::<Costume>().list_package_contents(package_name) }
            ResourceType::Tileset => { game.res().store_mut::<Tileset>().list_package_contents(package_name) }
            ResourceType::SpawnableSet => { game.res().store_mut::<SpawnableSet>().list_package_contents(package_name) }
            ResourceType::Level => { game.res().store_mut::<Level>().list_package_contents(package_name) }
        }
    }

    pub fn open_document(&self, game : &mut GameControl, package_name : &str, pathname : &str) -> Box<dyn Document>
    {
        match self
        {
            ResourceType::Sheet => { Box::new(SheetDocument::load(game, package_name, pathname)) }
            ResourceType::Costume => { Box::new(CostumeDocument::load(game, package_name, pathname)) }
            ResourceType::Tileset => { Box::new(TilesetDocument::load(game, package_name, pathname)) }
            ResourceType::SpawnableSet => { Box::new(SpawnableSetDocument::load(game, package_name, pathname)) }
            ResourceType::Level => { Box::new(LevelDocument::load(game, package_name, pathname)) }
        }
    }

    pub fn save(&self, game : &mut GameControl, package_name : &str, pathname : &str, source_id : SourceId) -> Result<(), DataError>
    {
        match self
        {
            ResourceType::Sheet => { game.res().store_mut::<Sheet>().save(package_name, pathname, source_id) }
            ResourceType::Costume => { game.res().store_mut::<Costume>().save(package_name, pathname, source_id) }
            ResourceType::Tileset => { game.res().store_mut::<Tileset>().save(package_name, pathname, source_id) }
            ResourceType::SpawnableSet => { game.res().store_mut::<SpawnableSet>().save(package_name, pathname, source_id) }
            ResourceType::Level => { game.res().store_mut::<Level>().save(package_name, pathname, source_id) }
        }
    }

    pub fn reload(&self, game : &mut GameControl, package_name : &str, pathname : &str) -> Result<(), DataError>
    {
        match self
        {
            ResourceType::Sheet => { game.res().store_mut::<Sheet>().reload(package_name, pathname) }
            ResourceType::Costume => { game.res().store_mut::<Costume>().reload(package_name, pathname) }
            ResourceType::Tileset => { game.res().store_mut::<Tileset>().reload(package_name, pathname) }
            ResourceType::SpawnableSet => { game.res().store_mut::<SpawnableSet>().reload(package_name, pathname) }
            ResourceType::Level => { game.res().store_mut::<Level>().reload(package_name, pathname) }
        }
    }

    pub fn is_scene_document(&self) -> bool
    {
        match self
        {
            ResourceType::Sheet => { false }
            ResourceType::Costume => { false }
            ResourceType::Tileset => { false }
            ResourceType::SpawnableSet => { false }
            ResourceType::Level => { true }
        }
    }

    pub fn has_large_package_list_icon(&self) -> bool
    {
        match self
        {
            ResourceType::Sheet | ResourceType::Costume => { true }
            _ => { false }
        }
    }
}

pub struct ResourceContents
{
    packages_to_load : HashSet<String>,
    package_contents : HashMap<String, Vec<String>>
}

impl ResourceContents
{
    pub fn new() -> ResourceContents
    {
        ResourceContents
        {
            packages_to_load : HashSet::new(),
            package_contents : HashMap::new()
        }
    }
}

struct DocumentEntry
{
    document : Box<dyn Document>,
    res_type : ResourceType,
    package_name : String,
    item_name : String,
    dirty : bool
}

pub struct Documents
{
    documents : Vec<DocumentEntry>,
    name_to_document : HashMap<String, usize>,
    document_to_focus : Option<usize>,
    active_document : Option<usize>
}

impl Documents
{
    pub fn open_document(&mut self, game : &mut GameControl, res_type : ResourceType, package_name : &str, item_name : &str)
    {
        let document = res_type.open_document(game, package_name, item_name);
        let full_name = Documents::format_document_name(res_type, package_name, item_name);

        let package_string = package_name.to_string();
        let item_string = item_name.to_string();

        if let Some(window_index) = self.name_to_document.get(&full_name)
        {
            self.document_to_focus = Some(*window_index);
        }
        else
        {
            self.document_to_focus = Some(self.documents.len());

            self.name_to_document.insert(full_name.clone(), self.documents.len());
            self.documents.push(DocumentEntry
            {
                document : document,
                res_type,
                package_name : package_string,
                item_name : item_string,
                dirty : false
            });
        }
    }

    pub fn save_document(&mut self, packages : &mut Packages, game : &mut GameControl, res_type : ResourceType, package_name : &str, item_name : &str)
    {
        let full_name = Documents::format_document_name(res_type, package_name, item_name);

        if let Some(document_index) = self.name_to_document.get(&full_name)
        {
            if !self.documents[*document_index].dirty
            {
                return;
            }

            match res_type.save(game, package_name, item_name, packages.source_id)
            {
                Ok(_) =>
                {
                    self.documents[*document_index].dirty = false;
                },
                Err(error) =>
                {
                    let package_error = format!("Failed to save \"{}/{}/{}\": {}",
                                                &package_name, res_type.folder_name(), &item_name, error.to_string());
                    error!("{}", package_error);
                }
            }
        }
    }

    fn close_document_internal(&mut self, game : &mut GameControl, res_type : ResourceType, package_name : &str, item_name : &str, revert_only : bool)
    {
        let full_name = Documents::format_document_name(res_type, package_name, item_name);

        if let Some(document_index) = self.name_to_document.get(&full_name)
        {
            match res_type.reload(game, package_name, item_name)
            {
                Ok(_) =>
                {
                    if revert_only
                    {
                        self.documents[*document_index].dirty = false;
                    }
                    else
                    {
                        self.documents.remove(*document_index);
                        self.refresh_name_lookup();
                    }
                }
                Err(error) =>
                {
                    let package_error = format!("Failed to revert \"{}/{}/{}\": {}",
                                                &package_name, res_type.folder_name(), &item_name, error.to_string());
                    error!("{}", package_error);
                }
            }
        }
    }

    pub fn close_document(&mut self, game : &mut GameControl, res_type : ResourceType, package_name : &str, item_name : &str)
    {
        self.close_document_internal(game, res_type, package_name, item_name, false)
    }

    pub fn reload_document(&mut self, game : &mut GameControl, res_type : ResourceType, package_name : &str, item_name : &str)
    {
        self.close_document_internal(game, res_type, package_name, item_name, true)
    }

    pub fn close_all_documents(&mut self)
    {
        self.documents.clear();
        self.refresh_name_lookup();
    }

    pub fn refresh_name_lookup(&mut self)
    {
        self.name_to_document.clear();

        for i in 0..self.documents.len()
        {
            let entry = &self.documents[i];
            let full_name = Documents::format_document_name(entry.res_type, &entry.package_name, &entry.item_name);
            self.name_to_document.insert(full_name, i);
        }
    }

    pub fn document_info(&self, index : usize) -> Option<(ResourceType, String, String)>
    {
        if let Some(entry) = self.documents.get(index)
        {
            return Some((entry.res_type.clone(), entry.package_name.to_string(), entry.item_name.to_string()));
        }

        None
    }

    pub fn format_document_name(res_type : ResourceType, package_name : &str, item_name : &str) -> String
    {
        format!("{}/{}/{}", package_name, res_type.folder_name(), item_name)
    }

    pub fn full_document_name(&mut self, index : usize) -> String
    {
        if let Some(document) = self.documents.get(index)
        {
            return Documents::format_document_name(document.res_type, &document.package_name, &document.item_name);
        }

        String::new()
    }
}

pub struct Packages
{
    need_reload: bool,
    load_source : bool,
    source_id : SourceId,
    packages_listed : bool,
    project_path: String,
    package_list : Vec<String>,
    res : Vec<ResourceContents>
}

impl Packages
{
    pub fn source_id(&self) -> SourceId
    {
        self.source_id
    }

    pub(crate) fn package_iter(&self) -> Iter<String>
    {
        self.package_list.iter()
    }

    pub(crate) fn package_contents(&self, res_type : ResourceType, package_name : &str) -> Iter<String>
    {
        let res = &self.res[res_type as usize];

        if let Some(contents) = res.package_contents.get(package_name)
        {
            return contents.iter();
        }

        [].iter()
    }

    pub fn package_loaded(&self, res_type : ResourceType, package_name : &str) -> bool
    {
        let res = &self.res[res_type as usize];

        res.package_contents.contains_key(package_name)
    }

    pub fn load_package_later(&mut self, res_type : ResourceType, package_name : &str)
    {
        if self.package_loaded(res_type, package_name)
        {
            return;
        }

        let res = &mut self.res[res_type as usize];

        res.packages_to_load.insert(package_name.to_string());
    }

    pub fn package_load_queued(&self) -> bool
    {
        for res in &self.res
        {
            if !res.packages_to_load.is_empty()
            {
                return true;
            }
        }

        false
    }

    pub fn load_sources(&mut self, source_path : &str, game : &mut GameControl)
    {
        if self.source_id == 0 && self.load_source
        {
            let source_manager = game.source_manager();
            let source = FilesystemSource::new(source_path, TrustLevel::TrustedSource);

            self.source_id = source_manager.borrow_mut().add_source(Box::new(source));

            self.load_source = false;
            self.need_reload = true;
        }

        if self.need_reload
        {
            self.package_list.clear();

            game.res().unload_all();

            for res_type in ResourceType::iter()
            {
                let res = &mut self.res[res_type as usize];

                for (package, _) in &res.package_contents
                {
                    res.packages_to_load.insert(package.clone());
                }
            }

            self.packages_listed = false;
            self.need_reload = false;
        }

        if self.source_id != 0 && !self.packages_listed
        {
            let source_manager = game.source_manager();
            let mut source_manager_borrow = source_manager.borrow_mut();
            let source_option = source_manager_borrow.source(self.source_id);

            if let Some(source) = source_option
            {
                self.package_list = source.list_packages();
                self.package_list.sort_by(|a, b| { a.to_lowercase().cmp(&b.to_lowercase()) });
            }

            self.packages_listed = true;
        }

        for res_type in ResourceType::iter()
        {
            let res = &mut self.res[res_type as usize];
            for package_name in res.packages_to_load.iter()
            {
                let mut contents = Vec::new();

                match res_type.load_package(game, package_name)
                {
                    Ok(_) =>
                    {
                        contents = res_type.list_package_contents(game, &package_name);
                    },
                    Err(error) =>
                    {
                        let package_error = format!("Failed to load {} package \"{}\": {}",
                                                    res_type.folder_name(), &package_name, error.to_string());
                        error!("{}", package_error);
                    }
                }

                res.package_contents.insert(package_name.clone(), contents);
            }

            res.packages_to_load.clear();
        }
    }

    pub fn reload(&mut self)
    {
        self.need_reload = true;
    }
}

fn normalize_path(mut path : String) -> String
{
    #[cfg(target_os = "windows")]
    {
        path = path.replace("\\", "/");
    }

    path
}

#[derive(Component)]
pub struct EditorControl
{
    pub config : EditorConfig,
    pub project : ProjectConfig,
    pub documents : Documents,
    pub scene_documents : Documents,
    pub packages : Packages,
    pub wizard : Option<Box<dyn Wizard>>,
    pub auto_import : AutoImport,
    queue_auto_import : u32,
    last_console_index : usize,
    last_opened_console_index : usize,
    wizard_raise : bool,
    style_dirty : bool,
    viewport : Rect<f32>
}

impl EditorControl
{
    pub fn new() -> EditorControl
    {
        let mut res = Vec::new();
        
        for _ in 0..ResourceType::COUNT
        {
            res.push(ResourceContents::new());
        }

        let config = confy::load("keeshond_editor").unwrap_or(EditorConfig::default());

        let mut project_file = config.project_path.clone();
        project_file.push_str(&format!("/{}", KEESHOND_CONFIG_FILENAME));
        let project = confy::load_path(&project_file).unwrap_or(ProjectConfig::default());
        
        EditorControl
        {
            config,
            project,
            style_dirty : true,
            packages : Packages
            {
                need_reload : false,
                load_source : false,
                source_id : 0,
                packages_listed : false,
                project_path : String::new(),
                package_list : Vec::new(),
                res
            },
            documents : Documents
            {
                documents : Vec::new(),
                name_to_document : HashMap::new(),
                document_to_focus : None,
                active_document : None
            },
            scene_documents : Documents
            {
                documents : Vec::new(),
                name_to_document : HashMap::new(),
                document_to_focus : None,
                active_document : None
            },
            wizard : None,
            auto_import : AutoImport::new(),
            queue_auto_import : 0,
            last_console_index : 0,
            last_opened_console_index : 0,
            wizard_raise : false,
            viewport : Rect::default()
        }
    }

    fn load_project(&mut self, project_path : &str)
    {
        self.config.project_path = String::from(project_path);

        let mut project_file = self.config.project_path.clone();
        project_file.push_str(&format!("/{}", KEESHOND_CONFIG_FILENAME));

        self.project = confy::load_path(&project_file).unwrap_or(ProjectConfig::default());
        self.packages.load_source = true;
        self.config.skip_project_dialog = true;

        let mut import_path = self.config.project_path.clone();
        import_path.push_str(&format!("/{}", self.project.auto_import_path));

        #[allow(unused_must_use)] // Err handling not needed, will reattempt with try_auto_refresh_import()
        {
            self.auto_import.set_directory(&import_path);
        }
        self.try_auto_refresh_import(self.project.auto_refresh_import);

        if self.project.auto_refresh_import
        {
            self.auto_import.mark_all_pending(false);
        }
    }

    fn try_auto_refresh_import(&mut self, active : bool)
    {
        if let Err(error) = self.auto_import.set_auto_refresh_active(active)
        {
            error!("Could not set up auto refresh for AutoImport: {}", error.to_string());
        }

        self.project.auto_refresh_import = self.auto_import.auto_refresh_active();
    }

    fn new_resource_wizard(&mut self, game : &mut GameControl, res_type : ResourceType)
    {
        match res_type
        {
            ResourceType::Sheet => { self.wizard = Some(Box::new(SheetWizard::load(game))) }
            ResourceType::Costume => { self.wizard = Some(Box::new(CostumeWizard::load(game))) }
            ResourceType::Tileset => { self.wizard = Some(Box::new(TilesetWizard::load(game))) }
            ResourceType::SpawnableSet => { self.wizard = Some(Box::new(SpawnableSetWizard::load(game))) }
            ResourceType::Level => { self.wizard = Some(Box::new(LevelWizard::load(game))) }
        }

        self.wizard_raise = true;
    }

    fn adjust_color(&self, color : [f32; 4]) -> [f32; 4]
    {
        let hue = match self.config.flavor_accent
        {
            FlavorAccent::Raspberry => 350.0,
            FlavorAccent::Orange => 35.0,
            FlavorAccent::Lime => 90.0,
            FlavorAccent::Blueberry => 225.0,
            FlavorAccent::Grape => 260.0
        };

        let saturation = match self.config.flavor_accent
        {
            FlavorAccent::Raspberry => 0.0,
            FlavorAccent::Orange => 0.65,
            FlavorAccent::Lime => 0.1,
            FlavorAccent::Blueberry => 0.0,
            FlavorAccent::Grape => 0.0
        };

        let value = match self.config.flavor_accent
        {
            FlavorAccent::Raspberry => -0.1,
            FlavorAccent::Orange => -0.15,
            FlavorAccent::Lime => -0.5,
            FlavorAccent::Blueberry => 0.0,
            FlavorAccent::Grape => 0.35
        };

        let mut color_hue : Hsv = Srgb::new(color[0], color[1], color[2]).into();

        if color_hue.saturation > 0.1
        {
            color_hue.value *= 1.0 + (value * (color_hue.saturation));
            let new_color = Srgb::from(color_hue.with_hue(hue).saturate(saturation));

            return [new_color.red, new_color.green, new_color.blue, color[3]];
        }

        color
    }

    fn set_flavor_main(&mut self, flavor : FlavorMain)
    {
        self.config.flavor_main = flavor;
        self.style_dirty = true;
    }

    fn set_flavor_accent(&mut self, flavor : FlavorAccent)
    {
        self.config.flavor_accent = flavor;
        self.style_dirty = true;
    }

    fn set_flavor_font(&mut self, flavor : FlavorFont)
    {
        self.config.flavor_font = flavor;
        self.style_dirty = true;
    }

    fn set_keyboard_navigation(&mut self, enabled : bool)
    {
        self.config.keyboard_navigation = enabled;
        self.style_dirty = true;
    }

    fn set_drag_window_from_anywhere(&mut self, enabled : bool)
    {
        self.config.drag_window_from_anywhere = enabled;
        self.style_dirty = true;
    }

    fn apply_imgui_style(&mut self, imgui: &mut imgui::Context, background : &mut Drawable)
    {
        if !self.style_dirty
        {
            return;
        }

        imgui.io_mut().config_flags.set(imgui::ConfigFlags::NAV_ENABLE_KEYBOARD, self.config.keyboard_navigation);
        imgui.io_mut().config_windows_move_from_title_bar_only = !self.config.drag_window_from_anywhere;
        imgui.io_mut().config_drag_click_to_input_text = true;

        let mut style = imgui.style_mut();

        match self.config.flavor_main
        {
            FlavorMain::Vanilla | FlavorMain::CookiesNCream =>
            {
                style.use_light_colors();
            },
            FlavorMain::MilkChocolate | FlavorMain::DarkChocolate =>
            {
                style.use_dark_colors();
            }
        }

        style.window_title_align = [0.5, 0.5];
        style.item_spacing = [8.0, 4.0];
        style.frame_padding = [6.0, 5.0];
        style.frame_rounding = 1.0;
        style.frame_border_size = 1.0;
        style.window_rounding = 2.0;
        style.popup_rounding = 1.0;
        style.child_rounding = 1.0;
        style.tab_rounding = 2.0;
        style.tab_border_size = 1.0;
        style.scrollbar_rounding = 2.0;
        style.indent_spacing = 28.0;

        let colors = &mut style.colors;

        match self.config.flavor_main
        {
            FlavorMain::Vanilla =>
            {
                colors[StyleColor::Text as usize] = [0.06, 0.06, 0.06, 1.0];
                colors[StyleColor::TitleBgActive as usize] = [0.4, 0.58, 0.92, 1.0];
                colors[StyleColor::Border as usize][3] = 0.4;

                *background = Drawable::with_background_color(0.75, 0.75, 0.75);
            },
            FlavorMain::CookiesNCream =>
            {
                colors[StyleColor::TitleBgActive as usize] = [0.3, 0.6, 0.9, 1.0];
                colors[StyleColor::WindowBg as usize] = [0.82, 0.82, 0.82, 1.0];
                colors[StyleColor::FrameBg as usize] = [0.96, 0.96, 0.96, 1.0];
                colors[StyleColor::PopupBg as usize] = [0.9, 0.9, 0.9, 1.0];
                colors[StyleColor::MenuBarBg as usize] = [0.75, 0.75, 0.75, 1.0];
                colors[StyleColor::TitleBg as usize] = [0.85, 0.85, 0.85, 1.0];
                colors[StyleColor::Border as usize][3] = 0.4;

                *background = Drawable::with_background_color(0.6, 0.6, 0.6);
            },
            FlavorMain::MilkChocolate =>
            {
                colors[StyleColor::WindowBg as usize] = [0.28, 0.28, 0.28, 1.0];
                colors[StyleColor::FrameBg as usize] = [0.2, 0.2, 0.2, 1.0];
                colors[StyleColor::PopupBg as usize] = [0.25, 0.25, 0.25, 1.0];
                colors[StyleColor::MenuBarBg as usize] = [0.22, 0.22, 0.22, 1.0];
                colors[StyleColor::TitleBg as usize] = colors[StyleColor::MenuBarBg as usize];
                colors[StyleColor::TitleBgActive as usize] = [0.16, 0.34, 0.64, 1.0];
                colors[StyleColor::TabActive as usize] = [0.22, 0.44, 0.75, 1.0];
                colors[StyleColor::Border as usize] = [0.0, 0.0, 0.0, 0.7];
                colors[StyleColor::Button as usize][3] = 0.6;
                colors[StyleColor::Header as usize][3] = 0.5;
                colors[StyleColor::Separator as usize] = colors[StyleColor::Border as usize];
                colors[StyleColor::NavWindowingDimBg as usize] = [0.0, 0.0, 0.0, 0.25];
                colors[StyleColor::ModalWindowDimBg as usize] = [0.0, 0.0, 0.0, 0.35];

                *background = Drawable::with_background_color(0.12, 0.12, 0.12);
            },
            FlavorMain::DarkChocolate =>
            {
                colors[StyleColor::Border as usize] = [0.6, 0.75, 1.0, 0.8];

                *background = Drawable::with_background_color(0.0, 0.0, 0.0);
            }
        }

        colors[StyleColor::WindowBg as usize][3] = 1.0;
        colors[StyleColor::TitleBgCollapsed as usize] = colors[StyleColor::TitleBg as usize];
        colors[StyleColor::ScrollbarGrab as usize] = colors[StyleColor::Button as usize];
        colors[StyleColor::ScrollbarGrabHovered as usize] = colors[StyleColor::ButtonHovered as usize];
        colors[StyleColor::ScrollbarGrabActive as usize] = colors[StyleColor::ButtonActive as usize];
        colors[StyleColor::NavWindowingHighlight as usize] = colors[StyleColor::TitleBgActive as usize];

        for style_color in [StyleColor::TitleBgActive, StyleColor::Border, StyleColor::FrameBg, StyleColor::FrameBgHovered,
            StyleColor::FrameBgActive, StyleColor::TitleBg, StyleColor::TitleBgCollapsed,
            StyleColor::CheckMark, StyleColor::SliderGrab, StyleColor::SliderGrabActive, StyleColor::Button,
            StyleColor::ButtonHovered, StyleColor::ButtonActive, StyleColor::Header, StyleColor::HeaderHovered,
            StyleColor::HeaderActive, StyleColor::SeparatorHovered, StyleColor::SeparatorActive,
            StyleColor::ScrollbarGrab, StyleColor::ScrollbarGrabHovered, StyleColor::ScrollbarGrabActive,
            StyleColor::ResizeGripHovered, StyleColor::ResizeGripActive, StyleColor::Tab,
            StyleColor::TabHovered, StyleColor::TabActive, StyleColor::TabUnfocusedActive,
            StyleColor::TextSelectedBg, StyleColor::DragDropTarget, StyleColor::NavHighlight,
            StyleColor::ResizeGrip, StyleColor::ResizeGripActive, StyleColor::ResizeGripHovered,
            StyleColor::TableHeaderBg, StyleColor::TableBorderLight, StyleColor::TableBorderStrong,
            StyleColor::NavWindowingHighlight].iter()
        {
            colors[*style_color as usize] = self.adjust_color(colors[*style_color as usize]);
        }

        self.style_dirty = false;
    }

    pub fn save_config(&mut self)
    {
        if self.packages.source_id != 0
        {
            let mut project_path = normalize_path(self.packages.project_path.clone());

            self.config.project_path = project_path.clone();

            project_path.push_str(&format!("/{}", KEESHOND_CONFIG_FILENAME));

            match confy::store_path(&project_path, self.project.clone())
            {
                Ok(_) =>
                {
                    info!("Project config saved.");
                },
                Err(error) =>
                {
                    error!("Error while saving project config: {}", error);
                }
            }
        }

        match confy::store("keeshond_editor", self.config.clone())
        {
            Ok(_) =>
            {
                info!("Editor config saved.");
            },
            Err(error) =>
            {
                error!("Error while saving editor config: {}", error);
            }
        }
    }

    pub fn switch_project(&mut self, game : &mut GameControl)
    {
        game.source_manager().borrow_mut().clear();

        self.config.skip_project_dialog = false;

        EditorGui::set_scene(game);
    }
}

#[derive(Eq, PartialEq)]
enum MenubarShortcut
{
    None,
    SaveDocument,
    ViewSidebar,
    ViewEditors,
    ViewAutoImport,
    ViewConsole,
    ArrangeWindows
}

pub struct EditorGui
{
    menubar_shortcut : MenubarShortcut,
    selected_package : String,
    package_items_buffer : Vec<String>,
    sidebar_open: bool,
    editors_open : bool,
    editors_focused : bool,
    editors_collapsed : Option<bool>,
    scene_editor_focused : bool,
    level_sidebar_focus : bool,
    scene_active : bool,
    auto_import_open : bool,
    console_open : bool,
    console_focused : bool,
    console_allow_focused : bool,
    demo_open : bool,
    metrics_open : bool,
    imgui_guide_open : bool,
    imgui_about_open : bool,
    arrange_windows : bool
}

impl EditorGui
{
    pub fn set_scene(game : &mut GameControl)
    {
        game.goto_new_scene::<EditorScene>();
    }

    pub fn new() -> EditorGui
    {
        EditorGui
        {
            menubar_shortcut : MenubarShortcut::None,
            selected_package : String::new(),
            package_items_buffer : Vec::new(),
            sidebar_open: true,
            editors_open : true,
            editors_focused : false,
            editors_collapsed : None,
            scene_editor_focused : false,
            level_sidebar_focus : false,
            scene_active : false,
            auto_import_open : false,
            console_open : false,
            console_focused : false,
            console_allow_focused : false,
            demo_open : false,
            metrics_open : false,
            imgui_guide_open : false,
            imgui_about_open : false,
            arrange_windows : false
        }
    }

    fn window_pos_mode(&self) -> imgui::Condition
    {
        if self.arrange_windows
        {
            return imgui::Condition::Always;
        }

        imgui::Condition::Appearing
    }

    fn menubar_shortcuts(&mut self, editor : &mut EditorControl, ui : &imgui::Ui, game : &mut GameControl)
    {
        if crate::util::keycode_pressed(Keycode::S, false, ui) && ui.io().key_ctrl
        {
            self.menubar_shortcut = MenubarShortcut::SaveDocument;
        }
        else if crate::util::keycode_pressed(Keycode::F2, false, ui)
        {
            self.menubar_shortcut = MenubarShortcut::ViewSidebar;
        }
        else if crate::util::keycode_pressed(Keycode::F3, false, ui)
        {
            self.menubar_shortcut = MenubarShortcut::ViewEditors;
        }
        else if crate::util::keycode_pressed(Keycode::F4, false, ui)
        {
            self.menubar_shortcut = MenubarShortcut::ViewAutoImport;
        }
        else if crate::util::keycode_pressed(Keycode::F11, false, ui)
        {
            self.menubar_shortcut = MenubarShortcut::ViewConsole;
        }
        else if crate::util::keycode_pressed(Keycode::W, false, ui) && ui.io().key_ctrl && ui.io().key_shift
        {
            self.menubar_shortcut = MenubarShortcut::ArrangeWindows;
        }

        match self.menubar_shortcut
        {
            MenubarShortcut::None => {}
            MenubarShortcut::SaveDocument =>
            {
                let mut package_name = String::new();
                let mut item_name = String::new();
                let mut res_type = ResourceType::Sheet;

                let documents;

                if self.scene_active
                {
                    documents = &mut editor.scene_documents;
                }
                else
                {
                    documents = &mut editor.documents;
                }

                if let Some(active_index) = documents.active_document
                {
                    if let Some(document) = documents.documents.get(active_index)
                    {
                        package_name = document.package_name.clone();
                        item_name = document.item_name.clone();
                        res_type = document.res_type;
                    }
                }

                documents.save_document(&mut editor.packages, game, res_type, &package_name, &item_name);
            }
            MenubarShortcut::ViewSidebar => { self.sidebar_open = !self.sidebar_open; }
            MenubarShortcut::ViewEditors => { self.editors_open = !self.editors_open; }
            MenubarShortcut::ViewAutoImport => { self.auto_import_open = !self.auto_import_open; }
            MenubarShortcut::ViewConsole => { self.console_open = !self.console_open; }
            MenubarShortcut::ArrangeWindows => { self.arrange_windows = true; }
        }

        self.menubar_shortcut = MenubarShortcut::None;
    }

    fn main_menubar(&mut self, editor : &mut EditorControl, ui : &imgui::Ui, game : &mut GameControl)
    {
        ui.main_menu_bar(||
        {
            ui.menu("File", ||
            {
                ui.menu("New Resource", ||
                {
                    for res_type in ResourceType::iter()
                    {
                        if imgui::MenuItem::new(res_type.to_string()).build(ui)
                        {
                            editor.new_resource_wizard(game, res_type);
                        }
                    }
                });

                ui.separator();

                let mut document_name = String::from("Document");
                let mut package_name = String::new();
                let mut item_name = String::new();
                let mut res_type = ResourceType::Sheet;
                let mut dirty = false;

                let documents;

                if self.scene_active
                {
                    documents = &mut editor.scene_documents;
                }
                else
                {
                    documents = &mut editor.documents;
                }

                if let Some(active_index) = documents.active_document
                {
                    if let Some(document) = documents.documents.get(active_index)
                    {
                        package_name = document.package_name.clone();
                        item_name = document.item_name.clone();
                        res_type = document.res_type;
                        dirty = document.dirty;

                        document_name = format!("{}/{}/{}", package_name, res_type.folder_name(), item_name);
                    }
                }
                if imgui::MenuItem::new(format!("Save {}", document_name)).shortcut("Ctrl + S").enabled(dirty).build(ui)
                {
                    self.menubar_shortcut = MenubarShortcut::SaveDocument;
                }
                if imgui::MenuItem::new(format!("Revert {}", document_name)).enabled(dirty).build(ui)
                {
                    documents.reload_document(game, res_type, &package_name, &item_name);
                }

                ui.separator();

                if imgui::MenuItem::new("Close All Documents").build(ui)
                {
                    editor.documents.close_all_documents();
                    editor.scene_documents.close_all_documents();
                }

                ui.separator();

                if imgui::MenuItem::new("Reload Packages").build(ui)
                {
                    editor.packages.reload();
                }
                if imgui::MenuItem::new("Switch Project").build(ui)
                {
                    editor.switch_project(game);
                }
                ui.separator();
                if imgui::MenuItem::new("Quit").build(ui)
                {
                    game.quit();
                }
            });

            ui.menu("AutoImport", ||
            {
                if imgui::MenuItem::new("AutoImport Window").shortcut("F4").selected(self.auto_import_open).build(ui)
                {
                    self.auto_import_open = !self.auto_import_open;
                }
                ui.separator();
                if imgui::MenuItem::new("Auto Refresh On Filesystem Changes").selected(editor.project.auto_refresh_import).build(ui)
                {
                    editor.try_auto_refresh_import(!editor.project.auto_refresh_import);
                }
                if imgui::MenuItem::new("Refresh Now").build(ui)
                {
                    editor.auto_import.mark_all_pending(false);
                }
                ui.separator();
                if imgui::MenuItem::new("Force Reimport All Files").build(ui)
                {
                    editor.auto_import.mark_all_pending(true);
                }
            });
            
            ui.menu("View", ||
            {
                if imgui::MenuItem::new("Sidebar").shortcut("F2").selected(self.sidebar_open).build(ui)
                {
                    self.menubar_shortcut = MenubarShortcut::ViewSidebar;
                }
                if imgui::MenuItem::new("Editors").shortcut("F3").selected(self.editors_open).build(ui)
                {
                    self.menubar_shortcut = MenubarShortcut::ViewEditors;
                }
                if imgui::MenuItem::new("AutoImport").shortcut("F4").selected(self.auto_import_open).build(ui)
                {
                    self.menubar_shortcut = MenubarShortcut::ViewAutoImport;
                }
                if imgui::MenuItem::new("Console").shortcut("F11").selected(self.console_open).build(ui)
                {
                    self.menubar_shortcut = MenubarShortcut::ViewConsole;
                }
                ui.separator();
                if imgui::MenuItem::new("Arrange Windows").shortcut("Ctrl + Shift + W").build(ui)
                {
                    self.menubar_shortcut = MenubarShortcut::ArrangeWindows;
                }
            });

            ui.menu("Settings", ||
            {
                ui.menu("Flavor", ||
                {
                    for flavor_main in FlavorMain::iter()
                    {
                        if imgui::MenuItem::new(format!("{}", flavor_main)).selected(editor.config.flavor_main == flavor_main).build(ui)
                        {
                            editor.set_flavor_main(flavor_main);
                        }
                    }

                    ui.separator();

                    for flavor_accent in FlavorAccent::iter()
                    {
                        if imgui::MenuItem::new(format!("{}", flavor_accent)).selected(editor.config.flavor_accent == flavor_accent).build(ui)
                        {
                            editor.set_flavor_accent(flavor_accent);
                        }
                    }

                    ui.separator();

                    for flavor_font in FlavorFont::iter()
                    {
                        if imgui::MenuItem::new(format!("{}", flavor_font)).selected(editor.config.flavor_font == flavor_font).build(ui)
                        {
                            editor.set_flavor_font(flavor_font);
                        }
                    }
                });

                ui.separator();

                if imgui::MenuItem::new("Keyboard Navigation").selected(editor.config.keyboard_navigation).build(ui)
                {
                    editor.set_keyboard_navigation(!editor.config.keyboard_navigation);
                }

                if imgui::MenuItem::new("Drag Windows From Anywhere").selected(editor.config.drag_window_from_anywhere).build(ui)
                {
                    editor.set_drag_window_from_anywhere(!editor.config.drag_window_from_anywhere);
                }

                if imgui::MenuItem::new("Always Arrange Windows").selected(editor.config.force_arrange_windows).build(ui)
                {
                    editor.config.force_arrange_windows = !editor.config.force_arrange_windows;
                }
            });

            if ui.io().key_ctrl && ui.io().key_alt && ui.io().key_shift
            {
                ui.menu("Debug", ||
                {
                    if imgui::MenuItem::new("ImGui Metrics").selected(self.metrics_open).build(ui)
                    {
                        self.metrics_open = !self.metrics_open;
                    }
                    if imgui::MenuItem::new("ImGui Demo Window").selected(self.demo_open).build(ui)
                    {
                        self.demo_open = !self.demo_open;
                    }
                });
            }
            
            ui.menu("Help", ||
            {
                if imgui::MenuItem::new("ImGui User Guide").selected(self.imgui_guide_open).build(ui)
                {
                    self.imgui_guide_open = !self.imgui_guide_open;
                }
                if imgui::MenuItem::new("About ImGui").selected(self.imgui_about_open).build(ui)
                {
                    self.imgui_about_open = !self.imgui_about_open;
                }
            });
        });
    }

    fn package_label(&self, name : &String, ui : &imgui::Ui)
    {
        let old_cursor = ui.cursor_pos();

        ui.same_line();

        let cursor = ui.cursor_pos();
        ui.set_cursor_pos([cursor[0] + PACKAGE_SELECTABLE_PADDING, cursor[1] + PACKAGE_SELECTABLE_PADDING]);

        crate::util::widget::icon_label(crate::util::widget::ICON_PACKAGE, name, ui);

        ui.set_cursor_pos(old_cursor);
    }

    fn resource_label(&self, res_type : ResourceType, ui : &imgui::Ui)
    {
        ui.same_line_with_spacing(0.0, 8.0);
        crate::util::widget::icon_label(res_type.icon_string(), res_type.friendly_name(), ui);
    }

    fn resource_item_label(&self, package : &str, name : &str, res_type : ResourceType,
                           game : &mut GameControl, packages : &mut Packages, ui : &imgui::Ui)
    {
        let costume_store = game.res().store();
        let sheet_store = game.res().store();

        let cursor_pos = ui.cursor_pos();

        ui.same_line_with_spacing(0.0, 8.0);

        match res_type
        {
            ResourceType::Sheet =>
            {
                crate::util::widget::sheet_icon_label_fit(&DataHandle::with_path(package, name), &sheet_store, packages, ui);
            }
            ResourceType::Costume =>
            {
                crate::util::widget::costume_icon_label_fit(&DataHandle::with_path(package, name), &costume_store, &sheet_store, packages, ui);
            }
            _ =>
            {
                let main_style = ui.clone_style();
                let cursor = ui.cursor_pos();
                ui.set_cursor_pos([cursor[0] + 8.0, cursor[1] + main_style.frame_padding[1]]);

                crate::util::widget::icon_label(res_type.icon_string(), name, ui);
            }
        }

        ui.set_cursor_pos(cursor_pos);
    }

    fn handle_auto_import(&mut self, editor : &mut EditorControl, ui : &imgui::Ui, game : &mut GameControl)
    {
        editor.auto_import.receive_changes();

        if editor.queue_auto_import >= AUTO_IMPORT_DIALOG_DELAY
        {
            self.console_allow_focused = true;
            self.move_console_read_marker(editor, game);

            if editor.auto_import.import_pending(&mut editor.packages, game) == ImportPendingResult::NeedsConfig
            {
                self.auto_import_open = true;
            }
            editor.queue_auto_import = 0;
        }
        else if editor.auto_import.has_pending_items()
        {
            let size = ui.io().display_size;

            if let Some(auto_import_window) = imgui::Window::new("AutoImport Status").collapsible(false).save_settings(false)
                .size([350.0, 100.0], imgui::Condition::Always).position([size[0] / 2.0, size[1] / 2.5], imgui::Condition::Appearing)
                .position_pivot([0.5, 0.5]).resizable(false).focused(true).begin(ui)
            {
                let pos = ui.cursor_pos();
                ui.set_cursor_pos([pos[0], pos[1] + 20.0]);
                crate::util::widget::window_placeholder_text("AutoImport in progress...", ui);

                auto_import_window.end();
            }

            editor.queue_auto_import += 1;
        }

        if self.auto_import_open
        {
            editor.auto_import.do_ui(ui, &mut self.auto_import_open);
        }
    }
    
    fn view_sidebar(&mut self, editor : &mut EditorControl, ui : &imgui::Ui, game : &mut GameControl)
    {
        let mut load_res = ResourceType::Sheet;
        let mut load_package_name = String::new();
        let mut load_item_name = String::new();

        if self.sidebar_open
        {
            let pos_mode = self.window_pos_mode();

            if let Some(sidebar) = imgui::Window::new("Sidebar")
                .position([editor.viewport.x + ARRANGE_PADDING, editor.viewport.y + ARRANGE_PADDING], pos_mode)
                .size([ARRANGE_PACKAGES_WIDTH, editor.viewport.h - ARRANGE_PADDING * 2.0], pos_mode)
                .begin(ui)
            {
                if let Some(sidebar_tabs) = imgui::TabBar::new("sidebar_tabs").begin(ui)
                {
                    self.packages_tab(editor, game, ui, &mut load_res, &mut load_package_name, &mut load_item_name);

                    let mut level_tab_builder = imgui::TabItem::new("Level Edit");

                    if self.level_sidebar_focus
                    {
                        level_tab_builder = level_tab_builder.flags(imgui::TabItemFlags::SET_SELECTED);
                        self.level_sidebar_focus = false;
                    }

                    if let Some(level_tab) = level_tab_builder.begin(ui)
                    {
                        let mut used = false;

                        if ui.is_window_focused_with_flags(imgui::WindowFocusedFlags::ROOT_AND_CHILD_WINDOWS)
                        {
                            self.scene_active = true;
                        }

                        if let Some(active_index) = editor.scene_documents.active_document
                        {
                            if let Some(document) = editor.scene_documents.documents.get_mut(active_index)
                            {
                                used = document.document.do_detached_ui("level_tab", &mut editor.packages,
                                                                        &mut editor.config, &mut editor.project, game, ui);
                            }
                        }

                        if !used
                        {
                            crate::util::widget::window_placeholder_text("Select a level from the Packages tab.", ui);
                        }

                        level_tab.end();
                    }

                    sidebar_tabs.end();
                };

                sidebar.end();
            };
        }

        if !load_package_name.is_empty()
        {
            if load_res.is_scene_document()
            {
                editor.scene_documents.open_document(game, load_res, &load_package_name, &load_item_name);

                self.editors_collapsed = Some(true);
                self.level_sidebar_focus = true;
                self.scene_editor_focused = true;
            }
            else
            {
                editor.documents.open_document(game, load_res, &load_package_name, &load_item_name);

                self.editors_open = true;
                self.editors_collapsed = Some(false);
                self.editors_focused = true;
            }
        }
    }

    fn packages_tab(&mut self, editor : &mut EditorControl, game : &mut GameControl, ui : &imgui::Ui,
                    load_res : &mut ResourceType, load_package_name : &mut String, load_item_name : &mut String)
    {
        self.package_items_buffer.clear();
        self.package_items_buffer.shrink_to_fit();

        let main_style = ui.clone_style();

        if let Some(tab) = imgui::TabItem::new("Packages").begin(ui)
        {
            let mut package_list = Vec::new();

            std::mem::swap(&mut editor.packages.package_list, &mut package_list);

            let style_indent = ui.push_style_var(imgui::StyleVar::IndentSpacing(16.0));
            let style_padding = ui.push_style_var(imgui::StyleVar::FramePadding([4.0, 3.0]));
            let style_window_padding = ui.push_style_var(imgui::StyleVar::WindowPadding([1.0, 1.0]));
            let color_bg = ui.push_style_color(imgui::StyleColor::ChildBg, main_style.colors[imgui::StyleColor::FrameBg as usize]);
            let style_spacing = ui.push_style_var(imgui::StyleVar::ItemSpacing([3.0, 0.0]));
            let avail_size = ui.content_region_avail();
            let package_item_height = crate::util::widget::ICON_FONT_SIZE + PACKAGE_SELECTABLE_PADDING * 2.0;
            let package_list_size = (avail_size[1] * 0.22)
                .min(package_list.len().max(3) as f32 * package_item_height + PACKAGE_SELECTABLE_PADDING + 4.0);

            if let Some(package_region) = imgui::ChildWindow::new("package_region")
                .border(true).size([0.0, package_list_size]).begin(ui)
            {
                if package_list.is_empty()
                {
                    let cursor = ui.cursor_pos();
                    ui.set_cursor_pos([cursor[0], cursor[1] + 4.0]);

                    crate::util::widget::window_placeholder_text("No packages found in data folder", ui);
                }
                else
                {
                    for package_name in &package_list
                    {
                        if imgui::Selectable::new(&format!("###{}", package_name))
                            .size([0.0, package_item_height])
                            .selected(self.selected_package == *package_name).build(ui)
                        {
                            ui.set_scroll_here_y();

                            self.package_label(package_name, ui);
                            self.selected_package = package_name.clone();
                        }
                        else
                        {
                            self.package_label(package_name, ui);
                        }
                    }
                }

                package_region.end();
            }

            style_spacing.pop();

            ui.spacing();

            if !self.selected_package.is_empty()
            {
                ui.spacing();
                ui.indent_by(PACKAGE_SELECTABLE_PADDING);
                crate::util::widget::icon_label(crate::util::widget::ICON_PACKAGE, &self.selected_package, ui);
                ui.unindent_by(PACKAGE_SELECTABLE_PADDING);
                ui.spacing();
            }

            if let Some(package_items_region) = imgui::ChildWindow::new("package_items_region")
                .border(true).begin(ui)
            {
                if self.selected_package.is_empty()
                {
                    ui.spacing();
                    crate::util::widget::window_placeholder_text("No package selected", ui);
                }
                else
                {
                    let mut first = true;

                    for res_type in ResourceType::iter()
                    {
                        if !first { ui.separator(); }

                        first = false;

                        if let Some(tree_res) = imgui::TreeNode::new(res_type.folder_name())
                            .label::<&str, &str>("")
                            .opened(true, imgui::Condition::Once)
                            .flags(imgui::TreeNodeFlags::SPAN_AVAIL_WIDTH)
                            .frame_padding(true).push(ui)
                        {
                            let tree_item_height = ui.item_rect_size()[1];

                            self.resource_label(res_type, ui);

                            if !editor.packages.package_loaded(res_type, &self.selected_package)
                            {
                                editor.packages.load_package_later(res_type, &self.selected_package);
                            }
                            else
                            {
                                let style_spacing = ui.push_style_var(imgui::StyleVar::ItemSpacing([3.0, 0.0]));

                                let res = &mut editor.packages.res[res_type as usize];

                                // Very annoyed that Rust is forcing me to do this!
                                //  No, using two separate members in editor.packages is not going
                                //  to be unsound just because I fed the reference into a function!!
                                //  Nope, gotta alloc and memcopy a bunch instead!
                                //  This language sometimes.
                                self.package_items_buffer.clear();
                                for item in &res.package_contents[&self.selected_package]
                                {
                                    self.package_items_buffer.push(item.clone());
                                }

                                for item in &self.package_items_buffer
                                {
                                    let item_height = match res_type.has_large_package_list_icon()
                                    {
                                        true => { 32.0 }
                                        false => { tree_item_height }
                                    };

                                    if imgui::Selectable::new(&format!("###{}", item)).size([0.0, item_height]).build(ui)
                                    {
                                        *load_res = res_type;
                                        *load_package_name = self.selected_package.clone();
                                        *load_item_name = item.clone();
                                    }

                                    self.resource_item_label(&self.selected_package, item, res_type, game, &mut editor.packages, ui);
                                }

                                style_spacing.pop();
                            }

                            ui.spacing();

                            tree_res.pop();
                        }
                        else
                        {
                            let mut text_color = ui.style_color(imgui::StyleColor::Text);
                            text_color[3] *= 0.5;
                            let color_collapsed = ui.push_style_color(imgui::StyleColor::Text, text_color);

                            self.resource_label(res_type, ui);

                            color_collapsed.pop();
                        }
                    }
                }

                package_items_region.end();
            }

            color_bg.pop();
            style_window_padding.pop();
            style_padding.pop();
            style_indent.pop();

            std::mem::swap(&mut editor.packages.package_list, &mut package_list);

            tab.end();
        }
    }

    fn scene_document_windows(&mut self, editor : &mut EditorControl, ui : &imgui::Ui, game : &mut GameControl)
    {
        let mut documents_to_close = Vec::new();

        let style = ui.clone_style();
        let padding = style.window_padding;

        let mut screen_pos = ui.cursor_pos();
        let mut screen_size = ui.io().display_size;
        screen_pos[0] -= padding[0];
        screen_pos[1] -= padding[1];
        screen_size[0] -= screen_pos[0];
        screen_size[1] -= screen_pos[1];

        editor.viewport = Rect
        {
            x : screen_pos[0],
            y : screen_pos[1],
            w : screen_size[0],
            h : screen_size[1]
        };

        let style_padding = ui.push_style_var(imgui::StyleVar::WindowPadding([0.0, 0.0]));
        let style_border = ui.push_style_var(imgui::StyleVar::WindowBorderSize(0.0));

        if let Some(window) = imgui::Window::new("Scene Editors")
            .position(screen_pos, imgui::Condition::Always)
            .size(screen_size, imgui::Condition::Always).focused(self.scene_editor_focused)
            .no_decoration().draw_background(false)
            .bring_to_front_on_focus(false)
            .save_settings(false).begin(ui)
        {
            self.scene_editor_focused = false;

            style_border.pop();
            style_padding.pop();

            if ui.is_window_focused_with_flags(imgui::WindowFocusedFlags::ROOT_AND_CHILD_WINDOWS)
            {
                self.scene_active = true;
            }

            if let Some(tab_bar) = imgui::TabBar::new("scene_document_tabs")
                .flags(imgui::TabBarFlags::REORDERABLE
                    | imgui::TabBarFlags::TAB_LIST_POPUP_BUTTON
                    | imgui::TabBarFlags::FITTING_POLICY_SCROLL)
                .begin(ui)
            {
                let tab_bar_size = ui.current_font_size() + style.frame_padding[1] * 2.0;

                editor.viewport.y += tab_bar_size;
                editor.viewport.h -= tab_bar_size;

                {
                    let draw_list = ui.get_window_draw_list();
                    let mut back_color = style.colors[imgui::StyleColor::TitleBg as usize];
                    let border_color = style.colors[imgui::StyleColor::Border as usize];

                    back_color[3] = 0.85;

                    draw_list.add_rect([editor.viewport.x, 0.0], [editor.viewport.x2(), editor.viewport.y], back_color).filled(true).build();
                    draw_list.add_rect([editor.viewport.x, 0.0], [editor.viewport.x2(), editor.viewport.y], border_color).build();
                }

                for i in 0..editor.scene_documents.documents.len()
                {
                    let document_window_name = editor.scene_documents.full_document_name(i);
                    let dirty = editor.scene_documents.documents[i].dirty;
                    let document = &mut editor.scene_documents.documents[i].document;
                    let raise = editor.scene_documents.document_to_focus == Some(i);
                    let mut tab_open = true;
                    let mut flags : imgui::TabItemFlags = imgui::TabItemFlags::empty();

                    flags.set(imgui::TabItemFlags::UNSAVED_DOCUMENT, dirty);
                    flags.set(imgui::TabItemFlags::SET_SELECTED, raise);

                    if raise
                    {
                        editor.scene_documents.document_to_focus = None;
                    }

                    if let Some(window_tab) = imgui::TabItem::new(&document_window_name).opened(&mut tab_open).flags(flags)
                        .begin(ui)
                    {
                        let result = document.do_ui(&mut editor.packages, &mut editor.config, &mut editor.project, game, ui);
                        let dirty = document.do_action_queue(game);

                        // Workaround column setting affecting tab bar
                        ui.columns(1, "", false);

                        editor.scene_documents.documents[i].dirty |= dirty;

                        if result.active
                        {
                            editor.scene_documents.active_document = Some(i);
                            self.scene_active = true;
                        }
                        if result.make_active
                        {
                            self.scene_editor_focused = true;
                            editor.scene_documents.document_to_focus = Some(i);
                        }

                        window_tab.end();
                    }

                    if !tab_open
                    {
                        documents_to_close.push(i);
                    }
                }

                tab_bar.end();
            }

            window.end();
        }
        else
        {
            style_border.pop();
            style_padding.pop();

            editor.scene_documents.document_to_focus = None;
            self.scene_editor_focused = false;
        }

        for i in &documents_to_close
        {
            if let Some((res_type, package_name, item_name)) = editor.scene_documents.document_info(*i)
            {
                editor.scene_documents.close_document(game, res_type, &package_name, &item_name);
            }
        }
    }

    fn document_windows(&mut self, editor : &mut EditorControl, ui : &imgui::Ui, game : &mut GameControl)
    {
        let mut documents_to_close = Vec::new();

        if self.editors_open
        {
            let pos_mode = self.window_pos_mode();
            let offset_x = ARRANGE_PACKAGES_WIDTH + ARRANGE_PADDING * 2.0;

            let mut window_builder = imgui::Window::new("Editors")
                .position([editor.viewport.x + ARRANGE_PACKAGES_WIDTH + ARRANGE_PADDING * 2.0, editor.viewport.y + ARRANGE_PADDING], pos_mode)
                .size([editor.viewport.w - offset_x - ARRANGE_PADDING, editor.viewport.h - ARRANGE_PADDING * 2.0], pos_mode)
                .focused(self.editors_focused).menu_bar(true);

            if let Some(collapsed) = self.editors_collapsed
            {
                window_builder = window_builder.collapsed(collapsed, imgui::Condition::Always);
            }

            let style_var = ui.push_style_var(imgui::StyleVar::WindowPadding([2.0, 8.0]));

            if let Some(window) = window_builder.begin(ui)
            {
                style_var.pop();

                if ui.is_window_focused_with_flags(imgui::WindowFocusedFlags::ROOT_AND_CHILD_WINDOWS)
                {
                    self.scene_active = false;
                }

                if editor.documents.documents.is_empty()
                {
                    crate::util::widget::window_placeholder_text("Select an item from the Packages tab in the Sidebar.", ui);
                }
                else
                {
                    let cursor_pos = ui.cursor_pos();
                    ui.set_cursor_pos([cursor_pos[0], cursor_pos[1] - 4.0]);

                    if let Some(tab_bar) = imgui::TabBar::new("document_tabs")
                        .flags(imgui::TabBarFlags::REORDERABLE
                            | imgui::TabBarFlags::TAB_LIST_POPUP_BUTTON
                            | imgui::TabBarFlags::FITTING_POLICY_SCROLL)
                        .begin(ui)
                    {
                        for i in 0..editor.documents.documents.len()
                        {
                            let document_window_name = editor.documents.full_document_name(i);
                            let dirty = editor.documents.documents[i].dirty;
                            let document = &mut editor.documents.documents[i].document;
                            let raise = editor.documents.document_to_focus == Some(i);
                            let mut tab_open = true;
                            let window_name = format!("{}", document_window_name);
                            let mut flags : imgui::TabItemFlags = imgui::TabItemFlags::empty();

                            flags.set(imgui::TabItemFlags::UNSAVED_DOCUMENT, dirty);
                            flags.set(imgui::TabItemFlags::SET_SELECTED, raise);

                            if raise
                            {
                                editor.documents.document_to_focus = None;
                            }

                            if let Some(window_tab) = imgui::TabItem::new(&window_name).opened(&mut tab_open).flags(flags)
                                .begin(ui)
                            {
                                let result = document.do_ui(&mut editor.packages, &mut editor.config, &mut editor.project, game, ui);
                                let dirty = document.do_action_queue(game);

                                // Workaround column setting affecting tab bar
                                ui.columns(1, "", false);

                                editor.documents.documents[i].dirty |= dirty;

                                if result.active
                                {
                                    editor.documents.active_document = Some(i);
                                    self.scene_active = false;
                                }
                                if result.make_active
                                {
                                    editor.documents.document_to_focus = Some(i);
                                }

                                window_tab.end();
                            }

                            if !tab_open
                            {
                                documents_to_close.push(i);
                            }
                        }

                        tab_bar.end();
                    }
                }

                window.end();
            }
            else
            {
                style_var.pop();

                editor.documents.document_to_focus = None;
            }
        }
        else
        {
            editor.documents.document_to_focus = None;
        }

        for i in &documents_to_close
        {
            if let Some((res_type, package_name, item_name)) = editor.documents.document_info(*i)
            {
                editor.documents.close_document(game, res_type, &package_name, &item_name);
            }
        }

        self.editors_collapsed = None;
        self.editors_focused = false;
    }

    fn move_console_read_marker(&mut self, editor : &mut EditorControl, game : &mut GameControl)
    {
        let history = game.console_history();

        editor.last_opened_console_index = history.history_max();
    }

    fn console_window(&mut self, editor : &mut EditorControl, ui : &imgui::Ui, game : &mut GameControl)
    {
        let history = game.console_history();
        let history_max = history.history_max();
        let mut scroll = false;

        if history_max > editor.last_console_index
        {
            let min = editor.last_console_index.max(history.history_min());

            for i in min..history.history_max()
            {
                if let Some(entry) = history.entry(i)
                {
                    if entry.level <= log::Level::Warn
                    {
                        self.console_open = true;
                        if self.console_allow_focused
                        {
                            self.console_focused = true;
                            self.console_allow_focused = false;
                        }
                    }
                }
            }

            editor.last_console_index = history_max;
            scroll = true;
        }

        if !self.console_open
        {
            return;
        }

        let size = ui.io().display_size;
        let style = ui.clone_style();

        if let Some(console_window) = imgui::Window::new("Console")
            .position([size[0] / 2.0, size[1] / 2.05], imgui::Condition::Appearing)
            .position_pivot([0.5, 0.50]).size([700.0, 600.0], imgui::Condition::Appearing)
            .collapsible(false).opened(&mut self.console_open).focused(self.console_focused)
            .save_settings(false).begin(ui)
        {
            let main_style = ui.clone_style();
            let colors_console = ui.push_style_color(imgui::StyleColor::ChildBg, main_style.colors[imgui::StyleColor::FrameBg as usize]);

            if let Some(console_history_window) = imgui::ChildWindow::new("Console History")
                .size([0.0, -30.0 - style.item_spacing[1]]).border(true).begin(ui)
            {
                ui.columns(2, "console_columns", false);
                ui.set_column_width(0, 240.0);

                for i in history.history_min()..history.history_max()
                {
                    if i == editor.last_opened_console_index
                    {
                        ui.separator();

                        if scroll
                        {
                            ui.set_scroll_here_y_with_ratio(0.2);
                        }
                    }

                    if let Some(entry) = history.entry(i)
                    {
                        let color = ui.push_style_color(imgui::StyleColor::Text,
                                                        crate::util::widget::label_color_for_log_level(entry.level, ui));
                        let entry_label = format!("{} {} ", entry.level, entry.source);
                        crate::util::widget::right_aligned_label(&entry_label, false, ui);
                        color.pop();

                        if ui.is_item_hovered()
                        {
                            ui.tooltip_text(&entry_label);
                        }

                        ui.next_column();

                        let color = ui.push_style_color(imgui::StyleColor::Text,
                                                        crate::util::widget::text_color_for_log_level(entry.level, ui));
                        ui.text_wrapped(format!("{}", entry.message));
                        color.pop();
                        ui.next_column();
                    }
                }

                console_history_window.end();
            }

            colors_console.pop();

            if ui.button_with_size("Copy to Clipboard", [0.0, 30.0])
            {
                let mut copy_text = String::new();

                for i in history.history_min()..history.history_max()
                {
                    if let Some(entry) = history.entry(i)
                    {
                        if !copy_text.is_empty()
                        {
                            copy_text.push('\n');
                        }
                        copy_text.push_str(&format!("{}", entry));
                    }
                }

                game.renderer().set_clipboard(&copy_text);
            }

            console_window.end();
        }

        self.console_focused = false;

        std::mem::drop(history);

        if !self.console_open
        {
            self.move_console_read_marker(editor, game);
        }
    }

    fn main_gui(&mut self, editor : &mut EditorControl, ui : &imgui::Ui, game : &mut GameControl)
    {
        self.main_menubar(editor, ui, game);
        self.menubar_shortcuts(editor, ui, game);
        self.scene_document_windows(editor, ui, game);
        self.document_windows(editor, ui, game);
        self.view_sidebar(editor, ui, game);

        if let Some(wizard) = &mut editor.wizard
        {
            let size = ui.io().display_size;
            let mut window_open = true;
            let title = wizard.window_title();

            let window = imgui::Window::new(title).collapsible(false).save_settings(false)
                .position([size[0] / 2.0, size[1] / 2.0], imgui::Condition::Appearing)
                .size([440.0, 0.0], imgui::Condition::Appearing)
                .position_pivot([0.5, 0.5]).focused(editor.wizard_raise).opened(&mut window_open);

            let state = wizard.do_ui(&mut editor.packages, game, window, ui);

            window_open &= state.open;
            editor.wizard_raise = false;

            if !window_open
            {
                editor.wizard = None;
            }
        }

        if self.demo_open
        {
            ui.show_demo_window(&mut self.demo_open);
        }

        if self.metrics_open
        {
            ui.show_metrics_window(&mut self.metrics_open);
        }

        if self.imgui_guide_open
        {
            imgui::Window::new("ImGui User Guide").always_auto_resize(true)
                .opened(&mut self.imgui_guide_open).build(ui, ||
            {
                ui.show_user_guide();
            });
        }

        if self.imgui_about_open
        {
            ui.show_about_window(&mut self.imgui_about_open);
        }

        self.handle_auto_import(editor, ui, game);
        self.console_window(editor, ui, game);
    }
}

impl ImGuiSystem<EditorScene> for EditorGui
{
    fn start(&mut self, args : ThinkerArgs<EditorScene>)
    {
        let mut editor = EditorControl::new();
        let mut background = Drawable::new();

        let bg_entity = args.scene.add_entity_later();
        let editor_entity = args.scene.add_entity_later();

        if let Some(imgui) = args.game.imgui_mut()
        {
            editor.apply_imgui_style(imgui, &mut background);
        }

        editor.packages.project_path.push_str(&editor.config.project_path);

        args.scene.add_component_later(&bg_entity, background);
        args.scene.add_component_later(&editor_entity, editor);
    }

    fn end(&mut self, args : ThinkerArgs<EditorScene>)
    {
        let mut comp_editor_control = args.components.store_mut::<EditorControl>();
        let (_, editor) = comp_editor_control.first_mut().unwrap();

        editor.save_config();
    }

    fn imgui_think(&mut self, ui : &mut imgui::Ui, args : ThinkerArgs<EditorScene>)
    {
        let mut comp_editor_control = args.components.store_mut::<EditorControl>();
        let (_, editor) = comp_editor_control.first_mut().unwrap();

        let mut font_token = None;

        if let Some(font_id) = ui.fonts().fonts().get(editor.config.flavor_font as usize)
        {
            font_token = Some(ui.push_font(*font_id));
        }

        if editor.packages.source_id == 0 && editor.config.skip_project_dialog
        {
            editor.load_project(&normalize_path(editor.packages.project_path.clone()));
        }

        let source_path = format!("{}/{}", editor.config.project_path, editor.project.source_path);

        editor.packages.load_sources(&source_path, args.game);

        if editor.packages.source_id == 0
        {
            let size = ui.io().display_size;

            imgui::Window::new("Select Project Directory").collapsible(false).save_settings(false)
                .size([370.0, 0.0], imgui::Condition::Always).position([size[0] / 2.0, size[1] / 2.5], imgui::Condition::Appearing)
                .position_pivot([0.5, 0.5]).build(ui, ||
            {
                let mut do_load = false;

                ui.text_wrapped("Please enter the path to your game's project folder.");

                ui.spacing();

                ui.bullet();
                ui.text_wrapped("This is the folder that contains \"Keeshond.toml\". \
                    If this file does not exist, it will be created within the folder.");

                ui.spacing();

                ui.bullet();
                ui.text_wrapped("By default, the editor will look for datafiles within the \"data/\" subdirectory. \
                    You may change this behavior by closing the editor and changing \"Keeshond.toml\".");

                ui.spacing();
                ui.spacing();

                let push = ui.push_item_width(350.0);
                if ui.input_text("", &mut editor.packages.project_path)
                    .enter_returns_true(true).build()
                {
                    do_load = true;
                }
                push.pop(ui);

                ui.spacing();

                if ui.button_with_size("Next", [100.0, 30.0])
                {
                    do_load = true;
                }

                if do_load
                {
                    editor.load_project(&normalize_path(editor.packages.project_path.clone()));
                }
            });
        }
        else
        {
            self.main_gui(editor, ui, args.game);
        }

        if let Some(some_token) = font_token
        {
            some_token.pop();
        }

        self.arrange_windows = editor.config.force_arrange_windows;
    }
}

struct EditorThinker
{

}

impl EditorThinker
{
    fn new() -> EditorThinker { EditorThinker {} }
}

impl ThinkerSystem<EditorScene> for EditorThinker
{
    fn think(&mut self, args : ThinkerArgs<EditorScene>)
    {
        let mut comp_editor_control = args.components.store_mut::<EditorControl>();
        let (_, editor) = comp_editor_control.first_mut().unwrap();

        let mut drawable_store = args.components.store_mut::<Drawable>();

        if let Some((_, drawable)) = drawable_store.first_mut()
        {
            if let Some(imgui) = args.game.imgui_mut()
            {
                editor.apply_imgui_style(imgui, drawable);
            }
        }
    }
}

struct EditorDrawer
{

}

impl EditorDrawer
{
    fn new() -> EditorDrawer { EditorDrawer {} }
}

impl DrawerSystem for EditorDrawer
{
    fn draw(&self, args : DrawerArgs)
    {
        let comp_editor_control = args.components.store::<EditorControl>();
        let (_, editor) = comp_editor_control.first().unwrap();

        if let Some(active_index) = editor.scene_documents.active_document
        {
            if let Some(document) = editor.scene_documents.documents.get(active_index)
            {
                document.document.do_draw(&editor.packages, args.resources, args.drawing, args.transform, args.interpolation);
            }
        }
    }
}
