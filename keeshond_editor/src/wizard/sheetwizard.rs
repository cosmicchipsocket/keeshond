use keeshond::gameloop::GameControl;

use crate::editorgui::Packages;
use crate::wizard::{Wizard, WizardState};

pub struct SheetWizard
{

}

impl Wizard for SheetWizard
{
    fn window_title(&self) -> &'static str
    {
        "Sheet Wizard"
    }

    fn load(_game : &mut GameControl) -> Self where Self: Sized
    {
        SheetWizard
        {

        }
    }

    fn do_ui(&mut self, packages : &mut Packages, _game : &mut GameControl, window_builder : imgui::Window<&'static str>, ui : &imgui::Ui) -> WizardState
    {
        let mut state = WizardState::new();

        window_builder.build(ui, ||
        {
            ui.text_wrapped("Sheets are textures that contain one or more sprite graphics you can draw in your game.");

            ui.spacing();
            ui.spacing();

            ui.text_wrapped("To make a new Sheet, copy a .png into the sheets/ directory of your package folder.");

            ui.spacing();
            ui.spacing();

            ui.text_wrapped("For best results, your .png should be a spritesheet with:");
            ui.bullet_text("Sheet width and height a power of two");
            ui.bullet_text("Sheet size no larger than 4096x4096");
            ui.bullet_text("2px of transparent spacing between each sprite");
            ui.bullet_text("1px of spacing from the edges of the sheet");

            ui.spacing();
            ui.spacing();

            ui.text_wrapped("Color format may be either 32-bit RGBA or indexed.");

            ui.spacing();
            ui.spacing();

            ui.text_wrapped("Click the button below when you are done.");

            ui.spacing();
            ui.spacing();

            if ui.button("Reload Packages")
            {
                packages.reload();
                state.open = false;
            }

            ui.spacing();

            ui.text("More options coming soon!");
        });

        state
    }
}
