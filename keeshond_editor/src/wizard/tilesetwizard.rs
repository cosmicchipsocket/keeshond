use keeshond::gameloop::GameControl;

use crate::editorgui::Packages;
use crate::wizard::{Wizard, WizardState};

use keeshond::datapack::DataHandle;
use keeshond_treats::tilemap::{Tileset, TilesetTile};

pub struct TilesetWizard
{
    package_name : String,
    item : String
}

impl Wizard for TilesetWizard
{
    fn window_title(&self) -> &'static str
    {
        "Tileset Wizard"
    }

    fn load(_game : &mut GameControl) -> Self where Self: Sized
    {
        TilesetWizard
        {
            package_name : String::new(),
            item : String::new()
        }
    }

    fn do_ui(&mut self, packages : &mut Packages, game : &mut GameControl, window_builder : imgui::Window<&'static str>, ui : &imgui::Ui) -> WizardState
    {
        let mut state = WizardState::new();

        window_builder.build(ui, ||
        {
            ui.text_wrapped("Tilesets are collections of background tiles, including their Sheet slices, collision, and other properties.");

            ui.spacing();
            ui.spacing();

            ui.input_text("Package", &mut self.package_name).build();
            ui.input_text("Name (.json)", &mut self.item).build();

            ui.spacing();
            ui.spacing();

            if ui.button("Create New Tileset")
            {
                let mut tileset = Tileset::new(DataHandle::default());

                tileset.insert_tile(0, TilesetTile::new(None));

                if self.make_resource(tileset, &self.package_name, &format!("{}.json", self.item), packages, game)
                {
                    state.open = false;
                }
            }
        });

        state
    }
}
