use keeshond::gameloop::GameControl;
use keeshond_treats::visual::Costume;

use crate::editorgui::{Packages};
use crate::wizard::{Wizard, WizardState};

pub struct CostumeWizard
{
    package_name : String,
    item : String
}

impl Wizard for CostumeWizard
{
    fn window_title(&self) -> &'static str
    {
        "Costume Wizard"
    }

    fn load(_game : &mut GameControl) -> Self where Self: Sized
    {
        CostumeWizard
        {
            package_name : String::new(),
            item : String::new()
        }
    }

    fn do_ui(&mut self, packages : &mut Packages, game : &mut GameControl, window_builder : imgui::Window<&'static str>, ui : &imgui::Ui) -> WizardState
    {
        let mut state = WizardState::new();

        window_builder.build(ui, ||
        {
            ui.text_wrapped("Costumes are sprite animation sequences that can be used to represent objects. \
                            In code, give an entity an Actor component to allow it to use Costumes.");

            ui.spacing();
            ui.spacing();

            ui.input_text("Package", &mut self.package_name).build();
            ui.input_text("Name (.json)", &mut self.item).build();

            ui.spacing();
            ui.spacing();

            if ui.button("Create New Costume")
            {
                let costume = Costume::new();

                if self.make_resource(costume, &self.package_name, &format!("{}.json", self.item), packages, game)
                {
                    state.open = false;
                }
            }
        });

        state
    }
}
