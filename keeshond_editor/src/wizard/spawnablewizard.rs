use keeshond::gameloop::GameControl;
use keeshond_treats::spawnable::SpawnableSet;

use crate::editorgui::{Packages};
use crate::wizard::{Wizard, WizardState};

pub struct SpawnableSetWizard
{
    package_name : String,
    item : String
}

impl Wizard for SpawnableSetWizard
{
    fn window_title(&self) -> &'static str
    {
        "SpawnableSet Wizard"
    }

    fn load(_game : &mut GameControl) -> Self where Self: Sized
    {
        SpawnableSetWizard
        {
            package_name : String::new(),
            item : String::new()
        }
    }

    fn do_ui(&mut self, packages : &mut Packages, game : &mut GameControl, window_builder : imgui::Window<&'static str>, ui : &imgui::Ui) -> WizardState
    {
        let mut state = WizardState::new();

        window_builder.build(ui, ||
            {
                ui.text_wrapped("SpawnableSets are lists of Spawnable objects including data \
                                            such as their Costumes, and other information.");

                ui.spacing();
                ui.spacing();

                ui.input_text("Package", &mut self.package_name).build();
                ui.input_text("Name (.json)", &mut self.item).build();

                ui.spacing();
                ui.spacing();

                if ui.button("Create New SpawnableSet")
                {
                    let spawnables = SpawnableSet::new();

                    if self.make_resource(spawnables, &self.package_name, &format!("{}.json", self.item), packages, game)
                    {
                        state.open = false;
                    }
                }
            });

        state
    }
}
