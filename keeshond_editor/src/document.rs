use crate::editorgui::{Packages, EditorConfig, ProjectConfig};
use keeshond::gameloop::GameControl;
use std::any::Any;
use keeshond::renderer::{DrawControl, DrawTransform};
use keeshond::datapack::DataMultistore;

pub mod sheetdocument;
pub mod costumedocument;
pub mod tilesetdocument;
pub mod spawnabledocument;
pub mod leveldocument;

pub struct DocumentState
{
    pub active : bool,
    pub make_active : bool
}

impl DocumentState
{
    pub fn new() -> DocumentState
    {
        DocumentState
        {
            active : false,
            make_active : false
        }
    }
}

pub struct ActionHistory
{
    queue : Vec<Box<dyn Any>>
}

impl ActionHistory
{
    pub fn new() -> ActionHistory
    {
        ActionHistory
        {
            queue : Vec::new()
        }
    }
}

pub trait Document
{
    fn load(game : &mut GameControl, package_name : &str, item_name : &str) -> Self where Self : Sized;
    fn do_ui(&mut self, packages : &mut Packages, editor_config : &mut EditorConfig, project : &mut ProjectConfig, game : &mut GameControl, ui : &imgui::Ui) -> DocumentState;
    #[allow(unused_variables)]
    fn do_detached_ui(&mut self, id : &str, packages : &mut Packages, config : &mut EditorConfig, project : &mut ProjectConfig, game: &mut GameControl, ui : &imgui::Ui) -> bool
    {
        false
    }
    #[allow(unused_variables)]
    fn do_draw(&self, packages : &Packages, resources : &DataMultistore, drawing : &mut Box<dyn DrawControl>, transform : &DrawTransform, interpolation : f32) {}
    fn action_history(&mut self) -> &mut ActionHistory;
    fn queue_action(&mut self, action : Box<dyn Any>)
    {
        let history = self.action_history();
        history.queue.push(action);
    }
    fn apply_action(&mut self, game: &mut GameControl, action : &Box<dyn Any>) -> bool;
    fn do_action_queue(&mut self, game: &mut GameControl) -> bool
    {
        let mut dirty = false;
        let history = self.action_history();
        let mut queue = Vec::new();

        std::mem::swap(&mut history.queue, &mut queue);

        for action in queue
        {
            dirty |= self.apply_action(game, &action);
        }

        dirty
    }
}
