use std::collections::HashMap;
use asefile::{AnimationDirection, AsepriteFile};

use crate::import::{ImportItem, ImporterResult, ImportItemPath, Importer, ImportConfig};
use crate::import::sheet::{ImageImportData, ImportedImage, SpriteImportData, AnimationImportData, FrameImportData, BackgroundImportData, TilesetGenInfo, FrameTrackData};

struct AsepriteFrameData
{
    pub index : usize,
    pub delay : f64
}

struct AsepriteTagData
{
    pub animation_name : String,
    pub track_name : String,
    pub frames : Vec<AsepriteFrameData>,
    pub looping : bool
}

fn file_image_frame(file : &AsepriteFile, frame_id : u32) -> ImageImportData
{
    if frame_id >= file.num_frames()
    {
        panic!("Frame number out of range, {} is not less than {}", frame_id, file.num_frames());
    }

    let frame = file.frame(frame_id);
    let image = ImportedImage::Rgb(frame.image());

    ImageImportData
    {
        image,
        origin_x : 0.0,
        origin_y : 0.0
    }
}

fn file_animation_tag(file : &AsepriteFile, tag_id : u32, fps : f64) -> AsepriteTagData
{
    if file.num_frames() == 0
    {
        panic!("No frames in file!");
    }
    if tag_id >= file.num_tags()
    {
        panic!("Tag number out of range, {} is not less than {}", tag_id, file.num_tags());
    }

    let tag = file.tag(tag_id);
    let first = tag.from_frame().min(file.num_frames() - 1);
    let last = tag.to_frame().min(file.num_frames() - 1);

    let name = tag.name();
    let mut animation_name = name.to_string();
    let mut track_name = String::new();
    let mut frames = Vec::new();
    let mut looping = false;

    if name.len() > 1
    {
        if let Some((animation_str, track_str)) = name.rsplit_once(':')
        {
            animation_name = animation_str.to_string();
            track_name = track_str.to_string();
        }
    }

    if animation_name.ends_with('#') && animation_name.len() >= 2
    {
        animation_name.pop();
        looping = true;
    }

    let mut frame_ids = Vec::new();

    match tag.animation_direction()
    {
        AnimationDirection::Forward =>
        {
            frame_ids.extend(first..(last + 1));
        }
        AnimationDirection::Reverse =>
        {
            frame_ids.extend((first..(last + 1)).rev());
        }
        AnimationDirection::PingPong =>
        {
            frame_ids.extend(first..(last + 1));
            frame_ids.extend(((first + 1)..last).rev());
        }
    }

    for i in &frame_ids
    {
        let frame = file.frame(*i);
        let mut delay = 1.0;

        if fps > 0.0
        {
            delay = (frame.duration() as f64 / (1000.0 / fps)).round().max(1.0);
        }

        frames.push(AsepriteFrameData
        {
            index : *i as usize,
            delay
        });
    }

    AsepriteTagData
    {
        animation_name,
        track_name,
        frames,
        looping
    }
}

fn file_palette(file : &AsepriteFile) -> Option<Vec<keeshond::renderer::Color>>
{
    match file.palette()
    {
        None => { None }
        Some(file_palette) =>
        {
            let mut palette = Vec::with_capacity(file_palette.num_colors() as usize);

            for i in 0..file_palette.num_colors()
            {
                if let Some(file_color) = file_palette.color(i)
                {
                    palette.push(keeshond::renderer::Color
                    {
                        r : file_color.red(),
                        g : file_color.green(),
                        b : file_color.blue(),
                        a : file_color.alpha()
                    });
                }
            }

            Some(palette)
        }
    }
}

pub struct AsepriteSpriteImporter
{

}

impl AsepriteSpriteImporter
{

}

impl Importer for AsepriteSpriteImporter
{
    fn name(&self) -> &'static str
    {
        "Aseprite Sprite"
    }

    fn extensions(&self) -> &[&str]
    {
        &["ase", "aseprite"]
    }

    fn description(&self) -> &'static str
    {
        "Imports native Aseprite animations as Sheets, Costumes, and Spawnables."
    }

    fn new() -> Self
    {
        AsepriteSpriteImporter
        {

        }
    }

    fn import(&mut self, path : &ImportItemPath, config : Option<&mut ImportConfig>) -> ImporterResult
    {
        if config.is_none()
        {
            return ImporterResult::NeedsConfig;
        }

        let config = config.unwrap();

        let origin_x = config.value_float_or_default("origin_x", 0.0);
        let origin_y = config.value_float_or_default("origin_y", 0.0);
        let fps = config.value_float_or_default("fps", 60.0) as f64;

        let file = match AsepriteFile::read_file(&path.path)
        {
            Ok(file) => { file }
            Err(error) => { return ImporterResult::Err(format!("{}", error)); }
        };
        let spawn_type_name = match config.value("spawn_type_name")
        {
            None => { return ImporterResult::NeedsConfig; }
            Some(spawn_type_name) => { spawn_type_name.clone() }
        };

        if file.is_indexed_color()
        {
            info!("Indexed color images currently unsupported, converting to RGBA.");
        }

        let mut images = Vec::new();
        let mut animations = Vec::new();

        for i in 0..file.num_frames()
        {
            let mut image = file_image_frame(&file, i);

            image.origin_x = origin_x;
            image.origin_y = origin_y;

            images.push(image);
        }

        let mut animation_names : HashMap<String, usize> = HashMap::new();

        if file.num_tags() == 0
        {
            warn!("Input Aseprite file has no tags. Costume will have no animation data.");
        }

        for i in 0..file.num_tags()
        {
            let tag_data = file_animation_tag(&file, i, fps);
            let animation_name = &tag_data.animation_name;

            if !animation_names.contains_key(animation_name)
            {
                animation_names.insert(animation_name.clone(), animations.len());

                animations.push(AnimationImportData
                {
                    animation_name : animation_name.to_string(),
                    frames : Vec::new(),
                    looping : tag_data.looping
                });
            }

            let index = animation_names[animation_name];

            for j in 0..tag_data.frames.len()
            {
                if animations[index].frames.len() <= j
                {
                    animations[index].frames.push(FrameImportData
                    {
                        tracks : Vec::new(),
                        delay : tag_data.frames[j].delay
                    });
                }

                animations[index].frames[j].tracks.push(FrameTrackData
                {
                    track_name : tag_data.track_name.clone(),
                    index : tag_data.frames[j].index
                });
            }
        }

        let import = ImportItem::Sprite(SpriteImportData
        {
            palette : None,
            images,
            animations,
            default_track_name : config.value_or_default("default_track_name", "default").clone(),
            spawn_type_name,
            dest_sheet_package : config.value_or_default("sheet_package", &path.package).clone(),
            dest_sheet : config.value_or_default("sheet_name", &format!("{}.png", path.name)).clone(),
            dest_costume_package : config.value_or_default("costume_package", &path.package).clone(),
            dest_costume : config.value_or_default("costume_name", &format!("{}.json", path.name)).clone(),
            dest_spawnable_package : config.value_or_default("spawnable_package", "common").clone(),
            dest_spawnable : config.value_or_default("spawnables_name", "objects.json").clone()
        });

        ImporterResult::Ok(import)
    }

    fn config_ui(&self, path : &ImportItemPath, config : &mut ImportConfig, ui : &imgui::Ui)
    {
        let default_package = path.package.clone();
        let default_name = path.name.clone();
        let default_costume = format!("{}.json", default_name);

        let mut fps = config.value_float_or_default("fps", 60.0);
        let origin_x = config.value_float_or_default("origin_x", 0.0);
        let origin_y = config.value_float_or_default("origin_y", 0.0);
        let mut origins = [origin_x, origin_y];

        let mut sheet_package_string = config.value_or_default("sheet_package", &default_package).clone();
        let mut sheet_string = config.value_or_default("sheet_name", "sprites.png").clone();
        let mut costume_package_string = config.value_or_default("costume_package", &default_package).clone();
        let mut costume_string = config.value_or_default("costume_name", &default_costume).clone();
        let mut spawnable_package_string = config.value_or_default("spawnable_package", "common").clone();
        let mut spawnables_string = config.value_or_default("spawnables_name", "objects.json").clone();
        let mut spawn_name_string = config.value_or_default("spawn_type_name", &crate::util::name_to_pascal_case(&default_name)).clone();
        let mut default_track_name = config.value_or_default("default_track_name", "default").clone();

        ui.spacing();
        ui.text("Target Resources:");
        ui.spacing();

        ui.indent();

        if crate::util::widget::data_path_edit(ui, "Sheet", &mut sheet_package_string, &mut sheet_string)
        {
            config.set_value("sheet_package", &sheet_package_string);
            config.set_value("sheet_name", &sheet_string);
        }

        if crate::util::widget::data_path_edit(ui, "Costume", &mut costume_package_string, &mut costume_string)
        {
            config.set_value("costume_package", &costume_package_string);
            config.set_value("costume_name", &costume_string);
        }

        if crate::util::widget::data_path_edit(ui, "Spawnable Set", &mut spawnable_package_string, &mut spawnables_string)
        {
            config.set_value("spawnable_package", &spawnable_package_string);
            config.set_value("spawnables_name", &spawnables_string);
        }

        ui.spacing();

        let width = ui.push_item_width(200.0);
        if ui.input_text("Spawn Type Name", &mut spawn_name_string).build()
        {
            config.set_value("spawn_type_name", &spawn_name_string);
        }
        width.pop(ui);

        let width = ui.push_item_width(120.0);
        if ui.input_text("Default Track Name \t(used when not present in tag)", &mut default_track_name).build()
        {
            config.set_value("default_track_name", &default_track_name);
        }
        width.pop(ui);

        ui.unindent();

        ui.spacing();
        ui.text("Sprite Info:");
        ui.spacing();

        ui.indent();
        let width = ui.push_item_width(200.0);
        if imgui::Drag::new("Sprite Origin Point").display_format("%.2f").build_array(ui, &mut origins)
        {
            config.set_float("origin_x", origins[0]);
            config.set_float("origin_y", origins[1]);
        }
        width.pop(ui);

        let width = ui.push_item_width(150.0);
        if imgui::Drag::new("Animation FPS").display_format("%.2f").build(ui, &mut fps)
        {
            config.set_float("fps", fps);
        }
        width.pop(ui);
        ui.unindent();

        ui.spacing();
        ui.spacing();

        ui.spacing();
        ui.text("Tips:");
        ui.spacing();

        ui.bullet();
        ui.text_wrapped("Aseprite tag name format is animation:track\n\twhere \"animation\" is the animation name and \"track\" is the track name.");
        ui.bullet();
        ui.text_wrapped("To make an animation loop, add a # to the end of the animation name in the Aseprite tag.");
        ui.bullet();
        ui.text_wrapped("Frame delays are based on the frame ms defined in Aseprite and the FPS defined here, \
            rounded to the nearest whole number. For example, 100ms at 60 FPS creates a delay of 6.");
        ui.bullet();
        ui.text_wrapped("Indexed color images are currently converted to RGBA. This will be fixed in a future release.");

        ui.spacing();
    }
}

pub struct AsepriteBackgroundImporter
{

}

impl AsepriteBackgroundImporter
{

}

impl Importer for AsepriteBackgroundImporter
{
    fn name(&self) -> &'static str
    {
        "Aseprite Background"
    }

    fn extensions(&self) -> &[&str]
    {
        &["ase", "aseprite"]
    }

    fn description(&self) -> &'static str
    {
        "Imports native Aseprite images as Sheets for textures or tilesets."
    }

    fn new() -> Self
    {
        AsepriteBackgroundImporter
        {

        }
    }

    fn import(&mut self, path : &ImportItemPath, config : Option<&mut ImportConfig>) -> ImporterResult
    {
        if config.is_none()
        {
            return ImporterResult::NeedsConfig;
        }

        let config = config.unwrap();

        let file = match AsepriteFile::read_file(&path.path)
        {
            Ok(file) => { file }
            Err(error) => { return ImporterResult::Err(format!("{}", error)); }
        };

        if file.is_indexed_color()
        {
            info!("Indexed color images currently unsupported, converting to RGBA.");
        }
        if file.num_frames() == 0
        {
            return ImporterResult::Err(String::from("Input file has no frames."));
        }
        else if file.num_frames() > 1
        {
            return ImporterResult::Err(String::from("Input file has multiple frames. \
                If you need animation please use the Sprite importer instead."));
        }

        let frame = file_image_frame(&file, 0);

        let tilegen = match config.value_bool_or_default("tilegen_enabled", false)
        {
            true => { Some(TilesetGenInfo::from_config(config)) }
            false => { None }
        };

        let import = ImportItem::Background(BackgroundImportData
        {
            palette : file_palette(&file),
            image : frame.image,
            dest_package : config.value_or_default("sheet_package", &path.package).clone(),
            dest_sheet : config.value_or_default("sheet_name", &format!("{}.png", path.name)).clone(),
            tilegen
        });

        ImporterResult::Ok(import)
    }

    fn config_ui(&self, path : &ImportItemPath, config : &mut ImportConfig, ui : &imgui::Ui)
    {
        let default_package = path.package.clone();
        let default_name = path.name.clone();
        let default_sheet = format!("{}.png", default_name);
        let mut sheet_package_string = config.value_or_default("sheet_package", &default_package).clone();
        let mut sheet_string = config.value_or_default("sheet_name", &default_sheet).clone();

        let mut tilegen_enabled = config.value_bool_or_default("tilegen_enabled", false);
        let mut tilegen = TilesetGenInfo::from_config(config);

        ui.spacing();
        ui.text("Target Resource:");
        ui.spacing();

        ui.indent();

        if crate::util::widget::data_path_edit(ui, "Sheet", &mut sheet_package_string, &mut sheet_string)
        {
            config.set_value("sheet_package", &sheet_package_string);
            config.set_value("sheet_name", &sheet_string);
        }

        ui.spacing();
        if ui.checkbox("Generate Tileset Texture and Slices", &mut tilegen_enabled)
        {
            config.set_bool("tilegen_enabled", tilegen_enabled);
        }
        ui.spacing();

        ui.indent();

        if tilegen_enabled
        {
            let width = ui.push_item_width(200.0);

            if tilegen.tilegen_ui(ui)
            {
                tilegen.save_to_config(config);
            }

            width.pop(ui);
        }

        ui.unindent();
        ui.unindent();

        ui.spacing();
        ui.text_wrapped("Note: Indexed color images are currently converted to RGBA. This will be fixed in a future release.");
        ui.spacing();
    }
}
