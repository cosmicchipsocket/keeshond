+++
title = "The Resource System"
weight = 5
sort_by = "weight"
+++

For most games, you'll probably want to have some sort of media, such as graphics and sound effects. You need a way of loading these from disk when needed. You need to be able to index these for common operations, such as drawing specific sprites. You might also want to be able to unload resources to save on memory, or allow your game to be modded.

Keeshond uses its own resource system, which is also available as a separate crate, [keeshond_datapack](https://crates.io/crates/keeshond_datapack). It uses IDs to keep track of individual resources, borrowing from the concept of Entities in ECS. Resources are grouped into packages, as well as the different resource types you can use. You will learn more about that in this chapter.
