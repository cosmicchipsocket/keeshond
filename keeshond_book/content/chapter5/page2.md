+++
title = "Loading Resources"
weight = 2
+++

Loading, querying, and accessing resources is done through the `DataStore` type. Each `DataStore` object handles operations for a specific `DataObject` type. For example, a `DataStore<Foo>` would handle loading of all `DataObject`s of type `Foo`.

Additionally, there is `DataMultiStore`, which holds multiple `DataStore`s, one for each type. Keeshond provides one that you can get at from the `GameControl` object:

```rust
let resources = game.res();
```

You can access individual `DataStore`s with the following:

```rust
let mut sheet_res = game.res().store_mut::<Sheet>();
let mut shader_res = game.res().store_mut::<Shader>();
let mut sound_res = game.res().store_mut::<Sound>();
````

There are two ways to load resources, and both should generally be done at the start of your Scene. First, you can load the Package containing the resources ahead of time with `load_package()`:

```rust
sheet_res.load_package("doggymark");
```

This loads all `Sheet`s within the Package named "doggymark". From the filesystem's perspective, they are loaded from `doggymark/sheets`. So assuming you have several files in there named `doggy.png`, `hud.png`, and `background.png`, they would all be loaded together.

The second way to load resources is also the way to query them:

```rust
let sheet_id = sheet_res.get_id_mut("doggymark", "doggy.png").expect("Failed to load package");
```

On success, this function returns a `DataId`, which you can use to get at the `DataObject`, or simply to reference it. As mentioned in Chapter 3.2, you should look up this ID once and reuse it instead of performing the lookup multiple times. Also, if the given package ("doggymark" here) is not loaded, it will be loaded for you as if you called `load_package()`.

Once you have the `DataId`, you can access the object it refers to:

```rust
let mut sheet_opt = sheet_res.get_mut(sheet_id);

if let Some(sheet) = sheet
{
    // Do stuff with the Sheet
}
```

But for the Keeshond data types, you typically won't be doing this kind of direct access, instead passing around the `DataId`, for example to specify the Sheet to use for a rendering operation.
