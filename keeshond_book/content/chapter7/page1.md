+++
title = "Resource Type: Sounds"
weight = 1
+++

Providing Keeshond with audio to play is quite simple through the Sound resource type. Simply place a `.wav` or `.ogg` file within the `sounds` folder of a Package and your audio will be loaded along with the Package. Unlike Sheets, every Sound resource is an individual piece of audio rather than possibly multiple sounds, as audio playback does not require this kind of optimization the way graphics do.

Audio files loaded from Packages are stored in memory uncompressed. So if you have a 3 MiB .ogg file, it may expand to be about 30 MiB when loaded. Currently there is no support for streaming audio from disk, so just keep in mind that you probably shouldn't place all your music in the same Package, at least for now. A future update will likely resolve this.
