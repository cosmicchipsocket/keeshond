+++
title = "Introduction"
weight = 1
sort_by = "weight"
+++

Keeshond is a 2D game engine/framework for the Rust programming language.

## Useful links

- [Keeshond on crates.io](https://crates.io/crates/keeshond)
- [API documentation on docs.rs](https://docs.rs/keeshond)
- [Source repository](https://gitlab.com/cosmicchipsocket/keeshond)

## Scope of this book

The Keeshond Game Engine Book is designed to acquaint you with how Keeshond works at a high level, and how to make games with it. It does not serve as an exhaustive API documentation, but rather a series of tutorials that provide a conceptual understanding where the API cannot.

This is a living document. Expect to see new pages, as well as additions to existing ones, while Keeshond continues to evolve.

## Code license

Licensed under either of

 * Apache License, Version 2.0
   (<http://www.apache.org/licenses/LICENSE-2.0>)
 * MIT license
   (<http://opensource.org/licenses/MIT>)

at your option.

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.
