+++
title = "What Keeshond is and isn't"
weight = 2
+++

## What Keeshond is

- A library serving as a game engine and framework. It is a game engine in that it is aimed at developing games, and it is a framework in that it provides a skeleton that you use to structure your game's code.
- A 2D renderer that abstracts away low-level graphics APIs, allowing you to easily draw sprites when and where you want.
- An Entity Component System framework that is integrated with the engine and shares its core values.
- An attempt to straddle the gap between multimedia libraries and heavyweight do-it-all engines.

## What Keeshond is not, but might be soon

- An editor for scenes. Editor support exists for sheets and costumes/tilemaps. Scene editing is currently being worked on.
- An engine with networking support. I (the author of Keeshond) am mainly prioritizing single-player experiences with this engine, but online play is something I would like to see soon, as long as it can be done right.

## What Keeshond is not, and probably never will be

- A heavy engine like Unity/Unreal, as this is counter to Keeshond's design goals.
- A game creation program. But I do think it'd be cool to see Keeshond as the basis for one!
