+++
title = "About Keeshond Treats"
weight = 3
+++

Keeshond is all about providing useful primitives for games without assuming too much about what _kind_ of game you're making. To that end, Keeshond focuses on providing a good ECS, correct timekeeping, fast sprite drawing, a simple but effective input system, and responsive audio. The rest is a blank canvas that you can use to make whatever 2D game you want.

But sometimes you don't want to start from scratch when programming gameplay concepts and you want a little more. This is where Keeshond Treats comes in. Treats is a separate library that provides some additional building blocks such as sprite animation, bitmap text, tilemaps, and scrolling levels. While this book and the editor make use of Treats, in practice its use is optional if you feel that you need to take a more custom approach. But it can save a lot of work for most games, so I suggest you at least check it out.

[Keeshond Treats on crates.io](https://crates.io/crates/keeshond_treats)
