+++
title = "Costumes and Actors"
weight = 4
+++

Displaying individual static sprites is fine, but you probably want to be able to use moving, animated sprites as well. This is where Costumes and Actors come in.

Costumes are a resource type provided by Keeshond Treats. With Costumes, you can sequence multiple sprite slices into animations. With Actors, you can play back these animations in your game. Costumes can be created within the editor:

![Keeshond Editor: Costumes](editor_costumes.png)

In your game, you can use a Costume on an Entity by using three Components, all provided by Treats: `Position`, `Drawable`, and `Actor`.

- `Position` gives the Entity an X/Y position which is used when positioning the sprite.
- `Drawable` stores the information used to draw the sprite. This may be left empty on initialization.
- `Actor` takes a Costume ID and tracks animation state, then pairs this with the `Position` to set the data in `Drawable`.

Such an Entity can be spawned like so:

```rust
spawn.with(Position::new(x, y));
spawn.with(Drawable::new());
spawn.with(Actor::with_costume_lookup("common", "skater.json", game));
```

As long as you include the default Treats Systems in the SceneConfig via `default_systems()`, everything should work automatically.

The `Actor` component provides a few functions for manipulating animation playback:

- `play_animation()` and `play_animation_name()` - Allows switching to another animation, either by ID or string name. If using a name, the Costume DataStore needs to be provided to perform the name lookup. Both functions have a `restart` parameter, which lets you determine whether the animation should restart from the beginning if the function is called while the target animation is already playing.
- `is_playing()` and `is_playing_name()` - Returns true if the given animation is the current animation.
- `animation_speed()` and `set_animation_speed()` - Lets you set a multiplier for how fast to process the animation (normal speed is 1.0, double speed is 2.0, etc.)

Full source for the Actors example can be found here: <https://gitlab.com/cosmicchipsocket/keeshond/-/blob/master/keeshond_treats/examples/actor/main.rs>
