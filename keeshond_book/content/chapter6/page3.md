+++
title = "Displaying Sprites"
weight = 3
+++
 
In the ECS chapter we briefly touched on DrawerSystems. These are where we write our drawing code after all game logic updates are processed. As a reminder, game state cannot be modified inside a DrawerSystem. If you need to move an object around, do it in a ThinkerSystem beforehand. DrawerSystems are purely meant for display purposes.

For this guide, we will use a DrawerSystem and related infrastructure that has been made for us. The Keeshond Treats library provides the `Drawable` Component, which allows us to associate a visual representation with an entity. Additionally, it has the `DrawableDrawer` System which displays our `Drawable`s. A `Drawable` has a position, draw ordering, and an enum item which describes the type of graphic we want to display:

```rust
pub struct Drawable
{
    pub item : DrawableItem,
    pub x : Lerpable<f32>,
    pub y : Lerpable<f32>,
    pub scroll_rate_x : f32,
    pub scroll_rate_y : f32,
    pub depth : f32,
    pub visible : bool
}
```

There are several different kinds of items we can use, but here we will just focus on basic sprites. The structure of a `Sprite` looks like this:

```rust
pub struct Sprite
{
    pub sheet_id : DataId,
    pub slice_id : usize,
    pub shader_id : DataId,
    pub offset_x : f32,
    pub offset_y : f32,
    pub angle : f32,
    pub scale_x : f32,
    pub scale_y : f32,
    pub r : f32,
    pub g : f32,
    pub b : f32,
    pub alpha : f32,
}
```

As you can see, given already-resolved IDs for the Sheet and Slice, we can specify what sprite we want to draw. We may additionally specify an offset, angle, scale, color, and alpha transparency level for the sprite.

But how do you make use of this stuff quickly and easily? Let's look in the `spawn()` logic for a theoretical SceneType:

```rust
fn spawn(&mut self, spawn : &mut SpawnControl, spawnable_id : SpawnId,
         game : &mut GameControl, x : f64, y : f64, _args : &SpawnArgList)
{
    match spawnable_id
    {
        SpawnId::Blob =>
        {
            spawn.with(Drawable::with_sprite_slice_lookup(
                x as f32, y as f32,
                "common", "ball.png", "red", game));
        }
    }
}
```

`Drawable::with_sprite_slice_lookup()` is a convenience function that will create a `Drawable` with a sprite from the given resource and slice names. All we have left to do is spawn it:

```rust
impl ThinkerSystem<GameScene> for ExampleThinker
{
    fn start(&mut self, _components : &mut ComponentControl,
        scene : &mut SceneControl<GameScene>, game : &mut GameControl)
    {
        scene.spawn_later(SpawnId::Blob, game, 32.0, 32.0);
    }
    
    /// ...
}
```

And now we have an on-screen sprite! Keep in mind that if this is to be paired with an entity that has a `Position` component, you will need to update the `Drawable`'s position yourself. In the next section we will look at `Costumes` and `Actors` which handle all this for us, and let us have animation to boot!

If you want to see the full example, check out the source here: <https://gitlab.com/cosmicchipsocket/keeshond/-/blob/master/keeshond_treats/examples/sprite/main.rs>
