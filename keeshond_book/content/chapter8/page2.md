+++
title = "Key Name Reference"
weight = 2
+++

The following strings can be used when binding actions to keys:


## Keyboard

### Letter/Number Keys

`a` `b` `c` `d` `e` `f` `g` `h` `i` `j` `k` `l` `m` `n` `o` `p` `q` `r` `s` `t` `u` `v` `w` `x` `y` `z`

`1` `2` `3` `4` `5` `6` `7` `8` `9` `0`

### Symbol Keys

`` ` `` `-` `=` `[` `]` `\\` `;` `'` `,` `.` `/`

### Function Keys

`f1` `f2` `f3` `f4` `f5` `f6` `f7` `f8` `f9` `f10` `f11` `f12`

### Modifier/Typing Keys

`escape` `tab` `space` `backspace` `return` (main Enter key)

`left shift` `right shift` `left ctrl` `right ctrl` `left alt` `right alt`

### Arrow/Editing Keys

`left` `right` `up` `down` `insert` `delete` `home` `end` `pageup` `pagedown`

### Keypad Keys

`keypad 1` `keypad 2` `keypad 3` `keypad 4` `keypad 5` `keypad 6` `keypad 7` `keypad 8` `keypad 9` `keypad 0`

`keypad .` `keypad +` `keypad -` `keypad *` `keypad /` `keypad enter`

### Other

A full list of key names can be found [here, in the leftmost column titled "Key Name"](https://wiki.libsdl.org/SDL_Scancode).

## Gamepad

The following key names bind to any controller that works with the SDL controller API:

### Buttons

- `gamepad left` (D-pad left)
- `gamepad right` (D-pad right)
- `gamepad up` (D-pad up)
- `gamepad down` (D-pad down)
- `gamepad a` (A face button, south)
- `gamepad b` (B face button, east)
- `gamepad x` (X face button, west)
- `gamepad y` (Y face button, north)
- `gamepad l` (L shoulder button, i.e. L1 or "left bumper")
- `gamepad r` (R shoulder button, i.e. R1 or "right bumper")
- `gamepad lstick` (click left stick inward, i.e. L3)
- `gamepad rstick` (click right stick inward, i.e. R3)
- `gamepad back`
- `gamepad start`
- `gamepad guide`

### Axes

- `gamepad lstick left` (left stick negative X)
- `gamepad lstick right` (left stick positive X)
- `gamepad lstick up` (left stick negative Y)
- `gamepad lstick down` (left stick positive Y)
- `gamepad rstick left` (right stick negative X)
- `gamepad rstick right` (right stick positive X)
- `gamepad rstick up` (right stick negative Y)
- `gamepad rstick down` (right stick positive Y)
- `gamepad ltrigger` (L trigger, i.e. L2, positive only)
- `gamepad rtrigger` (R trigger, i.e. R2, positive only)
