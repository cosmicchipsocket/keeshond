+++
title = "Why Fixed Timestep?"
weight = 3
+++

Computers have finite resources, yet for the purposes of games they must execute code within a finite amount of time. If for some reason the computer cannot run the game at its intended speed, the whole game will slow down unless there is some strategy for handling this scenario. This problem becomes much worse when you realize that PCs all run on different hardware, so the game might not run at quite the same speed across two different machines. Back in the DOS era, many games mistakenly made the assumption that all CPUs ran the software at the same speed, so on newer hardware they would end up running too fast. This necessitated having a "turbo" switch on computers for the sake of backward compatibility - disabling turbo would let these programs run at their intended speeds.

Of course, this problem is much rarer these days because having some sort of framerate limit on game logic is now common practice. This solves the issue of running too fast, but what if the game runs too slow? There is no perfect solution for this, but ideally we want users across machines to have as close to the same experience as possible. There are two general strategies that can be used here:

- Variable Timestep: run the logic once per frame but have objects move at a different speed based on the time delta
- Fixed Timestep with Synchronization: run the logic multiple times between displayed frames to "catch up"

If you come from a mathematical background, you might opt for the first option, Variable Timestep. It certainly seems good on paper - scale the velocities of moving objects by multiplying them by deltatime, the difference in time between the previous frame and the current one. It's computationally efficient and it scales to a wide range of framerates. However, this method has two big issues that prevent it from working well in practice.

First, going back to the idea that "computers have finite resources", any equation you come up with is going to have its precision limited by the fact that computer games must process math in terms of finite, discrete steps. The size of the step varies based on the framerate as well, with low framerates resulting in fewer, larger steps with very imprecise movement. For example:

- Two objects might pass through each other entirely without registering a collision if the framerate is too low
- The player's jump trajectory in a platformer might be subtly affected by sudden changes in the framerate, causing the player to miss a jump
- Limitations in mathematical precision could result in very high framerate players having an unfair advantage (e.g. being able to move faster) due to the smaller time delta

Even subtle changes can affect the results of a simulation. Two objects falling from different heights might hit the ground at the same time, but only sometimes. Then after they land and bounce, you try to fire a third object at them. Sometimes it misses, sometimes it hits, and sometimes it hits the objects from a different angle. And if you were to try the same simulation on another computer with slight differences in CPU and display timings, you might end up with different results altogether. Imagine a jump that becomes impossible for only some of your players just because of the variable timestep!

The other issue with this method is that implementing it is tedious and error-prone. You must be sure to apply deltatime to every equation that involves a continuous action, and avoid applying it to anything that represents an instantaneous action. Failure to do so will result in some parts of the game simply not scaling with the framerate. This is a heavy violation of the DRY (Don't Repeat Yourself) Principle of programming. Keeshond is meant to be fun to use, and easy to do things the correct way. Having to apply deltatime across an entire codebase is not something I would wish on the users of my library.

Because of these two shortcomings, Keeshond does not use Variable Timestep. Having said that, Fixed Timestep with Synchronization does have a couple faults:

- The logic rate is fixed, so it doesn't scale to high frequency displays (e.g. Adaptive Sync monitors). This can be worked around somewhat by smoothing out the motion between two given frames in the rendering step (rendering interpolation), though the result is not perfect.
- All logic must be processed multiple times if the framerate drops below the target. For a game targeting 60 FPS, a rate of 30 FPS would mean the logic runs twice between each draw of the graphics. This might seem inconsequential, but the processing overhead can add up. If too much logic is processed between draws, the game has no choice but to slow everything down.

Despite this, I feel that Keeshond makes the right tradeoffs by choosing fixed timestep over variable. Keeshond addresses the first issue by providing basic support for rendering interpolation. As for the second issue, Rust's performance certainly helps here - as long as you follow best practices and don't try anything too weird like huge object counts or super detailed physics simulations, you probably won't run into slowdown issues.
