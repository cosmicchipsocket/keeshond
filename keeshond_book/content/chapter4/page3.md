+++
title = "Components"
weight = 3
+++

A Component is an object you can attach to an Entity to give it additional data. Technically you're not actually attaching it to the Entity, but this helps explain it at a high level. More on that later.

So how do we make a Component? First, come up with a struct that you would like to make a Component. But what makes a good Component? It helps to think up some singular behavior or attribute you want to give to an Entity. For example, "objects that move", or "objects that can hurt the player". Let's look at the first one. First we create a struct:

```rust
struct Movable
{
    x : f64,
    y : f64,
    vel_x : f64,
    vel_y : f64
}
```

We now have a struct that represents an X and Y coordinate, as well as an X and Y velocity, which could be applied to objects to give them a location and the ability to move around. So how do we turn this struct into a Component? Well, `Component` is a trait, so we just do this:

```rust
impl Component for Movable {}
```

And that's it. Now let's go ahead and create the other component:

```rust
struct Enemy
{
    contact_damage : f32
}

impl Component for Enemy {}
```

Now we need to attach these Components to an Entity. First we need to create an instance of each struct:

```rust
let movable = Movable
{
    x : 0.0,
    y : 0.0,
    vel_x : 0.0,
    vel_y : 0.0
};

let enemy = Enemy
{
    contact_damage : 10.0
};
```

If you are doing this during manual setup:

```rust
// let scene = Scene<FooType>::new();

let entity = scene.add_entity();
scene.add_component(&entity, movable);
scene.add_component(&entity, enemy);
```

Or if it's during a Scene, from within a System:

```rust
let entity = scene_control.add_entity_later();

scene_control.add_component_later(&entity, movable);
scene_control.add_component_later(&entity, enemy);
```

And now we have an object that can move and is an enemy!

Previously I mentioned that you can add and remove behaviors from objects during the game just by using Components. So imagine you want the main character to be able to make peace with enemies. You can do this:

```rust
scene_control.remove_component_later::<Enemy>(&entity);
```

And the enemy will no longer act like an enemy but still move around in the game world! Neat!

You might notice that `remove_component_later()` only takes a template argument (`Enemy` here) and an Entity. Even though you can give an Entity multiple different Components, you can't give an Entity more than one of a given type of Component. If you were to try to add two `Enemy` Components to the same Entity, it wouldn't work. Sure, there are some ECS libraries out there that allow this, but Keeshond doesn't. And the more I think about it, the more I feel that it is better this way, as allowing multiple Components of the same type on the same Entity would increase code complexity and probably reduce performance just to handle a few corner cases. And in such cases there are other ways to do what you need to do.
