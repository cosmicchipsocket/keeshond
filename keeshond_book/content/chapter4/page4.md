+++
title = "Systems"
weight = 4
+++

A System is a function you can use to add behaviors to a Scene. More formally, you use it to run code on all Entities that have a certain set of Components. Keeshond does not actually codify this into Systems at all, however, so this is completely optional.

There are two types of Systems: the `ThinkerSystem` and the `DrawerSystem`. Each frame, all the `ThinkerSystem`s are processed, followed by all the `DrawerSystem`s. There are a couple important differences between the two:

- A `ThinkerSystem` can modify game state, whereas a `DrawerSystem` cannot. This is to prevent you from accidentally writing framerate-dependent code. Make all the changes to your game objects during the "thinking" phase, so that you can then see those changes (rather than make them) during the "drawing" phase.
- You can only perform drawing within a `DrawerSystem`.

Drawing graphics will be covered in the next chapter, so let's instead focus only on the `ThinkerSystem` for now.

We'll start by creating a System for the Movable Component we created in the last section. The basic skeleton looks like this:

```rust
struct MovableThinker {}

impl ThinkerSystem<NullSceneType> for MovableThinker
{
    fn think(&mut self,
        components : &mut ComponentControl,
        scene : &mut SceneControl<NullSceneType>,
        game : &mut GameControl)
    {
        // Code goes here
    }
}
```

Like `Component`, `ThinkerSystem` (and by extension `DrawerSystem`) is a trait. To define what it does, we implement the function `think()`. You might notice the `NullSceneType`. Ignore that for now. It will be covered shortly.

Notice that the function takes three "Control" objects. We mentioned these briefly before, but to recap:

- `ComponentControl` is used to access Components
- `SceneControl` is used to add or remove Entities or Components
- `GameControl` is used to access different engine subsystems (for example, to get resources or play sounds, or change settings)

All three of these are important, but `ComponentControl` is probably the most important. Let's use it to implement `MovableThinker::think()`:

```rust
let mut movable_store = components.store_mut::<Movable>();

for (_, movable) in movable_store.iter_mut()
{
    movable.x += movable.vel_x;
    movable.y += movable.vel_y;
}
```

This code grabs the `ComponentStore` object associated with the `Movable` component type. Next, it creates an iterator from it that goes over all `Movable` Components. For each, we apply the velocity by adding the X and Y velocity values to the X and Y position values.

## Adding Systems

In order for Systems to execute, they need to be added to the Scene. All Systems will be executed in the order that they are added. Once added, Systems cannot be removed. Additionally, Systems cannot be added while a Scene is running, so be sure to add all Systems you plan to use upfront.

```rust
scene.add_thinker_system(Box::new(MovableThinker::new()));
```

```rust
scene.add_drawer_system(Box::new(SpriteDrawer::new()));
```
