+++
title = "Acknowledgements"
weight = 10
sort_by = "weight"
+++

- Clickteam, for getting me into game programming
- The Rust crate "specs", which gave me the idea to use both an index and a generation for the entity ID
- The folks on #rust-gamedev on Mozilla IRC, for giving me some starting pointers
- The Rust language team, for proving that my game engine needn't be written in C++!
- Sam Lantinga, Ryan C. Gordon, and all other contributors to SDL2 which has helped advance the cause of Linux gaming
- My friends on Glorious Trainwrecks and adjacent communities, for cheering me on.
